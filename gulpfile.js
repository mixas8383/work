var gulp = require('gulp');
var concat = require('gulp-concat');
var fileinclude = require('gulp-file-include');
var sass = require('gulp-sass');
var uglify = require('gulp-uglify');
var minify = require('gulp-minify');


var commonScripts = [
    //'./bower_components/jquery/dist/jquery.min.js',
    //'./bower_components/jquery-ui/jquery-ui.min.js',

    './app/js/globals.js',
    './app/js/CometServerApi.js',
    './bower_components/angular/angular.min.js',
    //'./bower_components/angular-component-router/angular_1_router.js',
    './app/js/angular-locale_ka-ge.js',
    './bower_components/moment/moment.js',
    './bower_components/moment/locale/ka.js',
    './bower_components/angular-translate/angular-translate.min.js',
    './bower_components/angular-sanitize/angular-sanitize.min.js',
    './bower_components/angular-moment/angular-moment.min.js',

    //'./bower_components/tv4/tv4.js',
    //'./bower_components/objectpath/lib/ObjectPath.js',
    //'./bower_components/angularjs-datepicker/dist/angular-datepicker.min.js',
    //'./bower_components/angular-schema-form/dist/schema-form.js',
    // './bower_components/angular-schema-form/dist/bootstrap-decorator.min.js',

    './bower_components/angular-bootstrap/ui-bootstrap.min.js',
    './bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js',
    // './bower_components/angular-chosen-js/angular-chosen.min.js',
    './bower_components/angularjs-dropdown-multiselect/src/angularjs-dropdown-multiselect.js',
    './bower_components/angular-file-upload/dist/angular-file-upload.min.js',
    './bower_components/ng-youtube-embed/build/ng-youtube-embed.min.js',

    './app/js/lib/intl/intlTelInput.js',
    './app/js/lib/intl/utils.js',
    './bower_components/ng-intl-tel-input/dist/ng-intl-tel-input.js',

    './app/js/directives/ngthumb.js',
    './app/js/directives/focusme.js',
    './app/js/main.js',
    './app/js/mytranslate.js',
            // './app/js/components/main_component/component.js',
            //'./app/js/myschemaform/googlecoords.js',
            // './bower_components/ng-finder/ng-finder.js',

];

var commonCss = [
    //'./bower_components/bootstrap/dist/css/bootstrap.min.css',
    './bower_components/angularjs-datepicker/dist/angular-datepicker.min.css',
    './app/css/intlTelInput.css',
]

gulp.task('cssmain', function () {
    var src = commonCss.concat([
        './app/css/home.css',
                //  './app/css/jquery-ui.css',
                //  './app/css/elfinder.min.css',
                //  './app/css/theme.css',
    ]);

    return gulp.src(src)
            .pipe(concat('main.css'))
            .pipe(gulp.dest('./app/'));
});

gulp.task('scripts-prematch', function () {
    var src = commonScripts.concat([
        './app/js/services/myservice.js',
        './app/js/services/chatservice.js',
        './app/js/components/mchat/mchat.js',
        './app/js/components/userportfolio/userportfolio.js',
        './app/js/components/addportfolio/component.js',
        './app/js/components/smartselect/component.js',
        './app/js/components/skilseditor/component.js',
        './app/js/components/selecttosearch/component.js',
        './app/js/components/usersettings/component.js',
        './app/js/components/education/education.js',
        './app/js/components/education/educationlist/educationlist.js',
        './app/js/components/education/educationedit/educationedit.js',
        './app/js/components/employment/employment.js',
        './app/js/components/employment/employmentlist/employmentlist.js',
        './app/js/components/employment/employmentedit/employmentedit.js',
              
    ]);

    return gulp.src(src)
            .pipe(concat('src.js'))
            // .pipe(minify())
            .pipe(gulp.dest('./app/js/'));
});













gulp.task('sass-live', function () {
    gulp.src(['.app/scss/*.scss'])
            .pipe(sass())
            .pipe(gulp.dest('.app/css'));
});


gulp.task('watch', function () {
    //watch javascript files
    gulp.watch([
        './app/js/**/*.js'

    ], ['scripts-prematch']);
    gulp.watch([
        './app/css/**/*.css'

    ], ['cssmain']);









    //watch html files

    gulp.watch([
        './app/html/*.html',
        './app/html/app.html',
    ], ['fileinclude-sports']);


    gulp.watch([
        './.app/scss/*.scss'
    ], ['sass-live']);
});

gulp.task('fileinclude-sports', function () {
    gulp.src(['./app/html/app.html'])
            .pipe(fileinclude({
                prefix: '@@',
                basepath: '@file'
            }))
            .pipe(gulp.dest('.'));
});


gulp.task('default', ['watch']);