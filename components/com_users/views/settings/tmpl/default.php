<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

// Load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load('plg_user_profile', JPATH_ADMINISTRATOR);
$user = workFactory::getUser();
JLoader::import('wslib.media.multipic');
$wsPic = new wsMultiPic();
$wsPic->setCacheDir('pictures');
?><!-- Compny Profile -->
<div class="compny-profile"> 

    <!-- Profile Company Content -->
    <div class="profile-company-content has-bg-image" data-bg-color="f5f5f5" style="background-image: url(&quot;undefined&quot;); background-color: rgb(245, 245, 245);">
        <div class="container">
            <div class="row"> 

                <!-- SIDE BAR -->
                <div class="col-md-4"> 
                    <!-- Company Information -->
                    <div class="sidebar">
                        <h5 class="main-title"><?php echo $user->get('name') . ' ' . $user->get('surname'); ?></h5>
                        <div class="sidebar-thumbnail">
                            <?php
                            $uploadItemid = wsHelper::getItemid('com_users', 'profile', 'edit');
                            ?>

                            <img class="avatar-image-tochange" src="<?php echo $wsPic->getImage('image20', $user->getAvatar()); ?>" alt="">
                            <div class="overlay"></div>


                            <div class="edit-button">
                                <a href="#" class="showUploadForm" data-url="<?php echo JRoute::_('index.php?option=com_users&task=profile.saveProfileImage&Itemid=' . $uploadItemid); ?>" >
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </div>

                        </div>
                        <div class="uploadAvatarConteiner" style="display: none;">
                            <span class="btn btn-success fileinput-button">
                                <i class="fa fa-plus"></i>
                                <span><?php echo JText::_('add'); ?></span>
                            </span>
                        </div>






                        <div class="sidebar-information">
                            <ul class="single-category">
                                <li class="row">

                                    <?php 
                                    $sItemid = wsHelper::getItemid('com_users','settings');
                                    $ssettingsLink = JRoute::_('index.php?option=com_users&view=settings&Itemid='.$sItemid);
                                    
                                    ?>
                                    <span class="subtitle col-xs-12">
                                        <a href="<?php echo $ssettingsLink; ?>"><?php echo JText::_('Edit settings'); ?></a>
                                    </span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('jcategory'); ?></h6>
                                    <span class="subtitle col-xs-6">Automotive</span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('Location'); ?></h6>
                                    <span class="subtitle col-xs-6">California, USA</span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('NUMBER_OF_WORK'); ?></h6>
                                    <span class="subtitle col-xs-6">11,245</span> </li>

                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('Operating Hours'); ?></h6>
                                    <span class="subtitle col-xs-6">10:00 AM - 5:00 PM</span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('Contacts'); ?></h6>
                                    <div class="col-xs-6"> <span class="subtitle">*****************<i class="fa fa-exclamation-circle"></i></span> <span class="subtitle">***************** <i class="fa fa-exclamation-circle"></i></span> <a href="#."><?php echo $user->get('email'); ?></a> <a href="#.">example.com</a> </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>

                <!-- Tab Content -->
                <div class="col-md-8">
                    <div class="network">
                        <h4><?php echo JText::_('EDIT PROFILE'); ?></h4>
                        <user-settings-component></user-settings-component>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>