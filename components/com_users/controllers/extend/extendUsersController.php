<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of extendUsersController
 *
 * @author Mixas
 */
JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

class extendUsersController extends UsersController
{
    public function __construct($config = array())
    {
        parent::__construct($config);
       
    }
    public function jsonResponse($response = null, $message = null, $error = false)
    {
        header('Content-Type: application/json');

        echo new JResponseJson($response, $message);
        die;
    }
    public function jsonErrorResponse($response = null, $message = null, $error = false)
    {
        header('Content-Type: application/json');

        echo new JResponseJson($response, $message,true);
        die;
    }
}
