<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

/**
 * Profile controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerChat extends extendUsersController
{

    public function getData()
    {
        $ret = new stdClass;
        $chat = workFactory::getChat();
        $user = workFactory::getUser();
        $ret->chats = $chat->getChats();
        $ret->me = new stdClass;
        $ret->me->id = $user->get('id');
        $ret->me->avatar = $user->getAvatar('image18');
        $this->jsonResponse($ret, 'fined');
    }

    public function sendMessage()
    {
        $input = JFactory::getApplication()->input->json;
        $chatId = $input->getInt('id', 0);
        $message = $input->getString('message', '');
        if (empty($chatId) || empty($message))
        {
            $this->jsonErrorResponse(array(), JText::_('Some parameters are wrong'));
        }
        $chat = workFactory::getChat();
        try
        {
            $ret = $chat->sendMessage($chatId, $message);
        } catch (Exception $ex)
        {
            $this->jsonErrorResponse(array(), JText::_($ex->getMessage()));
        }
        $this->jsonResponse($ret, 'message added');
    }

    public function getMessages()
    {
        $input = JFactory::getApplication()->input->json;
        $chatId = $input->getInt('chatId', 0);
        $limitstart = $input->getInt('limitstart', 0);
        if (empty($chatId))
        {
            $this->jsonErrorResponse(array(), JText::_('Some parameters are wrong'));
        }
        $chat = workFactory::getChat();
        try
        {
            $ret = $chat->getChatMessages($chatId, $limitstart);
        } catch (Exception $ex)
        {
            $this->jsonErrorResponse(array(), JText::_($ex->getMessage()));
        }


        $this->jsonResponse($ret, 'message fined');
    }

}
