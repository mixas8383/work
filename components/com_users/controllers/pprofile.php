<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('UsersController', JPATH_COMPONENT . '/controller.php');

/**
 * Profile controller class for Users.
 *
 * @since  1.6
 */
class UsersControllerPProfile extends UsersController
{

    public function uploadfoto()
    {
        $ret = new stdClass;
        $ret->success = true;

        echo json_encode(array('files' => $ret));
        die;
    }

    /**
     * Method to check out a user for editing and redirect to the edit form.
     *
     * @return  boolean
     *
     * @since   1.6
     */
    public function edit()
    {
        $app = JFactory::getApplication();
        $user = JFactory::getUser();
        $loginUserId = (int) $user->get('id');

        // Get the previous user id (if any) and the current user id.
        $previousId = (int) $app->getUserState('com_users.edit.profile.id');
        $userId = $this->input->getInt('user_id', null, 'array');

        // Check if the user is trying to edit another users profile.
        if ($userId != $loginUserId)
        {
            $app->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'error');
            $app->setHeader('status', 403, true);

            return false;
        }

        $cookieLogin = $user->get('cookieLogin');

        // Check if the user logged in with a cookie
        if (!empty($cookieLogin))
        {
            // If so, the user must login to edit the password and other data.
            $app->enqueueMessage(JText::_('JGLOBAL_REMEMBER_MUST_LOGIN'), 'message');
            $this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));

            return false;
        }

        // Set the user id for the user to edit in the session.
        $app->setUserState('com_users.edit.profile.id', $userId);

        // Get the model.
        $model = $this->getModel('Profile', 'UsersModel');

        // Check out the user.
        if ($userId)
        {
            $model->checkout($userId);
        }

        // Check in the previous user.
        if ($previousId)
        {
            $model->checkin($previousId);
        }

        // Redirect to the edit screen.
        $this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit', false));

        return true;
    }

    /**
     * Method to save a user's profile data.
     *
     * @return  void
     *
     * @since   1.6
     */
    public function save()
    {
        // Check for request forgeries.
        $this->checkToken();

        $app = JFactory::getApplication();
        $model = $this->getModel('Profile', 'UsersModel');
        $user = JFactory::getUser();
        $userId = (int) $user->get('id');

        // Get the user data.
        $requestData = $app->input->post->get('jform', array(), 'array');

        // Force the ID to this user.
        $requestData['id'] = $userId;

        // Validate the posted data.
        $form = $model->getForm();

        if (!$form)
        {
            JError::raiseError(500, $model->getError());

            return false;
        }

        // Validate the posted data.
        $data = $model->validate($form, $requestData);

        // Check for errors.
        if ($data === false)
        {
            // Get the validation messages.
            $errors = $model->getErrors();

            // Push up to three validation messages out to the user.
            for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
            {
                if ($errors[$i] instanceof Exception)
                {
                    $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
                } else
                {
                    $app->enqueueMessage($errors[$i], 'warning');
                }
            }

            // Unset the passwords.
            unset($requestData['password1'], $requestData['password2']);

            // Save the data in the session.
            $app->setUserState('com_users.edit.profile.data', $requestData);

            // Redirect back to the edit screen.
            $userId = (int) $app->getUserState('com_users.edit.profile.id');
            $this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&user_id=' . $userId, false));

            return false;
        }

        // Attempt to save the data.
        $return = $model->save($data);

        // Check for errors.
        if ($return === false)
        {
            // Save the data in the session.
            $app->setUserState('com_users.edit.profile.data', $data);

            // Redirect back to the edit screen.
            $userId = (int) $app->getUserState('com_users.edit.profile.id');
            $this->setMessage(JText::sprintf('COM_USERS_PROFILE_SAVE_FAILED', $model->getError()), 'warning');
            $this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit&user_id=' . $userId, false));

            return false;
        }

        // Redirect the user and adjust session state based on the chosen task.
        switch ($this->getTask())
        {
            case 'apply':
                // Check out the profile.
                $app->setUserState('com_users.edit.profile.id', $return);
                $model->checkout($return);

                // Redirect back to the edit screen.
                $this->setMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));

                $redirect = $app->getUserState('com_users.edit.profile.redirect');

                // Don't redirect to an external URL.
                if (!JUri::isInternal($redirect))
                {
                    $redirect = null;
                }

                if (!$redirect)
                {
                    $redirect = 'index.php?option=com_users&view=profile&layout=edit&hidemainmenu=1';
                }

                $this->setRedirect(JRoute::_($redirect, false));
                break;

            default:
                // Check in the profile.
                $userId = (int) $app->getUserState('com_users.edit.profile.id');

                if ($userId)
                {
                    $model->checkin($userId);
                }

                // Clear the profile id from the session.
                $app->setUserState('com_users.edit.profile.id', null);

                $redirect = $app->getUserState('com_users.edit.profile.redirect');

                // Don't redirect to an external URL.
                if (!JUri::isInternal($redirect))
                {
                    $redirect = null;
                }

                if (!$redirect)
                {
                    $redirect = 'index.php?option=com_users&view=profile&user_id=' . $return;
                }

                // Redirect to the list screen.
                $this->setMessage(JText::_('COM_USERS_PROFILE_SAVE_SUCCESS'));
                $this->setRedirect(JRoute::_($redirect, false));
                break;
        }

        // Flush the data from the session.
        $app->setUserState('com_users.edit.profile.data', null);
    }
  

    /**
     * Function that allows child controller access to model data after the data has been saved.
     *
     * @param   JModelLegacy  $model      The data model object.
     * @param   array         $validData  The validated data.
     *
     * @return  void
     *
     * @since   3.1
     */
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {
        $item = $model->getData();
        $tags = $validData['tags'];

        if ($tags)
        {
            $item->tags = new JHelperTags;
            $item->tags->getTagIds($item->id, 'com_users.user');
            $item->metadata['tags'] = $item->tags;
        }
    }

    public function saveProfileImage()
    {
        $ret = new stdClass();
        JLoader::import('wslib.media.multipic');
        $wsPic = new wsMultiPic();
        $wsPic->setCacheDir('pictures');

        $user = workFactory::getUser();





        if (!$user->isLoged())
        {
            return false;
        }
        $userId = $user->get('id');

        $app = JFactory::getApplication()->input;

        $file = $app->files->get('primage');
        if (!empty($file))
        {
            JLoader::import('cms.helper.media');
            $mediaHelper = new JHelperMedia;
            $contentLength = (int) $_SERVER['CONTENT_LENGTH'];
            $postMaxSize = $mediaHelper->toBytes(ini_get('post_max_size'));
            $memoryLimit = $mediaHelper->toBytes(ini_get('memory_limit'));

            if (($postMaxSize > 0 && $contentLength > $postMaxSize) || ($memoryLimit != -1 && $contentLength > $memoryLimit))
            {

                $ret->name = $file['name'];
                $ret->size = $file['size'];
                $ret->error = JText::_('COM_MEDIA_ERROR_WARNUPLOADTOOLARGE');
                $this->ajaxResponse(array('files' => $ret));
            }
            $params = JComponentHelper::getParams('com_media');
            $uploadMaxSize = $params->get('upload_maxsize', 0) * 1024 * 1024;
            $uploadMaxFileSize = $mediaHelper->toBytes(ini_get('upload_max_filesize'));



            $file['name'] = JFile::makeSafe($file['name']);
            $file['name'] = str_replace(' ', '-', $file['name']);
            $file['filepath'] = JPath::clean($user->getAvatarPath());

            if (($file['error'] == 1) || ($uploadMaxSize > 0 && $file['size'] > $uploadMaxSize) || ($uploadMaxFileSize > 0 && $file['size'] > $uploadMaxFileSize))
            {

                $ret->name = $file['name'];
                $ret->size = $file['size'];
                $ret->error = JText::_('COM_MEDIA_ERROR_WARNFILETOOLARGE');
                $this->ajaxResponse(array('files' => $ret));
            }

            if (JFile::exists($file['filepath']))
            {
                JFile::delete($file['filepath']);
            }

            if (!isset($file['name']))
            {
                $ret->name = $file['name'];
                $ret->size = $file['size'];
                $ret->error = JText::_('COM_MEDIA_INVALID_REQUEST');
                $this->ajaxResponse(array('files' => $ret));
            }
            require_once JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_media' . DS . 'helpers' . DS . 'media.php';
            $err = null;
            if (!MediaHelper::canUpload($file, $err))
            {
                // The file can't be uploaded
                return false;
            }
            $object_file = new JObject($file);

            if (!JFile::upload($object_file->tmp_name, $object_file->filepath))
            {
                $ret->name = $object_file->name;
                $ret->size = $object_file->size;
                $ret->error = JText::_('COM_MEDIA_ERROR_UNABLE_TO_UPLOAD_FILE');
                $this->ajaxResponse(array('files' => $ret));
            }



            $avatar = $wsPic->getImage('image20', $user->getAvatar());
            if (JFile::exists(JPATH_ROOT . DS . $avatar))
            {
                JFile::delete(JPATH_ROOT . DS . $avatar);
                $avatar = $wsPic->getImage('image20', $user->getAvatar());
            }

            if (empty($avatar))
            {
                $ret->name = $object_file->name;
                $ret->size = $object_file->size;
                $ret->error = JText::_('cannot generate avatar');
                $this->ajaxResponse(array('files' => $ret));
            }

            $ret->name = $object_file->name;
            $ret->size = $object_file->size;
            $ret->thumbnailUrl = $object_file->size;
            $ret->type = $object_file->type;
           // $ret->error = 'some error';
            $ret->url = JUri::root() . $avatar . '?t=' . rand(1, 1000000);

            $this->ajaxResponse(array('files' => $ret));
        }
        $ret->name = 'error';
        $ret->size = 0;
        $ret->error = JText::_('file not selected');
        $this->ajaxResponse(array('files' => $ret));
    }

    public function ajaxResponse($data)
    {
        echo json_encode($data);
        die;
    }

}
