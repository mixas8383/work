<?php

/**
 * @version 		$Id:$
 * @name			Work (Release 1.0.0)
 * @author			 ()
 * @package			com_work
 * @subpackage		com_work.site
 * @copyright		
 * @license			GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html 
 * 
 * The following Component Architect header section must remain in any distribution of this file
 *
 * @CAversion		Id: compobjectplural.php 423 2014-10-23 14:08:16Z BrianWade $
 * @CAauthor		Component Architect (www.componentarchitect.com)
 * @CApackage		architectcomp
 * @CAsubpackage	architectcomp.site
 * @CAtemplate		joomla_3_3_standard (Release 1.0.3)
 * @CAcopyright		Copyright (c)2013 - 2014  Simply Open Source Ltd. (trading as Component Architect). All Rights Reserved
 * @Joomlacopyright Copyright (c)2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @CAlicense		GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
defined('_JEXEC') or die;

/**
 * This models supports retrieving lists of items.
 *
 */
class WorkModelProfessionals extends JModelList
{

    /**
     * @var    string	$context	Context string for the model type.  This is used to handle uniqueness within sessions data.
     */
    protected $context = 'com_work.professionals';

    /**
     * Constructor.
     *
     * @param	array	An optional associative array of configuration settings.
     * 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields']))
        {
            $config['filter_fields'] = array(
                'id', 'a.id',
                'name', 'a.name',
                'alias', 'a.alias',
                'checked_out', 'a.checked_out',
                'checked_out_time', 'a.checked_out_time',
                'catid', 'a.catid', 'category_title',
                'created', 'a.created',
                'created_by', 'a.created_by',
                'created_by_name', 'ua.name',
                'modified', 'a.modified',
                'modified_by', 'a.modified_by',
                'modified_by_name', 'uam.name',
                'publish_up', 'a.publish_up',
                'publish_down', 'a.publish_down',
                'featured', 'a.featured',
                'language', 'a.language',
                'hits', 'a.hits',
                'rating',
                'ordering', 'a.ordering',
            );
        }

        parent::__construct($config);
    }

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return	void
     * 
     */
    protected function populateState($ordering = null, $direction = null)
    {
        $app = JFactory::getApplication();
        // Load the parameters. Merge Global and Menu Item params into new object
        $params = $app->getParams();
        $menu_params = new JRegistry;

        if ($menu = $app->getMenu()->getActive())
        {
            $menu_params->loadString($menu->params);
        }

        $merged_params = clone $menu_params;
        $merged_params->merge($params);

        $this->setState('params', $merged_params);

        $params = $this->state->params;

        $user = JFactory::getUser();

        $item_id = $app->input->getInt('id', 0) . ':' . $app->input->getInt('Itemid', 0);

        // Check to see if a single item has been specified either as a parameter or in the url Request
        $pk = $params->get('item_id', '') == '' ? $app->input->getInt('id', '') : $params->get('item_id');
        $this->setState('filter.item_id', $pk);

        // List state information
        if ($app->input->getString('layout', 'default') == 'blog')
        {
            $limit = $params->def('item_num_leading', 1) + $params->def('item_num_intro', 4) + $params->def('item_num_links', 4);
        } else
        {
            $limit = $app->getUserStateFromRequest($this->context . '.list.' . $item_id . '.limit', 'limit', $params->get('item_num_per_page'), 'integer');
        }
        $this->setState('list.limit', $limit);

        $value = $app->getUserStateFromRequest($this->context . '.limitstart', 'limitstart', 0, 'integer');
        $this->setState('list.start', $value);

        $search = $app->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);

        $category_id = $app->getUserStateFromRequest($this->context . '.filter.category_id', 'filter_category_id');
        $this->setState('filter.category_id', $category_id);

        $order_col = $app->getUserStateFromRequest($this->context . '.filter_order', 'filter_order', $params->get('item_initial_sort', 'a.id'), 'string');
        if (!in_array($order_col, $this->filter_fields))
        {
            $order_col = $params->get('item_initial_sort', 'a.ordering');
        }

        $this->setState('list.ordering', $order_col);

        $list_order = $app->getUserStateFromRequest($this->context . '.filter_order_Dir', 'filter_order_Dir', $params->get('item_initial_direction', 'ASC'), 'cmd');
        if (!in_array(JString::strtoupper($list_order), array('ASC', 'DESC', '')))
        {
            $list_order = $params->get('item_initial_direction', 'ASC');
        }
        $this->setState('list.direction', $list_order);


        if ((!$user->authorise('core.edit.state', 'com_work')) AND ( !$user->authorise('core.edit', 'com_work')))
        {
            // filter on status of published for those who do not have edit or edit.state rights.
            $this->setState('filter.published', 1);
        } else
        {
            $this->setState('filter.published', array(0, 1, 2));
        }

        $this->setState('filter.language', JLanguageMultilang::isEnabled());

        // process show_item_noauth parameter
        if (!$params->get('show_item_noauth'))
        {
            $this->setState('filter.access', true);
        } else
        {
            $this->setState('filter.access', false);
        }
        // check for category selection
        if ($params->get('filter_item_categories') AND implode(',', $params->get('filter_item_categories')) == true)
        {
            $selected_categories = $params->get('filter_item_categories');
            $this->setState('filter.category_id', $selected_categories);
        }
        if ($params->get('filter_item_featured') <> "")
        {
            $this->setState('filter.featured', $params->get('filter_item_featured'));
        }
        if ($params->get('filter_item_archived'))
        {
            $this->setState('filter.archived', $params->get('filter_item_archived'));
        }
        $this->setState('layout', $app->input->getString('layout'));
    }

    /**
     * Method to get a store id based on model configuration state.
     *
     * This is necessary because the model is used by the component and
     * different modules that might need different sets of data or different
     * ordering requirements.
     *
     * @param	string		$id	A prefix for the store id.
     *
     * @return	string		A store id.
     * 
     */
    protected function getStoreId($id = '')
    {
        // Compile the store id.
        $id .= ':' . $this->getState('filter.search');
        $id .= ':' . serialize($this->getState('filter.published'));
        $id .= ':' . $this->getState('filter.archived');
        $id .= ':' . $this->getState('filter.access');
        $id .= ':' . $this->getState('filter.featured');
        $id .= ':' . serialize($this->getState('filter.category_id'));
        $id .= ':' . serialize($this->getState('filter.category_id.include'));
        $id .= ':' . serialize($this->getState('filter.created_by_id'));
        $id .= ':' . $this->getState('filter.created_by_id.include');
        $id .= ':' . $this->getState('filter.created_by_name');
        $id .= ':' . $this->getState('filter.created_by_name.include');
        $id .= ':' . serialize($this->getState('filter.item_id'));
        $id .= ':' . $this->getState('filter.item_id.include');


        return parent::getStoreId($id);
    }

    /**
     * Get the main query for retrieving a list of items subject to the model state.
     *
     * @return	JDatabaseQuery
     * 
     */
    protected function getListQuery()
    {
        // Get the current user for authorisation checks
        $user = JFactory::getUser();
        // Create a new query object.
        $db = $this->getDbo();
        $query = $db->getQuery(true);
        // Set date values
        $null_date = $db->quote($db->getNullDate());
        $now_date = $db->quote(JFactory::getDate()->toSQL());

        // Select the required fields from the table.
        $query->select(
                $this->getState(
                        'list.select', 'a.*'
                )
        );


        $query->from($db->quoteName('#__users') . ' AS a');



        $query->select($db->quoteName('ua.group_id') . ' AS group_id');
        $query->join('LEFT', $db->quoteName('#__user_usergroup_map') . ' AS ua on ' . $db->quoteName('ua.user_id') . ' = ' . $db->quoteName('a.id'));

        // Filter by language
        if ($this->getState('filter.language'))
        {
            $query->where($db->quoteName('a.language') . ' IN (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
        }

        // Join on vote rating table
        // Filter by access level.
        if ($access = $this->getState('filter.access'))
        {
            $groups = implode(',', $user->getAuthorisedViewLevels());
            $query->where($db->quoteName('a.access') . ' IN (' . $groups . ')');
            $query->where($db->quoteName('c.access') . ' IN (' . $groups . ')');
        }










        // Filter by created_by
        $created_by_id = $this->getState('filter.created_by_id');
        $created_by_where = '';

        if (is_numeric($created_by_id))
        {
            $type = $this->getState('filter.created_by_id.include', true) ? '= ' : '<> ';
            $created_by_where = $db->quoteName('a.created_by') . ' ' . $type . (int) $created_by_id;
        } else
        {
            if (is_array($created_by_id))
            {
                JArrayHelper::toInteger($created_by_id);
                $created_by_id = implode(',', $created_by_id);

                if ($created_by_id)
                {
                    $type = $this->getState('filter.created_by_id.include', true) ? 'IN' : 'NOT IN';
                    $created_by_where = $db->quoteName('a.created_by') . ' ' . $type . ' (' . $created_by_id . ')';
                }
            }
        }


        if (!empty($created_by_where))
        {
            $query->where('(' . $created_by_where . ')');
        }

        // Filter by created_by_name
//		$created_by_name = $this->getState('filter.created_by_name');
//		$created_by_name_where = '';
//
//		if (is_string($created_by_name))
//		{
//			$type = $this->getState('filter.created_by_name.include', true) ? '= ' : '<> ';
//			$created_by_name_where = $db->quoteName('ua.name').' '.$type.$db->quote($created_by_name);
//		}
//		else
//		{
//			if (is_array($created_by_name))
//			{
//				$first = current($created_by_name);
//
//				if (!empty($first))
//				{
//					JArrayHelper::toString($created_by_name);
//
//					foreach ($created_by_name as $key => $alias)
//					{
//						$created_by_name[$key] = $db->quote($alias);
//					}
//
//					$created_by_name = implode(',', $created_by_name);
//
//					if ($created_by_name)
//					{
//						$type = $this->getState('filter.created_by_name.include', true) ? 'IN' : 'NOT IN';
//						$created_by_name_where = $db->quoteName('ua.name').' '.$type.' ('.$created_by_name .
//							')';
//					}
//				}
//			}
//		}

        if (!empty($created_by_name_where))
        {
            $query->where('(' . $created_by_name_where . ')');
        }

        // process the filter for list views with user-entered filters
        $params = $this->getState('params');

        if ((is_object($params)) AND ( $params->get('show_item_filter_field') != 'hide') AND ( $filter = $this->getState('filter.search')))
        {
            // clean filter variable
            $filter = JString::strtolower($filter);
            $hits_filter = (int) $filter;
            $filter = $db->quote('%' . $db->escape($filter, true) . '%', false);

            switch ($params->get('show_item_filter_field'))
            {
                case 'hits':
                    $query->where($db->quoteName('a.hits') . ' >= ' . (int) $hits_filter . ' ');
                    break;
                case 'created_by':
                    $query->where('LOWER(' . $db->quoteName('ua.name') . ') LIKE ' . $filter . ' ');
                    break;
                case 'name':
                default: // default to 'name' if parameter is not valid
                    $query->where('LOWER(' . $db->quoteName('a.name') . ') LIKE ' . $filter);
                    break;
            }
        }
        // Filter by language
        if ($this->getState('filter.language'))
        {
            $query->where($db->quoteName('a.language') . ' IN (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
        }
        $initial_sort = 'id';
        // Add the list ordering clause.
        if (is_object($params))
        {
            $initial_sort = $params->get('field_initial_sort');
        } else
        {
            $initial_sort = 'id';
        }



        // Fall back to old style if the parameter hasn't been set yet.
        if (empty($initial_sort) OR $this->getState('list.ordering') != '')
        {
            $order_col = '';
            $order_dirn = $this->state->get('list.direction');


            if ($this->state->get('list.ordering') == 'category_title')
            {
                $order_col = $db->quoteName('c.title') . ' ' . $order_dirn . ', ' . $db->quoteName('a.ordering');
            }


            if ($this->state->get('list.ordering') == 'a.ordering' OR $this->state->get('list.ordering') == 'ordering')
            {
                $order_col = '';
                $order_col .= $db->quoteName('a.ordering') . ' ' . $order_dirn;
            }

            if ($order_col == '')
            {
                $order_col = is_string($this->getState('list.ordering')) ? $db->quoteName($this->getState('list.ordering')) : $db->quoteName('a.ordering');
                $order_col .= ' ' . $order_dirn;
            }
            $query->order($db->escape($order_col));
        } else
        {

            if (isset($_SERVER['HTTP_USER_AGENT']) && $_SERVER['HTTP_USER_AGENT'] == 'Debug')
            {
                echo '<pre>' . __FILE__ . ' -->>| <b> Line </b>' . __LINE__ . '</pre><pre>';
                print_r($initial_sort);
                die;
            }

            $query->order($db->quoteName('a.' . $initial_sort) . ' ' . $db->escape($this->getState('list.direction', 'ASC')));
        }


        return $query;
    }

    /**
     * Method to get a list of items.
     *
     * Overriden to inject convert the params fields into an object.
     *
     * @return	mixed	An array of objects on success, false on failure.
     * 
     */
    public function getItems()
    {
        $db = $this->getDbo();
        $query = $db->getQuery(true);

        $user = JFactory::getUser();
        $user_id = $user->get('id');
        $guest = $user->get('guest');
        $groups = $user->getAuthorisedViewLevels();

        // Get the global params
        $global_params = JComponentHelper::getParams('com_work', true);
        $ret = array();
        if ($items = parent::getItems())
        {
            // Convert the parameter fields into objects.
            foreach ($items as &$item)
            {
               $ret[]= new wUser($item);
            }
        }
        return $ret;
    }

}
