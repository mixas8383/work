<?php

/**
 * @version 		$Id:$
 * @name			Work (Release 1.0.0)
 * @author			 ()
 * @package			com_work
 * @subpackage		com_work.site
 * @copyright		
 * @license			GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html 
 * 
 * The following Component Architect header section must remain in any distribution of this file
 *
 * @CAversion		Id: compobject.php 408 2014-10-19 18:31:00Z BrianWade $
 * @CAauthor		Component Architect (www.componentarchitect.com)
 * @CApackage		architectcomp
 * @CAsubpackage	architectcomp.site
 * @CAtemplate		joomla_3_3_standard (Release 1.0.3)
 * @CAcopyright		Copyright (c)2013 - 2014  Simply Open Source Ltd. (trading as Component Architect). All Rights Reserved
 * @Joomlacopyright Copyright (c)2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @CAlicense		GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
defined('_JEXEC') or die;

/**
 * Item controller class.
 * 
 */
class WorkControllerItem extends extendJControllerForm
{

    /**
     * @var    string	$view_item	The URL view item variable.
     */
    protected $view_item = 'itemform';


    /**
     * @var    string	$view_list	The URL view list variable.
     */
    protected $view_list = 'items';


    /**
     * 
     * @var    string	$url_var	The URL edit variable
     */
    protected $url_var = 'a.id';

    /**
     * Constructor
     *
     */
    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->registerTask('apply', 'save');
        $this->registerTask('save2new', 'save');
        $this->registerTask('save2copy', 'save');
    }

    /**
     * Method override to check if you can add a new record.
     *
     * @param	array	$data	An array of input data.
     *
     * @return	boolean
     * 
     */
    protected function allowAdd($data = array())
    {
        $user = JFactory::getUser();
        $category_id = JArrayHelper::getValue($data, 'catid', $this->input->getInt('catid'), 'int');
        $allow = null;
        if ($category_id)
        {
            // If the category has been passed in the data or URL check it.
            $allow = $user->authorise('core.create', 'com_work.category.' . $category_id);
        }
        if ($allow === null)
        {
            // In the absense of better information, revert to the component permissions.
            return parent::allowAdd();
        } else
        {
            return $allow;
        }
    }

    /**
     * Method override to check if you can edit an existing record.
     *
     * @param	array	$data	An array of input data.
     * @param	string	$key	The name of the key for the primary key; default is id
     *
     * @return	boolean
     * 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        $record_id = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $asset = 'com_work.item.' . $record_id;
        // Check general edit permission first.
        if ($user->authorise('core.edit', $asset))
        {
            return true;
        }

        // Fallback on edit.own.
        // First test if the permission is available.
        if ($user->authorise('core.edit.own', $asset))
        {
            $owner_id = 0;
            // Now test the owner is the user.
            if (isset($data['created_by']))
            {
                $owner_id = (int) $data['created_by'];
            }
            if (empty($owner_id) AND $record_id)
            {
                // Need to do a lookup from the model.
                $record = $this->getModel('itemform')->getItem($record_id);

                if (empty($record))
                {
                    return false;
                }

                $owner_id = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($owner_id == $user->id)
            {
                return true;
            }
        }

        // Since there is no asset tracking, revert to the component permissions.
        return parent::allowEdit($data, $key);
    }

    /**
     * Method override to check if you can delete an existing record.
     *
     * @param	array	$data	An array of input data.
     * @param	string	$key	The name of the key for the primary key; default is id
     *
     * @return	boolean
     *
     */
    protected function allowDelete($data = array(), $key = 'id')
    {
        $record_id = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $asset = 'com_work.item.' . $record_id;

        // Check general delete permission.
        if ($user->authorise('core.delete', $asset))
        {
            return true;
        }

        // Fallback on delete.own.
        // First test if the permission is available.
        if ($user->authorise('core.delete.own', $asset))
        {
            $owner_id = 0;
            // Now test the owner is the user.
            if (isset($data['created_by']))
            {
                $owner_id = (int) $data['created_by'];
            }
            if (empty($owner_id) AND $record_id)
            {
                // Need to do a lookup from the model.
                $record = $this->getModel('itemform')->getItem($record_id);

                if (empty($record))
                {
                    return false;
                }

                $owner_id = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($owner_id == $user_id)
            {
                return true;
            }
            // If the owner matches 'me' then do the test.
            if ($owner_id == $user->id)
            {
                return true;
            } else
            {
                return false;
            }
        }
    }

    /**
     * Method to get a model object, loading it if required.
     *
     * @param	string	$name	The model name. Optional.
     * @param	string	$prefix	The class prefix. Optional.
     * @param	array	$config	Configuration array for model. Optional.
     *
     * @return	object	The model.
     * 
     */
    public function getModel($name = 'itemform', $prefix = '', $config = array('ignore_request' => true))
    {
        $model = parent::getModel($name, $prefix, $config);

        return $model;
    }

    /**
     * Method to get the return page saved in session data.
     *
     * @param	string	$context	The context string used to store the return data
     *
     * @return	string	The url string for the return page
     * 
     */
    protected function getReturnPage($context)
    {
        $app = JFactory::getApplication();

        if (!($return = $app->getUserState($context . '.return')))
        {
            return JUri::base();
        }

        $return = base64_decode($return);

        if (!JUri::isInternal($return))
        {
            $return = JUri::base();
        }

        return $return;
    }

    /**
     * Method to set the return page as a saved entry in session data.
     *
     * @param	string	$context	The context string used to store the return data
     *
     * @return	void
     * 
     */
    protected function setReturnPage($context)
    {
        $app = JFactory::getApplication();

        $return = $this->input->get('return', null, 'base64');

        if (empty($return) OR ! JUri::isInternal(base64_decode($return)))
        {
            $return = base64_encode(JUri::base());
        }

        $app->setUserState($context . '.return', $return);
    }

    /**
     * Method to clear the return page in session data.
     *
     * @param	string	$context	The context string used to store the return data
     *
     * @return	void
     * 
     */
    protected function clearReturnPage($context)
    {
        $app = JFactory::getApplication();

        $app->setUserState($context . '.return', null);
    }

    /**
     * Method to add a new record.
     *
     * @return  mixed  True if the Item can be added, a error object if not.
     *
     */
    public function add()
    {
        $app = JFactory::getApplication();
        $context = $this->option . '.edit.' . $this->context;

        // Access check
        if (!$this->allowAdd())
        {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));

            return false;
        }

        // Clear the record edit information from the session.
        $app->setUserState($context . '.data', null);

        // Clear the return page.
        // TODO: We should be including an optional 'return' variable in the URL.
        $this->setReturnPage($context);

        $redirect = JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item . '&layout=edit', false);

        $this->setRedirect($redirect);

        return true;
    }

    /**
     * Method to edit a object
     *
     * Sets object ID in the session from the request, checks the item out, and then redirects to the edit page.
     * 
     * @param   string  $key     The name of the primary key of the URL variable.
     * @param   string  $url_var  The name of the URL variable if different from the primary key
     * (sometimes required to avoid router collisions).
     *
     * @return  boolean  True if access level check and checkout passes, false otherwise.
     *
     */
    public function edit($key = 'id', $url_var = null)
    {

        $app = JFactory::getApplication();
        $context = $this->option . '.edit.' . $this->context;
        $ids = $this->input->get('cid', array(), 'array');

        // Get the id of the group to edit.
        $record_id = (int) (empty($ids) ? $this->input->getInt('id') : array_pop($ids));

        // Access check
        if (!$this->allowEdit(array('id' => $record_id)))
        {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));

            return false;
        }

        // Get the menu item model.
        $model = $this->getModel('itemform');
        // Set the return url
        $this->setReturnPage($context);

        // Check that this is not a new item.

        if ($record_id > 0)
        {
            $item = $model->getItem($record_id);

            // If not already checked out, do so.
            if ($item->checked_out == 0)
            {
                if (!$model->checkout($record_id))
                {
                    // Check-out failed, go back to the list and display a notice.
                    $this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKOUT_FAILED', $model->getError()));
                    $this->setMessage($this->getError(), 'error');

                    // Redirect to the list screen.
                    $this->setRedirect($this->getReturnPage($context));

                    // Make sure return url is cleared
                    $this->clearReturnPage($context);

                    return false;
                }
            }
        }

        // Check-out succeeded, register the ID for editing.
        $this->holdEditId($context, $record_id);
        $app->setUserState($context . '.data', null);

        $redirect = JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item
                        . $this->getRedirectToItemAppend($record_id, $key), false);

        $this->setRedirect($redirect);

        return true;
    }

    /**
     * Method to cancel an edit
     *
     * Checks the item in, sets item ID in the session to null, and then redirects to the list page.
     * 
     * @param   string  $key  The name of the primary key of the URL variable.
     *
     * @return  boolean  True if access level checks pass, false otherwise.
     */
    public function cancel($key = 'id')
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));


        $app = JFactory::getApplication();
        $model = $this->getModel('itemform');
        $context = $this->option . '.edit.' . $this->context;
        $record_id = $this->input->getInt('id');

        if ($record_id)
        {
            // Check we are holding the id in the edit list.
            if (!$this->checkEditId($context, $record_id))
            {
                // Somehow the person just went to the form - we don't allow that.
                $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $record_id), 'error');
                $this->setRedirect($this->getReturnPage($context));

                // Make sure return url is cleared
                $this->clearReturnPage($context);
                return false;
            }

            // If rows ids do not match, checkin previous row.
            if ($model->checkin($record_id) === false)
            {
                // Check-in failed, go back to the menu item and display a notice.
                $this->setMessage(JText::sprintf('JERROR_CHECKIN_FAILED', $model->getError()), 'error');
                $this->setRedirect(JRoute::_(
                                'index.php?option=' . $this->option . '&view=' . $this->view_item
                                . $this->getRedirectToItemAppend($record_id, $key), false
                        )
                );
                return false;
            }
        }

        // Clear the menu item edit information from the session.
        $this->releaseEditId($context, $record_id);
        $app->setUserState($context . '.data', null);

        // Redirect to the list screen.
        $this->setRedirect($this->getReturnPage($context));

        // Make sure return url is cleared
        $this->clearReturnPage($context);
        return true;
    }

    /**
     * Method to save the record
     *
     * @param   string  $key     The name of the primary key of the URL variable.
     * @param   string  $url_var  The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
     *
     * @return  boolean  True if successful, false otherwise.* 
     */
    public function save($key = 'id', $url_var = null)
    {
        // Check for request forgeries.
        // temporary remove
        //JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));


        $app = JFactory::getApplication();
        // Think I need to leave this as JRequest for now as the App input only returns a copy of the post data so any changes will be lost
        // Replace with direct POST variable after some checking
        $data = $app->input->json->get('jform', array(), 'array'); // JRequest::getVar('jform', array(), 'post', 'array');
              



        $model = $this->getModel('itemform');
        $task = $this->getTask();
        $context = $this->option . '.edit.' . $this->context;
        $record_id = !empty($data['id']) ? $data['id'] : 0;
        if (in_array($this->input->getString('view'), array('category', 'categories')))
        {
            $record_id = 0;
        }
//     if (!$this->checkEditId($context, $record_id))
//        {
//            // Somehow the person just went to the form and saved it - we don't allow that.
//            $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $record_id), 'error');
//            $this->setRedirect($this->getReturnPage($context));
//
//            // Make sure return url is cleared
//            $this->clearReturnPage($context);
//            return false;
//        }
        // Populate the row id from the session.
        $data['id'] = $record_id;
        // Split intro from description
        $categories = array();
        if (!empty($data['categories']) && is_array($data['categories']))
        {
            foreach ($data['categories'] as $value)
            {
                $tmp = (int) $value['id'];
                if (!empty($tmp))
                {
                    $categories[] = $tmp;
                }
            }
        }
        $skils = array();
        if (!empty($data['skils']) && is_array($data['skils']))
        {
            foreach ($data['skils'] as $value)
            {
                $tmp = (int) $value['id'];
                if (!empty($tmp))
                {
                    $skils[] = $tmp;
                }
            }
        }
                
        if (!empty($categories))
        {
            $data['catid'] = $categories[0];
        } else
        {
            $this->jsonErrorResponse(array(), JText::_('select category'));
        }


        // Validate the posted data.
        $form = $model->getForm();


        if (!$form)
        {
            $this->jsonErrorResponse(array(), JText::_($model->getError()));
            JError::raiseError(500, $model->getError());
            return false;
        }




        $datastore = $data;
        $data = $model->validate($form, $data);
                

        // Check for validation errors.
        if ($data === false)
        {
            // Get the validation messages.
            $errors = $model->getErrors();

            // Push up to three validation messages out to the user.
            for ($i = 0, $n = count($errors); $i < $n AND $i < 3; $i++)
            {
                if (JError::isError($errors[$i]))
                {
                    $app->enqueueMessage($errors[$i]->getMessage(), 'warning');
                } else
                {
                    $app->enqueueMessage($errors[$i], 'warning');
                }
            }

            // Save the data in the session.
            $app->setUserState($context . '.data', $datastore);

            // Redirect back to the edit screen.
            $this->setRedirect(JRoute::_(
                            'index.php?option=' . $this->option . '&view=' . $this->view_item
                            . $this->getRedirectToItemAppend($record_id, $key), false
                    )
            );
            return false;
        }

        // Attempt to save the data.
        if (!$model->save($data))
        {
            // Save the data in the session.
            $app->setUserState($context . '.data', $data);

            // Redirect back to the edit screen.
            $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()), 'warning');
            $this->setRedirect(JRoute::_(
                            'index.php?option=' . $this->option . '&view=' . $this->view_item
                            . $this->getRedirectToItemAppend($record_id, $key), false
                    )
            );
            return false;
        }
        $record_id = $model->getState('item.id');


        // Save succeeded, check-in the row.
        if ($model->checkin() === false)
        {
            // Check-in failed, go back to the row and display a notice.
            $this->setMessage(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()), 'error');
            $this->setRedirect(JRoute::_(
                            'index.php?option=' . $this->option . '&view=' . $this->view_item
                            . $this->getRedirectToItemAppend($record_id, $key), false
                    )
            );
            return false;
        }

        if ($record_id == 0)
        {
            $this->setMessage(JText::_('COM_WORK_ITEMS_SUBMIT_SAVE_SUCCESS'));
        } else
        {
            $this->setMessage(JText::_('COM_WORK_ITEMS_SAVE_SUCCESS'));
        }

        // Redirect the user and adjust session state based on the chosen task.
        $data = new stdClass;
        $data->itemId = $record_id;
        $model->saveCategoryMap($record_id, $categories);
        $model->saveSkilsMap($record_id, $skils);
        $this->jsonResponse($data, JText::_('saved success'));
        return true;
    }

    /**
     * Method to delete a object
     *
     * Sets object ID in the session from the request and then deletes the object.
     *
     * @return	boolean	True if the record can be edited, false if not.
     */
    public function delete()
    {
        // Check for request forgeries
        (JSession::checkToken('get') OR JSession::checkToken()) OR die(JText::_('JINVALID_TOKEN'));

        $app = JFactory::getApplication();
        $context = "$this->option.delete.$this->context";
        $ids = $this->input->get('cid', array(), 'array');

        // Get the id of the group to edit.
        $id = (int) (empty($ids) ? $this->input->getInt('id') : array_pop($ids));

        // Access check
        if (!$this->allowDelete(array('id' => $id)))
        {
            JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));

            return false;
        }

        // Get the menu item model.
        $model = $this->getModel('item');

        // Check that this is not a new item.

        if ($id > 0)
        {

            $trash_state = -2;
            if ($model->publish($id, $trash_state))
            {
                $this->setMessage(JText::_('COM_WORK_ITEMS_DELETE_SUCCESS'));
            } else
            {
                $this->setMessage(JText::_('COM_WORK_ITEMS_DELETE_FAILED'));
            }
        }

        $this->setReturnPage($context);

        $this->setRedirect($this->getReturnPage($context));

        // Make sure return url is cleared
        $this->clearReturnPage($context);

        return true;
    }

    /**
     * Method to save a vote.
     *
     * @return	void
     * 
     */
    public function vote()
    {
        // Check for request forgeries.
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $user_rating = $this->input->getInt('user_rating', -1);

        if ($user_rating > -1)
        {
            $url = $this->input->getString('url', '');
            $id = $this->input->getInt('id', 0);
            $view_name = $this->input->getString('view', $this->default_view);
            $model = $this->getModel($view_name);

            if ($model->storeVote($id, $user_rating))
            {
                $this->setRedirect($url, JText::_('COM_WORK_ITEMS_VOTE_SUCCESS'));
            } else
            {
                $this->setRedirect($url, JText::_('COM_WORK_ITEMS_VOTE_FAILURE'));
            }
        }
    }

    /**
     * Function that allows child controller access to model data after the data has been saved.
     *
     * @param   JModelLegacy  $model  The data model object.
     * @param   array         $validData   The validated data.
     *
     * @return  void
     *
     */
    protected function postSaveHook(JModelLegacy $model, $valid_data = array())
    {
        return;
    }

    public function uploadImages()
    {
        $db = JFactory::getDbo();
        JLoader::import('joomla.filesystem.folder');
        $input = JFactory::getApplication()->input;
        $id = $input->getInt('id', 0);
        $file = $input->files->get('file');
        if (!empty($file))
        {
            JLoader::import('cms.helper.media');
            $mediaHelper = new JHelperMedia;
            $contentLength = (int) $_SERVER['CONTENT_LENGTH'];
            $postMaxSize = $mediaHelper->toBytes(ini_get('post_max_size'));
            $memoryLimit = $mediaHelper->toBytes(ini_get('memory_limit'));

            if (($postMaxSize > 0 && $contentLength > $postMaxSize) || ($memoryLimit != -1 && $contentLength > $memoryLimit))
            {

                $ret->name = $file['name'];
                $ret->size = $file['size'];
                $ret->error = JText::_('COM_MEDIA_ERROR_WARNUPLOADTOOLARGE');
                $this->jsonErrorResponse($ret, JText::_('COM_MEDIA_ERROR_WARNUPLOADTOOLARGE'));
            }
            $params = JComponentHelper::getParams('com_media');
            $uploadMaxSize = $params->get('upload_maxsize', 0) * 1024 * 1024;
            $uploadMaxFileSize = $mediaHelper->toBytes(ini_get('upload_max_filesize'));



            if (($img_info = getimagesize($file['tmp_name'])) === FALSE)
            {
                $this->jsonErrorResponse($ret, JText::_('Image not found or not an image'));
            }



            switch ($img_info[2])
            {
                case IMAGETYPE_GIF : $src = imagecreatefromgif($file['tmp_name']);
                    break;
                case IMAGETYPE_JPEG : $src = imagecreatefromjpeg($file['tmp_name']);
                    break;
                case IMAGETYPE_PNG : $src = imagecreatefrompng($file['tmp_name']);
                    break;
                default :$this->jsonErrorResponse($ret, JText::_('Unknown filetype'));
            }







            $file['name'] = md5($file['name'] . $id . rand(1, 1000000)) . '.jpg';
            $file['filepath'] = JPath::clean(JPATH_ROOT . DS . 'images' . DS . 'work' . DS . 'items' . DS . $id);

            if (($file['error'] == 1) || ($uploadMaxSize > 0 && $file['size'] > $uploadMaxSize) || ($uploadMaxFileSize > 0 && $file['size'] > $uploadMaxFileSize))
            {

                $ret->name = $file['name'];
                $ret->size = $file['size'];
                $ret->error = JText::_('COM_MEDIA_ERROR_WARNFILETOOLARGE');
                $this->jsonErrorResponse($ret, JText::_('COM_MEDIA_ERROR_WARNFILETOOLARGE'));
            }


            if (!isset($file['name']))
            {
                $ret->name = $file['name'];
                $ret->size = $file['size'];
                $ret->error = JText::_('COM_MEDIA_INVALID_REQUEST');
                $this->jsonErrorResponse($ret, JText::_('COM_MEDIA_INVALID_REQUEST'));
            }
            require_once JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_media' . DS . 'helpers' . DS . 'media.php';
            $err = null;
            if (!MediaHelper::canUpload($file, $err))
            {
                // The file can't be uploaded
                $this->jsonErrorResponse($ret, JText::_('The file cant be uploaded'));
                return false;
            }
            $object_file = new JObject($file);

            if (!JFolder::exists($file['filepath']))
            {
                JFolder::create($file['filepath'], '755');
            }


            if (!imagejpeg($src, $file['filepath'] . DS . $file['name']))
            {
                $ret->name = $object_file->name;
                $ret->size = $object_file->size;
                $ret->error = JText::_('COM_MEDIA_ERROR_UNABLE_TO_UPLOAD_FILE');
                $this->jsonErrorResponse($ret, JText::_('COM_MEDIA_ERROR_UNABLE_TO_UPLOAD_FILE'));
            }
            JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_work' . DS . 'tables');

            $table = JTable::getInstance('Galleries', 'WorkTable');
            $ret = array();
            $ret['name'] = 'image';
            $ret['src'] = 'images' . DS . 'work' . DS . 'items' . DS . $id . DS . $file['name'];
            $ret['item_id'] = $id;

            $table->bind($ret);
            $table->store();
                
            $this->jsonResponse(array($id), JText::_('uploaded file'));
        }
    }

}
