<?php
defined('_JEXEC') or die;
JLoader::import('wslib.media.multipic');
$wsPic = new wsMultiPic;
$wsPic->setCacheDir('pictures');
?>
<div class="compny-profile">

    <div class="blog-content pt60">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <article class="uou-block-7f blog-post-content">
                        <?php
                        if (!empty($this->item->youtubeLink))
                        {
                            ?>
                            <ng-youtube-embed  video="'<?php echo $this->item->youtubeLink; ?>'" width="100%">
                            </ng-youtube-embed>
                            <?php
                        } else
                        {
                            if (!empty($this->images) && !empty($this->images[0]))
                            {
                                $image = $wsPic->getImage('image16', $this->images[0]->src);
                                ?>
                                <img src="<?php echo $image; ?>" /> 
                                <?php
                            }
                        }
                        ?>
                        <div class="meta">
                            <span class="time-ago">
                                <time datetime="<?php echo JHtml::_('date', $this->item->created, 'c'); ?>" itemprop="dateCreated">
                                    <?php echo JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC2')); ?>
                                </time>
                            </span>
                            <span class="category"><?php echo JText::_('Posted in'); ?>: <a href="#"><?php echo $this->escape($this->item->category_title); ?></a></span>
                            <a href="#" class="comments">12 Comments</a>
                        </div>

                        <h1><a href="#"><?php echo $this->escape($this->item->name); ?></a></h1>
                        <div>
                            <?php echo $this->escape($this->item->description); ?>
                        </div>
                        <?php
                        if (!empty($this->categories))
                        {
                            ?>
                            <div class="listing-contenthh">
                                <h6 class="title-tags"><?php echo JText::_('JCategories'); ?>:</h6>
                                <ul class="tags list-inline">
                                    <?php
                                    foreach ($this->categories as $value)
                                    {
                                        ?>
                                        <li><a href="#"><?php echo $value->title; ?></a></li>
                                        <?php
                                    }
                                    ?>


                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        if (!empty($this->skils))
                        {
                            ?>
                            <div class="listing-contenthh">
                                <h6 class="title-tags"><?php echo JText::_('added skils'); ?>:</h6>
                                <ul class="tags list-inline">
                                    <?php
                                    foreach ($this->skils as $value)
                                    {
                                        ?>
                                        <li><a href="#"><?php echo $value->name; ?></a></li>
                                        <?php
                                    }
                                    ?>


                                </ul>
                            </div>
                            <?php
                        }
                        ?>
                        <?php
                        if (!empty($this->item->url))
                        {
                            ?>
                            <div>
                                <div class="listing-contenthh">
                                    <h6 class="title-tags"><?php echo JText::_('Url'); ?>:</h6>
                                    <a href="<?php echo $this->item->url; ?>" target="_blank">
                                        <i class="fa fa-globe"></i> <?php echo $this->item->url; ?>
                                    </a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        <?php
                        if (!empty($this->images))
                        {
                            ?>
                            <div id="portfolio" class="tab-pane fade active in">
                                <div class="profile-main">
                                    <h3><?php echo JText::_('images'); ?></h3>
                                    <div class="profile-in">
                                        <div class="uou-portfolio"> 
                                            <!-- Portfolio Item -->
                                            <section class="portfolio">
                                                <div class="portfolio-filters-content"> 
                                                    <?php
                                                    foreach ($this->images as $one)
                                                    {
                                                        $image = $wsPic->getImage('image17', $one->src);
                                                        if (empty($image))
                                                        {
                                                            continue;
                                                        }
                                                        ?>

                                                        <article class="development design">
                                                            <a href="<?php echo $one->src; ?>" class="swipebox">
                                                                <img src="<?php echo $image; ?>" alt="" class="work img-responsive"> 
                                                                <span class="overlay">
                                                                    <i class="fa fa-plus"></i>
                                                                </span>
                                                            </a>
                                                        </article>
                                                        <?php
                                                    }
                                                    ?>  
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>



                        <div class="uou-share-story clearfix">
                            <div class="row">
                                <div class="col-sm-3">
                                    <h5 class="sidebar-title">Share This Story</h5>

                                </div>
                                <div class="col-sm-9 ">
                                    <div class="social-widget">
                                        <div class="uou-block-4b">

                                            <ul class="social-icons">
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                                <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                                            </ul>

                                        </div> <!-- end .uou-block-4b -->
                                    </div> <!-- end social widget -->
                                </div>
                            </div>
                        </div>


                        <a class="btn btn-primary " href="#">Back To Blog List</a>

                    </article> <!-- end .uou-block-7f -->









                </div> <!-- end grid layout -->

                <div class="col-md-3">
                    <h5 class="sidebar-title"><?php echo JText::_('Popular Posts'); ?></h5>
                    <div class="latest-post-widget">
                        <div class="post-single">
                            <img src="http://new.uouapps.com/tiger-html/img/p-post-1.png" alt="">
                            <p class="meta">
                                <time datetime="<?php echo JHtml::_('date', $this->item->created, 'c'); ?>" itemprop="dateCreated">
                                    <?php echo JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC')); ?>
                                </time>
                            </p>

                            <h6 class="post-title"><a href="#">სახალხო დამცველი დე ფაქტო</a></h6>

                        </div>
                        <div class="post-single">
                            <img src="http://new.uouapps.com/tiger-html/img/p-post-1.png" alt="">
                            <p class="meta">
                                <time datetime="<?php echo JHtml::_('date', $this->item->created, 'c'); ?>" itemprop="dateCreated">
                                    <?php echo JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC')); ?>
                                </time>
                            </p>

                            <h6 class="post-title"><a href="#">სახალხო დამცველი დე ფაქტო</a></h6>

                        </div>
                        <div class="post-single">
                            <img src="http://new.uouapps.com/tiger-html/img/p-post-1.png" alt="">
                            <p class="meta">
                                <time datetime="<?php echo JHtml::_('date', $this->item->created, 'c'); ?>" itemprop="dateCreated">
                                    <?php echo JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC')); ?>
                                </time>
                            </p>

                            <h6 class="post-title"><a href="#">სახალხო დამცველი დე ფაქტო</a></h6>

                        </div>


                    </div>
                </div>

            </div> <!-- end row -->

        </div> <!-- edn cotainer -->

    </div>
</div>