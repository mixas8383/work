<?php
/**
 * @version 		$Id:$
 * @name			Work (Release 1.0.0)
 * @author			 ()
 * @package			com_work
 * @subpackage		com_work.site
 * @copyright		
 * @license			GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html 
 * 
 * The following Component Architect header section must remain in any distribution of this file
 *
 * @CAversion		Id: default.php 408 2014-10-19 18:31:00Z BrianWade $
 * @CAauthor		Component Architect (www.componentarchitect.com)
 * @CApackage		architectcomp
 * @CAsubpackage	architectcomp.site
 * @CAtemplate		joomla_3_3_standard (Release 1.0.3)
 * @CAcopyright		Copyright (c)2013 - 2014  Simply Open Source Ltd. (trading as Component Architect). All Rights Reserved
 * @Joomlacopyright Copyright (c)2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @CAlicense		GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
defined('_JEXEC') or die;

/*
 * 	Add style sheets, javascript and behaviours here in the layout so they can be overridden, if required, in a template override 
 */

// Add css files for the work component and categories if they exist
$this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_work.css');
$this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_items.css');

if ($lang->isRTL())
{
    $this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_work-rtl.css');
    $this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_items-rtl.css');
}

// Add Javscript functions for field display
JHtml::_('behavior.caption');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

/*
 * 	Initialise values for the layout 
 */

// Create some shortcuts.
$user = JFactory::getUser();
$n = count($this->items);
$list_order = $this->state->get('list.ordering');
$list_dirn = $this->state->get('list.direction');

$layout = $this->params->get('item_layout', 'default');

$can_create = $user->authorise('core.create', 'com_work');
// Get from global settings the text to use for an empty field
$component = JComponentHelper::getComponent('com_work');
$empty = $component->params->get('default_empty_field', '');

/*
 * 	Layout HTML
 */
?> 

<!-- Members -->
<section class="pro-mem">
    <div class="container pb30">
        <h3><?php echo JText::_('Professionals'); ?></h3>
        <div class="row">
            <?php
            if (!empty($this->items))
            {
                foreach ($this->items as $one)
                {
                    $image =  $one->getAvatar('image3');
                    if(empty($image))
                    {
                        $image = $one->getDefaultImage('image3');
                    }
                    
                    ?>
                    <div class="col-sm-3">
                        <div class="uou-block-6a"> <img src="<?php echo $image; ?>" alt="">
                            <h6>
                                <a href="<?php echo $one->getProfileLink(); ?>"><?php echo $one->getFullName(); ?></a> 
                                <span>Founder &amp; Web Designer</span>
                            </h6>
                            <p><i class="fa fa-map-marker"></i> New York, USA</p>
                        </div>
                        <!-- end .uou-block-6a --> 
                    </div>

                    <?php
                }
            }
            ?>

            
        </div>
    </div>
</section>