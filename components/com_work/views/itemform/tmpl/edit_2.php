<?php
/**
 * @version 		$Id:$
 * @name			Work (Release 1.0.0)
 * @author			 ()
 * @package			com_work
 * @subpackage		com_work.admin
 * @copyright		
 * @license			GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html 
 * 
 * The following Component Architect header section must remain in any distribution of this file
 *
 * @CAversion		Id: edit.php 408 2014-10-19 18:31:00Z BrianWade $
 * @CAauthor		Component Architect (www.componentarchitect.com)
 * @CApackage		architectcomp
 * @CAsubpackage	architectcomp.site
 * @CAtemplate		joomla_3_3_standard (Release 1.0.3)
 * @CAcopyright		Copyright (c)2013 - 2014  Simply Open Source Ltd. (trading as Component Architect). All Rights Reserved
 * @Joomlacopyright Copyright (c)2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @CAlicense		GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
defined('_JEXEC') or die;

/*
 * 	Add style sheets, javascript and behaviours here in the layout so they can be overridden, if required, in a template override 
 */

// Add css files for the work component and categories if they exist
//$this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_work.css');
//$this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_items.css');
//
//if ($lang->isRTL())
//{
//    $this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_work-rtl.css');
//    $this->document->addStyleSheet(JUri::root() . 'media/com_work/css/site_items-rtl.css');
//}
// Add Javscript functions for field display
//JHtml::_('behavior.tabstate');
//JHtml::_('behavior.keepalive');
//JHtml::_('behavior.calendar');
//JHtml::_('behavior.formvalidation');
//JHtml::_('formbehavior.chosen', 'select');
if ($this->params->get('save_history') AND $this->params->get('item_save_history'))
{
    JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
}
$this->document->addScript(JUri::root() . 'media/com_work/js/workvalidate.js');

$this->document->addScript(JUri::root() . 'media/com_work/js/formsubmitbutton.js');

JText::script('COM_WORK_ERROR_ON_FORM');

/*
 * 	Initialise values for the layout 
 */

// Create shortcut to parameters.
$params = $this->state->get('params');

/*
 * 	Layout HTML
 */
?>
<noscript>
<p style="color: red;"><?php echo JText::_('COM_WORK_WARNING_NOSCRIPT'); ?><p>
    </noscript>
<div class="compny-profile"> 
    <div class="profile-company-content user-profile " data-bg-color="f5f5f5">
        <div class="container">
            <div class="row"> 
                <div class="col-md-12">
                    <div class="work item-edit<?php echo $this->escape($params->get('pageclass_sfx')); ?>">
                        <?php if ($params->get('show_page_heading')) : ?>
                            <div class="page-header">
                                <h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
                            </div>
                        <?php endif; ?>
                        <?php if ($params->get('show_item_name')) : ?>
                            <div style="float: left;">
                                <h2>
                                    <?php
                                    if (!is_null($this->item->id)) :
                                        echo JText::sprintf('COM_WORK_EDIT_ITEM', $this->escape($this->item->name));
                                    else :
                                        echo JText::_('COM_WORK_ITEMS_CREATE_ITEM');
                                    endif;
                                    ?>
                                </h2>
                            </div>
                            <div style="clear:both;"></div>
                        <?php endif; ?>
                            <add-portfolio-component></add-portfolio-component>
                        <form action="<?php echo JRoute::_('index.php?option=com_work&view=itemform&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-primary" onclick="Joomla.submitbutton('item.save')">
                                        <span class="icon-ok"></span>&#160;<?php echo JText::_('JSAVE') ?>
                                    </button>
                                </div>
                                <div class="btn-group">
                                    <button type="button" class="btn" onclick="Joomla.submitbutton('item.cancel')">
                                        <span class="icon-cancel"></span>&#160;<?php echo JText::_('JCANCEL') ?>
                                    </button>
                                </div>

                            </div>		
                            <div style="clear:both;padding-top: 10px;"></div>

                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#basic-details" data-toggle="tab"><?php echo JText::_('COM_WORK_ITEMS_FIELDSET_DETAILS_LABEL'); ?></a></li>
                                <li>
                                    <a href="#imageslinks" role="tab" data-toggle="tab"><?php echo JText::_('COM_WORK_FIELDSET_IMAGES_URLS_LABEL'); ?></a>
                                </li>
                                <?php if ($this->item->params->get('access-change')): ?>
                                    <li>
                                        <a href="#publishing" role="tab" data-toggle="tab"><?php echo JText::_('COM_WORK_FIELDSET_PUBLISHING_LABEL'); ?></a>
                                    </li>
                                    <li>
                                        <a href="#metadata" role="tab" data-toggle="tab"><?php echo JText::_('COM_WORK_FIELDSET_METADATA_LABEL'); ?></a>
                                    </li>
                                    <li>
                                        <a href="#language" role="tab" data-toggle="tab"><?php echo JText::_('COM_WORK_FIELDSET_LANGUAGE_LABEL'); ?></a>
                                    </li>
                                <?php endif; ?>				
                            </ul>		


                            <div class="tab-content">
                                <div class="tab-pane active" id="basic-details">



                                    <div class="control-group">
                                        <div class="control-label">
                                            <label id="jform_alias-lbl" for="jform_name" class="hasPopover" title="ფსევდონიმი" data-content="You can enter here an alias. Leave this blank and a default value will be automatically generated from the name.  The alias must have no spaces and punctuation marks except '-' which is used to replace spaces and punctuation marks." data-placement="bottom">
                                                <?php echo JText::_('name'); ?>
                                            </label>
                                        </div>
                                        <div class="controls">
                                            <input type="text" name="jform[name]" id="jform_name" value="" class="inputbox" size="50" />
                                        </div>
                                    </div>  

                                    <?php
                                    if (!empty($this->skils))
                                    {
                                        ?>

                                        <div class="control-group">
                                            <div class="control-label">
                                                <label id="jform_alias-lbl" for="jform_name" class="hasPopover" title="  <?php echo JText::_('skils'); ?>" data-placement="bottom">
                                                    <?php echo JText::_('skils'); ?>
                                                </label>
                                            </div>
                                            <div class="controls">
                                                <?php echo $this->skils; ?>
                                            </div>
                                        </div>  
                                        <?php
                                    }




                                    if ($this->item->params->get('access-change')):
                                        ?>
                                        <?php echo $this->form->renderField('alias', null, null, array('group_id' => 'field_alias')); ?>
                                    <?php endif; ?>
                                    <?php echo $this->form->renderField('catid', null, null, array('group_id' => 'field_catid')); ?>
                                    <?php echo $this->form->renderField('tags', null, null, array('group_id' => 'field_tags')); ?>


                                </div>

                                <div class="tab-pane" id="imageslinks">
                                    images
                                </div>

                                <input type="hidden" name="task" value="" />
                                <input type="hidden" name="form_id" id="form_id" value="item-form" />
                                <input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
                                <?php if ($this->params->get('enable_category', 0) == 1) : ?>
                                    <input type="hidden" name="jform[catid]" value="<?php echo $this->params->get('catid', 1); ?>"/>
                                <?php endif; ?>
                                <?php echo JHtml::_('form.token'); ?>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>