<?php
/**
 * @version 		$Id:$
 * @name			Work (Release 1.0.0)
 * @author			 ()
 * @package			com_work
 * @subpackage		com_work.admin
 * @copyright		
 * @license			GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html 
 * 
 * The following Component Architect header section must remain in any distribution of this file
 *
 * @CAversion		Id: edit.php 408 2014-10-19 18:31:00Z BrianWade $
 * @CAauthor		Component Architect (www.componentarchitect.com)
 * @CApackage		architectcomp
 * @CAsubpackage	architectcomp.site
 * @CAtemplate		joomla_3_3_standard (Release 1.0.3)
 * @CAcopyright		Copyright (c)2013 - 2014  Simply Open Source Ltd. (trading as Component Architect). All Rights Reserved
 * @Joomlacopyright Copyright (c)2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @CAlicense		GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
defined('_JEXEC') or die;



$user = workFactory::getUser();
$portfolio = $user->getPortfolioItems();
JLoader::import('wslib.media.multipic');
$wsPic = new wsMultiPic();
$wsPic->setCacheDir('pictures');
JText::script('COM_WORK_ERROR_ON_FORM');
$doc = JFactory::getDocument();
$doc->addScriptDeclaration(''
        . ''
        . 'var workUserPortfolio =' . json_encode($portfolio) . ';');



/*
 * 	Initialise values for the layout 
 */

// Create shortcut to parameters.
$params = $this->state->get('params');

/*
 * 	Layout HTML
 */
?>
<noscript>
<p style="color: red;"><?php echo JText::_('COM_WORK_WARNING_NOSCRIPT'); ?><p>
    </noscript>
<div class="compny-profile"> 
    <div class="profile-company-content user-profile " data-bg-color="f5f5f5">
        <div class="container">
            <div class="row"> 
                <div class="col-md-4"> 
                    <!-- Company Information -->
                    <div class="sidebar">
                        <h5 class="main-title"><?php echo $user->get('name') . ' ' . $user->get('surname'); ?></h5>
                        <div class="sidebar-thumbnail">
                            <?php
                            $uploadItemid = wsHelper::getItemid('com_users', 'profile', 'edit');
                            ?>

                            <img class="avatar-image-tochange" src="<?php echo $wsPic->getImage('image20', $user->getAvatar()); ?>" alt="">
                            <div class="overlay"></div>


                            <div class="edit-button">
                                <a href="#" class="showUploadForm" data-url="<?php echo JRoute::_('index.php?option=com_users&task=profile.saveProfileImage&Itemid=' . $uploadItemid); ?>" >
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                </a>
                            </div>

                        </div>
                        <div class="uploadAvatarConteiner" style="display: none;">
                            <span class="btn btn-success fileinput-button">
                                <i class="fa fa-plus"></i>
                                <span><?php echo JText::_('add'); ?></span>
                            </span>
                        </div>






                        <div class="sidebar-information">
                            <ul class="single-category">

                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('jcategory'); ?></h6>
                                    <span class="subtitle col-xs-6">Automotive</span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('Location'); ?></h6>
                                    <span class="subtitle col-xs-6">California, USA</span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('NUMBER_OF_WORK'); ?></h6>
                                    <span class="subtitle col-xs-6">11,245</span> </li>

                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('Operating Hours'); ?></h6>
                                    <span class="subtitle col-xs-6">10:00 AM - 5:00 PM</span> </li>
                                <li class="row">
                                    <h6 class="title col-xs-6"><?php echo JText::_('Contacts'); ?></h6>
                                    <div class="col-xs-6"> <span class="subtitle">*****************<i class="fa fa-exclamation-circle"></i></span> <span class="subtitle">***************** <i class="fa fa-exclamation-circle"></i></span> <a href="#."><?php echo $user->get('email'); ?></a> <a href="#.">example.com</a> </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="work item-edit<?php echo $this->escape($params->get('pageclass_sfx')); ?>">
                        <?php if ($params->get('show_page_heading')) : ?>
                            <div class="page-header">
                                <h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
                            </div>
                        <?php endif; ?>
                        <?php if ($params->get('show_item_name')) : ?>
                            <div style="float: left;">
                                <h2>
                                    <?php
                                    if (!is_null($this->item->id)) :
                                        echo JText::sprintf('COM_WORK_EDIT_ITEM', $this->escape($this->item->name));
                                    else :
                                        echo JText::_('COM_WORK_ITEMS_CREATE_ITEM');
                                    endif;
                                    ?>
                                </h2>
                            </div>
                            <div style="clear:both;"></div>
                        <?php endif; ?>

                        <user-portfolio></user-portfolio>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>