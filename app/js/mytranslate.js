//https://angular-translate.github.io/docs/#/guide/
'use strict';
(function (angular) {

    var translations = {
        "are you sure you wont delete": 'ნამდვილად გსურთ წაშლა?',
        "ok": 'კი',
        "cancle": 'გაუქმება',
        "SAVE": 'შენახვა',
        "JCANCEL": 'გაუქმება',
        "DATA": 'მონაცემები',
        "MEDIA": 'მედია',
        "SKILS": 'უნარები',
        "IMAGES": 'სურათები',
        "NAME IS TOO SHORT": 'დასახელება არი მოკლე',
        "NAME IS TOO LONG": 'დასახელება არი დაშვებულზე გრძელი',
        "REQUIRED": 'სავალდებულოა',
        "FORM_NAME": 'დასახელება',
        "MINIMUM_SYMBOLS": 'სიმბოლოების მინიმალური რაოდენობა',
        "INSERT YOUTUBE LINK": 'შეიყვანეთ YouTube-ის ლინკი , მაგ. https://www.youtube.com/watch?v=ccxxccxx',
        "UPLOAD IMAGES": 'აირჩიეთ სურათები',
        "NAME": 'დასახელება',
        "DESCRIPTION": 'აღწერა',
        "URL": 'ვებ მისამართი',
        "CATEGORY": 'კატეგორია',
        "VIDEO LINK": 'ვიდეოს მისამართი',
        "SELECT ONE CATEGORY": 'აირჩიეთ მინიმუმ ერთი კატეგორია',
        "SIZE": 'ზომა',
        "PROGRESS": 'პროგრესი',
        "PROGRESS": 'პროგრესი',
        "REMOVE": 'წაშლა',
        "PERSONAL_INFO": 'პირადი ინფორმაცია',
        "PROFESSIONS AND SKILS": 'პროფესია და უნარები',
        "FIRSTNAME": 'სახელი',
        "LASTNAME": 'გვარი',
        "CHANGE_PASSWORD": 'პაროლის შეცვლა',
        "EDUCATION": 'განათლება',
        "EMPLOYMENT HISTORY": 'სამუშაო გამოცდილება',
        "ERROR": 'შეცდომა',
        "CANCEL": 'გაუქმება',
        "Proffesions": 'პროფესიები',
        "CANNOT FINDE ITEM": 'აირჩიეთ განათლება',
        "ARE YOU SURE": 'გაფრთხილება',
        "REALLY WHONT TO DELETE": 'ნამდვილად გსურთ წაშლა?',
        "January": 'იანვარი',
        "February": 'თებერვალი',
        "March": 'მარტი',
        "April": 'აპრილი',
        "May": 'მაისი',
        "June": 'ივნისი',
        "July": 'ივლისიი',
        "August": 'აგვისტო',
        "September": 'სექტემბერი',
        "October": 'ოქტომბერი',
        "November": 'ნოემბერი',
        "December": 'დეკემბერი',
        "SELECT YEAR": 'აირჩიეთ წელი',
        "YEAR": 'წელი',
        "SELECT MONTH": 'აირჩიეთ თვე',
        "MONTH": 'თვე',
        "LOCATION": 'ადგილმდებარეობა',
        "COMPANY": 'კომპანია',
        "TITLE": 'დასახელება',
        "PERIOD": 'პერიოდი',
        "CITY": 'ქალაქი',
        "COMPANY PLACEHOLDER": 'შეიყვანეთ კომპანიის დასახელება',
        "TITLE PLACEHOLDER": 'შეიყვანეთ  დასახელება',
        "DESCRIPTION PLACEHOLDER": 'შეიყვანეთ აღწერა',
        "CURRENRLY WORK HERE": 'დღემდე ვმუშაობ',
        "ADD EMPLOYMENT": 'დაამატეთ სამუშაო გამოცდილება',
        "IS REQUIRED": 'არი სავალდებულო',
        "INPUT": 'ველი',
        "IS TOO SHORT": 'არი მოკლე',
        "MINIMUM SYMBOLS": 'სიმბოლოების მინიმალური რაოდენობა',
        "PLEAS SELECT CORRECT DATE PERIOD": 'გთხოვთ სწორად აირჩიოთ პერიოდი',
        "CURRENTLY": 'დღემდე',
        "WORKING PERIOD": 'სამუშაო პერიოდი',
        "DEGREE": 'ხარისხი',
        "DEGREE PLACEHOLDER": 'შეიყვანეთ სამეცნიერო ხარისხი',
        "SCHOOL": 'სასწავლებელი',
        "SCOOL PLACEHOLDER": 'შეიყვანეტ სასწავლებელლის დასახელება',
        "ADD ADUCATION": 'განათლების დამატება',
        "DATES ATTENDED": 'სასწავლო წლები',
        "AREA OF STUDY": 'განყოფილება',
        "AREAOFSTUDY PLACEHOLDER": 'შეიყვანეტ ფაკულტეტი/განყოფილება',
        "START YEAR": 'დაწყების წელი',
        "END YEAR": 'დამთავრების წელი',
        HEADLINE: 'What an awesome module!', 
        PARAGRAPH: 'Srsly!',
        NAMESPACE: {
            PARAGRAPH: 'And it comes with awesome features!'
        }
    };

    var app = angular.module('wapp');
    app.config(['$translateProvider', function ($translateProvider) {
            // add translation table
            $translateProvider
                    .translations('ka', translations)
                    .preferredLanguage('ka');
            $translateProvider.useSanitizeValueStrategy('escape');
        }]);




})(angular);

