angular.module('selecttosearch', []);
angular.module('selecttosearch')
        .component('selectToSearch', {
            templateUrl: '/app/js/components/selecttosearch/default.html',
            require: {
                ngModelCtrl: "ngModel",

            },

            bindings: {
                stsPlaceholder: '@',
                stsId: '<?',
                stsValidationName: '@',
                stsClass: '<?',
                required: '<?',
                stsRequired: '<?',
                stsOptions: '=',
                stsOpen: '=',
                stsValue: '=?',
                ngModel: '=?',
                stsSelectedId: '=?',
                "stsSearch": '&'
            },
            controllerAs: 'vv',
            controller: ['$scope', 'myService', '$attrs', function ($scope, myService, $attrs) {

                    var self = this;
                    self.drEvents = new Array();
                    self.isOpenDropdown = false;
                    self.randId = Math.floor((Math.random() * 100000) + 1);
                    self.ngModel = [];
                    self.name = '';
                    self.extrasetings = {styleActive: true, enableSearch: true};
                    this.$onInit = function () {
                        //   console.log(self.smOptions);
                         self.required = (typeof $attrs.required !== 'undefined') ? true : false;
                         self.minlenth = (typeof $attrs.minlenth !== 'undefined') ? $attrs.minlenth : false;
                        
                        angular.element(document).bind('click', function (e) {
                            $scope.$apply();
                           
                            


                            if (!(angular.element('#' + self.randId).find(e.target).length > 0))
                            {
                                $scope.$apply(function () {
                                    self.isOpenDropdown = false;
                                    self.stsOpen = false;
                                });
                            }
                        });
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.dosome = function () {
                        console.log('do some')
                    };
                    self.throwEvent = function () {
                        self.smChange();
                    };
                    self.isOpen = function () {
                        return  self.isOpenDropdown || self.stsOpen;
                    };
                    self.callSearch = function () {
                        try {
                            self.stsSearch();
                        } catch (e) {
                            console.log(e)
                        }
                    };

                    self.selectItem = function (item) {
                        t = angular.copy(item.label);
                        console.log(item)
                        // console.log(ngModelCtrl).
                        //   self.ngModelCtrl.$setValidity("required",false);
                        console.log($attrs)
                        console.log(self.stsRequired)
                        self.stsId = item.id;
                        self.ngModel = t;
                        // self.removeItem(item);
                        // self.seModel.push(t);


                        self.isOpenDropdown = false;
                        self.stsOpen = false;

                    };
                    self.removeItem = function (item) {
                        var index = self.stsOptions.indexOf(item);
                        self.stsOptions.splice(index, 1);
                        self.throwEvent();
                    };
                    self.position = function ()
                    {
                        return {"top": 38, "left": 0};
                    }

                }]
        }
        );    