angular.module('education', []);
angular.module('education')
        .component('educationComponent', {
            templateUrl: '/app/js/components/education/default.html',
            controllerAs: 'vv',
            controller: ['myService', '$uibModal', '$translate', function (myService, $uibModal, $translate) {

                    var self = this;
                    self.items = [];
                    self.editItem = {};

                    this.$onInit = function () {
                        //   console.log(self.smOptions);
                        self.userEducation();
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.saveAndClose = function () {
                        self.userEducation();
                        self.popupOen.dismiss();
                    };

                    self.userEducation = function () {
                        myService.loadUserEducation().then(function (data) {
                            if (data.data.success)
                            {
                                self.items = data.data.data;
                            }
                        });

                    };

                    self.delete = function (item) {

                        if (typeof item === 'undefined' || typeof item.id === 'undefined' || item.id === '') {
                            $translate('CANNOT FINDE ITEM').then(function (translation) {
                                self.userInfoError = translation;
                            });
                            return;
                        }
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/js/components/education/del_popup.html',
                            controllerAs: 'modalm',
                            ariaLabelledBy: 'modal-title',
                            backdrop: false,
                            openedClass: 'ovh',
                            size: 'sm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    $translate('REALLY WHONT TO DELETE').then(function (translation) {
                                        modalm.text = translation;
                                    });

                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    modalm.deleteEducation = function () {

                                        myService.deleteEducation(item.id).then(function (data) {
                                            if (data.data.success)
                                            {
                                                self.userInfoMessage = data.data.message;
                                                self.userInfoError = '';

                                            } else {
                                                self.userInfoError = data.data.message;
                                                self.userInfoMessage = '';

                                            }
                                            self.userEducation();
                                            modalm.close();
                                        });
                                    };






                                }]
                        });


                    };


                    self.edit = function (item) {
                        var t;
                        if (typeof item === 'undefined')
                        {
                            t = {"start": "", "end": ""};
                        } else {
                            t = item;
                        }


                        self.popupOen = $uibModal.open({
                            template: '<education-edit-component edit-item="modalm.item" on-save="modalm.saveAndClose()" on-close="modalm.close()"></education-edit-component>',
                            controllerAs: 'modalm',
                            ariaLabelledBy: 'modal-title',
                            backdrop: false,
                            openedClass: 'ovh',
                            size: 'md',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.item = t;
                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    modalm.saveAndClose = function () {

                                        self.saveAndClose();
                                    };

                                    if (typeof (item) !== 'undefined')
                                    {

                                    }





                                }]
                        });
                    }
                    ;

                    self.throwEvent = function () {
                        self.smChange();
                    };
                }]
        }
        );    