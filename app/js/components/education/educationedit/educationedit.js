angular.module('educationEdit', []);
angular.module('educationEdit')
        .component('educationEditComponent', {
            templateUrl: '/app/js/components/education/educationedit/default.html',
            bindings: {

                "editItem": '<?',
                "onSave": '&',
                "onClose": '&',
            },
            controllerAs: 'vv',
            controller: ['myService', function (myService) {

                    var self = this;
                    self.drEvents = new Array();
                    self.years = {};
                    self.schools = [];
                    self.schoolOpen = false;

                    self.degree = [];
                    self.degreeOpen = false;

                    self.areaOfStudy = [];
                    self.areaOfStudyOpen = false;
                    self.blockForSave = false;
                    self.userInfoMessage = '';
                    self.userInfoError = '';

                    self.item = {};
                    self.item.start = '';
                    self.item.end = '';
                    self.ngModel = [];
                    self.extrasetings = {styleActive: true, enableSearch: true};
                    this.$onInit = function () {
                        //   console.log(self.smOptions);
                        if (typeof self.editItem !== 'undefined')
                        {
                            self.item = angular.copy(self.editItem);
                            if (self.item.start == '')
                            {
                                self.item.start = self.item.start;
                            } else
                            {
                                self.item.start = self.item.start * 1
                            }
                            
                            if (self.item.end == '')
                            {
                                self.item.end = self.item.end;
                            } else
                            {
                                self.item.end = self.item.end * 1
                            }
                            
                           
                        }
                        console.log(self.editItem)
                        for (var i = 2018; i > 1939; i--)
                        {
                            self.years[i] = i;
                        }
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.dosome = function () {
                        console.log('do some')
                    };
                    self.throwEvent = function () {
                        self.smChange();
                    };

                    self.drEvents["onSelectionChanged"] = self.throwEvent;

                    self.getLabel = function (item) {
                        var text = '';
                        if (self.smOptions.length)
                        {
                            angular.forEach(self.smOptions, function (a) {
                                if (item.id === a.id)
                                {
                                    text = a.label;
                                }
                            });

                        }
                        if (text === '')
                        {
                            self.removeItem(item);
                        }
                        return text;

                    };
                    self.removeItem = function (item) {
                        var index = self.ngModel.indexOf(item);
                        self.ngModel.splice(index, 1);
                        self.throwEvent();
                    };
                    self.findSchools = function ()
                    {
                        self.schools = [{"id": 1, "label": 'first school'}, {"id": 2, "label": 'second school'}]
                        self.schoolOpen = true;
                    };
                    self.findDegree = function ()
                    {
                        self.degree = [{"id": 1, "label": 'first degree'}, {"id": 2, "label": 'second degree'}]
                        self.degreeOpen = true;
                    };
                    self.findAreaOfStudy = function ()
                    {
                        self.areaOfStudy = [{"id": 1, "label": 'first area'}, {"id": 2, "label": 'second degree'}]
                        self.areaOfStudyOpen = true;
                    };
                    self.saveEducation = function ()
                    {
                        //  self.blockForSave = true;


                        myService.saveUserEducation(self.item).then(function (data) {

                            console.log(data);
                            if (data.data.success)
                            {
                                self.userInfoMessage = data.data.message;
                                self.userInfoError = '';
                                self.onSave();
                            } else {
                                self.userInfoError = data.data.message;
                                self.userInfoMessage = '';

                            }


                        });
                    };


                }]
        }
        );    