angular.module('mChat', []);
angular.module('mChat')
        .component('mChat', {
            templateUrl: '/app/js/components/mchat/default.html',
            controllerAs: 'vv',
            controller: ['$scope', 'chatService', '$uibModal', '$translate', function ($scope, chatService, $uibModal, $translate) {
                    var self = this;
                    self.chats = [];
                    self.chatPanel = [];
                    self.messages = [];
                    this.$onInit = function () {
                        self.loadChats();
                        self.comet = chatService.getComet().then(function (data) {
                            self.comet = data;
                            self.comet.subscription("msg.newMessage", function (event) {
                                var k = {};
                                k.userId = event.data.user_id;
                                k.messages = [];
                                k.messages.push(event.data);
                                mindex = self.chats.findIndex(function (el) {
                                    return el.user == k.userId;
                                });
                                if (mindex !== -1)
                                {
                                    $scope.$apply(function () {
                                        self.chats[mindex].messages.push(k);
                                    });
                                }
                                // self.messages.push(event.data);
                            })
                        });
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.loadChats = function () {
                        chatService.getChatData().then(function (data) {
                            self.chats = data.chats;
                            self.me = data.me;
                        });
                    };
                    self.addToPanel = function (item)
                    {
                        if (self.chatPanel.indexOf(item) === -1)
                        {
                            item.showWindow = true;
                            self.chatPanel.push(item);
                            self.loadMessages(item, 0);
                        }
                    };
                    self.loadMessages = function (chat, limitstart) {
                        chatService.loadMessages(chat.id, limitstart).then(function (data) {

                            chat.messages = data;
                        });
                    };
                    self.sendMessage = function (chat)
                    {
                        chat.name = 'asasas';
                        console.log(chat);
                        chatService.sendMessage(chat.id, chat.currentMessage).then(function (data) {
                            if (data.data.success)
                            {
                                var k = {};
                                k.userId = self.me.id;
                                k.messages = [];
                                tmsg = {message: chat.currentMessage, type: 0, user_id: k.userId};
                                k.messages.push(tmsg);
                                mindex = self.chats.indexOf(chat);
                                console.log(self.chats.indexOf(chat))
                                if (mindex !== -1)
                                {
                                    self.chats[mindex].messages.push(k);
                                    chat.currentMessage = '';
                                }
                            }
                        });
                    };




                }]
        });    