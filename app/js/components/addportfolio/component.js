angular.module('addPortfolioComponent', []);
angular.module('addPortfolioComponent')
        .component('addPortfolioComponent', {
            templateUrl: '/app/js/components/addportfolio/default.html',
            bindings: {
                "editItem": '<?',
                "onSave": '&',
                "onClose": '&',
            },
            controllerAs: 'vv',
            controller: ['myService', 'FileUploader', '$window', '$uibModal', '$translate', function (myService, FileUploader, $window, $uibModal, $translate) {

                    var self = this;
                    self.formData = {};
                    self.formData.categories = [];
                    self.formData.skils = [];
                    self.formData.youtubeLink = null;
                    self._avalableSkillsSource = [];
                    self.avalabelSkils = [];
                    self.avalabelCategories = [];
                    self.canLoadCategorySkills = true;
                    self.uploader = new FileUploader({
                        url: 'index.php?option=com_work&task=item.uploadImages&id=' + self.formData.id
                    });


                    this.$onInit = function () {
                        if (typeof self.editItem !== 'undefined')
                        {
                            self.formData = angular.copy(self.editItem);



                        }
                        self.getAvalableSkils();
                        self.getAvalableCategories()
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.onButtonClick = function () {
                        return false;
                    };

                    self.getAvalableCategories = function () {
                        myService.getCategories().then(function (data) {
                            self.avalabelCategories = data;
                        });
                    };

                    self.getAvalableSkils = function () {
                        myService.getUserSkils().then(function (data) {
                            
                            console.log(data)
                            self.avalabelSkils = self.avalabelSkils.concat(data);
                            self._avalableSkillsSource = angular.copy(data);


                        });
                    };
                    self.reinitSkils = function () {
                        if (self.canLoadCategorySkills && self.formData.categories.length > 0)
                        {
                            self.canLoadCategorySkills = false;
                            myService.getCategorySkils(self.formData.categories).then(function (data) {
                                self.avalabelSkils = self._avalableSkillsSource.concat(data.data);
                                self.canLoadCategorySkills = true;
                            });

                        } else {
                            self.avalabelSkils = self._avalableSkillsSource
                        }

                        return false;

                    };
                    self.submitForm = function () {



                        myService.saveItem(self.formData).then(function (data) {
                            if (data.data.success)
                            {
                                console.log('saved')
                                self.formData.id = data.data.data.itemId;
                                self.uploader.url = 'index.php?option=com_work&task=item.uploadImages&id=' + data.data.data.itemId;
                                console.log(self.uploader)
                                if (self.uploader.queue.length)
                                {
                                    for (i = 0; i < self.uploader.queue.length; i++)
                                    {
                                        self.uploader.queue[i].url = 'index.php?option=com_work&task=item.uploadImages&id=' + data.data.data.itemId;
                                    }
                                    self.uploader.onCompleteAll = function () {
                                        var link = myService.getSuccessLink();
                                        $window.location.href = link + self.formData.id;
                                    };
                                    self.uploader.uploadAll();
                                } else {
                                    var link = myService.getSuccessLink();


                                    $window.location.href = link + self.formData.id;
                                }

                            } else {
                                console.log('error')
                            }
                        });


                        console.log(self.formData)
                        console.log('submit form ');
                        return false;
                    };
                    self.deleteItemImage = function (item) {

                        if (typeof item === 'undefined' || typeof item.id === 'undefined' || item.id === '') {
                            $translate('CANNOT FINDE ITEM').then(function (translation) {
                                self.userInfoError = translation;
                            });
                            return;
                        }
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/js/components/userportfolio/del_popup.html',
                            controllerAs: 'modalm',
                            ariaLabelledBy: 'modal-title',
                            backdrop: false,
                            openedClass: 'ovh',
                            size: 'sm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    $translate('REALLY WHONT TO DELETE').then(function (translation) {
                                        modalm.text = translation;
                                    });
                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    modalm.deleteEducation = function () {
                                        myService.deletePortfolioImage(item.id).then(function (data) {
                                            if (data.data.success)
                                            {
                                                self.userInfoMessage = data.data.message;
                                                self.userInfoError = '';
                                                index = self.formData.images.indexOf(item);
                                                delete(self.formData.images[index]);
                                            } else {
                                                self.userInfoError = data.data.message;
                                                self.userInfoMessage = '';
                                            }
                                                modalm.close();
                                        });
                                    };
                                }]
                        });
                    };
                }]
        });   