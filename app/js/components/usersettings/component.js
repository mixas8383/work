angular.module('userSettings', []);
angular.module('userSettings')
        .component('userSettingsComponent', {
            templateUrl: '/app/js/components/usersettings/default.html',
            controllerAs: 'vv',
            controller: ['myService', function (myService) {
                    var self = this;
                    self.skils = [];
                    self.skilsBackup = [];
                    self.userSkils = [];
                    self.userProffesions = [];
                    self.disabledSaveInfoButton = false;
                    self.disabledSaveSkilsButton = false;
                    self.userInfoMessage = null;
                    self.userInfoError = null;
                    self.proffesions = [];
                    self.skilSearchPhrase = '';
                    self.skilDropdownOpen = false;
                    self.proffesionSearchPhrase = '';
                    self.proffesionDropdownOpen = false;
                    self.user = {};

                    self.selectedSkils = {};
                    self.selectedProffesions = {};

                    this.$onInit = function () {
                        //   init accordion
                        jQuery('.uou-accordions').uouAccordions();
                        myService.getUser().then(function (data) {
                            self.user = data;
                        });
                        myService.getUserSkils().then(function (data) {
                            self.userSkils = data;
                        });
                        myService.getUserProffesions().then(function (data) {
                            self.userProffesions = data;
                        });
                        myService.getPopularSkils().then(function (data) {
                            self.skils = data;
                            self.skilsBackup = data;
                        });
                        myService.getPopularProffesions().then(function (data) {
                            self.proffesions = data;
                            self.proffesionsBackup = data;
                        });


                    };
                    self.onSkillSearch = function (e) {
                        if (self.skilSearchPhrase === '')
                        {
                            self.skils = angular.copy(self.skilsBackup);
                        }

                        myService.findSkill(self.skilSearchPhrase).then(function (resonse) {
                            if (resonse.data.success)
                            {

                                self.skils = resonse.data.data.items;
                                self.skilDropdownOpen = true;
                            }
                        });


                    };
                    self.onProffesionSearch = function () {
                        if (self.proffesionSearchPhrase === '')
                        {
                            self.proffesions = angular.copy(self.proffesionsBackup);
                        }

                        myService.findProffesions(self.proffesionSearchPhrase).then(function (resonse) {
                            if (resonse.data.success)
                            {

                                self.proffesions = resonse.data.data.items;
                                self.proffesionDropdownOpen = true;
                            }
                        });


                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.savePersonalInfo = function () {
                        self.disabledSaveInfoButton = true;
                        self.userInfoMessage = null;
                        self.userInfoError = null;
                        myService.savePersonalInfo(self.user).then(function (data) {
                            if (data.data.success)
                            {
                                self.userInfoMessage = data.data.message;
                            } else {
                                self.userInfoError = data.data.message;
                            }
                            self.disabledSaveInfoButton = false;
                        });
                    };
                    self.saveSkils = function () {
                        self.disabledSaveSkilsButton = true;
                        self.skilsMessage = null;
                        self.skilsError = null;
                        myService.saveSkilsProffesions({'skils': self.selectedSkils, 'proffesions': self.selectedProffesions}).then(function (data) {
                            if (data.data.success)
                            {
                                self.skilsMessage = data.data.message;
                            } else {
                                self.skilsError = data.data.message;
                            }
                            self.disabledSaveSkilsButton = false;
                        });
                    };


                }]
        }
        );    