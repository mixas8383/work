angular.module('skilsEditor', []);
angular.module('skilsEditor')
        .component('skilsEditorComponent', {
            templateUrl: '/app/js/components/skilseditor/default.html',
            require: {
                //  ngModelCtrl: "ngModel",

            },

            bindings: {
                "mePlaceholder": '@',
                "seOptions": '=',
                "seDefaults": '=',
                "seModel": '=?seModel',
                "smSearchPhrase": '=?smSearchPhrase',
                "smChange": '&',
                "smSearch": '&',
                "smOpen": "=?"
            },
            controllerAs: 'vv',
            controller: ['$scope', 'myService', '$q', function ($scope, myService, $q) {

                    var self = this;
                    self.randId = Math.floor((Math.random() * 100000) + 1);
                    self.drEvents = new Array();
                    self.useTypeahead = false;
                    self.isOpenDropdown = false;
                    self.hoveredIndex = 0;
                    self.searchPhrase = '';
                    self.extrasetings = {styleActive: true, enableSearch: true};
                    this.$onInit = function () {
                        if (typeof self.smSearchPhrase === 'undefined')
                        {
                            try {
                                self.smSearchPhrase = '';
                            } catch (e) {
                                console.log(e)
                            }


                        }
                        angular.element(document).bind('click', function (e) {
                            $scope.$apply();
                            if (!(angular.element('#' + self.randId).find(e.target).length > 0))
                            {
                                $scope.$apply(function () {
                                    self.isOpenDropdown = false;
                                    self.smOpen = false;
                                });
                            }
                        });
                        if (typeof (self.seDefaults))
                        {
                            self.seModel = [];
                        } else {
                            self.seModel = self.seDefaults;
                        }
                    };
                    self.callSearch = function () {

                        try {
                            self.smSearch();
                        } catch (e) {
                            console.log(e)
                        }


                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.dosome = function () {
                        console.log('do some');
                    };
                    self.throwEvent = function () {
                        self.smChange();
                    };

                    self.drEvents["onSelectionChanged"] = self.throwEvent;

                    self.getLabel = function (item) {
                        var text = '';
                        if (self.smOptions.length)
                        {
                            angular.forEach(self.smOptions, function (a) {
                                if (item.id === a.id)
                                {
                                    text = a.label;
                                }
                            });

                        }
                        if (text === '')
                        {
                            self.removeItem(item);
                        }
                        return text;

                    };
                    self.isOpen = function () {
                        return  self.isOpenDropdown || self.smOpen;
                    };
                    self.isOpenDropdownErrors = function () {
                        return false;
                    };

                    self.onmyBlur = function () {

                        // self.isOpenDropdown = false;
                    };

                    self.startByClicking = function () {
                        self.isOpenDropdown = true;
                    };
                    self.removeItem = function (item) {
                        var index = self.seOptions.indexOf(item);
                        self.seOptions.splice(index, 1);
                        self.throwEvent();
                    };
                    self.focusOnInput = function ()
                    {
                        self.useTypeahead = true;
                    };
                    self.selectItem = function (item) {
                        t = angular.copy(item);
                        self.removeItem(item);
                        self.seModel.push(t);
                        self.isOpenDropdown = false;
                        self.smOpen = false;
                    };
                    self.removeFromSelected = function (item) {
                        t = angular.copy(item);
                        var index = self.seModel.indexOf(item);
                        self.seModel.splice(index, 1);
                        self.seOptions.push(t);
                    };
                    self.selectActive = function (index, event) {
                        self.hoveredIndex = index;
                    };
                    self.isActive = function (index)
                    {
                        return index === self.hoveredIndex ? true : false;
                    };

                }]
        }
        );    