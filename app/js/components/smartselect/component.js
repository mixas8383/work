angular.module('smartSelect', []);
angular.module('smartSelect')
        .component('smartSelectComponent', {
            templateUrl: '/app/js/components/smartselect/default.html',
            require: {
                ngModelCtrl: "ngModel",

            },

            bindings: {
                myPlaceholder: '@',
                ngModel: '=',
                smOptions: '=',
                "smChange": '&'
            },
            controllerAs: 'vv',
            controller: ['myService', function (myService) {

                    var self = this;
                    self.drEvents = new Array();

                    self.ngModel = [];
                    self.extrasetings = {styleActive: true, enableSearch: true};
                    this.$onInit = function () {
                        //   console.log(self.smOptions);
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.dosome = function () {
                        console.log('do some')
                    };
                    self.throwEvent = function () {
                        self.smChange();
                    };

                    self.drEvents["onSelectionChanged"] = self.throwEvent;

                    self.getLabel = function (item) {
                        var text = '';
                        if (self.smOptions.length)
                        {
                            angular.forEach(self.smOptions, function (a) {
                                if (item.id === a.id)
                                {
                                    text = a.label;
                                }
                            });

                        }
                        if (text === '')
                        {
                            self.removeItem(item);
                        }
                        return text;

                    };
                    self.removeItem = function (item) {
                        var index = self.ngModel.indexOf(item);
                        self.ngModel.splice(index, 1);
                         self.throwEvent();
                    }
                }]
        }
        );    