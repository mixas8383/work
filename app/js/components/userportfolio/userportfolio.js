angular.module('userPortfolio', []);
angular.module('userPortfolio')
        .component('userPortfolio', {
            templateUrl: '/app/js/components/userportfolio/default.html',
            controllerAs: 'vv',
            controller: ['myService', '$uibModal', '$translate', function (myService, $uibModal, $translate) {

                    var self = this;
                    self.items = [];
                    self.editItem = {}; 
                    self.portfolios=[];

                    this.$onInit = function () {
                        //   console.log(self.smOptions);
                        self.userEducation();
                        
                        
                        
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.saveAndClose = function () {
                        self.userEducation();
                        self.popupOen.dismiss();
                    };

                    self.userEducation = function () {
                        myService.getUserPortfolio().then(function (data) {
                            if (data)
                            {
                                self.portfolios = data;
                            }
                        });

                    };

                    self.delete = function (item) {

                        if (typeof item === 'undefined' || typeof item.id === 'undefined' || item.id === '') {
                            $translate('CANNOT FINDE ITEM').then(function (translation) {
                                self.userInfoError = translation;
                            });
                            return;
                        }
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/js/components/userportfolio/del_popup.html',
                            controllerAs: 'modalm',
                            ariaLabelledBy: 'modal-title',
                            backdrop: false,
                            openedClass: 'ovh',
                            size: 'sm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    $translate('REALLY WHONT TO DELETE').then(function (translation) {
                                        modalm.text = translation;
                                    });

                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    modalm.deleteEducation = function () {
                                        myService.deletePortfolio(item.id).then(function (data) {
                                            if (data.data.success)
                                            {
                                                self.userInfoMessage = data.data.message;
                                                self.userInfoError = '';

                                            } else {
                                                self.userInfoError = data.data.message;
                                                self.userInfoMessage = '';

                                            }
                                            self.userEducation();
                                            modalm.close();
                                        });
                                    };






                                }]
                        });


                    };
                    self.deleteItemImage = function (item) {

                        if (typeof item === 'undefined' || typeof item.id === 'undefined' || item.id === '') {
                            $translate('CANNOT FINDE ITEM').then(function (translation) {
                                self.userInfoError = translation;
                            });
                            return;
                        }
                        self.popupOen = $uibModal.open({
                            templateUrl: 'app/js/components/userportfolio/del_popup.html',
                            controllerAs: 'modalm',
                            ariaLabelledBy: 'modal-title',
                            backdrop: false,
                            openedClass: 'ovh',
                            size: 'sm',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    $translate('REALLY WHONT TO DELETE').then(function (translation) {
                                        modalm.text = translation;
                                    });

                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    modalm.deleteEducation = function () {
                                        myService.deletePortfolio(item.id).then(function (data) {
                                            if (data.data.success)
                                            {
                                                self.userInfoMessage = data.data.message;
                                                self.userInfoError = '';

                                            } else {
                                                self.userInfoError = data.data.message;
                                                self.userInfoMessage = '';

                                            }
                                            self.userEducation();
                                            modalm.close();
                                        });
                                    };






                                }]
                        });


                    };

                    self.edit = function (item) {
                       
                       // console.log(item)
                        if (typeof item === 'undefined')
                        {
                            t = {"start": '', "end": ''};
                        } else {
                            t = item;
                        }
                      

                        self.popupOen = $uibModal.open({
                            template: '<add-portfolio-component edit-item="modalm.item" on-save="modalm.saveAndClose()" on-close="modalm.close()"></add-portfolio-component>',
                            controllerAs: 'modalm',
                            ariaLabelledBy: 'modal-title',
                            backdrop: false,
                            openedClass: 'ovh',
                            size: 'md',
                            controller: ['$http', '$uibModal', function ($http, $uibModal) {
                                    var modalm = this;
                                    modalm.item = t;
                                  
                                    modalm.close = function () {
                                        self.closePopup();
                                    };
                                    modalm.saveAndClose = function () {

                                        self.saveAndClose();
                                    };

                                    if (typeof (item) !== 'undefined')
                                    {

                                    }





                                }]
                        });
                    }
                    ;

                    self.throwEvent = function () {
                        self.smChange();
                    };
                }]
        }
        );    