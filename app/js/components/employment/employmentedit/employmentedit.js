angular.module('employmentEdit', []);
angular.module('employmentEdit')
        .component('employmentEditComponent', {
            templateUrl: '/app/js/components/employment/employmentedit/default.html',
            bindings: {

                "editItem": '<?',
                "onSave": '&',
                "onClose": '&',
            },
            controllerAs: 'vv',
            controller: ['myService', function (myService) {
                    var self = this;
                    self.years = {};
                    self.months = {};
                    self.companies = [];
                    self.companyOpen = false;
                    self.blockForSave = false;
                    self.userInfoMessage = '';
                    self.userInfoError = '';
                    self.item = {};
                    self.item.country_id = 78;
                    self.item.end_year = '';
                    self.item.end_month = '';
                    self.item.start_year = '';
                    self.item.start_month = '';
                    self.ngModel = [];
                    self.extrasetings = {styleActive: true, enableSearch: true};
                    this.$onInit = function () {
                        if (typeof self.editItem !== 'undefined')
                        {

                            self.item = angular.copy(self.editItem);
                            self.item.start_month = ((typeof self.item.start_month === 'undefined') || self.item.start_month === '') ? '' : self.item.start_month * 1;
                            self.item.end_month = ((typeof self.item.end_month === 'undefined') || self.item.end_month === '') ? '' : self.item.end_month * 1;
                            self.item.start_year = ((typeof self.item.start_year === 'undefined') || self.item.start_year === '') ? '' : self.item.start_year * 1;
                            self.item.end_year = ((typeof self.item.end_year === 'undefined') || self.item.end_year === '') ? '' : self.item.end_year * 1;
                            self.item.country_id = ((typeof self.item.country_id === 'undefined') || self.item.country_id === '') ? 78 : self.item.country_id * 1;

                            self.item.currently = self.item.currently * 1;
                            self.item.currently = (self.item.currently === 0) ? false : true;
                        }


                        for (var i = 2018; i > 1939; i--)
                        {
                            self.years['' + i + ''] = i;
                        }
                        myService.getCountries().then(function (data) {
                            self.countries = data.data;
                        });
                        myService.getMonthList().then(function (data) {
                            self.months = data;
                        });
                    };
                    self.isYearValid = function () {
                        if (self.item.start_year == '' && self.item.end_year == '')
                        {
                            return true;
                        }
                        if (!self.item.currently)
                        {
                            if ((self.item.start_year * 1) > (self.item.end_year * 1))
                            {
                                return false;
                            } else
                            if ((self.item.start_year * 1) == (self.item.end_year * 1))
                            {
                                if ((self.item.start_month * 1) > (self.item.end_month * 1))
                                {
                                    return false;
                                }
                            }
                        }
                        return true;
                    };

                    self.findCompany = function ()
                    {
                        self.companies = [{"id": 1, "label": 'first school'}, {"id": 2, "label": 'second school'}];
                        self.companyOpen = true;
                    };

                    self.saveEducation = function ()
                    {
                        myService.saveUserEmployment(self.item).then(function (data) {
                            console.log(data);
                            if (data.data.success)
                            {
                                self.userInfoMessage = data.data.message;
                                self.userInfoError = '';
                                self.onSave();
                            } else {
                                self.userInfoError = data.data.message;
                                self.userInfoMessage = '';
                            }
                        });
                    };
                }]
        }
        );    