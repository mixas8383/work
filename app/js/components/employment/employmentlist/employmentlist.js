angular.module('employmentList', []);
angular.module('employmentList')
        .component('employmentListComponent', {
            templateUrl: '/app/js/components/employment/employmentlist/default.html',
            bindings: {
                edItems: '<?',
                onEdit: '&',
                onDelete: '&',
                editItem: '=?'
            },
            controllerAs: 'vv',
            controller: ['myService', function (myService) {

                    var self = this;
                    self.drEvents = new Array();

                    self.ngModel = [];
                    self.extrasetings = {styleActive: true, enableSearch: true};
                    this.$onInit = function () {
                        //   console.log(self.smOptions);
                    };
                    self.closePopup = function () {
                        self.popupOen.dismiss();
                    };
                    self.dosome = function () {
                        console.log('do some')
                    };
                    self.throwEvent = function () {
                        self.smChange();
                    };

                    self.drEvents["onSelectionChanged"] = self.throwEvent;

                    self.edit = function (data) {
                        //console.log(data)
                        self.editItem = data;
                        self.onEdit({item: data});
                    };
                    self.delete = function (data) {
                    
                        self.onDelete({item: data});
                    };

                    self.removeItem = function (item) {
                        var index = self.ngModel.indexOf(item);
                        self.ngModel.splice(index, 1);
                        self.throwEvent();
                    };
                    self.getMonthName = function (month) {
                       return monthNumToName(month);
                    };
                }]
        }
        );    