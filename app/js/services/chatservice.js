/* global workItemCategory, chatUserHash, chatUserHash, chatUserHash, cometApi */


(function () {


    angular.module('wapp').factory('chatService', chatService);

    chatService.$inject = ['$http', '$q'];

    function chatService($http, $q) {
        var self = this;
        self.loadedItem = {};
        self.loadedItemForm = {};

        return {

            saveItem: function (data)
            {

                return $http.post('/index.php?option=com_work&task=item.save', {"jform": data})
                        .then(function (data) {
                            return data;
                        });

            },
            sendMessage: function (chatId, message)
            {

                return $http.post('/index.php?option=com_users&task=chat.sendMessage', {"id": chatId, 'message': message})
                        .then(function (data) {
                            return data;
                        });

            },

            getChatData: function (data)
            {
                return  $http.get('/index.php?option=com_users&task=chat.getData').then(function (data) {
                    if (data.status === 200)
                    {
                        if (data.data.success)
                        {
                            return data.data.data;
                        }
                    }
                    return false;
                });


            },
            loadMessages: function (chatId, limitstart)
            {
                if (typeof limitstart === 'undefined')
                {
                    limitstart = 0;
                }
                return  $http.post('/index.php?option=com_users&task=chat.getMessages', {"chatId": chatId, "limitstart": limitstart}).then(function (data) {
                    if (data.status === 200)
                    {
                        if (data.data.success)
                        {
                            return data.data.data;
                        }
                    }
                    return false;
                });


            },
            getComet: function () {
                console.log(cometApi)
                if (typeof cometApi !== 'undefined')

                {
                    console.log(chatUserId)
                    console.log(chatUserHash)
                    cometApi.start({dev_id: 0, user_id: chatUserId, user_key: chatUserHash, node: 'comet.com:8087'})

                    return $q.when(cometApi);
                }
                return $q.when(false)

            },

            getCategories: function () {
                if (typeof workItemCategory !== 'undefined')
                {
                    return $q.when(workItemCategory);
                }
                return $q.when([]);
            },

        };
    }
}());
