(function () {


    angular.module('wapp').factory('myService', itemsService);

    itemsService.$inject = ['$http', '$q'];

    function itemsService($http, $q) {
        var self = this;
        self.loadedItem = {};
        self.loadedItemForm = {};

        return {
 
            saveItem: function (data)
            {

                return $http.post('/index.php?option=com_work&task=item.save', {"jform": data})
                        .then(function (data) {
                            return data;
                        });

            },
            findSkill: function (data)
            {
                return $http.post('/index.php?option=com_work&task=skils.findSkill', {"searchword": data})
                        .then(function (data) {
                            return data;
                        });

            },
            findProffesions: function (data)
            {
                return $http.post('/index.php?option=com_work&task=proffesions.findProffesion', {"searchword": data})
                        .then(function (data) {
                            return data;
                        });

            },
            getFormData: function (data)
            {
                var nm = 'item_' + id;
                if (!self.loadedItemForm[nm]) {
                    return  $http.get('/index.php?option=com_mymenu&task=getFormData&id=' + id).then(function (data) {
                        self.loadedItemForm[nm] = data;
                        return self.loadedItemForm[nm];
                    });
                }
                return $q.when(self.loadedItemForm[nm]);
            },
            getEducationItems: function () {
                return [];
            },
            getCategories: function () {

                if (typeof workItemCategory !== 'undefined')
                {
                    return $q.when(workItemCategory);
                }
                return $q.when([]);
            },
            getUserSkils: function () {


                if (typeof workUserSkils !== 'undefined')
                {
                    return $q.when(workUserSkils);
                }
                return $q.when([]);
            },
            getUserPortfolio: function () { 
                if (typeof workUserPortfolio !== 'undefined')
                {
                  
                    return $q.when(workUserPortfolio);
                }
                return $q.when([]);
            },
            
            
            
            getCountries: function () {

               return  $http.get('/app/js/countries.json').then(function (data) {
                        
                        return data;
                    });
            },
            getMonthList:function(){
                return $q.when(M.months);
            },
            getUserProffesions: function () {

                if (typeof workUserProffesions !== 'undefined')
                {
                    return $q.when(workUserProffesions);
                }
                return $q.when([]);
            },
            getUser: function () {
                if (typeof workUser !== 'undefined')
                {
                    return $q.when(workUser);
                }
                return $q.when([]);
            },
            savePersonalInfo: function (data) {
                return $http.post('/index.php?option=com_users&task=profile.savePersonalInfo', {"jform": data})
                        .then(function (data) {
                            return data;
                        });

            },
            saveSkilsProffesions: function (data) {
                return $http.post('/index.php?option=com_users&task=profile.saveSkilsAndProffesions', data)
                        .then(function (response) {
                            return response;
                        });

            },
            saveUserEducation: function (data) {
                return $http.post('/index.php?option=com_users&task=profile.saveUserEducation', data)
                        .then(function (response) {
                            return response;
                        });

            },
            saveUserEmployment: function (data) {
                return $http.post('/index.php?option=com_users&task=profile.saveUserEmployment', data)
                        .then(function (response) {
                            return response;
                        });

            },
            loadUserEducation: function () {
                var data = {};
                return $http.post('/index.php?option=com_users&task=profile.getUserEducations', data)
                        .then(function (response) {
                            return response;
                        });
            },
            loadUserEmployment: function () {
                var data = {};
                return $http.post('/index.php?option=com_users&task=profile.getUserEmployments', data)
                        .then(function (response) {
                            return response;
                        });
            },
            deleteEducation: function (id) {
                var data = {id: id};
                return $http.post('/index.php?option=com_users&task=profile.deleteUserEducation', data)
                        .then(function (response) {
                            return response;
                        });
            },
            deleteEmployment: function (id) {
                var data = {id: id};
                return $http.post('/index.php?option=com_users&task=profile.deleteUserEmployment', data)
                        .then(function (response) {
                            return response;
                        });
            },
            deletePortfolio: function (id) {
                var data = {id: id};
                return $http.post('/index.php?option=com_users&task=profile.deleteUserPortfolio', data)
                        .then(function (response) {
                            return response;
                        });
            },
            deletePortfolioImage: function (id) {
                var data = {id: id};
                return $http.post('/index.php?option=com_users&task=profile.deleteUserPortfolioImage', data)
                        .then(function (response) {
                            return response;
                        });
            },

            getItemSkils: function () {

                if (typeof workItemSkils !== 'undefined')
                {
                    return $q.when(workItemSkils);
                }
                return $q.when([]);
            },
            getSuccessLink: function () {

                if (typeof successLink !== 'undefined')
                {
                    return successLink;
                }
                return '';
            },
            getCategorySkils: function (categories) {
                return $http.post('/index.php?option=com_work&task=items.getCategorySkils', {"categories": categories})
                        .then(function (data) {
                            return data;
                        });
            },

            getPopularSkils: function () {
                if (typeof popularSkils !== 'undefined')
                {
                    return $q.when(popularSkils);
                }
                return $q.when([]);
            },
            getPopularProffesions: function () {
                if (typeof popularProffesions !== 'undefined')
                {
                    return $q.when(popularProffesions);
                }
                return $q.when([]);
            },
            loadUserSkils: function () {
                var t = [
                    {
                        id: 1,
                        label: 'PHP'
                    }
                    , {
                        id: 2,
                        label: 'MySql'
                    }
                    , {
                        id: 3,
                        label: 'JavaScript'
                    }
                ];
                return $q.when(t);
            }



        };
    }
}());
