var app = angular.module("wapp", ['addPortfolioComponent'
            , 'pascalprecht.translate'
            , 'smartSelect'
            , 'angularjs-dropdown-multiselect'
            , 'ui.bootstrap'
            , 'angularFileUpload'
            , 'ngThumb'
            , 'ngFocusMe'
            , 'ngYoutubeEmbed'
            , 'skilsEditor'
            , 'selecttosearch'
            , 'userSettings'
            , 'ngIntlTelInput'
            , 'education'
            , 'educationList'
            , 'educationEdit'
            , 'employment'
            , 'employmentList'
            , 'employmentEdit'
            , 'userPortfolio'
            , 'mChat'
            ,'angularMoment'
]);
app.constant('angularMomentConfig', {
    fullDateThreshold: 1 // e.g. 'Europe/London'
   , amTimeAgoConfig:{fullDateThreshold: 1 }// e.g. 'Europe/London' 
}); 

  app.run(function(amMoment) {
	amMoment.changeLocale('ka');
});
 app.constant('amTimeAgoConfig', {
    fullDateThreshold: 1 // e.g. 'Europe/London'
   ,fullDateThresholdUnit: 'day'// e.g. 'Europe/London' 
   ,fullDateFormat: 'dddd, MMMM Do YYYY, hh:mm '// e.g. 'Europe/London' 
}); 