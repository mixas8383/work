-- phpMyAdmin SQL Dump
-- version 4.3.11.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 04, 2016 at 12:09 AM
-- Server version: 5.6.23
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bloomevents`
--

-- --------------------------------------------------------

--
-- Table structure for table `#__selectfile_sizes`
--

CREATE TABLE IF NOT EXISTS `#__selectfile_sizes` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `codename` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `quality` int(11) NOT NULL,
  `imgedit` int(11) NOT NULL,
  `watermark` int(11) NOT NULL,
  `ineditor` int(11) NOT NULL,
  `publish` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__selectfile_sizes`
--

INSERT INTO `#__selectfile_sizes` (`id`, `name`, `state`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `ordering`, `access`, `codename`, `width`, `height`, `quality`, `imgedit`, `watermark`, `ineditor`, `publish`) VALUES
(1, 'Original', 1, '2015-08-07 12:02:36', 906, '', '2015-08-31 12:06:15', 906, 906, '2015-09-07 13:16:36', 1, 1, '0', 1000, 1, 80, 1, -1, 0, 0),
(2, 'მთავარი გვერდის პროდუქტების მოდული', 1, '2015-08-25 14:15:02', 906, '', '2016-07-19 07:53:51', 906, 0, '0000-00-00 00:00:00', 2, 1, 'image1', 246, 176, 80, 3, 0, 0, 0),
(3, 'პროდუქტის სლაიდერის დიდი სურათი', 1, '2015-08-25 14:16:10', 906, '', '2016-07-20 08:41:33', 906, 0, '0000-00-00 00:00:00', 3, 1, 'image2', 550, 352, 80, 3, 0, 0, 0),
(4, 'სლაიდერის სურათები', 1, '2015-09-04 08:59:08', 906, '', '2016-07-13 08:14:40', 906, 0, '0000-00-00 00:00:00', 4, 1, 'image9', 1280, 416, 80, 3, -1, 0, 0),
(5, 'პრომო ვიდეო გალერეის დიდი სურათი', 1, '2015-09-07 08:54:45', 906, '', '2015-09-07 08:57:06', 906, 0, '0000-00-00 00:00:00', 5, 1, 'image5', 292, 196, 80, 3, -1, 0, 0),
(6, 'ჟურნალის მოდულის სურათი', 1, '2015-09-08 11:48:52', 906, '', '2015-09-08 11:54:06', 906, 0, '0000-00-00 00:00:00', 6, 1, 'image6', 226, 1, 80, 1, -1, 0, 0),
(7, 'ჟურნალის არქივის სურათი', 1, '2015-09-08 12:42:45', 906, '', '2015-09-08 12:44:09', 906, 0, '0000-00-00 00:00:00', 7, 1, 'image7', 320, 1, 80, 1, -1, 0, 0),
(8, 'გამოკითხვის მოდულის სურათი', 1, '2015-09-09 06:34:48', 906, '', '2015-09-09 06:35:47', 906, 906, '2015-09-14 12:36:30', 8, 1, 'image8', 292, 1, 80, 1, -1, 0, 0),
(9, 'facebook', 1, '2015-09-10 07:54:37', 906, '', '2015-09-10 07:57:46', 906, 906, '2015-09-18 09:33:49', 9, 1, 'image15', 470, 246, 80, 3, -1, 0, 0),
(10, 'ფოტო გალერეის კარუსელი', 1, '2015-09-10 08:12:38', 906, '', '2015-09-10 08:13:13', 906, 906, '2015-09-22 13:36:49', 10, 1, 'image10', 128, 100, 80, 3, -1, 0, 0),
(11, 'ფოტო გალერეის კატეგორიის სურათები', 1, '2015-09-10 09:38:58', 906, '', '2015-09-10 09:39:37', 906, 0, '0000-00-00 00:00:00', 11, 1, 'image11', 214, 176, 80, 3, -1, 0, 0),
(12, 'არტიკლების კატეგორიის სურათი', 1, '2015-09-11 07:24:44', 906, '', '2016-07-12 10:59:48', 906, 0, '0000-00-00 00:00:00', 12, 1, 'image12', 220, 238, 80, 3, -1, 0, 0),
(13, 'მთავარი გვერდის მარჯვენა სვეტის მოდულები', 1, '2015-09-11 11:11:46', 906, '', '2015-09-11 11:12:31', 906, 0, '0000-00-00 00:00:00', 13, 1, 'image13', 292, 354, 80, 3, -1, 0, 0),
(14, 'მსგავსი სიახლეები', 1, '2015-09-11 11:57:37', 906, '', '2015-09-11 11:58:53', 906, 0, '0000-00-00 00:00:00', 14, 1, 'image14', 214, 166, 80, 3, -1, 0, 0),
(15, 'მთავარი გვერდის მარცხენა სვეტის მოდულები', 1, '2015-09-11 12:03:51', 906, '', '2015-09-18 09:35:42', 906, 0, '0000-00-00 00:00:00', 15, 1, 'original', 325, 350, 80, 3, -1, 0, 0),
(16, 'სტატიის ფოტო გალერეის დიდი სურათი', 1, '2015-09-11 13:29:01', 906, '', '2015-10-22 17:13:17', 906, 0, '0000-00-00 00:00:00', 16, 1, 'image16', 660, 350, 80, 4, -1, 0, 0),
(17, 'სტატიის ფოტო გალერეის პატარა სურათი', 1, '2015-09-11 13:29:44', 906, '', '2015-09-11 13:30:28', 906, 0, '0000-00-00 00:00:00', 17, 1, 'image17', 128, 56, 80, 4, -1, 0, 0),
(18, 'rss ბლოკი', 1, '2015-09-14 12:07:05', 906, '', '2015-09-14 12:07:26', 906, 0, '0000-00-00 00:00:00', 18, 1, 'image18', 292, 140, 80, 3, -1, 0, 0),
(20, 'ტეგებით მსგავსი სიახლე სტატიაში', 1, '2015-09-16 12:47:18', 906, '', '2015-09-16 12:48:44', 906, 0, '0000-00-00 00:00:00', 19, 1, 'image19', 60, 60, 80, 4, -1, 0, 0),
(21, 'პროდუქტის სლაიდერის პატარა სურათი', 1, '2015-09-18 09:35:43', 906, '', '2016-07-20 08:43:32', 906, 0, '0000-00-00 00:00:00', 20, 1, 'image3', 130, 82, 80, 3, -1, 0, 0),
(22, 'FB popup image', 1, '2015-09-22 12:22:50', 906, '', '2015-09-24 08:47:17', 906, 0, '0000-00-00 00:00:00', 21, 1, 'image20', 420, 212, 80, 3, -1, 0, 0),
(23, 'მთავარი გვერდის მარჯვენა სვეტის მოდულები', -2, '2015-09-30 09:11:02', 906, '', '2015-09-30 09:11:48', 906, 0, '0000-00-00 00:00:00', 0, 1, 'image21', 292, 350, 80, 3, -1, 0, 0),
(24, 'პროდუქტს კატეგორიის სურათი', 1, '2016-07-20 14:00:21', 906, '', '2016-07-20 14:00:48', 906, 0, '0000-00-00 00:00:00', 22, 1, 'image4', 316, 226, 80, 3, -1, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `#__selectfile_sizes`
--
ALTER TABLE `#__selectfile_sizes`
  ADD PRIMARY KEY (`id`), ADD KEY `idx_access` (`access`), ADD KEY `idx_checkout` (`checked_out`), ADD KEY `idx_state` (`state`), ADD KEY `idx_createdby` (`created_by`), ADD KEY `idx_ordering` (`ordering`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `#__selectfile_sizes`
--
ALTER TABLE `#__selectfile_sizes`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;