<?php

/**
 * @version 		$Id:$ 
 * @name			Selectfile (Release 1.0.0)
 * @author			 ()
 * @package			com_selectfile
 * @subpackage		com_selectfile.admin
 * @copyright		
 * @license			GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html 
 * 
 * The following Component Architect header section must remain in any distribution of this file
 *
 * @CAversion		Id: compobject.php 408 2014-10-19 18:31:00Z BrianWade $
 * @CAauthor		Component Architect (www.componentarchitect.com)
 * @CApackage		architectcomp
 * @CAsubpackage	architectcomp.admin
 * @CAtemplate		joomla_3_3_standard (Release 1.0.3)
 * @CAcopyright		Copyright (c)2013 - 2014  Simply Open Source Ltd. (trading as Component Architect). All Rights Reserved
 * @Joomlacopyright Copyright (c)2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @CAlicense		GNU General Public License version 3 or later; See http://www.gnu.org/copyleft/gpl.html
 * 
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 */
defined ( 'JPATH_BASE' ) or die;

JFormHelper::loadFieldClass ( 'list' );

/**
 * Size Field class from the Joomla! Framework.
 *
 */
class JFormFieldImagesize extends JFormFieldList
{

    /**
     * The form field type.
     *
     * @var		string
     * 
     */
    protected $type = 'imagesize';

    /**
     * Method to get the field options.
     *
     * @return	array	The field options.
     * 
     */
    public function getOptions ()
    {

        $jinput = JFactory::getApplication ()->input;

        $id = $jinput->get ( 'id' );
        $db = JFactory::getDBO ();
        $where = $id ? ' WHERE `id`!="' . $id . '" ' : '';
        $query = ' SELECT'
                . ' `codename` AS `value`, '
                . ' `codename` AS `text` '
                . ' FROM `#__selectfile_sizes` '
                . $where
                . ' GROUP BY `codename` '
        ;
        $db->setQuery ( $query );

        $items = $db->loadObjectList ();

        $array = array (
            'original'=>'original',
            'image1'=>'image1',
            'image2'=>'image2',
            'image3'=>'image3',
            'image4'=>'image4',
            'image5'=>'image5',
            'image6'=>'image6',
            'image7'=>'image7',
            'image8'=>'image8',
            'image9'=>'image9',
            'image10'=>'image10',
            'image11'=>'image11',
            'image12'=>'image12',
            'image13'=>'image13',
            'image14'=>'image14',
            'image15'=>'image15',
            'image16'=>'image16',
            'image17'=>'image17',
            'image18'=>'image18',
            'image19'=>'image19',
            'image20'=>'image20',
            'image21'=>'image21',
            'image22'=>'image22',
            'image23'=>'image23',
            'image24'=>'image24',
            'image25'=>'image25',
            'image26'=>'image26',
            'image27'=>'image27',
            'image28'=>'image28',
            'image29'=>'image29',
            'image30'=>'image30',
            'image31'=>'image31',
            'image32'=>'image32',
            'image33'=>'image33',
            'image34'=>'image34',
            'image35'=>'image35',
        );
        foreach ($items as $val)
        {
            if ( in_array ( $val->value , $array  ) )
            {
                unset($array[$val->value]);
            }
            
        }
        $result = array();
        foreach ($array as $val)
        {
            $obj = new stdClass();
            $obj->value = $val;
            $obj->text = $val;
            $result[] = $obj;
        }
      
        return $result;
    }

}
