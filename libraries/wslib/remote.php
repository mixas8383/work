<?php

//Check Remote File
class RemoteToLocal
{
	public static function CheckRemoteFile( $file )
	{
		$ch = curl_init( $file );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 15 );
		curl_setopt( $ch, CURLOPT_NOBODY, true );
		curl_setopt( $ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)' );
		curl_exec( $ch );
		$retcode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
		curl_close( $ch );
		if ( $retcode == 200 )
		{
			return true;
		}
		return false;

	}

	public static function WriteEmpty( $Lfile )
	{
		$ext = RemoteToLocal::getExt( $Lfile );
		if ( 'jpg' == $ext )
		{
			$ext = 'jpeg';
		}
		if ( function_exists( 'image' . $ext ) )
		{
			$im = imagecreatetruecolor( 10, 20 );
			$text_color = imagecolorallocate( $im, 233, 14, 91 );
			imagestring( $im, 1, 5, 5, 'ABC', $text_color );
			eval( 'image' . $ext . '(' . $im . ',' . $Lfile . ')' );
		}
		else
		{
			$lfile = fopen( $Lfile, "w" );
			fwrite( $lfile, '' );
			fclose( $lfile );
		}

	}

	public static function CopyFromRemote( $RFile, $Lfile )
	{
		$dir = dirname( $Lfile );
		if ( !is_dir( $dir ) )
		{
			JFolder::create( $dir, 0777 );
		}
		$lfile = fopen( $Lfile, "w" );
		$ch = curl_init();
		curl_setopt( $ch, CURLOPT_URL, $RFile );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 60 );
		curl_setopt( $ch, CURLOPT_HEADER, 0 );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_FILE, $lfile );
		$buffer = curl_exec( $ch );
		fwrite( $lfile, $buffer );
		fclose( $lfile );
		curl_close( $ch );

	}

	public static function getExt( $file )
	{
		$chunks = explode( '.', $file );
		$chunksCount = count( $chunks ) - 1;

		if ( $chunksCount > 0 )
		{
			return $chunks[$chunksCount];
		}

		return false;

	}

	public static function getDir( $file )
	{
		$name = RemoteToLocal::getName( $file );
		return str_replace( $name, '', $file );

	}

	public static function getName( $file )
	{
		$slash = strrpos( $file, DS );
		if ( $slash !== false )
		{
			return substr( $file, $slash + 1 );
		}
		else
		{
			return $file;
		}

	}

	public static function IsWritable( $Dir )
	{
		if ( is_writable( $Dir ) )
		{
			return true;
		}
		else
		{
			return RemoteToLocal::setDirPermissions( $Dir, '0777' );
		}

	}

	function setDirPermissions( $path, $mode = '0777' )
	{
		if ( !@ chmod( $path, octdec( $mode ) ) )
		{
			return false;
		}
		return true;

	}

}