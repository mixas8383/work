<?php

/**
 * @version		$Id: helper.php 104 2011-09-23 06:22:53Z a.kikabidze $
 * @package	Libraries.WSLib
 * @copyright	Copyright (C) 2009 - 2011 WebSolutions LLC. All rights reserved.
 * @license		GNU General Public License version 2 or later
 */
defined ( '_JEXEC' ) or die ( 'Restricted access' );


// define constants
define ( 'WSMEDIA_JS', 'includes/wsmedia/js' );
define ( 'WSMEDIA_CSS', 'includes/wsmedia/css' );
define ( 'WSMEDIA_IMG', 'includes/wsmedia/img' );
define ( 'WSMEDIA_FONTS', 'includes/wsmedia/fonts' );

/**
 * Joomla WS Lib Helper class
 *
 * @static
 * @package		Joomla.WSLib
 * @since	1.5
 */
class wsHelper
{

    /**
     * The Itemids list.
     *
     * @var		array
     * @since	1.5
     */
    private static $ItemidList = array ();

    /**
     * The Configs list.
     *
     * @var		array
     * @since	1.5
     */
    private static $config = array ();

    /**
     * The pathway object.
     *
     * @var		object
     * @since	1.5
     */
    private static $pathway = null;

    /**
     * The captcha object.
     *
     * @var		object
     * @since	1.5
     */
    private static $captcha = null;

    /**
     * The log object.
     *
     * @var		object
     * @since	1.5
     */
    private static $log = array ();

    /**
     * The unique counter.
     *
     * @var		int
     * @since	1.5
     */
    private static $_unique = 0;

    /**
     * Get a configuration object
     *
     * Returns a reference to the global object, only creating it
     * if it doesn't already exist.
     *
     * @access public
     *
     * @param string $component Component context name
     * @param string $cfgTableName Config table suffix
     *
     * @return object
     */
    public static function getConfig ( $component = null, $cfgTableName = 'config' )
    {
        $component = self::checkComponent ( $component );
        $hash = 'custom.' . $component . '.' . $cfgTableName;

        if ( isset ( self::$config[ $hash ] ) )
        {
            return self::$config[ $hash ];
        }

        $table = substr ( $component, 4 );
        $table = '#__' . $table . '_' . $cfgTableName;
        $db = & JFactory::getDBO ();
        $query = ' SELECT * '
                . ' FROM `' . $table . '` ';
        $db->setQuery ( $query );
        $data = $db->loadObjectList ();
        if ( is_null ( $data ) )
        {
            JError::raiseError ( 500, 'WS Error! wsHelper::getConfig() - Table with name `' . $table . '` not exists!' );
        }
        jimport ( 'wslib.config' );
        $object = new wsConfig ( $data );

        self::$config[ $hash ] = $object;
        return $config[ $hash ];
    }

    /**
     * Get a Menu configuration object
     *
     * Returns a reference to the global object, only creating it
     * if it doesn't already exist.
     *
     * @access public
     *
     * @param int $Itemid	Menu id
     *
     * @return object
     */
    public static function getMenuConfig ( $Itemid = null )
    {
        if ( is_null ( $Itemid ) )
        {
            $Itemid = self::getItemid ();
        }
        $Itemid = ( int ) $Itemid;
        $hash = 'menu.' . $Itemid;

        if ( isset ( self::$config[ $hash ] ) )
        {
            return self::$config[ $hash ];
        }

        if ( !class_exists ( 'JSite' ) )
        {
            include_once(JPATH_ROOT . DS . 'includes' . DS . 'application.php');
        }
        $menu = JSite::getMenu ();
        self::$config[ $hash ] = $menu->getParams ( $Itemid );
        return self::$config[ $hash ];
    }

    /**
     * Get a Component configuration (preferences) object
     *
     * Returns a reference to the global object, only creating it
     * if it doesn't already exist.
     *
     * @access public
     *
     * @param string $component Component name
     *
     * @return object
     */
    public static function getComponentConfig ( $component = null )
    {
        $component = self::checkComponent ( $component );
        $hash = 'component.' . $component;

        if ( isset ( self::$config[ $hash ] ) )
        {
            return self::$config[ $hash ];
        }

        self::$config[ $hash ] = JComponentHelper::getParams ( $component );
        return self::$config[ $hash ];
    }

    private static function checkComponent ( $component = null )
    {
        global $option;
        static $components;

        if ( isset ( $components[ $component ] ) )
        {
            return $components[ $component ];
        }

        if ( is_null ( $component ) )
        {
            $component = $option;
        }

        if ( !$component || substr ( $component, 0, 4 ) !== 'com_' )
        {
            JError::raiseError ( 500, 'WS Error! wsHelper::getConfig() - Component name not defined!' );
        }
        return $component;
    }

    /**
     * Get Itemid
     *
     * Returns Itemid if it doesn't already exist for selected view/component.
     *
     * @param string $component Component name.
     * @param string $view View name.
     * @param string $layout Layout name.
     *
     * @return int	Itemid
     */
    public static function getItemid ( $component = null, $view = null, $layout = null )
    {
        global $option;

        if ( is_null ( $component ) )
        {
            $component = $option;
        }
        if ( is_null ( $view ) )
        {
            $view = substr ( $component, 4 );
        }
        if ( is_null ( $layout ) )
        {
            $layout = 'default';
        }

        if ( !$component )
        {
            JError::raiseError ( 500, 'WS Error! wsHelper::getItemid() - Component name not defined!' );
        }

        $hash = $component . '.' . $view . '.' . $layout;
        if ( isset ( self::$ItemidList[ $hash ] ) )
        {
            return self::$ItemidList[ $hash ];
        }

        $comp = JComponentHelper::getComponent ( $component );
        //Araik
        $app = JFactory::getApplication ();
        $menus = $app->getMenu ( 'site', array () );
        $items = $menus->getItems ( 'component_id', isset ( $comp->id ) ? $comp->id : 0  );
        //++
//        $menus = JApplication::getMenu ( 'site', array () );
//        $items = $menus->getItems ( 'componentid', isset ( $comp->id ) ? $comp->id : 0  );
        $match = NULL;


        $default = 1;
        if ( $items )
        {
            foreach ( $items as $item )
            {
                if ( empty ( $item->id ) )
                {
                    continue;
                }
                $cview = isset ( $item->query[ 'view' ] ) ? $item->query[ 'view' ] : '';
                $clayout = isset ( $item->query[ 'layout' ] ) ? $item->query[ 'layout' ] : '';

                if ( $default == 1 )
                {
                    $default = $item->id;
                }

                if ( $layout != 'default' )
                {
                    if ( $cview == $view && $clayout == $layout )
                    {
                        $match = $item->id;
                        break;
                    }
                }
                else
                {
                    if ( $cview == $view )
                    {
                        $match = $item->id;
                        break;
                    }
                }
            }
        }
        if ( !$match )
        {
            $match = $default;
        }
        self::$ItemidList[ $hash ] = $match;
        return $match;
    }

    /**
     * Render Joomla module
     *
     * @param string $name Module name.
     * @param string $cssPrefix CSS Class Suffix.
     *
     * @return string Module content
     */
    public static function renderModule ( $name, $cssPrefix = 'jos' )
    {
        if ( empty ( $name ) )
        {
            return false;
        }

        //$name='mod_fb_fanbox';
        $document = JFactory::getDocument ();
        $renderer = $document->loadRenderer ( 'module' );
        $params = array ( 'style' => -2 );
        $contents = '';

        foreach ( JModuleHelper::getModules ( $name ) as $mod )
        {
            $contents .= $renderer->render ( $mod, $params );
        }


        if ( empty ( $mod ) || !is_object ( $mod ) )
        {
            return false;
        }

        $modParams = new JRegistry ( $mod->params );
        $moduleclass_sfx = $modParams->get ( 'moduleclass_sfx', 0 );

        if ( !empty ( $contents ) )
        {
            if ( $mod->showtitle )
            {
                $contents = '<div class="' . $cssPrefix . '_title' . $moduleclass_sfx . '">' . $mod->title . '</div><div class="' . $cssPrefix . '_module_' . $name . '' . $moduleclass_sfx . '">' . $contents . '</div>';
            }
            else
            {
                $contents = '<div class="' . $cssPrefix . '_module_' . $name . '' . $moduleclass_sfx . '">' . $contents . '</div>';
            }
        }
        return $contents;
    }

    public static function getModule ( $name )
    {
        if ( empty ( $name ) )
        {
            return false;
        }
        $module = JModuleHelper::getModule ( $name );
        return $module;
    }

    public static function getModuleParams ( $name )
    {
        if ( empty ( $name ) )
        {
            return false;
        }
        $module = self::getModule ( $name );
        $modParams = new JParameter ( $module->params );
        return $modParams;
    }

    /**
     * Get valid sizes suffix
     *
     * @param int $bytes Bytes.
     *
     * @return string Size with valid prefix
     */
    public static function getSizeName ( $bytes )
    {
        if ( $bytes < 1024 )
        {
            $size = $bytes;
            return $size . ' ' . JText::_ ( 'Bytes' );
        }
        else if ( $bytes < 1048576 )
        {
            $size = round ( $bytes / 1024, 2 );
            return $size . ' ' . JText::_ ( 'Kbytes' );
        }
        else
        {
            $size = round ( $bytes / 1048576, 2 );
            return $size . ' ' . JText::_ ( 'Mbytes' );
        }
    }

    /**
     * Get max upload size in system
     *
     * @return string
     */
    public static function getUploadMaxSize ( $inbytes = false )
    {
        static $max;
        if ( is_null ( $max ) )
        {
            $uploads = ini_get ( 'file_uploads' );
            if ( $uploads )
            {
                $configMedia = & JComponentHelper::getParams ( 'com_media' );
                $max1 = ( int ) $configMedia->get ( 'upload_maxsize' ) / 1000000;
                $max2 = ( int ) ini_get ( 'upload_max_filesize' );
                $max3 = ( int ) ini_get ( 'post_max_size' );
                $max = min ( $max1, $max2, $max3 );
                if ( $inbytes )
                {
                    $max = $max * 1024 * 1024;
                }
            }
            else
            {
                JError::raiseWarning ( '', 'WS Error! wsHelper::getUploadMaxSize() - Uploads is disabled in server!' );
            }
        }
        return $max;
    }

    /**
     * Get an pathway object
     *
     * Returns the global {@link WSPathway} object, only creating it
     * if it doesn't already exist
     *
     * @return WSPathway object
     */
    public static function getPathway ()
    {
        if ( !self::$pathway )
        {
            jimport ( 'wslib.html.pathway' );
            self::$pathway = new WSPathway();
        }

        return self::$pathway;
    }

    /**
     * Get an captcha object
     *
     * Returns the global {@link WSCaptcha} object, only creating it
     * if it doesn't already exist
     *
     * @return WSCaptcha object
     */
    public static function getCaptcha ()
    {
        if ( !self::$captcha )
        {
            jimport ( 'wslib.html.captcha' );
            self::$captcha = new WSCaptcha();
        }
        return self::$captcha;
    }

    /**
     * Get an Log object
     *
     * Returns the global {@link WSLog} object, only creating it
     * if it doesn't already exist
     *
     * @return WSLog object
     */
    public static function getLog ( $options = null, $path = null )
    {
        // Set default path if not set
        if ( !$path )
        {
            $config = & JFactory::getConfig ();
            $path = $config->getValue ( 'config.log_path' );
        }

        $sig = md5 ( $path );
        if ( empty ( self::$log[ $sig ] ) )
        {
            echo time ();
            jimport ( 'wslib.error.log' );
            self::$log[ $sig ] = WSLog::getInstance ( $options, $path );
        }
        return self::$log[ $sig ];
    }

    /**
     * escape string for output
     *
     * @param string $text string to escape.
     * @param bool $nl2br add or not line ending replaces.
     *
     *
     * @return escaped string
     */
    public static function escape ( $text, $nl2br = false )
    {
        $text = trim ( $text );
        $text = preg_replace ( '#<script[^>]*>.*?</script>#si', '', $text );
        $text = htmlspecialchars ( $text, ENT_QUOTES, 'UTF-8' );
        if ( $nl2br )
        {
            $text = nl2br ( $text );
        }
        return $text;
    }

    public static function loadLanguage ( $extension = '', $basePath = JPATH_BASE )
    {
        global $option;
        if ( empty ( $extension ) )
        {
            $extension = $option;
        }

        $lang = JFactory::getLanguage ();
        return $lang->load ( strtolower ( $extension ), $basePath );
    }

    public static function getUnique ( $prefix = '' )
    {
        self::$_unique++;
        if ( empty ( $prefix ) )
        {
            $jcfg = JFactory::getConfig ();
            $prefix = strtolower ( substr ( $jcfg->getValue ( 'config.secret' ), 0, 3 ) );
        }

        $string = $prefix . self::$_unique;
        return $string;
    }

    public static function URLSlug ( $string )
    {
        // Replace double byte whitespaces by single byte (East Asian languages)
        $str = preg_replace ( '/\xE3\x80\x80/', ' ', $string );

        // Remove any '-' from the string as they will be used as concatenator.
        // Would be great to let the spaces in but only Firefox is friendly with this

        $str = str_replace ( '-', ' ', $str );

        // Replace forbidden characters by whitespaces
        $str = preg_replace ( '#[:\#\*"@+=;!><,&\.%()\]\/\'\\\\|\[]#', "\x20", $str );

        // Delete all '?'
        $str = str_replace ( '?', '', $str );

        // Trim white spaces at beginning and end of alias and make lowercase
        $str = trim ( JString::strtolower ( $str ) );

        // Remove any duplicate whitespace and replace whitespaces by hyphens
        $str = preg_replace ( '#\x20+#', '-', $str );

        return $str;
    }

}

return; //TODO Araik
global $mainframe;
// load current helper file
if ( $mainframe->isAdmin () )
{
    jimport ( 'wslib.admin.helper' );
}
else if ( $mainframe->isSite () )
{
    jimport ( 'wslib.site.helper' );
}
jimport ( 'wslib.application.cache' );

function wsDump ( $input, $die = true )
{
    if ( stripos ( $_SERVER[ 'HTTP_USER_AGENT' ], 'longdebug' ) !== false )
    {
        echo "<h4>\n";
        var_dump ( $input );
        echo "</h4>\n";
        if ( $die )
        {
            die ();
        }
    }
}

function wsPrint ( $input, $die = true )
{
    if ( stripos ( $_SERVER[ 'HTTP_USER_AGENT' ], 'longdebug' ) !== false )
    {
        echo "<h4>\n";
        print_r ( $input );
        echo "</h4>\n";
        if ( $die )
        {
            die ();
        }
    }
}

?>