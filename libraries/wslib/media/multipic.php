<?php
/**
* @version		$Id: multipic.php 101 2011-09-21 12:29:36Z a.kikabidze $
* @package	Libraries.WSLib
* @copyright	Copyright (C) 2009 - 2011 WebSolutions LLC. All rights reserved.
* @license		GNU General Public License version 2 or later
*/

defined('_JEXEC') or die('Direct Access to this location is not allowed.');

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');
jimport('wslib.media.image');

class wsMultiPic
{
	private $sizes;
	private $cacheDir;
	private $_gcFreq = 10;
	private $_expiration = 86400;
	private $_config;
	private $_watermark;
	
	public function __construct($sizes = array(), $cacheDir = 'pictures')
	{
		$this->cacheDir = $cacheDir;
		
		if (empty($this->_config))
		{
			$this->_config = JComponentHelper::getParams('com_selectfile');
			$watermark = new stdClass;
			$watermark->status = $this->_config->get('watermark_status', 0);
			$watermark->transparency = $this->_config->get('watermark_transparency', 0);
			$watermark->font = $this->_config->get('watermark_font', 0);
			$watermark->fontsize = $this->_config->get('watermark_fontsize', 0);
			$watermark->textangle = $this->_config->get('watermark_textangle', 0);
			$watermark->rotate = $this->_config->get('watermark_rotate', 0);
			$watermark->position = $this->_config->get('watermark_position', 0);
			$watermark->texttransparency = $this->_config->get('watermark_texttransparency', 0);
			$watermark->text = $this->_config->get('watermark_text', 0);
			$watermark->img = $this->_config->get('watermark_img', 0);
			$this->_watermark = $watermark;
		}

		
		if (!empty($sizes))
		{
			$this->sizes = $sizes;
		}
		else
		{
			$this->loadSizes();
		}

/*
		if (mt_rand(1, $this->getGcFreq()) == 1) 
		{
			$this->_gc();
		}

*/
	}
	
	public function setCacheDir($dir)
	{
		$this->cacheDir = $dir;
		return $this;
	}

	public function getCacheDir()
	{
		return $this->cacheDir;
	}

	public function setGcFreq($freq)
	{
		$this->_gcFreq = $freq;
		return $this;
	}

	public function getGcFreq()
	{
		return $this->_gcFreq;
	}

	public function setExpiration($exp)
	{
		$this->_expiration = $exp;
		return $this;
	}

	public function getExpiration()
	{
		return $this->_expiration;
	}


	public function loadSizes()
	{
		$cache = JFactory::getCache( 'MultiImagesSizes');
		$cache->setCaching(1);
                
                
		$lifeTime = $cache->options['lifetime'];
                //todo remove this
		//$cache->setLifeTime(86400);
		$cache->setLifeTime(1);
 
		$method = array( 'wsMultiPic', '_loadSizes' );
		$this->sizes = $cache->call($method);
		$cache->setLifeTime($lifeTime);
		$cache->setCaching(0);
   }
   
	public function getSizes($size = false)
	{
		if ($size)
		{
			if (isset($this->sizes[$size]))
			{
				return $this->sizes[$size];
			}
		}
		return $this->sizes;
   }

	public function getImage($size, $img, $setShadow = false)
	{

		if (empty($size))
		{
			return false;
		}
		if (empty($img))
		{
			return false;
		}

		$s = !empty($this->sizes[$size]) ? $this->sizes[$size] : '';
           
		if (empty($s))
		{
			return false;
		}
                        
		if (!is_object($s))
		{
			$s = (object)$s;
		}
		$s->width = (int)$s->width;
		$s->height = (int)$s->height;

		$shadow = !empty($s->shadow) && $setShadow ? $s->shadow : '';
		$shadowsize = !empty($s->shadowsize) && $setShadow ? $s->shadowsize : 1;

		$setwatermark = isset($s->watermark) ? $s->watermark : -1;

		$watermark = !empty($this->_watermark) ? $this->_watermark : '';
		if ($setwatermark == 0)
		{
			$watermark = null;
		}
		else if ($setwatermark == 1 && is_object($watermark))
		{
			$watermark->status = 1;
		}
		$ext = '.'.JFile::getExt($img);
		$cache_url = $this->getCacheDir().'/'.$size;
		$cache_dir = $this->getCacheDir().DS.$size;

		$hash = $img.'|'.$s->width.'|'.$s->height.'|'.$s->quality.'|'.$s->imgedit.'|'.$shadow.'|'.$shadowsize;
		if (!empty($watermark))
		{
			foreach($watermark as $k=>$v)
			{
				$hash .= '|'.$v;
			}
		}
		$img_hash = md5($hash);

		if (JFile::exists(JPATH_SITE.DS.$cache_dir.DS.$img_hash.$ext))
		{
			return $cache_url.'/'.$img_hash.$ext;
		}
		if (!JFolder::exists(JPATH_SITE.DS.$cache_dir))
		{
			JFolder::create(JPATH_SITE.DS.$cache_dir, 0777);
		}

		$jimg = new wsImage();

		$isRemote = JURI::isInternal($img) ? false : true;
		if ($isRemote)
		{
			$jimg->setImage($img, $isRemote);	
		}
		else
		{
			$jimg->setImage(JPATH_SITE.DS.$img, $isRemote);	
		}

		// watermark 
		if (!empty($watermark) && !empty($watermark->status))
		{
			if (!empty($watermark->img) || !empty($watermark->text))
			{
				$textangle = $watermark->textangle;
				$position = $watermark->position;
				$transparency = $watermark->transparency;
				$rotate = $watermark->rotate;
	
				if (empty($watermark->img))
				{
					$font = JPATH_ROOT.DS.'includes'.DS.'wsmedia'.DS.'fonts'.DS.$watermark->font;
					$wmimage = $jimg->createWatermark($font, $watermark->text, $watermark->fontsize, '#FFFFFF', '#000000', true);
					$watermark_img = JPath::clean($wmimage);	
				}
				else
				{
					$watermark_img = JPath::clean($watermark->img);
				}
				$watermark_padding = isset($watermark->padding) && is_array($watermark->padding) ? $watermark->padding : array(0, 0);
				$jimg->addWatermark($watermark_img, $position, $rotate, $transparency, $watermark_padding);
			}
		}


		switch($s->imgedit)
		{
			case 1:
				$jimg->resize($s->width, $s->height, false);
				break;
			case 2:
				$jimg->resize($s->width, $s->height, true);						
				break;
			case 3:
				if (empty($s->height))
				{
					$s->height = $s->width;
				}
				$jimg->Crop($s->width, $s->height, true);
				break;
			case 4:
				if (empty($s->height))
				{
					$s->height = $s->width;
				}
				$jimg->crop($s->width, $s->height, false);
				break;
		}
		
		
		// shadow
		if ($shadow)
		{
			$jimg->addShadow($shadowsize);
		}

		if ($jimg->save(JPATH_SITE.DS.$cache_dir.DS.$img_hash.$ext, $s->quality))
		{
			$jimg->free();		
			return $cache_url.'/'.$img_hash.$ext;
		}
		$jimg->free();
		return false;

   }
	
	public static function _loadSizes()
	{
		$db = JFactory::getDBO();
		$query = ' SELECT * '
						.' FROM `#__selectfile_sizes` ';
		$db->setQuery($query);
		$obj = $db->loadObjectList();

		$sizes = array();
		foreach($obj as $o)
		{
			$sizes[$o->codename] = $o;
		}

		return $sizes;	
   }


	private function _gc()
	{
		$expire = time() - $this->getExpiration();
		
		$cacheDir = $this->getCacheDir();
		foreach($this->sizes as $size=>$obj)
		{
			$imgdir = JPATH_SITE.DS.$cacheDir.DS.$size;
			if (!$imgdir || strlen($imgdir) < 2) 
			{
				return false;
			}
			
			foreach(new DirectoryIterator($imgdir) as $file) 
			{
				if (!$file->isDot() && !$file->isDir()) 
				{
					if ($file->getMTime() < $expire) 
					{
						JFile::delete($file->getPathname());
					}
				}
			}
		}
	}





}