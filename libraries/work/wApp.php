<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of workApp
 *
 * @author Mixas
 */
class wApp extends JObject
{
    //put your code here
    public function getPopularSkils()
    {
        $db = JFactory::getDbo();
        $db->setQuery(''
                . ''
                . ' SELECT s.*, s.name as label'
                . ' FROM #__work_skils AS s'
                . ' ORDER BY s.hits DESC'
                . ' LIMIT 15'
                . '');
       $rows =  $db->loadObjectList();
       return $rows;
    }
    public function getPopularProffesions()
    {
        $db = JFactory::getDbo();
        $db->setQuery(''
                . ''
                . ' SELECT s.*, s.name as label'
                . ' FROM #__work_proffesions AS s'
                . ' ORDER BY s.hits DESC'
                . ' LIMIT 15'
                . '');
       $rows =  $db->loadObjectList();
       return $rows;
    }
}
