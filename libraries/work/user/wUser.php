<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of wUser
 *
 * @author Mixas
 */
class wUser
{

    private $_user;
    private $_followers;
    private $_following;
    private $_db;
    private $_error;

    public function setError($err)
    {
        $this->_error = $err;
    }

    public function getError()
    {
        return $this->_error;
    }

    public function __construct($data = null)
    {
        JLoader::import('joomla.filesystem.file');
        JLoader::import('joomla.filesystem.folder');
        JLoader::import('wslib.media.multipic');
        $this->_db = JFactory::getDbo();

        if (empty($data))
        {
            $this->_user = JFactory::getUser();
        } else
        {
            if (is_object($data) || is_array($data))
            {
                $this->_user = $data;
            } elseif (is_numeric($data))
            {
                $this->_user = JFactory::getUser($data);
            }
        }
    }

    public function isLoged()
    {
        return empty($this->_user->id) ? false : true;
    }

    public function get($name)
    {
        return isset($this->_user->$name) ? $this->_user->$name : false;
    }

    public function saveAvatar()
    {
        
    }

    public function getAvatarPath()
    {
        $path = JPATH_ROOT . DS . 'images' . DS . 'avatars' . DS . md5('avatar' . $this->get('id')) . '.jpg';
        return $path;
    }

    private function _createAvatarFolder()
    {
        return true;
    }

    public function getAvatarName()
    {
        return md5('avatar' . $this->get('id')) . '.jpg';
    }

    public function getAvatar($imageSize = null)
    {
        
        $image = 'images' . DS . 'avatars' . DS . $this->getAvatarName();
         
        if(!JFile::exists($image))
        {
            return false;
        }
        
        
        if (!empty($imageSize))
        {
            
            
            $wsPic = new wsMultiPic();
            $wsPic->setCacheDir('pictures');
            $image = $wsPic->getImage($imageSize, $image);
        }
        return $image;
    }

    public function getDefaultImage($imageSize = null)
    {
        $image = 'images' . DS . 'theme' . DS . 'avatar.jpg';
        if (!empty($imageSize))
        {
            $wsPic = new wsMultiPic();
            $wsPic->setCacheDir('pictures');
            $image = $wsPic->getImage($imageSize, $image);
        }
        return $image;
    }

    public function getFullName()
    {
        return $this->get('name') . ' ' . $this->get('surname');
    }

    public function getProfileLink()
    {


        $itemid = wsHelper::getItemid('com_users', 'pprofile');

        return JRoute::_('index.php?option=com_users&view=pprofile&id=' . $this->get('id') . '&Itemid=' . $itemid);
    }

    public function getFollowers()
    {
        if (empty($this->_followers))
        {
            $this->_db->setQuery(''
                    . ''
                    . ' SELECT f.date as follow_date,u.* '
                    . ' FROM #__followers as f'
                    . ' INNER JOIN #__users as u on u.id=f.user'
                    . ' WHERE f.follow=' . $this->get('id')
                    . '');
            $followers = $this->_db->loadObjectList();
            $this->_followers = array();
            if (!empty($followers))
            {
                foreach ($followers as $value)
                {
                    $this->_followers[] = new wUser($value);
                }
            }
        } return $this->_followers;
    }

    public function getFollowing()
    {
        if (empty($this->_following))
        {

            $this->_db->setQuery(''
                    . ''
                    . ' SELECT f.date as follow_date,u.* '
                    . ' FROM #__followers as f'
                    . ' INNER JOIN #__users as u on u.id=f.follow'
                    . ' WHERE f.user=' . $this->get('id')
                    . '');
            $followers = $this->_db->loadObjectList();
            $this->_following = array();
            if (!empty($followers))
            {
                foreach ($followers as $value)
                {
                    $this->_following[] = new wUser($value);
                }
            }
        }
        return $this->_following;
    }

    public function getPortfolioItems()
    {

        $this->_db->setQuery(''
                . ''
                . ' SELECT i.*'
                . ' ,(select g.src from #__work_galleries as g where g.item_id=i.id limit 1) as src'
                . ' FROM #__work_items as  i'
                // . ' INNER JOIN #__users as u on u.id=f.follow'
                . ' WHERE i.created_by=' . $this->get('id')
                . ' ORDER BY i.id DESC'
                . '');
        $items = $this->_db->loadObjectList();

        if (!empty($items))
        {
            foreach ($items as $key => $one)
            {
                $this->_db->setQuery(''
                        . ''
                        . 'SELECT c.id,c.title as label '
                        . ' FROM #__work_category_map AS cm '
                        . ' INNER JOIN #__categories as c ON c.id=cm.catid'
                        . ' WHERE type=1 AND cm.item_id=' . $one->id
                        . '');
                $categories = $this->_db->loadObjectList();

                $items[$key]->categories = $categories;
                $this->_db->setQuery(''
                        . ''
                        . 'SELECT c.id,c.name as label '
                        . ' FROM #__work_skils_map AS cm '
                        . ' INNER JOIN #__work_skils as c ON c.id=cm.skill_id'
                        . ' WHERE type=1 AND cm.item_id=' . $one->id
                        . '');
                $skils = $this->_db->loadObjectList();
                $items[$key]->skils = $skils;

                $this->_db->setQuery(''
                        . ''
                        . 'SELECT cm.id,cm.src '
                        . ' FROM #__work_galleries AS cm '
                        . ' WHERE cm.item_id=' . $one->id
                        . '');
                $skils = $this->_db->loadObjectList();
                $items[$key]->images = $skils;
            }
        }

        return $items;
    }

    public function getFollowersCount()
    {
        $followers = $this->getFollowers();
        return count($followers);
    }

    public function getFollowingCount()
    {
        $followers = $this->getFollowing();
        return count($followers);
    }

    public function getSkils()
    {

        $this->_db->setQuery(''
                . ' '
                . 'SELECT c.name,c.id as value,c.id as id,c.name as label '
                . ' '
                . ' FROM #__work_skils_map as m'
                . ' INNER JOIN #__work_skils AS c ON m.skill_id=c.id '
                . ' where c. state=1'
                . ' AND m.item_id=' . $this->get('id')
                . ' AND m.type=4'
                . '');
        $skils = $this->_db->loadObjectList();
        return $skils;
    }

    public function getProffesions()
    {

        $this->_db->setQuery(''
                . ' '
                . 'SELECT c.name,c.id as value,c.id as id,c.name as label '
                . ' '
                . ' FROM #__work_proffesions_map as m'
                . ' INNER JOIN #__work_proffesions AS c ON m.proffesion_id=c.id '
                . ' where c. state=1'
                . ' AND m.item_id=' . $this->get('id')
                . ' AND m.type=4'
                . '');
        $skils = $this->_db->loadObjectList();
        return $skils;
    }

    public function saveSkils($data)
    {
        $this->_db->setQuery(''
                . ' '
                . 'DELETE FROM #__work_skils_map '
                . ' WHERE item_id=' . $this->get('id')
                . ' AND type=4'
                . '');

        $this->_db->execute();

        if (empty($data) && !is_array($data))
        {
            return false;
        }

        $data = array_unique($data);
        foreach ($data as $one)
        {
            $this->_db->setQuery(''
                    . ' '
                    . 'INSERT INTO #__work_skils_map (skill_id,item_id,type) VALUES'
                    . '('
                    . ' ' . $one
                    . ', ' . $this->get('id')
                    . ', 4'
                    . ')'
                    . '');
            $this->_db->execute();
        }
        return true;
    }

    public function saveProffesions($data)
    {
        $this->_db->setQuery(''
                . ' '
                . 'DELETE FROM #__work_proffesions_map '
                . ' WHERE item_id=' . $this->get('id')
                . ' AND type=4'
                . '');

        $this->_db->execute();

        if (empty($data) && !is_array($data))
        {
            return false;
        }

        $data = array_unique($data);
        foreach ($data as $one)
        {
            $this->_db->setQuery(''
                    . ' '
                    . 'INSERT INTO #__work_proffesions_map (proffesion_id,item_id,type) VALUES'
                    . '('
                    . ' ' . $one
                    . ', ' . $this->get('id')
                    . ', 4'
                    . ')'
                    . '');
            $this->_db->execute();
        }
        return true;
    }

    public function saveEducation($data)
    {
        JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_work' . DS . 'tables');
        $table = JTable::getInstance('Education', 'WorkTable');
        if (!$table->bind((array) $data))
        {
            $this->setError($table->getError());
            return false;
        }
        if (!$table->check())
        {
            $this->setError($table->getError());
            return false;
        }

        if (!$table->store())
        {
            $this->setError($table->getError());
            return false;
        }

        return true;
    }

    public function saveEmployment($data)
    {

        JTable::addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_work' . DS . 'tables');
        $table = JTable::getInstance('Employment', 'WorkTable');
        if (!$table->bind((array) $data))
        {
            $this->setError($table->getError());
            return false;
        }
        if (!$table->check())
        {
            $this->setError($table->getError());
            return false;
        }

        if (!$table->store())
        {
            $this->setError($table->getError());
            return false;
        }

        return true;
    }

    public function loadEducations()
    {
        $this->_db->setQuery(''
                . ' '
                . 'SELECT e.id'
                . ',e.description'
                . ',e.start'
                . ',e.end'
                . ',s.name as school'
                . ',d.name as degree'
                . ',a.name as area_of_study'
                . ' FROM #__work_education as e'
                . ' LEFT JOIN #__work_schools as s on s.id=e.school_id'
                . ' LEFT JOIN #__work_degrees as d on d.id=e.degree_id'
                . ' LEFT JOIN #__work_study_areas as a on a.id=e.area_of_study_id'
                . ' '
                . ''
                . ' WHERE e.created_by=' . $this->get('id')
                . ''
                . ''
                . ' order by e.end desc'
                . ''
                . '');
        $ret = $this->_db->loadObjectList();


        return $ret;
    }

    public function loadEmployments()
    {
        $this->_db->setQuery(''
                . ' '
                . 'SELECT e.id'
                . ',e.name'
                . ',e.description'
                . ',e.start_year'
                . ',e.start_month'
                . ',e.end_year'
                . ',e.end_month'
                . ',e.currently'
                . ',e.city'
                . ',e.country_id'
                . ',cn.name_ka_ge as country_name'
                . ',c.name as company'
                . ' FROM #__work_employment as e'
                . ' LEFT JOIN #__work_companies as c on c.id=e.company_id'
                . ' LEFT JOIN #__country as cn on cn.id=e.country_id '
                . ' '
                . ''
                . ' WHERE e.created_by=' . $this->get('id')
                . ''
                . ''
                . ' order by e.currently desc, e.end_month desc, e.end_year desc'
                . ''
                . '');
        $ret = $this->_db->loadObjectList();


        return $ret;
    }

    public function deleteEducation($id)
    {
        if (empty($id))
        {
            $this->setError(JText::_('Id not defined'));
            return false;
        }
        $this->_db->setQuery(''
                . ' SELECT * FROM #__work_education'
                . ' WHERE id=' . $id
                . ' AND created_by=' . $this->get('id')
                . ' ');
        $item = $this->_db->loadObject();
        if (empty($item))
        {
            $this->setError(JText::_('cannot delete this education'));
            return FALSE;
        }
        $this->_db->setQuery(''
                . 'DELETE FROM #__work_education WHERE id=' . $id
                . '');
        return $this->_db->execute();
    }

    public function deleteEmployment($id)
    {
        if (empty($id))
        {
            $this->setError(JText::_('Id not defined'));
            return false;
        }
        $this->_db->setQuery(''
                . ' SELECT * FROM #__work_employment'
                . ' WHERE id=' . $id . ' AND created_by=' . $this->get('id')
                . ' ');
        $item = $this->_db->loadObject();
        if (empty($item))
        {
            $this->setError(JText::_('cannot delete this education'));
            return FALSE;
        }
        $this->_db->setQuery(''
                . 'DELETE FROM #__work_employment WHERE id=' . $id
                . '');
        return $this->_db->execute();
    }

    public function deletePortfolio($id)
    {
        if (empty($id))
        {
            $this->setError(JText::_('Id not defined'));
            return false;
        }
        $this->_db->setQuery(''
                . ' SELECT * FROM #__work_items '
                . ' WHERE id=' . $id . ' AND created_by=' . $this->get('id')
                . ' ');
        $item = $this->_db->loadObject();
        if (empty($item))
        {
            $this->setError(JText::_('cannot delete this portpholio'));
            return FALSE;
        }
        $this->_db->setQuery(''
                . 'DELETE FROM #__work_items WHERE id=' . $id
                . '');
        $ret = $this->_db->execute();

        //deleteImages
        $this->_db->setQuery(''
                . ''
                . 'SELECT cm.id,cm.src '
                . ' FROM #__work_galleries AS cm '
                . ' WHERE cm.item_id=' . $id
                . '');
        $images = $this->_db->loadObjectList();


        if (!empty($images))
        {
            foreach ($images as $one)
            {
                unlink(JPATH_ROOT . DS . $one->src);
            }
        }
        $this->_db->setQuery(''
                . ''
                . 'DELETE  '
                . ' FROM #__work_galleries   '
                . ' WHERE  item_id=' . $id
                . '');


        $this->_db->execute();

        //deleteSkils

        $this->_db->setQuery(''
                . ''
                . 'DELETE '
                . ' FROM #__work_skils_map '
                . ' WHERE  type=1 AND  item_id=' . $id
                . '');
        $this->_db->execute();


        //deleteCategories

        $this->_db->setQuery(''
                . ''
                . 'DELETE'
                . ' FROM #__work_category_map   '
                . ' WHERE type=1 AND  item_id=' . $id
                . '');
        $this->_db->execute();
        
        return $ret;
    }

    public function deletePortfolioImage($id)
    {
        $this->_db->setQuery(''
                . ''
                . 'SELECT cm.id,cm.src '
                . ' FROM #__work_galleries AS cm '
                . ' INNER JOIN #__work_items as i on i.id=cm.item_id'
                . ' WHERE cm.id=' . $id . ' AND i.created_by=' . $this->get('id')
                . '');
        $image = $this->_db->loadObject();
        if (!empty($image))
        {
            unlink(JPATH_ROOT . DS . $image->src);
            $this->_db->setQuery(' delete from #__work_galleries where id=' . $id);
            return $this->_db->execute();
        }
        $this->setError(JText::_('image not found'));
        return false;
    }

}
