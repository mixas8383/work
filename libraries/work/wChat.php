<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of workApp
 *
 * @author Mixas
 */
class wChat extends JObject
{

    private $_db;
    private $_user;
    private $_avatarSize;
    private $_cometConnection;

    public function __construct($properties = null)
    {
        $this->_db = JFactory::getDbo();
        $this->_user = workFactory::getUser();
        $this->_avatarSize = 'image18';
         $this->_registerUserToComet();
    }

    public function getChats()
    {

        $chats = $this->_loadChats();
        return $chats;
    }

    private function _loadChats()
    {
        $this->_db->setQuery(''
                . ''
                . ' SELECT c1.*,u1.name , u1.surname '
                . ' FROM #__chats AS c1 '
                . ' LEFT JOIN #__users as u1 on u1.id=c1.user_2'
                . ' WHERE '
                . ' c1.user_1=' . $this->_user->get('id')
                . ' UNION '
                . ''
                . ' SELECT c2.* ,u2.name , u2.surname'
                . ' FROM #__chats AS c2 '
                . ' LEFT JOIN #__users as u2 on u2.id=c2.user_1'
                . ' WHERE '
                . ' c2.user_2=' . $this->_user->get('id')
                . ''
                . ''
                . '');
        $res = $this->_db->loadObjectList();
        return $this->_loadChatUsers($res);
    }

    private function _loadChatUsers($chats)
    {
        if (empty($chats))
        {
            return array();
        }
        $ret = array();

        foreach ($chats as $one)
        {
            $userId = 0;
            if ($one->user_1 == $this->_user->get('id'))
            {
                $userId = $one->user_2;
            }
            if ($one->user_2 == $this->_user->get('id'))
            {
                $userId = $one->user_1;
            }
            if (empty($userId))
            {
                continue;
            }
            unset($one->user_1);
            unset($one->user_2);
            $one->user = $userId;
            $one->chat_key = md5($one->id);
            $tmpUser = new stdClass;
            $tmpUser->id = $userId;
            $us = new wUser($tmpUser);
            $one->avatar = $us->getAvatar($this->_avatarSize);
            $ret[] = $one;
        }
        
        
        return $ret;
    }

    public function sendMessage($chatId, $message)
    {
        $chat = $this->_loaduserChatById($chatId);
        if (empty($chat))
        {
            throw new Exception(JText::_('chat not found'));
        }
        $toUser = $chat->user_1;
        if ($toUser == $this->_user->get('id'))
        {
            $toUser = $chat->user_2;
        }


        return $this->insertMessage($chatId, $message, $toUser);
    }

    private function _loaduserChatById($id)
    {
        $userId = $this->_user->get('id');
        $where = array();
        $where[] = 'c.id=' . $id;
        $where[] = 'c.user_1=' . $userId . ' OR c.user_2=' . $userId;
        $where = empty($where) ? '' : ' WHERE (' . implode(') AND (', $where) . ')';
        $this->_db->setQuery(''
                . ''
                . 'SELECT * '
                . ' FROM #__chats as c  '
                . ''
                . $where
                . ''
                . '');
        $chat = $this->_db->loadObject();
        if (empty($chat))
        {
            throw new Exception(JText::_('chat not found'));
        }
        return $chat;
    }

    private function _getCometConnetion()
    {
        if (!$this->_cometConnection)
        {
            $this->_cometConnection = mysqli_connect("comet.com", "root", "abramian", "CometQL_v1", 3300);
        }
        return $this->_cometConnection;
    }

    private function _registerUserToComet()
    {
        mysqli_query($this->_getCometConnetion(), "INSERT INTO users_auth (id, hash)VALUES (" . ((int) $this->_user->get('id')) . ", '" . md5($this->_user->get('id')) . "')");
    }

    private function insertMessage($chatId, $message, $toUser, $type = 0)
    {
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        $msg = '{"text":"'.$message.'","param2":"ещё что угодно"}';
        $link = $this->_getCometConnetion();
        $msg = array();
        $msg['user_id'] = $this->_user->get('id');
        $msg['message'] = $message;
        $q = mysqli_real_escape_string($this->_getCometConnetion(), json_encode($msg));
       $t = mysqli_query($this->_getCometConnetion(), "INSERT INTO users_messages (id, event, message)VALUES (" . $toUser . ", 'newMessage', '" . $q . "')");


//       
//        $commetDbo = workFactory::getCometDbo();
//        $commetDbo->setQuery(''
//                . ''
//                . "INSERT INTO pipes_messages (pipe_name, event, message)"
//                . " VALUES ('simplechat', 'newMessage', '" . $msg . "')"
//                . '');
//
//        $commetDbo->execute();
//        if (isset($_SERVER['HTTP_USER_AGENT']) && $_SERVER['HTTP_USER_AGENT'] == 'Debug')
//        {
//            echo '<pre>' . __FILE__ . ' -->>| <b> Line </b>' . __LINE__ . '</pre><pre>';
//            print_r($commetDbo);
//            die;
//        }
        $userId = $this->_user->get('id');
        $this->_db->setQuery(''
                . ''
                . 'INSERT INTO #__chats_messages (`chat_id`,`user_id`,`message`,`type`)'
                . 'VALUES'
                . '('
                . $chatId
                . ',' . $userId
                . ',' . $this->_db->quote($message)
                . ',' . $type
                . ''
                . ')'
                . '');
        if (!$this->_db->execute())
        {
            throw new Exception(JText::_('Cannot insert message'));
        }
        return $this->_db->insertid();
    }

    public function getChatMessages($chatId, $limitstart = 0)
    {
        $chat = $this->_loaduserChatById($chatId);
        if (empty($chat))
        {
            throw new Exception(JText::_('chat not found'));
        }


        $toUser = $chat->user_1;
        if ($toUser == $this->_user->get('id'))
        {
            $toUser = $chat->user_2;
        }

        

        return $this->getGroupedMessages($chatId, $limitstart);
    }

    public function getGroupedMessages($chatId, $limitstart)
    {

        $messages = $this->_loadChatMessages($chatId, $limitstart);
        if (empty($messages))
        {
            return array();
        }
        $messages = array_reverse($messages);
        $currUser = $messages[0]->user_id;
        $tmp = new stdClass;
        $tmp->userId = $currUser;
        $tmp->messages = array();
        $ret = array();
        $i=0;
        foreach ($messages as $one)
        {
            if ($one->user_id == $currUser)
            {
                $tmp->messages[] = $one;
            } else
            {
                $ret[]=$tmp;
                $currUser = $one->user_id;
                  $tmp = new stdClass;
                $tmp->userId = $currUser;
                $tmp->messages = array();
                $tmp->messages[] =$one;
            }
             $i++;
            if($i == count($messages))
            {
                 $ret[]=$tmp;
            }
                
               
        }
        return $ret;
    }

    private function _loadChatMessages($chatId, $limitstart)
    {
        $limit = 10;
        $where = array();
        $where[] = 'c.chat_id=' . $chatId;
        // $where[] = 'c.user_1=' . $userId . ' OR c.user_2=' . $userId;
        $where = empty($where) ? '' : ' WHERE (' . implode(') AND (', $where) . ')';
        $this->_db->setQuery(''
                . ''
                . 'SELECT * '
                . ' FROM #__chats_messages as c  '
                . ''
                . $where
                . ''
                . ''
                . ' ORDER BY id desc'
                . ' LIMIT ' . $limitstart . ',' . $limit
                . '');
        $messages = $this->_db->loadObjectList();
        return $messages;
    }

}
