<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of workFactory
 *
 * @author Mixas
 */
if (!class_exists('wUser'))
{
    JLoader::import('work.user.wUser');
}
if (!class_exists('wApp'))
{
    JLoader::import('work.wApp');
}

class workFactory extends JFactory
{

    public static $databaseComet = null;

    protected static function creatUser()
    {
        if (!class_exists('wUser'))
        {
            JLoader::import('work.user.wUser');
        }
        return new wUser();
    }

    protected static function creatApp()
    {
        if (!class_exists('wApp'))
        {
            JLoader::import('work.wApp');
        }
        return new wApp();
    }

    protected static function _creatChat()
    {
        if (!class_exists('wChat'))
        {
            JLoader::import('work.wChat');
        }
        return new wChat();
    }

    public static function getUser($id = null)
    {
        static $user;
        if (!$user)
        {
            $user = self::creatUser();
        }
        return $user;
    }

    public static function getApp($id = null)
    {
        static $app;
        if (!$app)
        {
            $app = self::creatApp();
        }
        return $app;
    }

    public static function getChat($id = null)
    {
        static $chat;
        if (!$chat)
        {
            $chat = self::_creatChat();
        }
        return $chat;
    }

    protected static function createComet()
    {
        $conf = JFactory::getConfig();

        $host = 'comet.com:3300';
        $user = 'root';
        $password = 'abramian';
        $database = 'CometQL_v1';
        $prefix = '';
        $driver = $conf->get('dbtype');
        $debug = $conf->get('debug');

        $options = array('driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $password, 'database' => $database, 'prefix' => $prefix);

        try
        {
            $db = JDatabaseDriver::getInstance($options);
        } catch (RuntimeException $e)
        {
            if (!headers_sent())
            {
                header('HTTP/1.1 500 Internal Server Error');
            }

            jexit('Database Error: ' . $e->getMessage());
        }

        $db->setDebug($debug);

        return $db;
    }

    public static function getCometDbo()
    {
        if (!self::$databaseComet)
        {
            self::$databaseComet = self::createComet();
        }

        return self::$databaseComet;
    }

}
