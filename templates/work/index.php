<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/** @var JDocumentHtml $this */
$app = JFactory::getApplication();
$user = JFactory::getUser();

// Output as HTML5
$this->setHtml5(true);


// Getting params from template
$params = $app->getTemplate(true)->params;

// Detecting Active Variables
$option = $app->input->getCmd('option', '');
$view = $app->input->getCmd('view', '');
$layout = $app->input->getCmd('layout', '');
$task = $app->input->getCmd('task', '');
$itemid = $app->input->getCmd('Itemid', '');
$sitename = $app->get('sitename');
$menu = $app->getMenu();
$default = $menu->getDefault();
$activeMenu = $menu->getActive();

$isHomePage = ($default->id == $activeMenu->id ) ? true : false;



if ($task === 'edit' || $layout === 'form')
{
    $fullWidth = 1;
} else
{
    $fullWidth = 0;
}


// Logo file or site title param
if ($this->params->get('logoFile'))
{
    $logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
} elseif ($this->params->get('sitetitle'))
{
    $logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle'), ENT_COMPAT, 'UTF-8') . '</span>';
} else
{
    $logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
}


$document = JFactory::getDocument();
$document->addScriptDeclaration(''
        . ''
        . ';var chatUserId=' . $user->id . ';'
        . ';var chatUserHash="' . md5($user->id) . '";'
        . ''
        . '');
unset($document->_scripts['/media/jui/js/bootstrap.js']);
?>
<!DOCTYPE html>
<html lang="en"><head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery-2.js"></script> 

    <jdoc:include type="head" />


    <!-- Fonts Online -->
    <link href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/css_002.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/css.css" rel="stylesheet" type="text/css">

    <!-- Style Sheet -->
    <link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/owl.css">
    <link rel="stylesheet" href="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/main-style_1.css">

    <link rel = "stylesheet" href = "<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/style.css">
    <link rel = "stylesheet" href = "<?php echo $this->baseurl . '/templates/' . $this->template; ?>/css/chosen.min.css">
    <link rel = "stylesheet" href = "<?php echo $this->baseurl ?>/app/main.css">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style id="style-1-cropbar-clipper">/* Copyright 2014 Evernote Corporation. All rights reserved. */
        .en-markup-crop-options {
            top: 18px !important;
            left: 50% !important;
            margin-left: -100px !important;
            width: 200px !important;
            border: 2px rgba(255,255,255,.38) solid !important;
            border-radius: 4px !important;
        }

        .en-markup-crop-options div div:first-of-type {
            margin-left: 0px !important;
        }
    </style>



</head>

<body ng-app="wapp">
    <div id="main-wrapper"> 

        <!-- Top Toolbar -->
        <div class="toolbar">
            <div class="uou-block-1a blog">
                <div class="container">
                    <jdoc:include type="modules" name="top-search" style="none" />
                    <jdoc:include type="modules" name="top-socials" style="none" />
                    <jdoc:include type="modules" name="top-login" style="none" />



                    <div class="language">

                        <m-chat></m-chat>

                        <!--                       
                        <a href="#" class="toggle">
                            <img src="<?php echo $this->baseurl . '/images/'; ?>/theme/US.png" alt=""> ENG
                        </a>
                        <ul>
                            <li>
                                <a href="#">

                                    <img src="<?php echo $this->baseurl . '/images/'; ?>/theme/PT.png" alt=""> ENG
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $this->baseurl . '/images/'; ?>/theme/FR.png" alt=""> FR
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo $this->baseurl . '/images/'; ?>/theme/ES.png" alt=""> ES
                                </a>
                            </li>
                        </ul>
                        -->
                    </div>

                </div>
            </div>
            <!-- end .uou-block-1a --> 
        </div>
        <!-- end toolbar -->

        <div class="box-shadow-for-ui">
            <div class="uou-block-2b">
                <div class="container">
                    <a href="<?php echo JUri::root() ?>" class="logo">
                        <?php echo $logo; ?>
                    </a> 
                    <a href="#" class="mobile-sidebar-button mobile-sidebar-toggle"><span></span></a>
                    <nav class="nav">
                        <jdoc:include type="modules" name="main-menu" style="none" />
                    </nav>
                </div>
            </div>
            <!-- end .uou-block-2b --> 
        </div>

        <?php
        if ($isHomePage)
        {
            ?>

            <!-- HOME PRO-->
            <div class="home-pro"> 

                <!-- PRO BANNER HEAD -->
                <div class="banr-head">
                    <div class="container">
                        <div class="row"> 
                            <!-- CONTENT -->
                            <div class="col-sm-7">
                                <div class="text-area">
                                    <jdoc:include type="modules" name="promo-text" style="none" />
                                </div>
                            </div>
                            <!-- FORM SECTION -->
                            <div class="col-sm-5">
                                <div class="login-sec"> 
                                    <!-- TABS -->
                                    <jdoc:include type="modules" name="main-page-login" style="none" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- SERVICES -->
                <section class="services"> 
                    <!-- SERVICES ROW -->
                    <jdoc:include type="modules" name="main-page-services" style="none" />
                </section>

                <!-- PRO SECTION -->
                <section class="pro-content">
                    <div class="container-fluid">
                        <jdoc:include type="modules" name="pro-content-1" style="none" />
                    </div>

                    <!-- PRO SECTION -->
                    <div class="container-fluid">
                        <jdoc:include type="modules" name="pro-content-2" style="none" />
                    </div>
                </section>

                <!-- APP IMAGE -->
                <section>
                    <jdoc:include type="modules" name="app-image" style="none" />
                </section>

                <!-- TESTIMONIALS -->
                <section class="clients-says">
                    <div class="container">
                        <jdoc:include type="modules" name="clients-says" style="titleh3" />
                    </div>
                </section>


                <!-- sponsors -->
                <div class="sponsors" style="background:url(images/sponser-bg.jpg) no-repeat;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <jdoc:include type="modules" name="sponsors" style="titleh3" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END HOME PRO-->
            <?php
        } else
        {
            ?>
            <jdoc:include type="modules" name="before-component" style="none" />
            <jdoc:include type="message" />
            <jdoc:include type="component" />


            <?php
        }
        ?>






    </div>
</div>
</div>

<!-- Footer -->
<div class="uou-block-4e">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6"> 
                <jdoc:include type="modules" name="contact-info" style="none" />
            </div>
            <div class="col-md-3 col-sm-6">
                <jdoc:include type="modules" name="twitter-feed" style="titleh5" />
            </div>
            <div class="col-md-3 col-sm-6">
                <jdoc:include type="modules" name="photo-stream" style="titleh5" />
            </div>
            <div class="col-md-3 col-sm-6">
                <jdoc:include type="modules" name="newsletter" style="titleh5" />
            </div>
        </div>
    </div>
</div>
<!-- end .uou-block-4e -->

<div class="uou-block-4a secondary dark">
    <div class="container">
        <jdoc:include type="modules" name="privacy-terms" style="none" />
        <jdoc:include type="modules" name="copyright" style="none" />
    </div>
</div>
<!-- end .uou-block-4a -->

<div class="uou-block-11a">
    <h5 class="title">Menu</h5>
    <a href="#" class="mobile-sidebar-close">×</a>
    <nav class="main-nav">
        <ul>
            <li class="active"><a href="http://new.uouapps.com/tiger-html/index.html">Index</a></li>
            <li> <a href="http://new.uouapps.com/tiger-html/listing-filter.html">Professionals</a> </li>
            <li><a href="http://new.uouapps.com/tiger-html/profile_company.html">Profile Company</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/profile_company-no-tabs.html">Profile Company No Tabs</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/user-dashboard%28connections%29%28hotkeys-disabled%29.html">User Dashboard 1</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/user-dashboard%28connections%29%28hotkeys-enabled%29.html">User Dashboard 2</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/user-dashboard%28followers%29.html">User Dashboard 3</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/user-dashboard%28following%29.html">User Dashboard 4</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/blog-post.html">Blog Post</a></li>
            <li> <a href="http://new.uouapps.com/tiger-html/user-profile%28layout-1%29.html">User Profile</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/blog.html">Blog</a></li>
            <li><a href="http://new.uouapps.com/tiger-html/gui-kit.html">GUI KIT</a></li>
        </ul>
    </nav>
    <hr>
</div>

<!-- Scripts --> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/bootstrap.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/superfish.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery_002.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/rangeslider.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/jquery.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/uou-accordions.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/uou-tabs.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/select2.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/owl.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/scripts.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/fileuploader/jquery.ui.widget.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/fileuploader/jquery.iframe-transport.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/fileuploader/jquery.fileupload.js"></script> 
<script src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/fileuploader/fileuploadermake.js"></script> 
<script type="text/javascript" charset="UTF-8" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/validator/jquery.validate.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/validator/additional-methods.min.js"></script>


<script type="text/javascript" charset="UTF-8" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/validator/localization/messages_ka.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo $this->baseurl . '/templates/' . $this->template; ?>/js/custom.js"></script>
<script type="text/javascript" charset="UTF-8" src="<?php echo $this->baseurl; ?>/app/js/src.js"></script>

<script>

</script>

</body>
</html>