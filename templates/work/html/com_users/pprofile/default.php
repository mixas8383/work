<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
$user = $this->data;

$wUser = new wUser($user);
JLoader::import('wslib.media.multipic');
$wsPic = new wsMultiPic();
$wsPic->setCacheDir('pictures');
?>
<?php if ($this->params->get('show_page_heading')) : ?>
    <div class="page-header">
        <h1>
            <?php echo $this->escape($this->params->get('page_heading')); ?>
        </h1>
    </div>





<?php endif; ?>
<!-- Compny Profile -->
<div class="compny-profile"> 
    <!-- SUB Banner -->
    <div class="profile-bnr user-profile-bnr">
        <div class="container">
            <div class="pull-left">
                <h2><?php echo $wUser->getFullName() ?></h2>
                <h5>Front-End Developer</h5>
            </div>

            <!-- Top Riht Button -->
            <div class="right-top-bnr">
                <div class="connect">
                    <a href="#." data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-user-plus"></i> Connect
                    </a> 
                    <a href="#.">
                        <i class="fa fa-share-alt"></i> Share
                    </a>
                    <div class="bt-ns"> 
                        <a href="#.">
                            <i class="fa fa-bookmark-o"></i> 
                        </a> 
                        <a href="#.">
                            <i class="fa fa-envelope-o"></i> 
                        </a> 
                        <a href="#.">
                            <i class="fa fa-exclamation"></i> 
                        </a> 
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal POPUP -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="container">
                        <h6>
                            <a class="close" href="#." data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </a> Send Message
                        </h6>

                        <!-- Forms -->
                        <form action="#">
                            <ul class="row">
                                <li class="col-xs-6">
                                    <input placeholder="First Name " type="text">
                                </li>
                                <li class="col-xs-6">
                                    <input placeholder="Last Name" type="text">
                                </li>
                                <li class="col-xs-6">
                                    <input placeholder="Country" type="text">
                                </li>
                                <li class="col-xs-6">
                                    <input placeholder="Email" type="text">
                                </li>
                                <li class="col-xs-12">
                                    <textarea placeholder="Your Message"></textarea>
                                </li>
                                <li class="col-xs-12">
                                    <button class="btn btn-primary">Send message</button>
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Profile Company Content -->
    <div class="profile-company-content user-profile main-user" data-bg-color="f5f5f5">
        <div class="container">
            <div class="row"> 

                <!-- Nav Tabs -->
                <div class="col-md-12">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#profile">Profile</a></li>
                        <li><a role="tab" data-toggle="tab" href="#jobs"><?php echo JText::_('offer') ?></a></li>
                        <li><a role="tab" data-toggle="tab" href="#contact">Contact</a></li>
                        <li><a role="tab" data-toggle="tab" href="#portfolio"><?php echo JText::_('Portfolio') ?></a></li>
                        <li><a role="tab" data-toggle="tab" href="#blog-tab">Blog Posts</a></li>
                        <li><a role="tab" data-toggle="tab" href="#Con-tab">Connections</a></li>
                        <li><a role="tab" data-toggle="tab" href="#flowrs-tabs">Followers (<?php echo $wUser->getFollowersCount(); ?>)</a></li>
                        <li><a role="tab" data-toggle="tab" href="#foll-tabs">Following (<?php echo $wUser->getFollowingCount() ?>)</a></li>
                    </ul>
                </div>

                <!-- Tab Content -->
                <div class="col-md-12">
                    <div class="tab-content"> 

                        <!-- PROFILE -->
                        <div id="profile" role="tabpanel" class="tab-pane fade  active">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="profile-main">
                                        <h3>About</h3>
                                        <div class="profile-in">
                                            <div class="media-left">
                                                <?php
                                                $image = $wUser->getAvatar('image19');
                                                if (empty($image))
                                                {
                                                    $image = $wUser->getDefaultImage('image19');
                                                }
                                                ?>
                                                <div class="img-profile">
                                                    <img class="media-object" src="<?php echo $image; ?>" alt="">
                                                </div>
                                            </div>
                                            <div class="media-body">
                                                <p>Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Suscipit, maxime, excepturi, mollitia, voluptatibus 
                                                    similique aliquidautem 
                                                    laudantium sapiente ad enim ipsa modi labo 
                                                    rum accusantium deleniti neque architecto vitae.<br>
                                                    <br>
                                                    Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Ea, nihil, dolores, culpa ullam vero ipsum placeat 
                                                    accusamus nemo
                                                    itate id molestiae consectetur quae pariatur
                                                    repudi andae vel ex quaerat nam iusto aliquid laborum quia adipisci aut
                                                    ut imcati 
                                                    nisi deleniti tempore maxime sequi fugit 
                                                    reiciendis libero quo. Rerum, assumenda. <br>
                                                    <br>
                                                    Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Exercitationem, at nemo inventore temporibus corporis 
                                                    suscipit.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8"> 

                                    <!-- Skills -->
                                    <div class="sidebar">
                                        <h5 class="main-title">Skills</h5>
                                        <div class="job-skills"> 

                                            <!-- Logo Design -->
                                            <ul class="row">
                                                <li class="col-sm-3">
                                                    <h6><i class="fa fa-plus"></i> HTML5 and Css3</h6>
                                                </li>
                                                <li class="col-sm-9">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> </div>
                                                    </div>
                                                </li>
                                            </ul>

                                            <!-- Logo Design -->
                                            <ul class="row">
                                                <li class="col-sm-3">
                                                    <h6><i class="fa fa-plus"></i> Logo Design</h6>
                                                </li>
                                                <li class="col-sm-9">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;"> </div>
                                                    </div>
                                                </li>
                                            </ul>

                                            <!-- Logo Design -->
                                            <ul class="row">
                                                <li class="col-sm-3">
                                                    <h6><i class="fa fa-plus"></i> Web Design</h6>
                                                </li>
                                                <li class="col-sm-9">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 90%;"> </div>
                                                    </div>
                                                </li>
                                            </ul>

                                            <!-- UI / UX -->
                                            <ul class="row">
                                                <li class="col-sm-3">
                                                    <h6><i class="fa fa-plus"></i> UI/UX</h6>
                                                </li>
                                                <li class="col-sm-9">
                                                    <div class="progress">
                                                        <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 80%;"> </div>
                                                    </div>
                                                    <p>Preferred languages are Arabic, French &amp; Italian. Proin nibh augue, suscipit asce lerisque sed, lacinia in, mi.</p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- Professional Details -->
                                    <div class="sidebar">
                                        <h5 class="main-title">Similar Professionals</h5>

                                        <!-- Similar -->
                                        <div class="similar">
                                            <div class="media">
                                                <div class="media-left">
                                                    <div class="inn-simi"> <img class="media-object" src="TIGER%20-%20Social%20Network%20HTML%20Template_files/med-avatar.jpg" alt=""> <a href="#">Profile </a> </div>
                                                </div>
                                                <div class="media-body">
                                                    <h5>Media heading</h5>
                                                    <p>SEO Specialist - New York, USA</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, 
                                                        voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum 
                                                        accusantium deleniti neque architecto vitae.</p>

                                                    <!-- Share -->
                                                    <div class="share-w"><a href="#."><i class="fa fa-bookmark-o"></i></a> <a href="#."><i class="fa fa-envelope-o"></i></a> <a href="#."><i class="fa fa-eye"></i></a></div>
                                                </div>
                                            </div>

                                            <!-- Similar -->
                                            <div class="media">
                                                <div class="media-left">
                                                    <div class="inn-simi"> <img class="media-object" src="TIGER%20-%20Social%20Network%20HTML%20Template_files/med-avatar.jpg" alt=""> <a href="#">Profile </a> </div>
                                                </div>
                                                <div class="media-body">
                                                    <h5>Denise Walsh</h5>
                                                    <p>SEO Specialist - New York, USA</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, 
                                                        voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum 
                                                        accusantium deleniti neque architecto vitae.</p>

                                                    <!-- Share -->
                                                    <div class="share-w"><a href="#."><i class="fa fa-bookmark-o"></i></a> <a href="#."><i class="fa fa-envelope-o"></i></a> <a href="#."><i class="fa fa-eye"></i></a></div>
                                                </div>
                                            </div>

                                            <!-- Similar -->
                                            <div class="media">
                                                <div class="media-left">
                                                    <div class="inn-simi"> <img class="media-object" src="TIGER%20-%20Social%20Network%20HTML%20Template_files/med-avatar.jpg" alt=""> <a href="#">Profile </a> </div>
                                                </div>
                                                <div class="media-body">
                                                    <h5>Denise Walsh</h5>
                                                    <p>SEO Specialist - New York, USA</p>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Suscipit, maxime, excepturi, mollitia, 
                                                        voluptatibus similique aliquid a dolores autem laudantium sapiente ad enim ipsa modi laborum 
                                                        accusantium deleniti neque architecto vitae.</p>

                                                    <!-- Share -->
                                                    <div class="share-w"><a href="#."><i class="fa fa-bookmark-o"></i></a> <a href="#."><i class="fa fa-envelope-o"></i></a> <a href="#."><i class="fa fa-eye"></i></a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Col -->
                                <div class="col-md-4"> 

                                    <!-- Professional Details -->
                                    <div class="sidebar">
                                        <h5 class="main-title">Professional Details</h5>
                                        <div class="sidebar-information">
                                            <ul class="single-category">
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Name</h6>
                                                    <span class="subtitle col-xs-6">Abu Antar</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Age</h6>
                                                    <span class="subtitle col-xs-6">38 Years Old</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Location</h6>
                                                    <span class="subtitle col-xs-6">Jordan Amman</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Experiance</h6>
                                                    <span class="subtitle col-xs-6">15 Years</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Dgree</h6>
                                                    <span class="subtitle col-xs-6">MBA</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Career Lavel</h6>
                                                    <span class="subtitle col-xs-6">Mid-Level</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Phone</h6>
                                                    <span class="subtitle col-xs-6">(800) 123-4567</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Fax </h6>
                                                    <span class="subtitle col-xs-6">(800) 123-4568</span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">E-mail</h6>
                                                    <span class="subtitle col-xs-6"><a href="#.">example@example.com</a></span></li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Website</h6>
                                                    <span class="subtitle col-xs-6"><a href="#.">example.com </a></span></li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- Rating -->
                                    <div class="sidebar">
                                        <h5 class="main-title">Rating</h5>
                                        <div class="sidebar-information">
                                            <ul class="single-category com-rate">
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Expertise:</h6>
                                                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></span> </li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Knowledge:</h6>
                                                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i></span> </li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Quality::</h6>
                                                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></span> </li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Price:</h6>
                                                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i> <i class="fa fa-star-o"></i></span> </li>
                                                <li class="row">
                                                    <h6 class="title col-xs-6">Services:</h6>
                                                    <span class="col-xs-6"><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star-o"></i></span> </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- Social Profiles -->
                                    <div class="sidebar">
                                        <h5 class="main-title">Social Profiles</h5>
                                        <ul class="socil">
                                            <li><a href="#."><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="#."><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="#."><i class="fa fa-linkedin"></i></a></li>
                                            <li><a href="#."><i class="fa fa-twitter"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Jobs -->
                        <div id="jobs" role="tabpanel" class="tab-pane fade">
                            <div class="header-listing">
                                <h6>Sort by</h6>
                                <div class="custom-select-box">
                                    <select name="order" class="custom-select select2-hidden-accessible" tabindex="-1" aria-hidden="true">
                                        <option value="0" selected="selected">Most popular</option>
                                        <option value="1">The latest</option>
                                        <option value="2">The best rating</option>
                                    </select><span class="select2 select2-container select2-container--default" dir="ltr" style="width: 100px;"><span class="selection"><span class="select2-selection select2-selection--single" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false" tabindex="0" aria-labelledby="select2-order-53-container"><span class="select2-selection__rendered" id="select2-order-53-container" title="Most popular">Most popular</span><span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span></span></span><span class="dropdown-wrapper" aria-hidden="true"></span></span>
                                </div>
                                <ul class="listing-views">
                                    <li class="active"><a href="#"><i class="fa fa-list"></i></a></li>
                                    <li><a href="#"><i class="fa fa-th"></i></a></li>
                                    <li><a href="#"><i class="fa fa-th-large"></i></a></li>
                                </ul>
                            </div>
                            <div class="listing listing-1">
                                <div class="listing-section">
                                    <div class="listing-ver-3">
                                        <div class="listing-heading">
                                            <h5>Front-End Web Developer</h5>
                                            <ul class="bookmark list-inline">
                                                <li><a href="#"><i class="fa fa-bookmark"></i></a></li>
                                                <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="listing-inner">
                                            <div class="listing-content">
                                                <h6 class="title-company">Mars Planet Telecommunications Inc.</h6>
                                                <span class="location"> <i class="fa fa-map-marker"></i> Manhattan, New york, USA </span> <span class="type-work full-time"> Full Time </span>
                                                <p>Proin gravida nibh vel velit auctor aliquet
                                                    aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat 
                                                    ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh 
                                                    vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec 
                                                    tellus a odio tincidunt auctor a ornare odio pellentesque habitant morbi
                                                    tristique senectus et netus et malesuada. <a href="http://new.uouapps.com/tiger-html/single_job.html">read more</a></p>
                                                <h6 class="title-tags">Skills required:</h6>
                                                <ul class="tags list-inline">
                                                    <li><a href="#">Javascript</a></li>
                                                    <li><a href="#">Wordpress</a></li>
                                                    <li><a href="#">Presta</a></li>
                                                    <li><a href="#">Sass</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="listing-tabs">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-envelope"></i> email@mail.com</a></li>
                                                <li><a href="#"><i class="fa fa-phone"></i> 012 345 678</a></li>
                                                <li><a href="#"><i class="fa fa-globe"></i> www.webstite.com</a></li>
                                                <li class="share-button"> <a href="#"><i class="fa fa-share"></i> Share</a>
                                                    <div class="contact-share">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="listing-ver-3">
                                        <div class="listing-heading">
                                            <h5>Front-End Web Developer</h5>
                                            <ul class="bookmark list-inline">
                                                <li><a href="#"><i class="fa fa-bookmark"></i></a></li>
                                                <li><a href="#"><i class="fa fa-eye"></i></a></li>
                                                <li><a href="#"><i class="fa fa-share"></i></a></li>
                                            </ul>
                                        </div>
                                        <div class="listing-inner">
                                            <div class="listing-content">
                                                <h6 class="title-company">Mars Planet Telecommunications Inc.</h6>
                                                <span class="location"> <i class="fa fa-map-marker"></i> Manhattan, New york, USA </span> <span class="type-work full-time"> Full Time </span>
                                                <p>Proin gravida nibh vel velit auctor aliquet
                                                    aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat 
                                                    ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh 
                                                    vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec 
                                                    tellus a odio tincidunt auctor a ornare odio pellentesque habitant morbi
                                                    tristique senectus et netus et malesuada. <a href="http://new.uouapps.com/tiger-html/single_job.html">read more</a></p>
                                                <h6 class="title-tags">Skills required:</h6>
                                                <ul class="tags list-inline">
                                                    <li><a href="#">Javascript</a></li>
                                                    <li><a href="#">Wordpress</a></li>
                                                    <li><a href="#">Presta</a></li>
                                                    <li><a href="#">Sass</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="listing-tabs">
                                            <ul>
                                                <li><a href="#"><i class="fa fa-envelope"></i> email@mail.com</a></li>
                                                <li><a href="#"><i class="fa fa-phone"></i> 012 345 678</a></li>
                                                <li><a href="#"><i class="fa fa-globe"></i> www.webstite.com</a></li>
                                                <li class="share-button"> <a href="#"><i class="fa fa-share"></i> Share</a>
                                                    <div class="contact-share">
                                                        <ul>
                                                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
                                                            <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Contact -->
                        <div  role="tabpanel" id="contact" class="tab-pane fade">
                            <div class="profile-main">
                                <h3>Contact the Company</h3>
                                <div class="profile-in">
                                    <p>Lorem ipsum dolor sit amet, consectetur 
                                        adipisicing elit. Voluptate quis tenetur velit! Provident eum molestias 
                                        aperiam suscipit distinctio ipsum cupiditate quasi, dolor sunt, cum 
                                        reprehenderit quibusdam, repellendus eaque, quas magni.</p>
                                    <form action="#">
                                        <input placeholder="Name &amp; Surname" type="text">
                                        <input placeholder="E-mail address" type="text">
                                        <input placeholder="Phone Number" type="text">
                                        <textarea placeholder="Your Message"></textarea>
                                        <button class="btn btn-primary">Send message</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <!-- Portfolio -->
                        <?php
                        $portfolio = $wUser->getPortfolioItems();
                        ?>
                        <div  role="tabpanel" id="portfolio" class="tab-pane fade">
                            <div class="profile-main">



                                <h3><?php echo JText::_('Portfolio'); ?></h3>

                                <div class="profile-in">
                                    <div class="uou-portfolio"> 
                                        <div class="row">
                                            <?php
                                            if (!empty($portfolio))
                                            {
                                                foreach ($portfolio as $one)
                                                {
                                                 $image =    $wsPic->getImage('image22',$one->src);
                                                    ?>

                                                    <div class="col-sm-4">
                                                        <article class="uou-block-7g" data-badge-color="0099ff">
                                                            <img class="image" src="<?php echo $image; ?>" alt="">
<!--                                                            <span class="badge" style="background-color: rgb(0, 153, 255);">In The Lab</span>-->
                                                            <div class="content">
                                                                <span class="date"><time datetime="<?php echo JHtml::_('date', $one->created, 'c'); ?>" itemprop="dateCreated">
                                    <?php echo JHtml::_('date', $one->created, JText::_('DATE_FORMAT_LC')); ?>
                                </time></span>
                                                                <h1><?php echo $one->name; ?></h1>
                                                                <p><?php echo $one->description; ?></p>
                                                            </div>
                                                        </article>
                                                        <!-- end .uou-block-7g --> 
                                                    </div>     
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </div>
                                        <!-- Portfolio Item -->
<!--                                        <section class="portfolio">
                                            <div class="portfolio-filters-content"> 
                                                 Portfolio Item 
                                                <article class="development design"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-1.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-1.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="design"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-2.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-2.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="development branding"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-3.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-3.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="development design"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-6.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-6.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="branding development"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-4.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-4.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="mobile branding"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-5.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-5.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="branding"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-7.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-7.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="branding"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-2.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-2.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                                 Portfolio Item 
                                                <article class="design development"> <a href="http://new.uouapps.com/tiger-html/img/portfolio-8.jpg" class="swipebox"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/portfolio-8.jpg" alt="" class="work img-responsive"> <span class="overlay"> <i class="fa fa-plus"></i> <b class="title"><strong>Photo Session</strong>Brands</b> </span> </a> </article>
                                            </div>
                                        </section>-->
                                    </div>
                                    <!-- end uou-portfolio --> 
                                </div>
                            </div>
                        </div>

                        <!-- Blog Post -->
                        <div role="tabpanel"  id="blog-tab" class="tab-pane fade">
                            <div class="profile-main">
                                <h3>Blog Post</h3>
                                <div class="profile-in">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <article class="uou-block-7f"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/blog-image-1.jpg" alt="" class="thumb">
                                                <div class="meta"> <span class="time-ago">3 days ago</span> <span class="category">Posted in: <a href="#">Design</a></span> <a href="#" class="comments">12 Comments</a> </div>
                                                <h1><a href="#">Perspiciatis Sint Pariatur Velit Corrupti</a></h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia
                                                    in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus 
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.</p>
                                                <a href="#" class="btn btn-small btn-primary">Read More</a> </article>
                                            <!-- end .uou-block-7f -->

                                            <article class="uou-block-7f"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/blog-image-2.jpg" alt="" class="thumb">
                                                <div class="meta"> <span class="time-ago">3 days ago</span> <span class="category">Posted in: <a href="#">Design</a></span> <a href="#" class="comments">12 Comments</a> </div>
                                                <h1><a href="#">Perspiciatis Sint Pariatur Velit Corrupti</a></h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia
                                                    in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus 
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.</p>
                                                <a href="#" class="btn btn-small btn-primary">Read More</a> </article>
                                            <!-- end .uou-block-7f -->

                                            <article class="uou-block-7f"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/blog-image-3.jpg" alt="" class="thumb">
                                                <div class="meta"> <span class="time-ago">3 days ago</span> <span class="category">Posted in: <a href="#">Design</a></span> <a href="#" class="comments">12 Comments</a> </div>
                                                <h1><a href="#">Perspiciatis Sint Pariatur Velit Corrupti</a></h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia
                                                    in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus 
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.</p>
                                                <a href="#" class="btn btn-small btn-primary">Read More</a> </article>
                                            <!-- end .uou-block-7f -->

                                            <article class="uou-block-7f"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/blog-image-4.jpg" alt="" class="thumb">
                                                <div class="meta"> <span class="time-ago">3 days ago</span> <span class="category">Posted in: <a href="#">Design</a></span> <a href="#" class="comments">12 Comments</a> </div>
                                                <h1><a href="#">Perspiciatis Sint Pariatur Velit Corrupti</a></h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia
                                                    in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus 
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.</p>
                                                <a href="#" class="btn btn-small btn-primary">Read More</a> </article>
                                            <!-- end .uou-block-7f -->

                                            <article class="uou-block-7f"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/blog-image-5.jpg" alt="" class="thumb">
                                                <div class="meta"> <span class="time-ago">3 days ago</span> <span class="category">Posted in: <a href="#">Design</a></span> <a href="#" class="comments">12 Comments</a> </div>
                                                <h1><a href="#">Perspiciatis Sint Pariatur Velit Corrupti</a></h1>
                                                <p>Lorem ipsum dolor sit amet, consectetur 
                                                    adipisicing elit. Proin nibh augue, suscipit a, scelerisque sed, lacinia
                                                    in, mi. Cras vel lorem. Etiam pellentesque aliquet tellus. Phasellus 
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.
                                                    Etiam pellentesque aliquet tellus. Phasellus
                                                    pharetra nulla ac diam.</p>
                                                <a href="#" class="btn btn-small btn-primary">Read More</a> </article>
                                            <!-- end .uou-block-7f -->

                                            <div class="text-center pt20">
                                                <ul class="uou-paginatin list-unstyled">
                                                    <li class="active"><a href="#">1</a></li>
                                                    <li><a href="#">2</a></li>
                                                    <li><a href="#">3</a></li>
                                                    <li><a href="#">4</a></li>
                                                    <li><a href="#">5</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end row --> 

                                    <!-- end blog-content --> 

                                </div>
                            </div>
                        </div>

                        <!-- Blog Post -->
                        <div  role="tabpanel" id="Con-tab" class="tab-pane fade">
                            <div class="profile-main">
                                <h3>People in Connections</h3>
                                <div class="profile-in">
                                    <div class="folow-persons">
                                        <ul>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row">
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-1" class="styled" type="checkbox">
                                                            <label for="checkbox1-1"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row"> 
                                                    <!-- Title -->
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-2" class="styled" type="checkbox">
                                                            <label for="checkbox1-2"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row"> 
                                                    <!-- Title -->
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-3" class="styled" type="checkbox">
                                                            <label for="checkbox1-3"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row"> 
                                                    <!-- Title -->
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-4" class="styled" type="checkbox">
                                                            <label for="checkbox1-4"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row"> 
                                                    <!-- Title -->
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-5" class="styled" type="checkbox">
                                                            <label for="checkbox1-5"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row"> 
                                                    <!-- Title -->
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-6" class="styled" type="checkbox">
                                                            <label for="checkbox1-6"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>

                                            <!-- MEMBER -->
                                            <li>
                                                <div class="row"> 
                                                    <!-- Title -->
                                                    <div class="col-xs-4"> 
                                                        <!-- Check Box -->
                                                        <div class="checkbox">
                                                            <input id="checkbox1-7" class="styled" type="checkbox">
                                                            <label for="checkbox1-7"></label>
                                                        </div>
                                                        <!-- Name -->
                                                        <div class="fol-name">
                                                            <div class="avatar"> <img src="TIGER%20-%20Social%20Network%20HTML%20Template_files/sm-avatar.jpg" alt=""> </div>
                                                            <h6>Collin Weiland</h6>
                                                            <span>Web Developer</span> </div>
                                                    </div>
                                                    <!-- Location -->
                                                    <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                    <!-- Network -->
                                                    <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                    <!-- Connections -->
                                                    <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Blog Post -->
                        <div  role="tabpanel" id="flowrs-tabs" class="tab-pane fade">
                            <div class="profile-main">
                                <h3>Followers</h3>
                                <div class="profile-in">
                                    <div class="folow-persons">
                                        <ul>
                                            <?php
                                            $followers = $wUser->getFollowers();
                                            if (!empty($followers))
                                            {
                                                foreach ($followers as $one)
                                                {
                                                    ?>
                                                    <!-- MEMBER -->
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-4"> 
                                                                <!-- Check Box -->
                                                                <div class="checkbox">
                                                                    <input id="checkbox2-1" class="styled" type="checkbox">
                                                                    <label for="checkbox2-1"></label>
                                                                </div>
                                                                <!-- Name -->
                                                                <div class="fol-name">
                                                                    <div class="avatar"> <img src="<?php echo $one->getAvatar('image18') ? $one->getAvatar('image18') : $one->getDefaultImage('image18'); ?>" alt=""> </div>
                                                                    <h6><?php echo $one->getFullname(); ?></h6>
                                                                    <span>Web Developer</span> </div>
                                                            </div>
                                                            <!-- Location -->
                                                            <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                            <!-- Network -->
                                                            <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                            <!-- Connections -->
                                                            <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                        </div>
                                                    </li>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Blog Post -->
                        <div  role="tabpanel" id="foll-tabs" class="tab-pane fade">
                            <div class="profile-main">
                                <h3>Following</h3>
                                <div class="profile-in">
                                    <div class="folow-persons">
                                        <ul>
                                            <?php
                                            $following = $wUser->getFollowing();
                                            if (!empty($following))
                                            {
                                                foreach ($following as $one)
                                                {
                                                    ?>
                                                    <!-- MEMBER -->
                                                    <li>
                                                        <div class="row">
                                                            <div class="col-xs-4"> 
                                                                <!-- Check Box -->
                                                                <div class="checkbox">
                                                                    <input id="checkbox2-1" class="styled" type="checkbox">
                                                                    <label for="checkbox2-1"></label>
                                                                </div>
                                                                <!-- Name -->
                                                                <div class="fol-name">
                                                                    <div class="avatar"> <img src="<?php echo $one->getAvatar('image18') ? $one->getAvatar('image18') : $one->getDefaultImage('image18'); ?>" alt=""> </div>
                                                                    <h6><?php echo $one->getFullname(); ?></h6>
                                                                    <span>Web Developer</span> </div>
                                                            </div>
                                                            <!-- Location -->
                                                            <div class="col-xs-3 n-p-r n-p-l"> <span>San Francisco, USA</span> </div>
                                                            <!-- Network -->
                                                            <div class="col-xs-3 n-p-r"> <span>21 Followers</span> <span>10 Following</span> </div>
                                                            <!-- Connections -->
                                                            <div class="col-xs-2 n-p-r n-p-l"> <span>31 Connections</span> </div>
                                                        </div>
                                                    </li>

                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>