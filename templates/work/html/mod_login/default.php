<!-- TABS -->
<?php
$itemid = wsHelper::getItemid('com_users', 'login');
$user = workFactory::getUser();
?>
<div class="uou-tabs accordion-xs">
    <ul class="tabs">
        <li><a href="#register"><?php echo JText::_('Register Now'); ?></a></li>
        <li class="active"><a href="#log-in"><?php echo JText::_('Member Login') //Member Login;     ?></a></li>
    </ul>

    <!-- REGISTER -->
    <div class="content">
<!--        <a href="#register" class="accordion-link first"><?php echo JText::_('Register Now'); ?></a>-->
        <div id="register">
            <form action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class="form-validate form-horizontal well" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6 col-sm-6">
                        <input name="front[name]" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_NAME'); ?>" type="text" style="width: 100%">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <input name="front[surname]" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_SURNAME'); ?>" type="text" style="width: 100%">
                    </div>
                </div>
                <input name="front[email]" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_EMAIL'); ?>" type="email">
                <input name="front[password1]"  placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_PASSWORD1'); ?>" type="text">
                <input name="front[password2]" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_PASSWORD2'); ?>" type="password">
                <button type="submit"><?php echo JText::_('MOD_LOGIN_VALUE_REGISTER'); ?></button>
                <div class="login-with"> <span><?php echo JText::_('Or login with'); ?>:</span> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-google"></i></a> <a href="#."><i class="fa fa-linkedin"></i></a> </div>
                <input type="hidden" name="option" value="com_users" />
                <input type="hidden" name="task" value="registration.register" />
<?php echo JHtml::_('form.token'); ?>

            </form>
        </div>

        <!-- LOGIN -->
<!--        <a href="#log-in" class="accordion-link active last"><?php echo JText::_('Member Login') //Member Login;     ?></a>-->
        <div id="log-in" class="active">
            <form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post">
                <input name="username" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>" type="text">
                <input  name="password" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>"type="password">
                <button type="submit"><?php echo JText::_('JLOGIN'); ?></button>
                <div class="login-with"> <span><?php echo JText::_('Or login with'); ?>:</span> <a href="#."><i class="fa fa-facebook"></i></a> <a href="#."><i class="fa fa-google"></i></a> <a href="#."><i class="fa fa-linkedin"></i></a> </div>

                <div class="forget"><?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?> <a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('Remind password'); ?></a></div>
                <input type="hidden" name="option" value="com_users" />
                <input type="hidden" name="task" value="user.login" />
                <input type="hidden" name="return" value="<?php echo $return; ?>" />
<?php echo JHtml::_('form.token'); ?>
            </form>
        </div>
        <div id="forget">
            <form>
                <input placeholder="Email Address" type="email">
                <button type="submit">Login</button>
            </form>
        </div>
    </div>
</div>