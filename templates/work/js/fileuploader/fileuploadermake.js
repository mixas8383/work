
$('.showUploadForm').click(function (event) {
    event.preventDefault();
    var ela = this;
    $('.uploadAvatarConteiner').show();

    $('<input/>').attr({type: 'file', name: 'primage', id: 'fileupload', class: "btn mb20 btn-small btn-primary", "data-url": $(ela).attr('data-url')})
            .appendTo('.fileinput-button')
            .fileupload({
                dataType: 'json',
                add: function (e, data) {
                    var jqXHR = data.submit()
                            .success(function (result, textStatus, jqXHR) {
                                $.each(result, function (a, b) {
                                    console.log(b)
                                    if (b.hasOwnProperty('error'))
                                    {
                                        $('<div>').attr({class: 'error'}).html(b.error).appendTo('.uploadAvatarConteiner');
                                    } else {
                                        $('.avatar-image-tochange').attr({src: b.url});
                                        $('.uploadAvatarConteiner').hide();
                                    }


                                })
                                console.log(result);


                            })
                            .error(function (jqXHR, textStatus, errorThrown) {
                                console.log(textStatus);
                            })
                            .complete(function (result, textStatus, jqXHR) {
                                console.log(result);
                            });
                }


            });

});



$(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        add: function (e, data) {
            var jqXHR = data.submit()
                    .success(function (result, textStatus, jqXHR) {
                        console.log(result);


                    })
                    .error(function (jqXHR, textStatus, errorThrown) {
                        console.log(textStatus);
                    })
                    .complete(function (result, textStatus, jqXHR) {
                        console.log(result);
                    });
        }


    });
});