<?php
/**
 * @package    Joomla.Language
 *
 * @copyright  Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * en-GB localise class.
 *
 * @since  1.6
 */
abstract class En_GBLocalise
{
	/**
	 * Returns the potential suffixes for a specific number of items
	 *
	 * @param   integer  $count  The number of items.
	 *
	 * @return  array  An array of potential suffixes.
	 *
	 * @since   1.6
	 */
	public static function getPluralSuffixes($count)
	{
		if ($count == 0)
		{
			return array('0');
		}
		elseif ($count == 1)
		{
			return array('1');
		}
		else
		{
			return array('MORE');
		}
	}

	/**
	 * Returns the ignored search words
	 *
	 * @return  array  An array of ignored search words.
	 *
	 * @since   1.6
	 */
	public static function getIgnoredSearchWords()
	{
		return array('and', 'in', 'on');
	}

	/**
	 * Returns the lower length limit of search words
	 *
	 * @return  integer  The lower length limit of search words.
	 *
	 * @since   1.6
	 */
	public static function getLowerLimitSearchWord()
	{
		return 3;
	}

	/**
	 * Returns the upper length limit of search words
	 *
	 * @return  integer  The upper length limit of search words.
	 *
	 * @since   1.6
	 */
	public static function getUpperLimitSearchWord()
	{
		return 20;
	}

	/**
	 * Returns the number of chars to display when searching
	 *
	 * @return  integer  The number of chars to display when searching.
	 *
	 * @since   1.6
	 */
	public static function getSearchDisplayedCharactersNumber()
	{
		return 200;
	}
     public static function transliterate($str)
    {

        $geo = array();
        $eng = array();

        $geo[] = "ა";
        $eng[] = "a";
        $geo[] = "ბ";
        $eng[] = "b";
        $geo[] = "გ";
        $eng[] = "g";
        $geo[] = "დ";
        $eng[] = "d";
        $geo[] = "ე";
        $eng[] = "e";
        $geo[] = "ვ";
        $eng[] = "v";
        $geo[] = "ზ";
        $eng[] = "z";
        $geo[] = "თ";
        $eng[] = "t";
        $geo[] = "ი";
        $eng[] = "i";
        $geo[] = "კ";
        $eng[] = "k";
        $geo[] = "ლ";
        $eng[] = "l";
        $geo[] = "მ";
        $eng[] = "m";
        $geo[] = "ნ";
        $eng[] = "n";
        $geo[] = "ო";
        $eng[] = "o";
        $geo[] = "პ";
        $eng[] = "p";
        $geo[] = "ჟ";
        $eng[] = "zh";
        $geo[] = "რ";
        $eng[] = "r";
        $geo[] = "ს";
        $eng[] = "s";
        $geo[] = "ტ";
        $eng[] = "t";
        $geo[] = "უ";
        $eng[] = "u";
        $geo[] = "ფ";
        $eng[] = "p";
        $geo[] = "ქ";
        $eng[] = "k";
        $geo[] = "ღ";
        $eng[] = "g";
        $geo[] = "ყ";
        $eng[] = "k";
        $geo[] = "შ";
        $eng[] = "sh";
        $geo[] = "ჩ";
        $eng[] = "ch";
        $geo[] = "ც";
        $eng[] = "ts";
        $geo[] = "ძ";
        $eng[] = "dz";
        $geo[] = "წ";
        $eng[] = "ts";
        $geo[] = "ჭ";
        $eng[] = "ch";
        $geo[] = "ხ";
        $eng[] = "kh";
        $geo[] = "ჯ";
        $eng[] = "j";
        $geo[] = "ჰ";
        $eng[] = "kh";
        $str = str_replace($geo, $eng, $str);
        return $str;
}
}