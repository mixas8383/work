-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 09, 2017 at 08:24 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.0.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `work`
--

-- --------------------------------------------------------

--
-- Table structure for table `kutah_assets`
--

CREATE TABLE `kutah_assets` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) UNSIGNED NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_assets`
--

INSERT INTO `kutah_assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 0, 353, 0, 'root.1', 'Root Asset', '{\"core.login.site\":{\"6\":1,\"2\":1},\"core.login.admin\":{\"6\":1},\"core.login.offline\":{\"6\":1},\"core.admin\":{\"8\":1},\"core.manage\":{\"7\":1},\"core.create\":{\"6\":1,\"3\":1},\"core.delete\":{\"6\":1},\"core.edit\":{\"6\":1,\"4\":1},\"core.edit.state\":{\"6\":1,\"5\":1},\"core.edit.own\":{\"6\":1,\"3\":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 6, 1, 'com_banners', 'com_banners', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(4, 1, 7, 8, 1, 'com_cache', 'com_cache', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(5, 1, 9, 10, 1, 'com_checkin', 'com_checkin', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(6, 1, 11, 12, 1, 'com_config', 'com_config', '{}'),
(7, 1, 13, 16, 1, 'com_contact', 'com_contact', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(8, 1, 17, 24, 1, 'com_content', 'com_content', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.edit\":{\"4\":1},\"core.edit.state\":{\"5\":1}}'),
(9, 1, 25, 26, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 27, 28, 1, 'com_installer', 'com_installer', '{\"core.manage\":{\"7\":0},\"core.delete\":{\"7\":0},\"core.edit.state\":{\"7\":0}}'),
(11, 1, 29, 30, 1, 'com_languages', 'com_languages', '{\"core.admin\":{\"7\":1}}'),
(12, 1, 31, 32, 1, 'com_login', 'com_login', '{}'),
(13, 1, 33, 34, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 35, 36, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 37, 38, 1, 'com_media', 'com_media', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1},\"core.create\":{\"3\":1},\"core.delete\":{\"5\":1}}'),
(16, 1, 39, 52, 1, 'com_menus', 'com_menus', '{\"core.admin\":{\"7\":1}}'),
(17, 1, 53, 54, 1, 'com_messages', 'com_messages', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"7\":1}}'),
(18, 1, 55, 134, 1, 'com_modules', 'com_modules', '{\"core.admin\":{\"7\":1}}'),
(19, 1, 135, 138, 1, 'com_newsfeeds', 'com_newsfeeds', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(20, 1, 139, 140, 1, 'com_plugins', 'com_plugins', '{\"core.admin\":{\"7\":1}}'),
(21, 1, 141, 142, 1, 'com_redirect', 'com_redirect', '{\"core.admin\":{\"7\":1}}'),
(22, 1, 143, 144, 1, 'com_search', 'com_search', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(23, 1, 145, 146, 1, 'com_templates', 'com_templates', '{\"core.admin\":{\"7\":1}}'),
(24, 1, 147, 150, 1, 'com_users', 'com_users', '{\"core.admin\":{\"7\":1}}'),
(26, 1, 151, 152, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 18, 23, 2, 'com_content.category.2', 'Uncategorised', '{}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{}'),
(29, 7, 14, 15, 2, 'com_contact.category.4', 'Uncategorised', '{}'),
(30, 19, 136, 137, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{}'),
(32, 24, 148, 149, 2, 'com_users.category.7', 'Uncategorised', '{}'),
(33, 1, 153, 154, 1, 'com_finder', 'com_finder', '{\"core.admin\":{\"7\":1},\"core.manage\":{\"6\":1}}'),
(34, 1, 155, 156, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{}'),
(35, 1, 157, 158, 1, 'com_tags', 'com_tags', '{}'),
(36, 1, 159, 160, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(37, 1, 161, 162, 1, 'com_ajax', 'com_ajax', '{}'),
(38, 1, 163, 164, 1, 'com_postinstall', 'com_postinstall', '{}'),
(39, 18, 56, 57, 2, 'com_modules.module.1', 'Main Menu', '{}'),
(40, 18, 58, 59, 2, 'com_modules.module.2', 'Login', '{}'),
(41, 18, 60, 61, 2, 'com_modules.module.3', 'Popular Articles', '{}'),
(42, 18, 62, 63, 2, 'com_modules.module.4', 'Recently Added Articles', '{}'),
(43, 18, 64, 65, 2, 'com_modules.module.8', 'Toolbar', '{}'),
(44, 18, 66, 67, 2, 'com_modules.module.9', 'Quick Icons', '{}'),
(45, 18, 68, 69, 2, 'com_modules.module.10', 'Logged-in Users', '{}'),
(46, 18, 70, 71, 2, 'com_modules.module.12', 'Admin Menu', '{}'),
(47, 18, 72, 73, 2, 'com_modules.module.13', 'Admin Submenu', '{}'),
(48, 18, 74, 75, 2, 'com_modules.module.14', 'User Status', '{}'),
(49, 18, 76, 77, 2, 'com_modules.module.15', 'Title', '{}'),
(50, 18, 78, 79, 2, 'com_modules.module.16', 'Login Form', '{}'),
(51, 18, 80, 81, 2, 'com_modules.module.17', 'Breadcrumbs', '{}'),
(52, 18, 82, 83, 2, 'com_modules.module.79', 'Multilanguage status', '{}'),
(53, 18, 84, 85, 2, 'com_modules.module.86', 'Joomla Version', '{}'),
(54, 16, 40, 41, 2, 'com_menus.menu.1', 'Main Menu', '{}'),
(55, 18, 86, 87, 2, 'com_modules.module.87', 'Display Work Items', '{}'),
(56, 18, 88, 89, 2, 'com_modules.module.88', 'Display Services', '{}'),
(57, 18, 90, 91, 2, 'com_modules.module.89', 'Display Videos', '{}'),
(58, 18, 92, 93, 2, 'com_modules.module.90', 'Display Galleries', '{}'),
(59, 18, 94, 95, 2, 'com_modules.module.91', 'Display Items', '{}'),
(60, 1, 165, 344, 1, 'com_work', 'com_work', '{}'),
(61, 1, 345, 346, 1, '#__languages.2', '#__languages.2', '{}'),
(62, 60, 166, 223, 2, 'com_work.category.8', 'მშენებლობა', '{}'),
(63, 60, 326, 327, 2, 'com_work.item.1', 'web პროგრამისტი1', '{}'),
(64, 60, 224, 235, 2, 'com_work.category.9', 'რემონტი', '{}'),
(65, 60, 236, 237, 2, 'com_work.category.10', 'მომსახურება                            ', '{}'),
(66, 60, 238, 239, 2, 'com_work.category.11', 'ჯანმრთელობა', '{}'),
(67, 60, 240, 241, 2, 'com_work.category.12', 'სილამაზე', '{}'),
(68, 60, 242, 243, 2, 'com_work.category.13', 'აუქციონი, ტენდერი            ', '{}'),
(69, 16, 42, 43, 2, 'com_menus.menu.2', 'UserMenu', '{}'),
(70, 18, 96, 97, 2, 'com_modules.module.92', 'მომხმარებლის მენიუ', '{}'),
(71, 18, 98, 99, 2, 'com_modules.module.93', 'Promo text main page', '{}'),
(72, 18, 100, 101, 2, 'com_modules.module.94', 'Main page serices', '{}'),
(73, 18, 102, 103, 2, 'com_modules.module.95', 'what our users say ', '{}'),
(74, 18, 104, 105, 2, 'com_modules.module.96', 'Our Sponsors', '{}'),
(75, 18, 106, 107, 2, 'com_modules.module.97', 'App Image', '{}'),
(76, 18, 108, 109, 2, 'com_modules.module.98', 'pro content 1', '{}'),
(77, 18, 110, 111, 2, 'com_modules.module.99', 'pro content 2', '{}'),
(78, 18, 112, 113, 2, 'com_modules.module.100', 'Photostream', '{}'),
(79, 18, 114, 115, 2, 'com_modules.module.101', 'Newsletter', '{}'),
(80, 18, 116, 117, 2, 'com_modules.module.102', 'Twitter Feed', '{}'),
(81, 18, 118, 119, 2, 'com_modules.module.103', 'contact info', '{}'),
(82, 27, 19, 20, 3, 'com_content.article.1', 'Privacy Policy', '{}'),
(83, 16, 44, 45, 2, 'com_menus.menu.3', 'Privacy & terms', '{}'),
(84, 27, 21, 22, 3, 'com_content.article.2', 'Terms & Conditions', '{}'),
(85, 18, 120, 121, 2, 'com_modules.module.104', 'Privacy & terms', '{}'),
(86, 18, 122, 123, 2, 'com_modules.module.105', 'Copyright', '{}'),
(87, 18, 124, 125, 2, 'com_modules.module.106', 'search', '{}'),
(88, 16, 46, 47, 2, 'com_menus.menu.4', 'Top login', '{}'),
(89, 18, 126, 127, 2, 'com_modules.module.107', 'Top login', '{}'),
(90, 16, 48, 49, 2, 'com_menus.menu.5', 'top socials', '{}'),
(91, 18, 128, 129, 2, 'com_modules.module.108', 'Top socials', '{}'),
(92, 18, 130, 131, 2, 'com_modules.module.109', 'Search Professionals', '{}'),
(93, 18, 132, 133, 2, 'com_modules.module.110', 'search-pro', '{}'),
(94, 1, 347, 348, 1, 'com_selectfile', 'com_selectfile', '{}'),
(95, 16, 50, 51, 2, 'com_menus.menu.6', 'Additional', '{}'),
(96, 1, 349, 350, 1, 'com_fields', 'com_fields', '{}'),
(97, 1, 351, 352, 1, 'com_associations', 'com_associations', '{}'),
(98, 60, 328, 329, 2, 'com_work.item.2', 'Veb programmer', '{}'),
(99, 60, 330, 331, 2, 'com_work.item.3', 'websolutions', '{}'),
(100, 60, 332, 333, 2, 'com_work.item.4', 'first school', '{}'),
(101, 60, 334, 335, 2, 'com_work.item.5', '', '{}'),
(102, 60, 336, 337, 2, 'com_work.item.6', 'first degree', '{}'),
(103, 60, 338, 339, 2, 'com_work.item.7', '', '{}'),
(104, 60, 340, 341, 2, 'com_work.item.8', 'sdf', '{}'),
(105, 62, 167, 168, 3, 'com_work.item.9', 'dfdf', '{}'),
(106, 62, 169, 170, 3, 'com_work.item.10', 'dfdf', '{}'),
(107, 62, 171, 172, 3, 'com_work.item.11', 'dfdf', '{}'),
(108, 62, 173, 174, 3, 'com_work.item.12', 'dfdf', '{}'),
(109, 62, 175, 176, 3, 'com_work.item.13', 'fdgd', '{}'),
(110, 62, 177, 178, 3, 'com_work.item.14', 'fdgd', '{}'),
(111, 62, 179, 180, 3, 'com_work.item.15', 'sdf', '{}'),
(112, 62, 181, 182, 3, 'com_work.item.16', 'sdfsdf', '{}'),
(113, 62, 183, 184, 3, 'com_work.item.17', 'aasd', '{}'),
(114, 64, 225, 226, 3, 'com_work.item.18', 'asd', '{}'),
(115, 60, 244, 245, 2, 'com_work.gallery.1', 'image', '{}'),
(116, 62, 185, 186, 3, 'com_work.item.19', 'asd', '{}'),
(117, 62, 187, 188, 3, 'com_work.item.20', 'asdasd', '{}'),
(118, 60, 246, 247, 2, 'com_work.gallery.2', 'image', '{}'),
(119, 60, 248, 249, 2, 'com_work.gallery.3', 'image', '{}'),
(120, 64, 227, 228, 3, 'com_work.item.21', 'sdfsdf', '{}'),
(121, 60, 250, 251, 2, 'com_work.gallery.4', 'image', '{}'),
(122, 62, 189, 190, 3, 'com_work.item.22', 'asdasd', '{}'),
(123, 60, 252, 253, 2, 'com_work.gallery.5', 'image', '{}'),
(124, 60, 254, 255, 2, 'com_work.gallery.6', 'image', '{}'),
(125, 64, 229, 230, 3, 'com_work.item.23', 'sdf', '{}'),
(126, 60, 256, 257, 2, 'com_work.gallery.7', 'image', '{}'),
(127, 60, 258, 259, 2, 'com_work.gallery.8', 'image', '{}'),
(128, 60, 260, 261, 2, 'com_work.gallery.9', 'image', '{}'),
(129, 62, 191, 192, 3, 'com_work.item.24', 'sdfsdf', '{}'),
(130, 60, 262, 263, 2, 'com_work.gallery.10', 'image', '{}'),
(131, 62, 193, 194, 3, 'com_work.item.25', 'dfgdfg', '{}'),
(132, 60, 264, 265, 2, 'com_work.gallery.11', 'image', '{}'),
(133, 62, 195, 196, 3, 'com_work.item.26', 'dfgdfgd', '{}'),
(134, 60, 266, 267, 2, 'com_work.gallery.12', 'image', '{}'),
(135, 60, 268, 269, 2, 'com_work.gallery.13', 'image', '{}'),
(136, 62, 197, 198, 3, 'com_work.item.27', 'sdfsdf', '{}'),
(137, 60, 270, 271, 2, 'com_work.gallery.14', 'image', '{}'),
(138, 60, 272, 273, 2, 'com_work.gallery.15', 'image', '{}'),
(139, 62, 199, 200, 3, 'com_work.item.28', 'sdfsdf', '{}'),
(140, 60, 274, 275, 2, 'com_work.gallery.16', 'image', '{}'),
(141, 60, 276, 277, 2, 'com_work.gallery.17', 'image', '{}'),
(142, 62, 201, 202, 3, 'com_work.item.29', 'asdasd', '{}'),
(143, 60, 278, 279, 2, 'com_work.gallery.18', 'image', '{}'),
(144, 60, 280, 281, 2, 'com_work.gallery.19', 'image', '{}'),
(145, 60, 282, 283, 2, 'com_work.gallery.20', 'image', '{}'),
(146, 60, 284, 285, 2, 'com_work.gallery.21', 'image', '{}'),
(147, 62, 203, 204, 3, 'com_work.item.30', 'asdasd', '{}'),
(148, 60, 286, 287, 2, 'com_work.gallery.22', 'image', '{}'),
(149, 60, 288, 289, 2, 'com_work.gallery.23', 'image', '{}'),
(150, 62, 205, 206, 3, 'com_work.item.31', 'asdasd', '{}'),
(151, 60, 290, 291, 2, 'com_work.gallery.24', 'image', '{}'),
(152, 62, 207, 208, 3, 'com_work.item.32', 'fghdfgh', '{}'),
(153, 60, 292, 293, 2, 'com_work.gallery.25', 'image', '{}'),
(154, 60, 294, 295, 2, 'com_work.gallery.26', 'image', '{}'),
(155, 64, 231, 232, 3, 'com_work.item.33', 'asdasd', '{}'),
(156, 60, 296, 297, 2, 'com_work.gallery.27', 'image', '{}'),
(157, 62, 209, 210, 3, 'com_work.item.34', 'sasas', '{}'),
(158, 60, 298, 299, 2, 'com_work.gallery.28', 'image', '{}'),
(159, 60, 300, 301, 2, 'com_work.gallery.29', 'image', '{}'),
(160, 62, 211, 212, 3, 'com_work.item.35', 'asdasd', '{}'),
(161, 60, 302, 303, 2, 'com_work.gallery.30', 'image', '{}'),
(162, 60, 304, 305, 2, 'com_work.gallery.31', 'image', '{}'),
(163, 62, 213, 214, 3, 'com_work.item.36', 'ertert', '{}'),
(164, 62, 215, 216, 3, 'com_work.item.37', 'ZXCZXCZX', '{}'),
(165, 64, 233, 234, 3, 'com_work.item.38', 'ადმინისტრაციული შენობა', '{}'),
(166, 60, 306, 307, 2, 'com_work.gallery.32', 'image', '{}'),
(167, 60, 308, 309, 2, 'com_work.gallery.33', 'image', '{}'),
(168, 62, 217, 218, 3, 'com_work.item.39', 'sadasd', '{}'),
(169, 60, 310, 311, 2, 'com_work.gallery.34', 'image', '{}'),
(170, 60, 312, 313, 2, 'com_work.gallery.35', 'image', '{}'),
(171, 62, 219, 220, 3, 'com_work.item.40', 'ჭონქაძის ქუჩაზე მომხდარი მკვლელობის', '{}'),
(172, 60, 314, 315, 2, 'com_work.gallery.36', 'image', '{}'),
(173, 60, 316, 317, 2, 'com_work.gallery.37', 'image', '{}'),
(174, 60, 318, 319, 2, 'com_work.gallery.38', 'image', '{}'),
(175, 60, 320, 321, 2, 'com_work.gallery.39', 'image', '{}'),
(176, 60, 322, 323, 2, 'com_work.gallery.40', 'image', '{}'),
(177, 60, 324, 325, 2, 'com_work.gallery.41', 'image', '{}'),
(178, 62, 221, 222, 3, 'com_work.item.41', 'ადმინისტრაციული შენობა', '{}'),
(179, 60, 342, 343, 2, 'com_work.gallery.42', 'image', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_associations`
--

CREATE TABLE `kutah_associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_banners`
--

CREATE TABLE `kutah_banners` (
  `id` int(11) NOT NULL,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custombannercode` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sticky` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_banner_clients`
--

CREATE TABLE `kutah_banner_clients` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extrainfo` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_banner_tracks`
--

CREATE TABLE `kutah_banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) UNSIGNED NOT NULL,
  `banner_id` int(10) UNSIGNED NOT NULL,
  `count` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_categories`
--

CREATE TABLE `kutah_categories` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `extension` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_categories`
--

INSERT INTO `kutah_categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 23, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '{}', 655, '2017-07-13 17:21:28', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 17:21:28', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 17:21:28', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 17:21:28', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 17:21:28', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 9, 10, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 17:21:28', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 62, 1, 11, 12, 1, 'mshenebloba', 'com_work', 'მშენებლობა', 'mshenebloba', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 17:47:29', 655, '2017-07-13 18:11:34', 0, '*', 1),
(9, 64, 1, 13, 14, 1, 'remonti', 'com_work', 'რემონტი', 'remonti', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 18:11:47', 0, '2017-07-13 18:11:47', 0, '*', 1),
(10, 65, 1, 15, 16, 1, 'momsakhureba', 'com_work', 'მომსახურება                            ', 'momsakhureba', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 18:13:04', 0, '2017-07-13 18:13:04', 0, '*', 1),
(11, 66, 1, 17, 18, 1, 'janmrteloba', 'com_work', 'ჯანმრთელობა', 'janmrteloba', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 18:13:21', 0, '2017-07-13 18:13:21', 0, '*', 1),
(12, 67, 1, 19, 20, 1, 'silamaze', 'com_work', 'სილამაზე', 'silamaze', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 18:13:32', 0, '2017-07-13 18:13:32', 0, '*', 1),
(13, 68, 1, 21, 22, 1, 'auktsioni-tenderi', 'com_work', 'აუქციონი, ტენდერი            ', 'auktsioni-tenderi', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{\"category_layout\":\"\",\"image\":\"\",\"image_alt\":\"\"}', '', '', '{\"author\":\"\",\"robots\":\"\"}', 655, '2017-07-13 18:13:57', 0, '2017-07-13 18:13:57', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_chats`
--

CREATE TABLE `kutah_chats` (
  `id` int(11) NOT NULL,
  `user_1` int(11) NOT NULL,
  `user_2` int(11) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_chats`
--

INSERT INTO `kutah_chats` (`id`, `user_1`, `user_2`, `published`, `date`) VALUES
(1, 655, 657, 0, '2017-09-13 21:52:08'),
(2, 658, 655, 0, '2017-09-13 21:52:08'),
(3, 655, 659, 0, '2017-09-13 21:52:08'),
(4, 655, 660, 0, '2017-09-13 21:52:08'),
(5, 655, 661, 0, '2017-09-13 21:52:08'),
(6, 655, 662, 0, '2017-09-13 21:52:08');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_chats_messages`
--

CREATE TABLE `kutah_chats_messages` (
  `id` int(11) NOT NULL,
  `chat_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0-text_message; 1-image',
  `seen` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_chats_messages`
--

INSERT INTO `kutah_chats_messages` (`id`, `chat_id`, `user_id`, `message`, `date`, `type`, `seen`, `parent_id`) VALUES
(1, 3, 655, 'asadasd', '2017-09-15 00:03:31', 0, 0, 0),
(2, 3, 655, 'asadasd', '2017-09-15 00:05:13', 0, 0, 0),
(3, 2, 655, 'asadasd', '2017-09-15 00:07:43', 0, 0, 0),
(4, 2, 655, 'asadasd', '2017-09-15 00:07:43', 0, 0, 0),
(5, 2, 655, 'asadasd', '2017-09-15 00:07:43', 0, 0, 0),
(6, 2, 655, 'sdfsdf', '2017-09-15 00:12:04', 0, 0, 0),
(7, 2, 655, 'sdfsdf', '2017-09-15 00:12:07', 0, 0, 0),
(8, 1, 655, 'asdas', '2017-09-17 18:29:00', 0, 0, 0),
(9, 1, 655, 'dfsd', '2017-09-17 20:52:00', 0, 0, 0),
(10, 1, 655, 'zdfs', '2017-09-17 22:05:38', 0, 0, 0),
(11, 1, 655, 'sdf', '2017-09-17 22:08:30', 0, 0, 0),
(12, 1, 655, 'fdf', '2017-09-17 22:09:28', 0, 0, 0),
(13, 1, 655, 'sdf', '2017-09-17 22:11:05', 0, 0, 0),
(14, 3, 659, 'fdfg', '2017-09-17 22:11:47', 0, 0, 0),
(15, 1, 655, 'dfg', '2017-09-17 22:14:59', 0, 0, 0),
(16, 1, 655, 'dscd', '2017-09-17 22:19:32', 0, 0, 0),
(17, 1, 655, 'sdfsdf', '2017-09-17 22:21:19', 0, 0, 0),
(18, 1, 655, 'sdfsdf', '2017-09-17 22:23:41', 0, 0, 0),
(19, 1, 655, 'sdfsdf', '2017-09-17 22:23:52', 0, 0, 0),
(20, 1, 655, 'xzcvxzcv', '2017-09-17 22:28:52', 0, 0, 0),
(21, 1, 655, 'xcvzxc', '2017-09-17 22:29:05', 0, 0, 0),
(22, 1, 655, 'xcvxcv', '2017-09-17 22:30:09', 0, 0, 0),
(23, 1, 655, 'sdfsdf', '2017-09-17 22:31:33', 0, 0, 0),
(24, 1, 655, 'sdfsdf', '2017-09-17 22:32:07', 0, 0, 0),
(25, 1, 655, 'zxczxc', '2017-09-17 22:33:07', 0, 0, 0),
(26, 1, 655, 'cvbxcv', '2017-09-17 22:33:31', 0, 0, 0),
(27, 1, 655, 'dfg', '2017-09-17 22:35:49', 0, 0, 0),
(28, 1, 655, 'dfgdfg', '2017-09-17 22:36:08', 0, 0, 0),
(29, 1, 655, 'asdasd', '2017-09-17 22:37:45', 0, 0, 0),
(30, 1, 655, 'asda', '2017-09-17 22:37:50', 0, 0, 0),
(31, 1, 655, 'sdfsfd', '2017-09-17 22:37:59', 0, 0, 0),
(32, 1, 655, 'sdf', '2017-09-17 22:38:09', 0, 0, 0),
(33, 1, 655, 'sdf', '2017-09-17 22:38:18', 0, 0, 0),
(34, 1, 655, 'sdfsfd', '2017-09-17 22:38:29', 0, 0, 0),
(35, 1, 655, 'sd', '2017-09-17 22:38:33', 0, 0, 0),
(36, 1, 655, 'sdsdf', '2017-09-17 22:39:00', 0, 0, 0),
(37, 1, 655, 'asd', '2017-09-17 22:41:50', 0, 0, 0),
(38, 1, 655, 'zxc', '2017-09-17 22:42:18', 0, 0, 0),
(39, 1, 655, 'xcxcv', '2017-09-17 22:42:48', 0, 0, 0),
(40, 1, 655, 'ZXZX', '2017-09-17 22:43:09', 0, 0, 0),
(41, 1, 655, 'ZX', '2017-09-17 22:43:52', 0, 0, 0),
(42, 1, 655, 'ZX', '2017-09-17 22:44:00', 0, 0, 0),
(43, 1, 655, 'Z', '2017-09-17 22:44:08', 0, 0, 0),
(44, 1, 655, 'fghfg', '2017-09-18 22:50:11', 0, 0, 0),
(45, 1, 655, 'fghfg', '2017-09-18 22:53:57', 0, 0, 0),
(46, 1, 655, 'sd', '2017-09-18 22:56:13', 0, 0, 0),
(47, 1, 657, 'sdfsd', '2017-09-18 22:57:34', 0, 0, 0),
(48, 1, 657, 'asd', '2017-09-18 23:08:15', 0, 0, 0),
(49, 1, 655, 'dfgf', '2017-09-18 23:19:54', 0, 0, 0),
(50, 1, 655, 'dfgdfg', '2017-09-18 23:20:10', 0, 0, 0),
(51, 1, 655, 'dfgdfg', '2017-09-18 23:20:19', 0, 0, 0),
(52, 1, 657, 'dfgdfg', '2017-09-18 23:28:57', 0, 0, 0),
(53, 3, 655, 'fgh', '2017-09-19 21:36:46', 0, 0, 0),
(54, 4, 655, 'fgh', '2017-09-19 21:36:50', 0, 0, 0),
(55, 1, 655, 'sxs', '2017-09-19 22:27:36', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_contact_details`
--

CREATE TABLE `kutah_contact_details` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `con_position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `suburb` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postcode` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `misc` mediumtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_to` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `default_con` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mobile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `webpage` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `sortname3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if contact is featured.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_content`
--

CREATE TABLE `kutah_content` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `introtext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `fulltext` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribs` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_content`
--

INSERT INTO `kutah_content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 82, 'Privacy Policy', 'privacy-policy', '<p>Privacy Policy</p>', '', 1, 2, '2017-07-16 16:17:32', 655, '', '2017-07-16 16:17:32', 0, 0, '0000-00-00 00:00:00', '2017-07-16 16:17:32', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 1, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', ''),
(2, 84, 'Terms & Conditions', 'terms-conditions', '<p>Terms &amp;amp; Conditions</p>', '', 1, 2, '2017-07-16 16:19:29', 655, '', '2017-07-16 16:19:29', 0, 0, '0000-00-00 00:00:00', '2017-07-16 16:19:29', '0000-00-00 00:00:00', '{\"image_intro\":\"\",\"float_intro\":\"\",\"image_intro_alt\":\"\",\"image_intro_caption\":\"\",\"image_fulltext\":\"\",\"float_fulltext\":\"\",\"image_fulltext_alt\":\"\",\"image_fulltext_caption\":\"\"}', '{\"urla\":false,\"urlatext\":\"\",\"targeta\":\"\",\"urlb\":false,\"urlbtext\":\"\",\"targetb\":\"\",\"urlc\":false,\"urlctext\":\"\",\"targetc\":\"\"}', '{\"article_layout\":\"\",\"show_title\":\"\",\"link_titles\":\"\",\"show_tags\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_vote\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"alternative_readmore\":\"\",\"article_page_title\":\"\",\"show_publishing_options\":\"\",\"show_article_options\":\"\",\"show_urls_images_backend\":\"\",\"show_urls_images_frontend\":\"\"}', 1, 0, '', '', 1, 0, '{\"robots\":\"\",\"author\":\"\",\"rights\":\"\",\"xreference\":\"\"}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_contentitem_tag_map`
--

CREATE TABLE `kutah_contentitem_tag_map` (
  `type_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_content_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) UNSIGNED NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Table structure for table `kutah_content_frontpage`
--

CREATE TABLE `kutah_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_content_rating`
--

CREATE TABLE `kutah_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_content_types`
--

CREATE TABLE `kutah_content_types` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `table` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `rules` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `field_mappings` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `router` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'JSON string for com_contenthistory options'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_content_types`
--

INSERT INTO `kutah_content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{\"special\":{\"dbtable\":\"#__content\",\"key\":\"id\",\"type\":\"Content\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"state\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"introtext\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"attribs\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"asset_id\"}, \"special\":{\"fulltext\":\"fulltext\"}}', 'ContentHelperRoute::getArticleRoute', '{\"formFile\":\"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(2, 'Contact', 'com_contact.contact', '{\"special\":{\"dbtable\":\"#__contact_details\",\"key\":\"id\",\"type\":\"Contact\",\"prefix\":\"ContactTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"address\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"image\", \"core_urls\":\"webpage\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"con_position\":\"con_position\",\"suburb\":\"suburb\",\"state\":\"state\",\"country\":\"country\",\"postcode\":\"postcode\",\"telephone\":\"telephone\",\"fax\":\"fax\",\"misc\":\"misc\",\"email_to\":\"email_to\",\"default_con\":\"default_con\",\"user_id\":\"user_id\",\"mobile\":\"mobile\",\"sortname1\":\"sortname1\",\"sortname2\":\"sortname2\",\"sortname3\":\"sortname3\"}}', 'ContactHelperRoute::getContactRoute', '{\"formFile\":\"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml\",\"hideFields\":[\"default_con\",\"checked_out\",\"checked_out_time\",\"version\",\"xreference\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"], \"displayLookup\":[ {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ] }'),
(3, 'Newsfeed', 'com_newsfeeds.newsfeed', '{\"special\":{\"dbtable\":\"#__newsfeeds\",\"key\":\"id\",\"type\":\"Newsfeed\",\"prefix\":\"NewsfeedsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"xreference\", \"asset_id\":\"null\"}, \"special\":{\"numarticles\":\"numarticles\",\"cache_time\":\"cache_time\",\"rtl\":\"rtl\"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{\"formFile\":\"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(4, 'User', 'com_users.user', '{\"special\":{\"dbtable\":\"#__users\",\"key\":\"id\",\"type\":\"User\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"username\",\"core_created_time\":\"registerdate\",\"core_modified_time\":\"lastvisitDate\",\"core_body\":\"null\", \"core_hits\":\"null\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"access\":\"null\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"null\", \"core_language\":\"null\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"null\", \"core_ordering\":\"null\", \"core_metakey\":\"null\", \"core_metadesc\":\"null\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{}}', 'UsersHelperRoute::getUserRoute', ''),
(5, 'Article Category', 'com_content.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContentHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(6, 'Contact Category', 'com_contact.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'ContactHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(7, 'Newsfeeds Category', 'com_newsfeeds.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(8, 'Tag', 'com_tags.tag', '{\"special\":{\"dbtable\":\"#__tags\",\"key\":\"tag_id\",\"type\":\"Tag\",\"prefix\":\"TagsTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"featured\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"urls\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"null\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\"}}', 'TagsHelperRoute::getTagRoute', '{\"formFile\":\"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"lft\", \"rgt\", \"level\", \"path\", \"urls\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"],\"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(9, 'Banner', 'com_banners.banner', '{\"special\":{\"dbtable\":\"#__banners\",\"key\":\"id\",\"type\":\"Banner\",\"prefix\":\"BannersTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\", \"core_hits\":\"null\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"images\", \"core_urls\":\"link\", \"core_version\":\"version\", \"core_ordering\":\"ordering\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"catid\", \"core_xreference\":\"null\", \"asset_id\":\"null\"}, \"special\":{\"imptotal\":\"imptotal\", \"impmade\":\"impmade\", \"clicks\":\"clicks\", \"clickurl\":\"clickurl\", \"custombannercode\":\"custombannercode\", \"cid\":\"cid\", \"purchase_type\":\"purchase_type\", \"track_impressions\":\"track_impressions\", \"track_clicks\":\"track_clicks\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\", \"reset\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"version\", \"imptotal\", \"impmade\", \"reset\"], \"convertToInt\":[\"publish_up\", \"publish_down\", \"ordering\"], \"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"cid\",\"targetTable\":\"#__banner_clients\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10, 'Banners Category', 'com_banners.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\": {\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(11, 'Banner Client', 'com_banners.client', '{\"special\":{\"dbtable\":\"#__banner_clients\",\"key\":\"id\",\"type\":\"Client\",\"prefix\":\"BannersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\"], \"ignoreChanges\":[\"checked_out\", \"checked_out_time\"], \"convertToInt\":[], \"displayLookup\":[]}'),
(12, 'User Notes', 'com_users.note', '{\"special\":{\"dbtable\":\"#__user_notes\",\"key\":\"id\",\"type\":\"Note\",\"prefix\":\"UsersTable\"}}', '', '', '', '{\"formFile\":\"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\", \"publish_up\", \"publish_down\"],\"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\"], \"convertToInt\":[\"publish_up\", \"publish_down\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}, {\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}]}'),
(13, 'User Notes Category', 'com_users.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__ucm_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}, \"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', '', '{\"formFile\":\"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml\", \"hideFields\":[\"checked_out\",\"checked_out_time\",\"version\",\"lft\",\"rgt\",\"level\",\"path\",\"extension\"], \"ignoreChanges\":[\"modified_user_id\", \"modified_time\", \"checked_out\", \"checked_out_time\", \"version\", \"hits\", \"path\"], \"convertToInt\":[\"publish_up\", \"publish_down\"], \"displayLookup\":[{\"sourceColumn\":\"created_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"}, {\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_user_id\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"parent_id\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"}]}'),
(10000, 'Work Category', 'com_work.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__core_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":[{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\", \"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\", \"core_params\":\"params\", \"core_featured\":\"null\", \"core_metadata\":\"metadata\", \"core_language\":\"language\", \"core_images\":\"null\", \"core_urls\":\"null\", \"core_version\":\"version\", \"core_ordering\":\"null\", \"core_metakey\":\"metakey\", \"core_metadesc\":\"metadesc\", \"core_catid\":\"parent_id\", \"core_xreference\":\"null\", \"asset_id\":\"asset_id\"}], \"special\": [{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}]}', 'WorkHelperRoute::getCategoryRoute', NULL),
(10001, 'Service', 'com_work.service', '{\"special\":{\"dbtable\":\"work_services\",\"key\":\"id\",\"type\":\"Services\",\"prefix\":\"WorkTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__core_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"special\":{},\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\",\"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\",\"core_params\":\"params\",\"core_featured\":\"featured\",\"core_metadata\":\"metadata\",\"core_language\":\"language\",\"core_images\":\"images\",\"core_urls\":\"urls\",\"core_version\":\"version\",\"core_ordering\":\"ordering\",\"core_metakey\":\"metakey\",\"core_metadesc\":\"metadesc\",\"core_catid\":\"null\",\"core_xreference\":\"xreference\",\"asset_id\":\"asset_id\"}}', 'WorkHelperRoute::getServiceRoute', '{\"formFile\":\"administrator\\/components\\/work\\/models\\/forms\\/service.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"hits\", \"version\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10002, 'Video', 'com_work.video', '{\"special\":{\"dbtable\":\"work_videos\",\"key\":\"id\",\"type\":\"Videos\",\"prefix\":\"WorkTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__core_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"special\":{},\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\",\"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\",\"core_params\":\"params\",\"core_featured\":\"featured\",\"core_metadata\":\"metadata\",\"core_language\":\"language\",\"core_images\":\"images\",\"core_urls\":\"urls\",\"core_version\":\"version\",\"core_ordering\":\"ordering\",\"core_metakey\":\"metakey\",\"core_metadesc\":\"metadesc\",\"core_catid\":\"null\",\"core_xreference\":\"xreference\",\"asset_id\":\"asset_id\"}}', 'WorkHelperRoute::getVideoRoute', '{\"formFile\":\"administrator\\/components\\/work\\/models\\/forms\\/video.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"hits\", \"version\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10003, 'Gallery', 'com_work.gallery', '{\"special\":{\"dbtable\":\"work_galleries\",\"key\":\"id\",\"type\":\"Galleries\",\"prefix\":\"WorkTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__core_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"special\":{},\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\",\"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\",\"core_params\":\"params\",\"core_featured\":\"featured\",\"core_metadata\":\"metadata\",\"core_language\":\"language\",\"core_images\":\"images\",\"core_urls\":\"urls\",\"core_version\":\"version\",\"core_ordering\":\"ordering\",\"core_metakey\":\"metakey\",\"core_metadesc\":\"metadesc\",\"core_catid\":\"null\",\"core_xreference\":\"xreference\",\"asset_id\":\"asset_id\"}}', 'WorkHelperRoute::getGalleryRoute', '{\"formFile\":\"administrator\\/components\\/work\\/models\\/forms\\/gallery.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"hits\", \"version\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10004, 'Item', 'com_work.item', '{\"special\":{\"dbtable\":\"work_items\",\"key\":\"id\",\"type\":\"Items\",\"prefix\":\"WorkTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__core_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"special\":{},\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"name\",\"core_state\":\"null\",\"core_alias\":\"alias\",\"core_created_time\":\"created\",\"core_modified_time\":\"modified\",\"core_body\":\"description\",\"core_hits\":\"hits\",\"core_publish_up\":\"publish_up\",\"core_publish_down\":\"publish_down\",\"core_access\":\"access\",\"core_params\":\"params\",\"core_featured\":\"featured\",\"core_metadata\":\"metadata\",\"core_language\":\"language\",\"core_images\":\"images\",\"core_urls\":\"urls\",\"core_version\":\"version\",\"core_ordering\":\"ordering\",\"core_metakey\":\"metakey\",\"core_metadesc\":\"metadesc\",\"core_catid\":\"catid\",\"core_xreference\":\"xreference\",\"asset_id\":\"asset_id\"}}', 'WorkHelperRoute::getItemRoute', '{\"formFile\":\"administrator\\/components\\/work\\/models\\/forms\\/item.xml\",\"hideFields\":[\"asset_id\",\"checked_out\",\"checked_out_time\",\"version\"],\"ignoreChanges\":[\"modified_by\", \"modified\", \"checked_out\", \"checked_out_time\", \"hits\", \"version\"],\"convertToInt\":[\"publish_up\", \"publish_down\", \"featured\", \"ordering\"],\"displayLookup\":[{\"sourceColumn\":\"catid\",\"targetTable\":\"#__categories\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"created_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"},{\"sourceColumn\":\"access\",\"targetTable\":\"#__viewlevels\",\"targetColumn\":\"id\",\"displayColumn\":\"title\"},{\"sourceColumn\":\"modified_by\",\"targetTable\":\"#__users\",\"targetColumn\":\"id\",\"displayColumn\":\"name\"} ]}'),
(10005, 'Work Category', 'com_work.category', '{\"special\":{\"dbtable\":\"#__categories\",\"key\":\"id\",\"type\":\"Category\",\"prefix\":\"JTable\",\"config\":\"array()\"},\"common\":{\"dbtable\":\"#__core_content\",\"key\":\"ucm_id\",\"type\":\"Corecontent\",\"prefix\":\"JTable\",\"config\":\"array()\"}}', '', '{\"common\":{\"core_content_item_id\":\"id\",\"core_title\":\"title\",\"core_state\":\"published\",\"core_alias\":\"alias\",\"core_created_time\":\"created_time\",\"core_modified_time\":\"modified_time\",\"core_body\":\"description\",\"core_hits\":\"hits\",\"core_publish_up\":\"null\",\"core_publish_down\":\"null\",\"core_access\":\"access\",\"core_params\":\"params\",\"core_featured\":\"null\",\"core_metadata\":\"metadata\",\"core_language\":\"language\",\"core_images\":\"null\",\"core_urls\":\"null\",\"core_version\":\"version\",\"core_ordering\":\"null\",\"core_metakey\":\"metakey\",\"core_metadesc\":\"metadesc\",\"core_catid\":\"parent_id\",\"core_xreference\":\"null\",\"asset_id\":\"asset_id\"},	\"special\":{\"parent_id\":\"parent_id\",\"lft\":\"lft\",\"rgt\":\"rgt\",\"level\":\"level\",\"path\":\"path\",\"extension\":\"extension\",\"note\":\"note\"}}', 'WorkHelperRoute::getCategoryRoute', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_core_log_searches`
--

CREATE TABLE `kutah_core_log_searches` (
  `search_term` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_country`
--

CREATE TABLE `kutah_country` (
  `id` int(11) NOT NULL,
  `Code` char(3) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `Name` char(52) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `name_ka_ge` varchar(255) NOT NULL,
  `Continent` enum('Asia','Europe','North America','Africa','Oceania','Antarctica','South America') CHARACTER SET latin1 NOT NULL DEFAULT 'Asia',
  `Region` char(26) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `SurfaceArea` float(10,2) NOT NULL DEFAULT '0.00',
  `IndepYear` smallint(6) DEFAULT NULL,
  `Population` int(11) NOT NULL DEFAULT '0',
  `LifeExpectancy` float(3,1) DEFAULT NULL,
  `GNP` float(10,2) DEFAULT NULL,
  `GNPOld` float(10,2) DEFAULT NULL,
  `LocalName` char(45) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `GovernmentForm` char(45) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `HeadOfState` char(60) CHARACTER SET latin1 DEFAULT NULL,
  `Capital` int(11) DEFAULT NULL,
  `Code2` char(2) CHARACTER SET latin1 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_country`
--

INSERT INTO `kutah_country` (`id`, `Code`, `Name`, `name_ka_ge`, `Continent`, `Region`, `SurfaceArea`, `IndepYear`, `Population`, `LifeExpectancy`, `GNP`, `GNPOld`, `LocalName`, `GovernmentForm`, `HeadOfState`, `Capital`, `Code2`) VALUES
(1, 'ABW', 'Aruba', 'არუბა', 'North America', 'Caribbean', 193.00, NULL, 103000, 78.4, 828.00, 793.00, 'Aruba', 'Nonmetropolitan Territory of The Netherlands', 'Beatrix', 129, 'AW'),
(2, 'AFG', 'Afghanistan', 'ავღანეთი', 'Asia', 'Southern and Central Asia', 652090.00, 1919, 22720000, 45.9, 5976.00, NULL, 'Afganistan/Afqanestan', 'Islamic Emirate', 'Mohammad Omar', 1, 'AF'),
(3, 'AGO', 'Angola', 'ანგოლა', 'Africa', 'Central Africa', 1246700.00, 1975, 12878000, 38.3, 6648.00, 7984.00, 'Angola', 'Republic', 'José Eduardo dos Santos', 56, 'AO'),
(4, 'AIA', 'Anguilla', 'ანგვილა', 'North America', 'Caribbean', 96.00, NULL, 8000, 76.1, 63.20, NULL, 'Anguilla', 'Dependent Territory of the UK', 'Elisabeth II', 62, 'AI'),
(5, 'ALB', 'Albania', 'ალბანეთი', 'Europe', 'Southern Europe', 28748.00, 1912, 3401200, 71.6, 3205.00, 2500.00, 'Shqipëria', 'Republic', 'Rexhep Mejdani', 34, 'AL'),
(6, 'AND', 'Andorra', 'ანდორა', 'Europe', 'Southern Europe', 468.00, 1278, 78000, 83.5, 1630.00, NULL, 'Andorra', 'Parliamentary Coprincipality', '', 55, 'AD'),
(7, 'ANT', 'Netherlands Antilles', 'ნიდერლანდების ანტილები', 'North America', 'Caribbean', 800.00, NULL, 217000, 74.7, 1941.00, NULL, 'Nederlandse Antillen', 'Nonmetropolitan Territory of The Netherlands', 'Beatrix', 33, 'AN'),
(8, 'ARE', 'United Arab Emirates', 'არაბთა გაერთიანებული საამიროები', 'Asia', 'Middle East', 83600.00, 1971, 2441000, 74.1, 37966.00, 36846.00, 'Al-Imarat al-´Arabiya al-Muttahida', 'Emirate Federation', 'Zayid bin Sultan al-Nahayan', 65, 'AE'),
(9, 'ARG', 'Argentina', 'არგენტინა', 'South America', 'South America', 2780400.00, 1816, 37032000, 75.1, 340238.00, 323310.00, 'Argentina', 'Federal Republic', 'Fernando de la Rúa', 69, 'AR'),
(10, 'ARM', 'Armenia', 'სომხეთი', 'Asia', 'Middle East', 29800.00, 1991, 3520000, 66.4, 1813.00, 1627.00, 'Hajastan', 'Republic', 'Robert Kotšarjan', 126, 'AM'),
(11, 'ASM', 'American Samoa', 'ამერიკული სამოა', 'Oceania', 'Polynesia', 199.00, NULL, 68000, 75.1, 334.00, NULL, 'Amerika Samoa', 'US Territory', 'George W. Bush', 54, 'AS'),
(12, 'ATA', 'Antarctica', 'ანტარქტიკა', 'Antarctica', 'Antarctica', 13120000.00, NULL, 0, NULL, 0.00, NULL, '–', 'Co-administrated', '', NULL, 'AQ'),
(13, 'ATF', 'French Southern territories', 'ფრანგული სამხრეთის ტერიტორიები', 'Antarctica', 'Antarctica', 7780.00, NULL, 0, NULL, 0.00, NULL, 'Terres australes françaises', 'Nonmetropolitan Territory of France', 'Jacques Chirac', NULL, 'TF'),
(14, 'ATG', 'Antigua and Barbuda', 'ანტიგუა და ბარბუდა', 'North America', 'Caribbean', 442.00, 1981, 68000, 70.5, 612.00, 584.00, 'Antigua and Barbuda', 'Constitutional Monarchy', 'Elisabeth II', 63, 'AG'),
(15, 'AUS', 'Australia', 'ავსტრალია', 'Oceania', 'Australia and New Zealand', 7741220.00, 1901, 18886000, 79.8, 351182.00, 392911.00, 'Australia', 'Constitutional Monarchy, Federation', 'Elisabeth II', 135, 'AU'),
(16, 'AUT', 'Austria', 'ავსტრია', 'Europe', 'Western Europe', 83859.00, 1918, 8091800, 77.7, 211860.00, 206025.00, 'Österreich', 'Federal Republic', 'Thomas Klestil', 1523, 'AT'),
(17, 'AZE', 'Azerbaijan', 'აზერბაიჯანი', 'Asia', 'Middle East', 86600.00, 1991, 7734000, 62.9, 4127.00, 4100.00, 'Azärbaycan', 'Federal Republic', 'Heydär Äliyev', 144, 'AZ'),
(18, 'BDI', 'Burundi', 'ბურუნდი', 'Africa', 'Eastern Africa', 27834.00, 1962, 6695000, 46.2, 903.00, 982.00, 'Burundi/Uburundi', 'Republic', 'Pierre Buyoya', 552, 'BI'),
(19, 'BEL', 'Belgium', 'ბელგია', 'Europe', 'Western Europe', 30518.00, 1830, 10239000, 77.8, 249704.00, 243948.00, 'België/Belgique', 'Constitutional Monarchy, Federation', 'Albert II', 179, 'BE'),
(20, 'BEN', 'Benin', 'ბენინი', 'Africa', 'Western Africa', 112622.00, 1960, 6097000, 50.2, 2357.00, 2141.00, 'Bénin', 'Republic', 'Mathieu Kérékou', 187, 'BJ'),
(21, 'BFA', 'Burkina Faso', 'ბურკინა-ფასო', 'Africa', 'Western Africa', 274000.00, 1960, 11937000, 46.7, 2425.00, 2201.00, 'Burkina Faso', 'Republic', 'Blaise Compaoré', 549, 'BF'),
(22, 'BGD', 'Bangladesh', 'ბანგლადეში', 'Asia', 'Southern and Central Asia', 143998.00, 1971, 129155000, 60.2, 32852.00, 31966.00, 'Bangladesh', 'Republic', 'Shahabuddin Ahmad', 150, 'BD'),
(23, 'BGR', 'Bulgaria', 'ბულგარეთი', 'Europe', 'Eastern Europe', 110994.00, 1908, 8190900, 70.9, 12178.00, 10169.00, 'Balgarija', 'Republic', 'Petar Stojanov', 539, 'BG'),
(24, 'BHR', 'Bahrain', 'ბაჰრეინი', 'Asia', 'Middle East', 694.00, 1971, 617000, 73.0, 6366.00, 6097.00, 'Al-Bahrayn', 'Monarchy (Emirate)', 'Hamad ibn Isa al-Khalifa', 149, 'BH'),
(25, 'BHS', 'Bahamas', 'ბაჰამის კუნძულები', 'North America', 'Caribbean', 13878.00, 1973, 307000, 71.1, 3527.00, 3347.00, 'The Bahamas', 'Constitutional Monarchy', 'Elisabeth II', 148, 'BS'),
(26, 'BIH', 'Bosnia and Herzegovina', 'ბოსნია და ჰერცეგოვინა', 'Europe', 'Southern Europe', 51197.00, 1992, 3972000, 71.5, 2841.00, NULL, 'Bosna i Hercegovina', 'Federal Republic', 'Ante Jelavic', 201, 'BA'),
(27, 'BLR', 'Belarus', 'ბელორუსია', 'Europe', 'Eastern Europe', 207600.00, 1991, 10236000, 68.0, 13714.00, NULL, 'Belarus', 'Republic', 'Aljaksandr Lukašenka', 3520, 'BY'),
(28, 'BLZ', 'Belize', 'ბელიზი', 'North America', 'Central America', 22696.00, 1981, 241000, 70.9, 630.00, 616.00, 'Belize', 'Constitutional Monarchy', 'Elisabeth II', 185, 'BZ'),
(29, 'BMU', 'Bermuda', 'ბერმუდი', 'North America', 'North America', 53.00, NULL, 65000, 76.9, 2328.00, 2190.00, 'Bermuda', 'Dependent Territory of the UK', 'Elisabeth II', 191, 'BM'),
(30, 'BOL', 'Bolivia', 'ბოლივია', 'South America', 'South America', 1098581.00, 1825, 8329000, 63.7, 8571.00, 7967.00, 'Bolivia', 'Republic', 'Hugo Bánzer Suárez', 194, 'BO'),
(31, 'BRA', 'Brazil', 'ბრაზილია', 'South America', 'South America', 8547403.00, 1822, 170115000, 62.9, 776739.00, 804108.00, 'Brasil', 'Federal Republic', 'Fernando Henrique Cardoso', 211, 'BR'),
(32, 'BRB', 'Barbados', 'ბარბადოსი', 'North America', 'Caribbean', 430.00, 1966, 270000, 73.0, 2223.00, 2186.00, 'Barbados', 'Constitutional Monarchy', 'Elisabeth II', 174, 'BB'),
(33, 'BRN', 'Brunei', 'ბრუნეი', 'Asia', 'Southeast Asia', 5765.00, 1984, 328000, 73.6, 11705.00, 12460.00, 'Brunei Darussalam', 'Monarchy (Sultanate)', 'Haji Hassan al-Bolkiah', 538, 'BN'),
(34, 'BTN', 'Bhutan', 'ბუტანი', 'Asia', 'Southern and Central Asia', 47000.00, 1910, 2124000, 52.4, 372.00, 383.00, 'Druk-Yul', 'Monarchy', 'Jigme Singye Wangchuk', 192, 'BT'),
(35, 'BVT', 'Bouvet Island', 'ბუვეს კუნძული', 'Antarctica', 'Antarctica', 59.00, NULL, 0, NULL, 0.00, NULL, 'Bouvetøya', 'Dependent Territory of Norway', 'Harald V', NULL, 'BV'),
(36, 'BWA', 'Botswana', 'ბოტსვანა', 'Africa', 'Southern Africa', 581730.00, 1966, 1622000, 39.3, 4834.00, 4935.00, 'Botswana', 'Republic', 'Festus G. Mogae', 204, 'BW'),
(37, 'CAF', 'Central African Republic', 'ცენტრალური აფრიკის რესპუბლიკა', 'Africa', 'Central Africa', 622984.00, 1960, 3615000, 44.0, 1054.00, 993.00, 'Centrafrique/Bê-Afrîka', 'Republic', 'Ange-Félix Patassé', 1889, 'CF'),
(38, 'CAN', 'Canada', 'კანადა', 'North America', 'North America', 9970610.00, 1867, 31147000, 79.4, 598862.00, 625626.00, 'Canada', 'Constitutional Monarchy, Federation', 'Elisabeth II', 1822, 'CA'),
(39, 'CCK', 'Cocos (Keeling) Islands', 'ქოქოსის (კილინგის) კუნძულები', 'Oceania', 'Australia and New Zealand', 14.00, NULL, 600, NULL, 0.00, NULL, 'Cocos (Keeling) Islands', 'Territory of Australia', 'Elisabeth II', 2317, 'CC'),
(40, 'CHE', 'Switzerland', 'შვეიცარია', 'Europe', 'Western Europe', 41284.00, 1499, 7160400, 79.6, 264478.00, 256092.00, 'Schweiz/Suisse/Svizzera/Svizra', 'Federation', 'Adolf Ogi', 3248, 'CH'),
(41, 'CHL', 'Chile', 'ჩილე', 'South America', 'South America', 756626.00, 1810, 15211000, 75.7, 72949.00, 75780.00, 'Chile', 'Republic', 'Ricardo Lagos Escobar', 554, 'CL'),
(42, 'CHN', 'China', 'ჩინეთი', 'Asia', 'Eastern Asia', 9572900.00, -1523, 1277558000, 71.4, 982268.00, 917719.00, 'Zhongquo', 'People\'sRepublic', 'Jiang Zemin', 1891, 'CN'),
(43, 'CIV', 'Côte d’Ivoire', 'კოტ-დივუარი', 'Africa', 'Western Africa', 322463.00, 1960, 14786000, 45.2, 11345.00, 10285.00, 'Côte d’Ivoire', 'Republic', 'Laurent Gbagbo', 2814, 'CI'),
(44, 'CMR', 'Cameroon', 'კამერუნი', 'Africa', 'Central Africa', 475442.00, 1960, 15085000, 54.8, 9174.00, 8596.00, 'Cameroun/Cameroon', 'Republic', 'Paul Biya', 1804, 'CM'),
(45, 'COD', 'Congo, The Democratic Republic of the', 'კონგო - კინშასა', 'Africa', 'Central Africa', 2344858.00, 1960, 51654000, 48.8, 6964.00, 2474.00, 'République Démocratique du Congo', 'Republic', 'Joseph Kabila', 2298, 'CD'),
(46, 'COG', 'Congo', 'კონგო - ბრაზავილი', 'Africa', 'Central Africa', 342000.00, 1960, 2943000, 47.4, 2108.00, 2287.00, 'Congo', 'Republic', 'Denis Sassou-Nguesso', 2296, 'CG'),
(47, 'COK', 'Cook Islands', 'კუკის კუნძულები', 'Oceania', 'Polynesia', 236.00, NULL, 20000, 71.1, 100.00, NULL, 'The Cook Islands', 'Nonmetropolitan Territory of New Zealand', 'Elisabeth II', 583, 'CK'),
(48, 'COL', 'Colombia', 'კოლუმბია', 'South America', 'South America', 1138914.00, 1810, 42321000, 70.3, 102896.00, 105116.00, 'Colombia', 'Republic', 'Andrés Pastrana Arango', 2257, 'CO'),
(49, 'COM', 'Comoros', 'კომორის კუნძულები', 'Africa', 'Eastern Africa', 1862.00, 1975, 578000, 60.0, 4401.00, 4361.00, 'Komori/Comores', 'Republic', 'Azali Assoumani', 2295, 'KM'),
(50, 'CPV', 'Cape Verde', 'კაბო-ვერდე', 'Africa', 'Western Africa', 4033.00, 1975, 428000, 68.9, 435.00, 420.00, 'Cabo Verde', 'Republic', 'António Mascarenhas Monteiro', 1859, 'CV'),
(51, 'CRI', 'Costa Rica', 'კოსტა-რიკა', 'North America', 'Central America', 51100.00, 1821, 4023000, 75.8, 10226.00, 9757.00, 'Costa Rica', 'Republic', 'Miguel Ángel Rodríguez Echeverría', 584, 'CR'),
(52, 'CUB', 'Cuba', 'კუბა', 'North America', 'Caribbean', 110861.00, 1902, 11201000, 76.2, 17843.00, 18862.00, 'Cuba', 'Socialistic Republic', 'Fidel Castro Ruz', 2413, 'CU'),
(53, 'CXR', 'Christmas Island', 'შობის კუნძული', 'Oceania', 'Australia and New Zealand', 135.00, NULL, 2500, NULL, 0.00, NULL, 'Christmas Island', 'Territory of Australia', 'Elisabeth II', 1791, 'CX'),
(54, 'CYM', 'Cayman Islands', 'კაიმანის კუნძულები', 'North America', 'Caribbean', 264.00, NULL, 38000, 78.9, 1263.00, 1186.00, 'Cayman Islands', 'Dependent Territory of the UK', 'Elisabeth II', 553, 'KY'),
(55, 'CYP', 'Cyprus', 'კვიპროსი', 'Asia', 'Middle East', 9251.00, 1960, 754700, 76.7, 9333.00, 8246.00, 'Kýpros/Kibris', 'Republic', 'Glafkos Klerides', 2430, 'CY'),
(56, 'CZE', 'Czech Republic', 'ჩეხეთის რესპუბლიკა', 'Europe', 'Eastern Europe', 78866.00, 1993, 10278100, 74.5, 55017.00, 52037.00, '¸esko', 'Republic', 'Václav Havel', 3339, 'CZ'),
(57, 'DEU', 'Germany', 'გერმანია', 'Europe', 'Western Europe', 357022.00, 1955, 82164700, 77.4, 2133367.00, 2102826.00, 'Deutschland', 'Federal Republic', 'Johannes Rau', 3068, 'DE'),
(58, 'DJI', 'Djibouti', 'ჯიბუტი', 'Africa', 'Eastern Africa', 23200.00, 1977, 638000, 50.8, 382.00, 373.00, 'Djibouti/Jibuti', 'Republic', 'Ismail Omar Guelleh', 585, 'DJ'),
(59, 'DMA', 'Dominica', 'დომინიკა', 'North America', 'Caribbean', 751.00, 1978, 71000, 73.4, 256.00, 243.00, 'Dominica', 'Republic', 'Vernon Shaw', 586, 'DM'),
(60, 'DNK', 'Denmark', 'დანია', 'Europe', 'Nordic Countries', 43094.00, 800, 5330000, 76.5, 174099.00, 169264.00, 'Danmark', 'Constitutional Monarchy', 'Margrethe II', 3315, 'DK'),
(61, 'DOM', 'Dominican Republic', 'დომინიკანის რესპუბლიკა', 'North America', 'Caribbean', 48511.00, 1844, 8495000, 73.2, 15846.00, 15076.00, 'República Dominicana', 'Republic', 'Hipólito Mejía Domínguez', 587, 'DO'),
(62, 'DZA', 'Algeria', 'ალჟირი', 'Africa', 'Northern Africa', 2381741.00, 1962, 31471000, 69.7, 49982.00, 46966.00, 'Al-Jaza’ir/Algérie', 'Republic', 'Abdelaziz Bouteflika', 35, 'DZ'),
(63, 'ECU', 'Ecuador', 'ეკვადორი', 'South America', 'South America', 283561.00, 1822, 12646000, 71.1, 19770.00, 19769.00, 'Ecuador', 'Republic', 'Gustavo Noboa Bejarano', 594, 'EC'),
(64, 'EGY', 'Egypt', 'ეგვიპტე', 'Africa', 'Northern Africa', 1001449.00, 1922, 68470000, 63.3, 82710.00, 75617.00, 'Misr', 'Republic', 'Hosni Mubarak', 608, 'EG'),
(65, 'ERI', 'Eritrea', 'ერიტრეა', 'Africa', 'Eastern Africa', 117600.00, 1993, 3850000, 55.8, 650.00, 755.00, 'Ertra', 'Republic', 'Isayas Afewerki [Isaias Afwerki]', 652, 'ER'),
(66, 'ESH', 'Western Sahara', 'დასავლეთი საჰარა', 'Africa', 'Northern Africa', 266000.00, NULL, 293000, 49.8, 60.00, NULL, 'As-Sahrawiya', 'Occupied by Marocco', 'Mohammed Abdel Aziz', 2453, 'EH'),
(67, 'ESP', 'Spain', 'ესპანეთი', 'Europe', 'Southern Europe', 505992.00, 1492, 39441700, 78.8, 553233.00, 532031.00, 'España', 'Constitutional Monarchy', 'Juan Carlos I', 653, 'ES'),
(68, 'EST', 'Estonia', 'ესტონეთი', 'Europe', 'Baltic Countries', 45227.00, 1991, 1439200, 69.5, 5328.00, 3371.00, 'Eesti', 'Republic', 'Lennart Meri', 3791, 'EE'),
(69, 'ETH', 'Ethiopia', 'ეთიოპია', 'Africa', 'Eastern Africa', 1104300.00, -1000, 62565000, 45.2, 6353.00, 6180.00, 'YeItyop´iya', 'Republic', 'Negasso Gidada', 756, 'ET'),
(70, 'FIN', 'Finland', 'ფინეთი', 'Europe', 'Nordic Countries', 338145.00, 1917, 5171300, 77.4, 121914.00, 119833.00, 'Suomi', 'Republic', 'Tarja Halonen', 3236, 'FI'),
(71, 'FJI', 'Fiji Islands', 'ფიჯი', 'Oceania', 'Melanesia', 18274.00, 1970, 817000, 67.9, 1536.00, 2149.00, 'Fiji Islands', 'Republic', 'Josefa Iloilo', 764, 'FJ'),
(72, 'FLK', 'Falkland Islands', 'ფოლკლენდის კუნძულები', 'South America', 'South America', 12173.00, NULL, 2000, NULL, 0.00, NULL, 'Falkland Islands', 'Dependent Territory of the UK', 'Elisabeth II', 763, 'FK'),
(73, 'FRA', 'France', 'საფრანგეთი', 'Europe', 'Western Europe', 551500.00, 843, 59225700, 78.8, 1424285.00, 1392448.00, 'France', 'Republic', 'Jacques Chirac', 2974, 'FR'),
(74, 'FRO', 'Faroe Islands', 'ფარერის კუნძულები', 'Europe', 'Nordic Countries', 1399.00, NULL, 43000, 78.4, 0.00, NULL, 'Føroyar', 'Part of Denmark', 'Margrethe II', 901, 'FO'),
(75, 'FSM', 'Micronesia, Federated States of', 'მიკრონეზია', 'Oceania', 'Micronesia', 702.00, 1990, 119000, 68.6, 212.00, NULL, 'Micronesia', 'Federal Republic', 'Leo A. Falcam', 2689, 'FM'),
(76, 'GAB', 'Gabon', 'გაბონი', 'Africa', 'Central Africa', 267668.00, 1960, 1226000, 50.1, 5493.00, 5279.00, 'Le Gabon', 'Republic', 'Omar Bongo', 902, 'GA'),
(77, 'GBR', 'United Kingdom', 'დიდი ბრიტანეთი', 'Europe', 'British Islands', 242900.00, 1066, 59623400, 77.7, 1378330.00, 1296830.00, 'United Kingdom', 'Constitutional Monarchy', 'Elisabeth II', 456, 'GB'),
(78, 'GEO', 'Georgia', 'საქართველო', 'Asia', 'Middle East', 69700.00, 1991, 4968000, 64.5, 6064.00, 5924.00, 'Sakartvelo', 'Republic', 'Eduard Ševardnadze', 905, 'GE'),
(79, 'GHA', 'Ghana', 'განა', 'Africa', 'Western Africa', 238533.00, 1957, 20212000, 57.4, 7137.00, 6884.00, 'Ghana', 'Republic', 'John Kufuor', 910, 'GH'),
(80, 'GIB', 'Gibraltar', 'გიბრალტარი', 'Europe', 'Southern Europe', 6.00, NULL, 25000, 79.0, 258.00, NULL, 'Gibraltar', 'Dependent Territory of the UK', 'Elisabeth II', 915, 'GI'),
(81, 'GIN', 'Guinea', 'გვინეა', 'Africa', 'Western Africa', 245857.00, 1958, 7430000, 45.6, 2352.00, 2383.00, 'Guinée', 'Republic', 'Lansana Conté', 926, 'GN'),
(82, 'GLP', 'Guadeloupe', 'გვადელუპე', 'North America', 'Caribbean', 1705.00, NULL, 456000, 77.0, 3501.00, NULL, 'Guadeloupe', 'Overseas Department of France', 'Jacques Chirac', 919, 'GP'),
(83, 'GMB', 'Gambia', 'გამბია', 'Africa', 'Western Africa', 11295.00, 1965, 1305000, 53.2, 320.00, 325.00, 'The Gambia', 'Republic', 'Yahya Jammeh', 904, 'GM'),
(84, 'GNB', 'Guinea-Bissau', 'გვინეა-ბისაუ', 'Africa', 'Western Africa', 36125.00, 1974, 1213000, 49.0, 293.00, 272.00, 'Guiné-Bissau', 'Republic', 'Kumba Ialá', 927, 'GW'),
(85, 'GNQ', 'Equatorial Guinea', 'ეკვატორული გვინეა', 'Africa', 'Central Africa', 28051.00, 1968, 453000, 53.6, 283.00, 542.00, 'Guinea Ecuatorial', 'Republic', 'Teodoro Obiang Nguema Mbasogo', 2972, 'GQ'),
(86, 'GRC', 'Greece', 'საბერძნეთი', 'Europe', 'Southern Europe', 131626.00, 1830, 10545700, 78.4, 120724.00, 119946.00, 'Elláda', 'Republic', 'Kostis Stefanopoulos', 2401, 'GR'),
(87, 'GRD', 'Grenada', 'გრენადა', 'North America', 'Caribbean', 344.00, 1974, 94000, 64.5, 318.00, NULL, 'Grenada', 'Constitutional Monarchy', 'Elisabeth II', 916, 'GD'),
(88, 'GRL', 'Greenland', 'გრენლანდია', 'North America', 'North America', 2166090.00, NULL, 56000, 68.1, 0.00, NULL, 'Kalaallit Nunaat/Grønland', 'Part of Denmark', 'Margrethe II', 917, 'GL'),
(89, 'GTM', 'Guatemala', 'გვატემალა', 'North America', 'Central America', 108889.00, 1821, 11385000, 66.2, 19008.00, 17797.00, 'Guatemala', 'Republic', 'Alfonso Portillo Cabrera', 922, 'GT'),
(90, 'GUF', 'French Guiana', 'ფრანგული გვიანა', 'South America', 'South America', 90000.00, NULL, 181000, 76.1, 681.00, NULL, 'Guyane française', 'Overseas Department of France', 'Jacques Chirac', 3014, 'GF'),
(91, 'GUM', 'Guam', 'გუამი', 'Oceania', 'Micronesia', 549.00, NULL, 168000, 77.8, 1197.00, 1136.00, 'Guam', 'US Territory', 'George W. Bush', 921, 'GU'),
(92, 'GUY', 'Guyana', 'გაიანა', 'South America', 'South America', 214969.00, 1966, 861000, 64.0, 722.00, 743.00, 'Guyana', 'Republic', 'Bharrat Jagdeo', 928, 'GY'),
(93, 'HKG', 'Hong Kong', 'ჰონკონგის სპეციალური ადმინისტრაციული რეგიონი ჩინეთი', 'Asia', 'Eastern Asia', 1075.00, NULL, 6782000, 79.5, 166448.00, 173610.00, 'Xianggang/Hong Kong', 'Special Administrative Region of China', 'Jiang Zemin', 937, 'HK'),
(94, 'HMD', 'Heard Island and McDonald Islands', 'ჰერდი და მაკდონალდის კუნძულები', 'Antarctica', 'Antarctica', 359.00, NULL, 0, NULL, 0.00, NULL, 'Heard and McDonald Islands', 'Territory of Australia', 'Elisabeth II', NULL, 'HM'),
(95, 'HND', 'Honduras', 'ჰონდურასი', 'North America', 'Central America', 112088.00, 1838, 6485000, 69.9, 5333.00, 4697.00, 'Honduras', 'Republic', 'Carlos Roberto Flores Facussé', 933, 'HN'),
(96, 'HRV', 'Croatia', 'ხორვატია', 'Europe', 'Southern Europe', 56538.00, 1991, 4473000, 73.7, 20208.00, 19300.00, 'Hrvatska', 'Republic', 'Štipe Mesic', 2409, 'HR'),
(97, 'HTI', 'Haiti', 'ჰაიტი', 'North America', 'Caribbean', 27750.00, 1804, 8222000, 49.2, 3459.00, 3107.00, 'Haïti/Dayti', 'Republic', 'Jean-Bertrand Aristide', 929, 'HT'),
(98, 'HUN', 'Hungary', 'უნგრეთი', 'Europe', 'Eastern Europe', 93030.00, 1918, 10043200, 71.4, 48267.00, 45914.00, 'Magyarország', 'Republic', 'Ferenc Mádl', 3483, 'HU'),
(99, 'IDN', 'Indonesia', 'ინდონეზია', 'Asia', 'Southeast Asia', 1904569.00, 1945, 212107000, 68.0, 84982.00, 215002.00, 'Indonesia', 'Republic', 'Abdurrahman Wahid', 939, 'ID'),
(100, 'IND', 'India', 'ინდოეთი', 'Asia', 'Southern and Central Asia', 3287263.00, 1947, 1013662000, 62.5, 447114.00, 430572.00, 'Bharat/India', 'Federal Republic', 'Kocheril Raman Narayanan', 1109, 'IN'),
(101, 'IOT', 'British Indian Ocean Territory', 'ბრიტანული ტერიტორია ინდოეთის ოკეანეში', 'Africa', 'Eastern Africa', 78.00, NULL, 0, NULL, 0.00, NULL, 'British Indian Ocean Territory', 'Dependent Territory of the UK', 'Elisabeth II', NULL, 'IO'),
(102, 'IRL', 'Ireland', 'ირლანდია', 'Europe', 'British Islands', 70273.00, 1921, 3775100, 76.8, 75921.00, 73132.00, 'Ireland/Éire', 'Republic', 'Mary McAleese', 1447, 'IE'),
(103, 'IRN', 'Iran', 'ირანი', 'Asia', 'Southern and Central Asia', 1648195.00, 1906, 67702000, 69.7, 195746.00, 160151.00, 'Iran', 'Islamic Republic', 'Ali Mohammad Khatami-Ardakani', 1380, 'IR'),
(104, 'IRQ', 'Iraq', 'ერაყი', 'Asia', 'Middle East', 438317.00, 1932, 23115000, 66.5, 11500.00, NULL, 'Al-´Iraq', 'Republic', 'Saddam Hussein al-Takriti', 1365, 'IQ'),
(105, 'ISL', 'Iceland', 'ისლანდია', 'Europe', 'Nordic Countries', 103000.00, 1944, 279000, 79.4, 8255.00, 7474.00, 'Ísland', 'Republic', 'Ólafur Ragnar Grímsson', 1449, 'IS'),
(106, 'ISR', 'Israel', 'ისრაელი', 'Asia', 'Middle East', 21056.00, 1948, 6217000, 78.6, 97477.00, 98577.00, 'Yisra’el/Isra’il', 'Republic', 'Moshe Katzav', 1450, 'IL'),
(107, 'ITA', 'Italy', 'იტალია', 'Europe', 'Southern Europe', 301316.00, 1861, 57680000, 79.0, 1161755.00, 1145372.00, 'Italia', 'Republic', 'Carlo Azeglio Ciampi', 1464, 'IT'),
(108, 'JAM', 'Jamaica', 'იამაიკა', 'North America', 'Caribbean', 10990.00, 1962, 2583000, 75.2, 6871.00, 6722.00, 'Jamaica', 'Constitutional Monarchy', 'Elisabeth II', 1530, 'JM'),
(109, 'JOR', 'Jordan', 'იორდანია', 'Asia', 'Middle East', 88946.00, 1946, 5083000, 77.4, 7526.00, 7051.00, 'Al-Urdunn', 'Constitutional Monarchy', 'Abdullah II', 1786, 'JO'),
(110, 'JPN', 'Japan', 'იაპონია', 'Asia', 'Eastern Asia', 377829.00, -660, 126714000, 80.7, 3787042.00, 4192638.00, 'Nihon/Nippon', 'Constitutional Monarchy', 'Akihito', 1532, 'JP'),
(111, 'KAZ', 'Kazakstan', 'ყაზახეთი', 'Asia', 'Southern and Central Asia', 2724900.00, 1991, 16223000, 63.2, 24375.00, 23383.00, 'Qazaqstan', 'Republic', 'Nursultan Nazarbajev', 1864, 'KZ'),
(112, 'KEN', 'Kenya', 'კენია', 'Africa', 'Eastern Africa', 580367.00, 1963, 30080000, 48.0, 9217.00, 10241.00, 'Kenya', 'Republic', 'Daniel arap Moi', 1881, 'KE'),
(113, 'KGZ', 'Kyrgyzstan', 'ყირგიზეთი', 'Asia', 'Southern and Central Asia', 199900.00, 1991, 4699000, 63.4, 1626.00, 1767.00, 'Kyrgyzstan', 'Republic', 'Askar Akajev', 2253, 'KG'),
(114, 'KHM', 'Cambodia', 'კამბოჯა', 'Asia', 'Southeast Asia', 181035.00, 1953, 11168000, 56.5, 5121.00, 5670.00, 'Kâmpuchéa', 'Constitutional Monarchy', 'Norodom Sihanouk', 1800, 'KH'),
(115, 'KIR', 'Kiribati', 'კირიბატი', 'Oceania', 'Micronesia', 726.00, 1979, 83000, 59.8, 40.70, NULL, 'Kiribati', 'Republic', 'Teburoro Tito', 2256, 'KI'),
(116, 'KNA', 'Saint Kitts and Nevis', 'სენტ-კიტსი და ნევისი', 'North America', 'Caribbean', 261.00, 1983, 38000, 70.7, 299.00, NULL, 'Saint Kitts and Nevis', 'Constitutional Monarchy', 'Elisabeth II', 3064, 'KN'),
(117, 'KOR', 'South Korea', 'სამხრეთი კორეა', 'Asia', 'Eastern Asia', 99434.00, 1948, 46844000, 74.4, 320749.00, 442544.00, 'Taehan Min’guk (Namhan)', 'Republic', 'Kim Dae-jung', 2331, 'KR'),
(118, 'KWT', 'Kuwait', 'ქუვეითი', 'Asia', 'Middle East', 17818.00, 1961, 1972000, 76.1, 27037.00, 30373.00, 'Al-Kuwayt', 'Constitutional Monarchy (Emirate)', 'Jabir al-Ahmad al-Jabir al-Sabah', 2429, 'KW'),
(119, 'LAO', 'Laos', 'ლაოსი', 'Asia', 'Southeast Asia', 236800.00, 1953, 5433000, 53.1, 1292.00, 1746.00, 'Lao', 'Republic', 'Khamtay Siphandone', 2432, 'LA'),
(120, 'LBN', 'Lebanon', 'ლიბანი', 'Asia', 'Middle East', 10400.00, 1941, 3282000, 71.3, 17121.00, 15129.00, 'Lubnan', 'Republic', 'Émile Lahoud', 2438, 'LB'),
(121, 'LBR', 'Liberia', 'ლიბერია', 'Africa', 'Western Africa', 111369.00, 1847, 3154000, 51.0, 2012.00, NULL, 'Liberia', 'Republic', 'Charles Taylor', 2440, 'LR'),
(122, 'LBY', 'Libyan Arab Jamahiriya', 'ლიბია', 'Africa', 'Northern Africa', 1759540.00, 1951, 5605000, 75.5, 44806.00, 40562.00, 'Libiya', 'Socialistic State', 'Muammar al-Qadhafi', 2441, 'LY'),
(123, 'LCA', 'Saint Lucia', 'სენტ-ლუსია', 'North America', 'Caribbean', 622.00, 1979, 154000, 72.3, 571.00, NULL, 'Saint Lucia', 'Constitutional Monarchy', 'Elisabeth II', 3065, 'LC'),
(124, 'LIE', 'Liechtenstein', 'ლიხტენშტეინი', 'Europe', 'Western Europe', 160.00, 1806, 32300, 78.8, 1119.00, 1084.00, 'Liechtenstein', 'Constitutional Monarchy', 'Hans-Adam II', 2446, 'LI'),
(125, 'LKA', 'Sri Lanka', 'შრი-ლანკა', 'Asia', 'Southern and Central Asia', 65610.00, 1948, 18827000, 71.8, 15706.00, 15091.00, 'Sri Lanka/Ilankai', 'Republic', 'Chandrika Kumaratunga', 3217, 'LK'),
(126, 'LSO', 'Lesotho', 'ლესოთო', 'Africa', 'Southern Africa', 30355.00, 1966, 2153000, 50.8, 1061.00, 1161.00, 'Lesotho', 'Constitutional Monarchy', 'Letsie III', 2437, 'LS'),
(127, 'LTU', 'Lithuania', 'ლიტვა', 'Europe', 'Baltic Countries', 65301.00, 1991, 3698500, 69.1, 10692.00, 9585.00, 'Lietuva', 'Republic', 'Valdas Adamkus', 2447, 'LT'),
(128, 'LUX', 'Luxembourg', 'ლუქსემბურგი', 'Europe', 'Western Europe', 2586.00, 1867, 435700, 77.1, 16321.00, 15519.00, 'Luxembourg/Lëtzebuerg', 'Constitutional Monarchy', 'Henri', 2452, 'LU'),
(129, 'LVA', 'Latvia', 'ლატვია', 'Europe', 'Baltic Countries', 64589.00, 1991, 2424200, 68.4, 6398.00, 5639.00, 'Latvija', 'Republic', 'Vaira Vike-Freiberga', 2434, 'LV'),
(130, 'MAC', 'Macao', 'მაკაოს სპეციალური ადმინისტრაციული რეგიონი ჩინეთი', 'Asia', 'Eastern Asia', 18.00, NULL, 473000, 81.6, 5749.00, 5940.00, 'Macau/Aomen', 'Special Administrative Region of China', 'Jiang Zemin', 2454, 'MO'),
(131, 'MAR', 'Morocco', 'მაროკო', 'Africa', 'Northern Africa', 446550.00, 1956, 28351000, 69.1, 36124.00, 33514.00, 'Al-Maghrib', 'Constitutional Monarchy', 'Mohammed VI', 2486, 'MA'),
(132, 'MCO', 'Monaco', 'მონაკო', 'Europe', 'Western Europe', 1.50, 1861, 34000, 78.8, 776.00, NULL, 'Monaco', 'Constitutional Monarchy', 'Rainier III', 2695, 'MC'),
(133, 'MDA', 'Moldova', 'მოლდოვა', 'Europe', 'Eastern Europe', 33851.00, 1991, 4380000, 64.5, 1579.00, 1872.00, 'Moldova', 'Republic', 'Vladimir Voronin', 2690, 'MD'),
(134, 'MDG', 'Madagascar', 'მადაგასკარი', 'Africa', 'Eastern Africa', 587041.00, 1960, 15942000, 55.0, 3750.00, 3545.00, 'Madagasikara/Madagascar', 'Federal Republic', 'Didier Ratsiraka', 2455, 'MG'),
(135, 'MDV', 'Maldives', 'მალდივის კუნძულები', 'Asia', 'Southern and Central Asia', 298.00, 1965, 286000, 62.2, 199.00, NULL, 'Dhivehi Raajje/Maldives', 'Republic', 'Maumoon Abdul Gayoom', 2463, 'MV'),
(136, 'MEX', 'Mexico', 'მექსიკა', 'North America', 'Central America', 1958201.00, 1810, 98881000, 71.5, 414972.00, 401461.00, 'México', 'Federal Republic', 'Vicente Fox Quesada', 2515, 'MX'),
(137, 'MHL', 'Marshall Islands', 'მარშალის კუნძულები', 'Oceania', 'Micronesia', 181.00, 1990, 64000, 65.5, 97.00, NULL, 'Marshall Islands/Majol', 'Republic', 'Kessai Note', 2507, 'MH'),
(138, 'MKD', 'Macedonia', 'მაკედონია', 'Europe', 'Southern Europe', 25713.00, 1991, 2024000, 73.8, 1694.00, 1915.00, 'Makedonija', 'Republic', 'Boris Trajkovski', 2460, 'MK'),
(139, 'MLI', 'Mali', 'მალი', 'Africa', 'Western Africa', 1240192.00, 1960, 11234000, 46.7, 2642.00, 2453.00, 'Mali', 'Republic', 'Alpha Oumar Konaré', 2482, 'ML'),
(140, 'MLT', 'Malta', 'მალტა', 'Europe', 'Southern Europe', 316.00, 1964, 380200, 77.9, 3512.00, 3338.00, 'Malta', 'Republic', 'Guido de Marco', 2484, 'MT'),
(141, 'MMR', 'Myanmar', 'მიანმარი (ბირმა)', 'Asia', 'Southeast Asia', 676578.00, 1948, 45611000, 54.9, 180375.00, 171028.00, 'Myanma Pye', 'Republic', 'kenraali Than Shwe', 2710, 'MM'),
(142, 'MNG', 'Mongolia', 'მონღოლეთი', 'Asia', 'Eastern Asia', 1566500.00, 1921, 2662000, 67.3, 1043.00, 933.00, 'Mongol Uls', 'Republic', 'Natsagiin Bagabandi', 2696, 'MN'),
(143, 'MNP', 'Northern Mariana Islands', 'ჩრდილოეთ მარიანას კუნძულები', 'Oceania', 'Micronesia', 464.00, NULL, 78000, 75.5, 0.00, NULL, 'Northern Mariana Islands', 'Commonwealth of the US', 'George W. Bush', 2913, 'MP'),
(144, 'MOZ', 'Mozambique', 'მოზამბიკი', 'Africa', 'Eastern Africa', 801590.00, 1975, 19680000, 37.5, 2891.00, 2711.00, 'Moçambique', 'Republic', 'Joaquím A. Chissano', 2698, 'MZ'),
(145, 'MRT', 'Mauritania', 'მავრიტანია', 'Africa', 'Western Africa', 1025520.00, 1960, 2670000, 50.8, 998.00, 1081.00, 'Muritaniya/Mauritanie', 'Republic', 'Maaouiya Ould Sid´Ahmad Taya', 2509, 'MR'),
(146, 'MSR', 'Montserrat', 'მონსერატი', 'North America', 'Caribbean', 102.00, NULL, 11000, 78.0, 109.00, NULL, 'Montserrat', 'Dependent Territory of the UK', 'Elisabeth II', 2697, 'MS'),
(147, 'MTQ', 'Martinique', 'მარტინიკა', 'North America', 'Caribbean', 1102.00, NULL, 395000, 78.3, 2731.00, 2559.00, 'Martinique', 'Overseas Department of France', 'Jacques Chirac', 2508, 'MQ'),
(148, 'MUS', 'Mauritius', 'მავრიკი', 'Africa', 'Eastern Africa', 2040.00, 1968, 1158000, 71.0, 4251.00, 4186.00, 'Mauritius', 'Republic', 'Cassam Uteem', 2511, 'MU'),
(149, 'MWI', 'Malawi', 'მალავი', 'Africa', 'Eastern Africa', 118484.00, 1964, 10925000, 37.6, 1687.00, 2527.00, 'Malawi', 'Republic', 'Bakili Muluzi', 2462, 'MW'),
(150, 'MYS', 'Malaysia', 'მალაიზია', 'Asia', 'Southeast Asia', 329758.00, 1957, 22244000, 70.8, 69213.00, 97884.00, 'Malaysia', 'Constitutional Monarchy, Federation', 'Salahuddin Abdul Aziz Shah Alhaj', 2464, 'MY'),
(151, 'MYT', 'Mayotte', 'მაიოტა', 'Africa', 'Eastern Africa', 373.00, NULL, 149000, 59.5, 0.00, NULL, 'Mayotte', 'Territorial Collectivity of France', 'Jacques Chirac', 2514, 'YT'),
(152, 'NAM', 'Namibia', 'ნამიბია', 'Africa', 'Southern Africa', 824292.00, 1990, 1726000, 42.5, 3101.00, 3384.00, 'Namibia', 'Republic', 'Sam Nujoma', 2726, 'NA'),
(153, 'NCL', 'New Caledonia', 'ახალი კალედონია', 'Oceania', 'Melanesia', 18575.00, NULL, 214000, 72.8, 3563.00, NULL, 'Nouvelle-Calédonie', 'Nonmetropolitan Territory of France', 'Jacques Chirac', 3493, 'NC'),
(154, 'NER', 'Niger', 'ნიგერი', 'Africa', 'Western Africa', 1267000.00, 1960, 10730000, 41.3, 1706.00, 1580.00, 'Niger', 'Republic', 'Mamadou Tandja', 2738, 'NE'),
(155, 'NFK', 'Norfolk Island', 'ნორფოლკის კუნძული', 'Oceania', 'Australia and New Zealand', 36.00, NULL, 2000, NULL, 0.00, NULL, 'Norfolk Island', 'Territory of Australia', 'Elisabeth II', 2806, 'NF'),
(156, 'NGA', 'Nigeria', 'ნიგერია', 'Africa', 'Western Africa', 923768.00, 1960, 111506000, 51.6, 65707.00, 58623.00, 'Nigeria', 'Federal Republic', 'Olusegun Obasanjo', 2754, 'NG'),
(157, 'NIC', 'Nicaragua', 'ნიკარაგუა', 'North America', 'Central America', 130000.00, 1838, 5074000, 68.7, 1988.00, 2023.00, 'Nicaragua', 'Republic', 'Arnoldo Alemán Lacayo', 2734, 'NI'),
(158, 'NIU', 'Niue', 'ნიუე', 'Oceania', 'Polynesia', 260.00, NULL, 2000, NULL, 0.00, NULL, 'Niue', 'Nonmetropolitan Territory of New Zealand', 'Elisabeth II', 2805, 'NU'),
(159, 'NLD', 'Netherlands', 'ნიდერლანდები', 'Europe', 'Western Europe', 41526.00, 1581, 15864000, 78.3, 371362.00, 360478.00, 'Nederland', 'Constitutional Monarchy', 'Beatrix', 5, 'NL'),
(160, 'NOR', 'Norway', '', 'Europe', 'Nordic Countries', 323877.00, 1905, 4478500, 78.7, 145895.00, 153370.00, 'Norge', 'Constitutional Monarchy', 'Harald V', 2807, 'NO'),
(161, 'NPL', 'Nepal', 'ნეპალი', 'Asia', 'Southern and Central Asia', 147181.00, 1769, 23930000, 57.8, 4768.00, 4837.00, 'Nepal', 'Constitutional Monarchy', 'Gyanendra Bir Bikram', 2729, 'NP'),
(162, 'NRU', 'Nauru', 'ნაურუ', 'Oceania', 'Micronesia', 21.00, 1968, 12000, 60.8, 197.00, NULL, 'Naoero/Nauru', 'Republic', 'Bernard Dowiyogo', 2728, 'NR'),
(163, 'NZL', 'New Zealand', 'ახალი ზელანდია', 'Oceania', 'Australia and New Zealand', 270534.00, 1907, 3862000, 77.8, 54669.00, 64960.00, 'New Zealand/Aotearoa', 'Constitutional Monarchy', 'Elisabeth II', 3499, 'NZ'),
(164, 'OMN', 'Oman', 'ომანი', 'Asia', 'Middle East', 309500.00, 1951, 2542000, 71.8, 16904.00, 16153.00, '´Uman', 'Monarchy (Sultanate)', 'Qabus ibn Sa´id', 2821, 'OM'),
(165, 'PAK', 'Pakistan', 'პაკისტანი', 'Asia', 'Southern and Central Asia', 796095.00, 1947, 156483000, 61.1, 61289.00, 58549.00, 'Pakistan', 'Republic', 'Mohammad Rafiq Tarar', 2831, 'PK'),
(166, 'PAN', 'Panama', 'პანამა', 'North America', 'Central America', 75517.00, 1903, 2856000, 75.5, 9131.00, 8700.00, 'Panamá', 'Republic', 'Mireya Elisa Moscoso Rodríguez', 2882, 'PA'),
(167, 'PCN', 'Pitcairn', 'პიტკერნის კუნძულები', 'Oceania', 'Polynesia', 49.00, NULL, 50, NULL, 0.00, NULL, 'Pitcairn', 'Dependent Territory of the UK', 'Elisabeth II', 2912, 'PN'),
(168, 'PER', 'Peru', 'პერუ', 'South America', 'South America', 1285216.00, 1821, 25662000, 70.0, 64140.00, 65186.00, 'Perú/Piruw', 'Republic', 'Valentin Paniagua Corazao', 2890, 'PE'),
(169, 'PHL', 'Philippines', 'ფილიპინები', 'Asia', 'Southeast Asia', 300000.00, 1946, 75967000, 67.5, 65107.00, 82239.00, 'Pilipinas', 'Republic', 'Gloria Macapagal-Arroyo', 766, 'PH'),
(170, 'PLW', 'Palau', 'პალაუ', 'Oceania', 'Micronesia', 459.00, 1994, 19000, 68.6, 105.00, NULL, 'Belau/Palau', 'Republic', 'Kuniwo Nakamura', 2881, 'PW'),
(171, 'PNG', 'Papua New Guinea', 'პაპუა-ახალი გვინეა', 'Oceania', 'Melanesia', 462840.00, 1975, 4807000, 63.1, 4988.00, 6328.00, 'Papua New Guinea/Papua Niugini', 'Constitutional Monarchy', 'Elisabeth II', 2884, 'PG'),
(172, 'POL', 'Poland', 'პოლონეთი', 'Europe', 'Eastern Europe', 323250.00, 1918, 38653600, 73.2, 151697.00, 135636.00, 'Polska', 'Republic', 'Aleksander Kwasniewski', 2928, 'PL'),
(173, 'PRI', 'Puerto Rico', 'პუერტო-რიკო', 'North America', 'Caribbean', 8875.00, NULL, 3869000, 75.6, 34100.00, 32100.00, 'Puerto Rico', 'Commonwealth of the US', 'George W. Bush', 2919, 'PR'),
(174, 'PRK', 'North Korea', 'ჩრდილოეთი კორეა', 'Asia', 'Eastern Asia', 120538.00, 1948, 24039000, 70.7, 5332.00, NULL, 'Choson Minjujuui In´min Konghwaguk (Bukhan)', 'Socialistic Republic', 'Kim Jong-il', 2318, 'KP'),
(175, 'PRT', 'Portugal', 'პორტუგალია', 'Europe', 'Southern Europe', 91982.00, 1143, 9997600, 75.8, 105954.00, 102133.00, 'Portugal', 'Republic', 'Jorge Sampãio', 2914, 'PT'),
(176, 'PRY', 'Paraguay', 'პარაგვაი', 'South America', 'South America', 406752.00, 1811, 5496000, 73.7, 8444.00, 9555.00, 'Paraguay', 'Republic', 'Luis Ángel González Macchi', 2885, 'PY'),
(177, 'PSE', 'Palestine', 'პალესტინის ტერიტორიები', 'Asia', 'Middle East', 6257.00, NULL, 3101000, 71.4, 4173.00, NULL, 'Filastin', 'Autonomous Area', 'Yasser (Yasir) Arafat', 4074, 'PS'),
(178, 'PYF', 'French Polynesia', 'ფრანგული პოლინეზია', 'Oceania', 'Polynesia', 4000.00, NULL, 235000, 74.8, 818.00, 781.00, 'Polynésie française', 'Nonmetropolitan Territory of France', 'Jacques Chirac', 3016, 'PF'),
(179, 'QAT', 'Qatar', 'კატარი', 'Asia', 'Middle East', 11000.00, 1971, 599000, 72.4, 9472.00, 8920.00, 'Qatar', 'Monarchy', 'Hamad ibn Khalifa al-Thani', 2973, 'QA'),
(180, 'REU', 'Réunion', 'რეუნიონი', 'Africa', 'Eastern Africa', 2510.00, NULL, 699000, 72.7, 8287.00, 7988.00, 'Réunion', 'Overseas Department of France', 'Jacques Chirac', 3017, 'RE'),
(181, 'ROM', 'Romania', 'რუმინეთი', 'Europe', 'Eastern Europe', 238391.00, 1878, 22455500, 69.9, 38158.00, 34843.00, 'România', 'Republic', 'Ion Iliescu', 3018, 'RO'),
(182, 'RUS', 'Russian Federation', 'რუსეთი', 'Europe', 'Eastern Europe', 17075400.00, 1991, 146934000, 67.2, 276608.00, 442989.00, 'Rossija', 'Federal Republic', 'Vladimir Putin', 3580, 'RU'),
(183, 'RWA', 'Rwanda', 'რუანდა', 'Africa', 'Eastern Africa', 26338.00, 1962, 7733000, 39.3, 2036.00, 1863.00, 'Rwanda/Urwanda', 'Republic', 'Paul Kagame', 3047, 'RW'),
(184, 'SAU', 'Saudi Arabia', 'საუდის არაბეთი', 'Asia', 'Middle East', 2149690.00, 1932, 21607000, 67.8, 137635.00, 146171.00, 'Al-´Arabiya as-Sa´udiya', 'Monarchy', 'Fahd ibn Abdul-Aziz al-Sa´ud', 3173, 'SA'),
(185, 'SDN', 'Sudan', 'სუდანი', 'Africa', 'Northern Africa', 2505813.00, 1956, 29490000, 56.6, 10162.00, NULL, 'As-Sudan', 'Islamic Republic', 'Omar Hassan Ahmad al-Bashir', 3225, 'SD'),
(186, 'SEN', 'Senegal', 'სენეგალი', 'Africa', 'Western Africa', 196722.00, 1960, 9481000, 62.2, 4787.00, 4542.00, 'Sénégal/Sounougal', 'Republic', 'Abdoulaye Wade', 3198, 'SN'),
(187, 'SGP', 'Singapore', 'სინგაპური', 'Asia', 'Southeast Asia', 618.00, 1965, 3567000, 80.1, 86503.00, 96318.00, 'Singapore/Singapura/Xinjiapo/Singapur', 'Republic', 'Sellapan Rama Nathan', 3208, 'SG'),
(188, 'SGS', 'South Georgia and the South Sandwich Islands', 'სამხრეთი გეორგია და სამხრეთ სენდვიჩის კუნძულები', 'Antarctica', 'Antarctica', 3903.00, NULL, 0, NULL, 0.00, NULL, 'South Georgia and the South Sandwich Islands', 'Dependent Territory of the UK', 'Elisabeth II', NULL, 'GS'),
(189, 'SHN', 'Saint Helena', 'წმინდა ელენეს კუნძული', 'Africa', 'Western Africa', 314.00, NULL, 6000, 76.8, 0.00, NULL, 'Saint Helena', 'Dependent Territory of the UK', 'Elisabeth II', 3063, 'SH'),
(190, 'SJM', 'Svalbard and Jan Mayen', 'შპიცბერგენი და იან-მაიენი', 'Europe', 'Nordic Countries', 62422.00, NULL, 3200, NULL, 0.00, NULL, 'Svalbard og Jan Mayen', 'Dependent Territory of Norway', 'Harald V', 938, 'SJ'),
(191, 'SLB', 'Solomon Islands', 'სოლომონის კუნძულები', 'Oceania', 'Melanesia', 28896.00, 1978, 444000, 71.3, 182.00, 220.00, 'Solomon Islands', 'Constitutional Monarchy', 'Elisabeth II', 3161, 'SB'),
(192, 'SLE', 'Sierra Leone', 'სიერა-ლეონე', 'Africa', 'Western Africa', 71740.00, 1961, 4854000, 45.3, 746.00, 858.00, 'Sierra Leone', 'Republic', 'Ahmed Tejan Kabbah', 3207, 'SL'),
(193, 'SLV', 'El Salvador', 'სალვადორი', 'North America', 'Central America', 21041.00, 1841, 6276000, 69.7, 11863.00, 11203.00, 'El Salvador', 'Republic', 'Francisco Guillermo Flores Pérez', 645, 'SV'),
(194, 'SMR', 'San Marino', 'სან-მარინო', 'Europe', 'Southern Europe', 61.00, 885, 27000, 81.1, 510.00, NULL, 'San Marino', 'Republic', NULL, 3171, 'SM'),
(195, 'SOM', 'Somalia', 'სომალი', 'Africa', 'Eastern Africa', 637657.00, 1960, 10097000, 46.2, 935.00, NULL, 'Soomaaliya', 'Republic', 'Abdiqassim Salad Hassan', 3214, 'SO'),
(196, 'SPM', 'Saint Pierre and Miquelon', 'სენ-პიერი და მიკელონი', 'North America', 'North America', 242.00, NULL, 7000, 77.6, 0.00, NULL, 'Saint-Pierre-et-Miquelon', 'Territorial Collectivity of France', 'Jacques Chirac', 3067, 'PM'),
(197, 'STP', 'Sao Tome and Principe', 'სან-ტომე და პრინსიპი', 'Africa', 'Central Africa', 964.00, 1975, 147000, 65.3, 6.00, NULL, 'São Tomé e Príncipe', 'Republic', 'Miguel Trovoada', 3172, 'ST'),
(198, 'SUR', 'Suriname', 'სურინამი', 'South America', 'South America', 163265.00, 1975, 417000, 71.4, 870.00, 706.00, 'Suriname', 'Republic', 'Ronald Venetiaan', 3243, 'SR'),
(199, 'SVK', 'Slovakia', 'სლოვაკეთი', 'Europe', 'Eastern Europe', 49012.00, 1993, 5398700, 73.7, 20594.00, 19452.00, 'Slovensko', 'Republic', 'Rudolf Schuster', 3209, 'SK'),
(200, 'SVN', 'Slovenia', 'სლოვენია', 'Europe', 'Southern Europe', 20256.00, 1991, 1987800, 74.9, 19756.00, 18202.00, 'Slovenija', 'Republic', 'Milan Kucan', 3212, 'SI'),
(201, 'SWE', 'Sweden', 'შვედეთი', 'Europe', 'Nordic Countries', 449964.00, 836, 8861400, 79.6, 226492.00, 227757.00, 'Sverige', 'Constitutional Monarchy', 'Carl XVI Gustaf', 3048, 'SE'),
(202, 'SWZ', 'Swaziland', 'სვაზილენდი', 'Africa', 'Southern Africa', 17364.00, 1968, 1008000, 40.4, 1206.00, 1312.00, 'kaNgwane', 'Monarchy', 'Mswati III', 3244, 'SZ'),
(203, 'SYC', 'Seychelles', 'სეიშელის კუნძულები', 'Africa', 'Eastern Africa', 455.00, 1976, 77000, 70.4, 536.00, 539.00, 'Sesel/Seychelles', 'Republic', 'France-Albert René', 3206, 'SC'),
(204, 'SYR', 'Syria', 'სირია', 'Asia', 'Middle East', 185180.00, 1941, 16125000, 68.5, 65984.00, 64926.00, 'Suriya', 'Republic', 'Bashar al-Assad', 3250, 'SY'),
(205, 'TCA', 'Turks and Caicos Islands', 'ტერკსის და კაიკოსის კუნძულები', 'North America', 'Caribbean', 430.00, NULL, 17000, 73.3, 96.00, NULL, 'The Turks and Caicos Islands', 'Dependent Territory of the UK', 'Elisabeth II', 3423, 'TC'),
(206, 'TCD', 'Chad', 'ჩადი', 'Africa', 'Central Africa', 1284000.00, 1960, 7651000, 50.5, 1208.00, 1102.00, 'Tchad/Tshad', 'Republic', 'Idriss Déby', 3337, 'TD'),
(207, 'TGO', 'Togo', 'ტოგო', 'Africa', 'Western Africa', 56785.00, 1960, 4629000, 54.7, 1449.00, 1400.00, 'Togo', 'Republic', 'Gnassingbé Eyadéma', 3332, 'TG'),
(208, 'THA', 'Thailand', 'ტაილანდი', 'Asia', 'Southeast Asia', 513115.00, 1350, 61399000, 68.6, 116416.00, 153907.00, 'Prathet Thai', 'Constitutional Monarchy', 'Bhumibol Adulyadej', 3320, 'TH'),
(209, 'TJK', 'Tajikistan', 'ტაჯიკეთი', 'Asia', 'Southern and Central Asia', 143100.00, 1991, 6188000, 64.1, 1990.00, 1056.00, 'Toçikiston', 'Republic', 'Emomali Rahmonov', 3261, 'TJ'),
(210, 'TKL', 'Tokelau', 'ტოკელაუ', 'Oceania', 'Polynesia', 12.00, NULL, 2000, NULL, 0.00, NULL, 'Tokelau', 'Nonmetropolitan Territory of New Zealand', 'Elisabeth II', 3333, 'TK'),
(211, 'TKM', 'Turkmenistan', 'თურქმენეთი', 'Asia', 'Southern and Central Asia', 488100.00, 1991, 4459000, 60.9, 4397.00, 2000.00, 'Türkmenostan', 'Republic', 'Saparmurad Nijazov', 3419, 'TM'),
(212, 'TMP', 'East Timor', '', 'Asia', 'Southeast Asia', 14874.00, NULL, 885000, 46.0, 0.00, NULL, 'Timor Timur', 'Administrated by the UN', 'José Alexandre Gusmão', 1522, 'TP'),
(213, 'TON', 'Tonga', 'ტონგა', 'Oceania', 'Polynesia', 650.00, 1970, 99000, 67.9, 146.00, 170.00, 'Tonga', 'Monarchy', 'Taufa\'ahau Tupou IV', 3334, 'TO'),
(214, 'TTO', 'Trinidad and Tobago', 'ტრინიდადი და ტობაგო', 'North America', 'Caribbean', 5130.00, 1962, 1295000, 68.0, 6232.00, 5867.00, 'Trinidad and Tobago', 'Republic', 'Arthur N. R. Robinson', 3336, 'TT'),
(215, 'TUN', 'Tunisia', 'ტუნისი', 'Africa', 'Northern Africa', 163610.00, 1956, 9586000, 73.7, 20026.00, 18898.00, 'Tunis/Tunisie', 'Republic', 'Zine al-Abidine Ben Ali', 3349, 'TN'),
(216, 'TUR', 'Turkey', 'თურქეთი', 'Asia', 'Middle East', 774815.00, 1923, 66591000, 71.0, 210721.00, 189122.00, 'Türkiye', 'Republic', 'Ahmet Necdet Sezer', 3358, 'TR'),
(217, 'TUV', 'Tuvalu', 'ტუვალუ', 'Oceania', 'Polynesia', 26.00, 1978, 12000, 66.3, 6.00, NULL, 'Tuvalu', 'Constitutional Monarchy', 'Elisabeth II', 3424, 'TV'),
(218, 'TWN', 'Taiwan', 'ტაივანი', 'Asia', 'Eastern Asia', 36188.00, 1945, 22256000, 76.4, 256254.00, 263451.00, 'T’ai-wan', 'Republic', 'Chen Shui-bian', 3263, 'TW'),
(219, 'TZA', 'Tanzania', 'ტანზანია', 'Africa', 'Eastern Africa', 883749.00, 1961, 33517000, 52.3, 8005.00, 7388.00, 'Tanzania', 'Republic', 'Benjamin William Mkapa', 3306, 'TZ'),
(220, 'UGA', 'Uganda', 'უგანდა', 'Africa', 'Eastern Africa', 241038.00, 1962, 21778000, 42.9, 6313.00, 6887.00, 'Uganda', 'Republic', 'Yoweri Museveni', 3425, 'UG'),
(221, 'UKR', 'Ukraine', 'უკრაინა', 'Europe', 'Eastern Europe', 603700.00, 1991, 50456000, 66.0, 42168.00, 49677.00, 'Ukrajina', 'Republic', 'Leonid Kutšma', 3426, 'UA'),
(222, 'UMI', 'United States Minor Outlying Islands', 'აშშ-ის შორეული კუნძულები', 'Oceania', 'Micronesia/Caribbean', 16.00, NULL, 0, NULL, 0.00, NULL, 'United States Minor Outlying Islands', 'Dependent Territory of the US', 'George W. Bush', NULL, 'UM'),
(223, 'URY', 'Uruguay', 'ურუგვაი', 'South America', 'South America', 175016.00, 1828, 3337000, 75.2, 20831.00, 19967.00, 'Uruguay', 'Republic', 'Jorge Batlle Ibáñez', 3492, 'UY'),
(224, 'USA', 'United States', 'ამერიკის შეერთებული შტატები', 'North America', 'North America', 9363520.00, 1776, 278357000, 77.1, 8510700.00, 8110900.00, 'United States', 'Federal Republic', 'George W. Bush', 3813, 'US'),
(225, 'UZB', 'Uzbekistan', 'უზბეკეთი', 'Asia', 'Southern and Central Asia', 447400.00, 1991, 24318000, 63.7, 14194.00, 21300.00, 'Uzbekiston', 'Republic', 'Islam Karimov', 3503, 'UZ'),
(226, 'VAT', 'Holy See (Vatican City State)', 'ქალაქი ვატიკანი', 'Europe', 'Southern Europe', 0.40, 1929, 1000, NULL, 9.00, NULL, 'Santa Sede/Città del Vaticano', 'Independent Church State', 'Johannes Paavali II', 3538, 'VA'),
(227, 'VCT', 'Saint Vincent and the Grenadines', 'სენტ-ვინსენტი და გრენადინები', 'North America', 'Caribbean', 388.00, 1979, 114000, 72.3, 285.00, NULL, 'Saint Vincent and the Grenadines', 'Constitutional Monarchy', 'Elisabeth II', 3066, 'VC'),
(228, 'VEN', 'Venezuela', 'ვენესუელა', 'South America', 'South America', 912050.00, 1811, 24170000, 73.1, 95023.00, 88434.00, 'Venezuela', 'Federal Republic', 'Hugo Chávez Frías', 3539, 'VE'),
(229, 'VGB', 'Virgin Islands, British', 'ბრიტანეთის ვირჯინიის კუნძულები', 'North America', 'Caribbean', 151.00, NULL, 21000, 75.4, 612.00, 573.00, 'British Virgin Islands', 'Dependent Territory of the UK', 'Elisabeth II', 537, 'VG'),
(230, 'VIR', 'Virgin Islands, U.S.', 'აშშ-ის ვირჯინიის კუნძულები', 'North America', 'Caribbean', 347.00, NULL, 93000, 78.1, 0.00, NULL, 'Virgin Islands of the United States', 'US Territory', 'George W. Bush', 4067, 'VI'),
(231, 'VNM', 'Vietnam', 'ვიეტნამი', 'Asia', 'Southeast Asia', 331689.00, 1945, 79832000, 69.3, 21929.00, 22834.00, 'Viêt Nam', 'Socialistic Republic', 'Trân Duc Luong', 3770, 'VN'),
(232, 'VUT', 'Vanuatu', 'ვანუატუ', 'Oceania', 'Melanesia', 12189.00, 1980, 190000, 60.6, 261.00, 246.00, 'Vanuatu', 'Republic', 'John Bani', 3537, 'VU'),
(233, 'WLF', 'Wallis and Futuna', 'უოლისი და ფუტუნა', 'Oceania', 'Polynesia', 200.00, NULL, 15000, NULL, 0.00, NULL, 'Wallis-et-Futuna', 'Nonmetropolitan Territory of France', 'Jacques Chirac', 3536, 'WF'),
(234, 'WSM', 'Samoa', 'სამოა', 'Oceania', 'Polynesia', 2831.00, 1962, 180000, 69.2, 141.00, 157.00, 'Samoa', 'Parlementary Monarchy', 'Malietoa Tanumafili II', 3169, 'WS'),
(235, 'YEM', 'Yemen', 'იემენი', 'Asia', 'Middle East', 527968.00, 1918, 18112000, 59.8, 6041.00, 5729.00, 'Al-Yaman', 'Republic', 'Ali Abdallah Salih', 1780, 'YE'),
(236, 'YUG', 'Yugoslavia', '', 'Europe', 'Southern Europe', 102173.00, 1918, 10640000, 72.4, 17000.00, NULL, 'Jugoslavija', 'Federal Republic', 'Vojislav Koštunica', 1792, 'YU'),
(237, 'ZAF', 'South Africa', 'სამხრეთ აფრიკა', 'Africa', 'Southern Africa', 1221037.00, 1910, 40377000, 51.1, 116729.00, 129092.00, 'South Africa', 'Republic', 'Thabo Mbeki', 716, 'ZA'),
(238, 'ZMB', 'Zambia', 'ზამბია', 'Africa', 'Eastern Africa', 752618.00, 1964, 9169000, 37.2, 3377.00, 3922.00, 'Zambia', 'Republic', 'Frederick Chiluba', 3162, 'ZM'),
(239, 'ZWE', 'Zimbabwe', 'ზიმბაბვე', 'Africa', 'Eastern Africa', 390757.00, 1980, 11669000, 37.8, 5951.00, 8670.00, 'Zimbabwe', 'Republic', 'Robert G. Mugabe', 4068, 'ZW');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_extensions`
--

CREATE TABLE `kutah_extensions` (
  `extension_id` int(11) NOT NULL,
  `package_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Parent package ID for extensions installed as a package.',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `system_data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_extensions`
--

INSERT INTO `kutah_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 0, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{\"name\":\"com_mailto\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MAILTO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mailto\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 0, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{\"name\":\"com_wrapper\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\\n\\t\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 0, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{\"name\":\"com_admin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_ADMIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 0, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{\"name\":\"com_banners\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"banners\"}', '{\"purchase_type\":\"3\",\"track_impressions\":\"0\",\"track_clicks\":\"0\",\"metakey_prefix\":\"\",\"save_history\":\"1\",\"history_limit\":10}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 0, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{\"name\":\"com_cache\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CACHE_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 0, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{\"name\":\"com_categories\",\"type\":\"component\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 0, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{\"name\":\"com_checkin\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CHECKIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 0, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{\"name\":\"com_contact\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '{\"contact_layout\":\"_:default\",\"show_contact_category\":\"hide\",\"save_history\":\"1\",\"history_limit\":10,\"show_contact_list\":\"0\",\"presentation_style\":\"sliders\",\"show_tags\":\"1\",\"show_info\":\"1\",\"show_name\":\"1\",\"show_position\":\"1\",\"show_email\":\"0\",\"show_street_address\":\"1\",\"show_suburb\":\"1\",\"show_state\":\"1\",\"show_postcode\":\"1\",\"show_country\":\"1\",\"show_telephone\":\"1\",\"show_mobile\":\"1\",\"show_fax\":\"1\",\"show_webpage\":\"1\",\"show_image\":\"1\",\"show_misc\":\"1\",\"image\":\"\",\"allow_vcard\":\"0\",\"show_articles\":\"0\",\"articles_display_num\":\"10\",\"show_profile\":\"0\",\"show_user_custom_fields\":[\"-1\"],\"show_links\":\"0\",\"linka_name\":\"\",\"linkb_name\":\"\",\"linkc_name\":\"\",\"linkd_name\":\"\",\"linke_name\":\"\",\"contact_icons\":\"0\",\"icon_address\":\"\",\"icon_email\":\"\",\"icon_telephone\":\"\",\"icon_mobile\":\"\",\"icon_fax\":\"\",\"icon_misc\":\"\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"0\",\"maxLevel\":\"-1\",\"show_subcat_desc\":\"1\",\"show_empty_categories\":\"0\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_subcat_desc_cat\":\"1\",\"show_empty_categories_cat\":\"0\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"0\",\"show_pagination_limit\":\"0\",\"show_headings\":\"1\",\"show_image_heading\":\"0\",\"show_position_headings\":\"1\",\"show_email_headings\":\"0\",\"show_telephone_headings\":\"1\",\"show_mobile_headings\":\"0\",\"show_fax_headings\":\"0\",\"show_suburb_headings\":\"1\",\"show_state_headings\":\"1\",\"show_country_headings\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"initial_sort\":\"ordering\",\"captcha\":\"\",\"show_email_form\":\"1\",\"show_email_copy\":\"1\",\"banned_email\":\"\",\"banned_subject\":\"\",\"banned_text\":\"\",\"validate_session\":\"1\",\"custom_reply\":\"0\",\"redirect\":\"\",\"show_feed_link\":\"1\",\"sef_advanced\":0,\"sef_ids\":0,\"custom_fields_enable\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 0, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{\"name\":\"com_cpanel\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CPANEL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 0, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{\"name\":\"com_installer\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_INSTALLER_XML_DESCRIPTION\",\"group\":\"\"}', '{\"show_jed_info\":\"1\",\"cachetimeout\":\"6\",\"minimum_stability\":\"4\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 0, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{\"name\":\"com_languages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"administrator\":\"en-GB\",\"site\":\"ka-GE\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 0, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{\"name\":\"com_login\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_LOGIN_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 0, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{\"name\":\"com_media\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '{\"upload_extensions\":\"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,TXT,XCF,XLS\",\"upload_maxsize\":\"10\",\"file_path\":\"images\",\"image_path\":\"images\",\"restrict_uploads\":\"0\",\"check_mime\":\"1\",\"image_extensions\":\"bmp,gif,jpg,png\",\"ignore_extensions\":\"\",\"upload_mime\":\"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip\",\"upload_mime_illegal\":\"text\\/html\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 0, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{\"name\":\"com_menus\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MENUS_XML_DESCRIPTION\",\"group\":\"\"}', '{\"page_title\":\"\",\"show_page_heading\":0,\"page_heading\":\"\",\"pageclass_sfx\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 0, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{\"name\":\"com_messages\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MESSAGES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 0, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{\"name\":\"com_modules\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_MODULES_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 0, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{\"name\":\"com_newsfeeds\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"newsfeed_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_feed_image\":\"1\",\"show_feed_description\":\"1\",\"show_item_description\":\"1\",\"feed_character_count\":\"0\",\"feed_display_order\":\"des\",\"float_first\":\"right\",\"float_second\":\"right\",\"show_tags\":\"1\",\"category_layout\":\"_:default\",\"show_category_title\":\"1\",\"show_description\":\"1\",\"show_description_image\":\"1\",\"maxLevel\":\"-1\",\"show_empty_categories\":\"0\",\"show_subcat_desc\":\"1\",\"show_cat_items\":\"1\",\"show_cat_tags\":\"1\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_items_cat\":\"1\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_headings\":\"1\",\"show_articles\":\"0\",\"show_link\":\"1\",\"show_pagination\":\"1\",\"show_pagination_results\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 0, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{\"name\":\"com_plugins\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_PLUGINS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 0, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{\"name\":\"com_search\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"search\"}', '{\"enabled\":\"0\",\"search_phrases\":\"1\",\"search_areas\":\"1\",\"show_date\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 0, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{\"name\":\"com_templates\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_TEMPLATES_XML_DESCRIPTION\",\"group\":\"\"}', '{\"template_positions_display\":\"0\",\"upload_limit\":\"10\",\"image_formats\":\"gif,bmp,jpg,jpeg,png\",\"source_formats\":\"txt,less,ini,xml,js,php,css,scss,sass\",\"font_formats\":\"woff,ttf,otf\",\"compressed_formats\":\"zip\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 0, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{\"name\":\"com_content\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"article_layout\":\"_:default\",\"show_title\":\"1\",\"link_titles\":\"1\",\"show_intro\":\"1\",\"show_category\":\"1\",\"link_category\":\"1\",\"show_parent_category\":\"0\",\"link_parent_category\":\"0\",\"show_author\":\"1\",\"link_author\":\"0\",\"show_create_date\":\"0\",\"show_modify_date\":\"0\",\"show_publish_date\":\"1\",\"show_item_navigation\":\"1\",\"show_vote\":\"0\",\"show_readmore\":\"1\",\"show_readmore_title\":\"1\",\"readmore_limit\":\"100\",\"show_icons\":\"1\",\"show_print_icon\":\"1\",\"show_email_icon\":\"1\",\"show_hits\":\"1\",\"show_noauth\":\"0\",\"show_publishing_options\":\"1\",\"show_article_options\":\"1\",\"save_history\":\"1\",\"history_limit\":10,\"show_urls_images_frontend\":\"0\",\"show_urls_images_backend\":\"1\",\"targeta\":0,\"targetb\":0,\"targetc\":0,\"float_intro\":\"left\",\"float_fulltext\":\"left\",\"category_layout\":\"_:blog\",\"show_category_title\":\"0\",\"show_description\":\"0\",\"show_description_image\":\"0\",\"maxLevel\":\"1\",\"show_empty_categories\":\"0\",\"show_no_articles\":\"1\",\"show_subcat_desc\":\"1\",\"show_cat_num_articles\":\"0\",\"show_base_description\":\"1\",\"maxLevelcat\":\"-1\",\"show_empty_categories_cat\":\"0\",\"show_subcat_desc_cat\":\"1\",\"show_cat_num_articles_cat\":\"1\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"4\",\"num_columns\":\"2\",\"num_links\":\"4\",\"multi_column_order\":\"0\",\"show_subcategory_content\":\"0\",\"show_pagination_limit\":\"1\",\"filter_field\":\"hide\",\"show_headings\":\"1\",\"list_show_date\":\"0\",\"date_format\":\"\",\"list_show_hits\":\"1\",\"list_show_author\":\"1\",\"orderby_pri\":\"order\",\"orderby_sec\":\"rdate\",\"order_date\":\"published\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 0, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{\"name\":\"com_config\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_CONFIG_XML_DESCRIPTION\",\"group\":\"\"}', '{\"filters\":{\"1\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"9\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"6\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"7\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"2\":{\"filter_type\":\"NH\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"3\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"4\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"5\":{\"filter_type\":\"BL\",\"filter_tags\":\"\",\"filter_attributes\":\"\"},\"8\":{\"filter_type\":\"NONE\",\"filter_tags\":\"\",\"filter_attributes\":\"\"}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 0, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{\"name\":\"com_redirect\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 0, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{\"name\":\"com_users\",\"type\":\"component\",\"creationDate\":\"April 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_USERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"users\"}', '{\"allowUserRegistration\":\"1\",\"new_usertype\":\"2\",\"guest_usergroup\":\"9\",\"sendpassword\":\"1\",\"useractivation\":\"2\",\"mail_to_admin\":\"1\",\"captcha\":\"\",\"frontend_userparams\":\"1\",\"site_language\":\"0\",\"change_login_name\":\"0\",\"reset_count\":\"10\",\"reset_time\":\"1\",\"minimum_length\":\"4\",\"minimum_integers\":\"0\",\"minimum_symbols\":\"0\",\"minimum_uppercase\":\"0\",\"save_history\":\"1\",\"history_limit\":5,\"mailSubjectPrefix\":\"\",\"mailBodySuffix\":\"\",\"debugUsers\":\"1\",\"debugGroups\":\"1\",\"custom_fields_enable\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 0, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{\"name\":\"com_finder\",\"type\":\"component\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"COM_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{\"enabled\":\"0\",\"show_description\":\"1\",\"description_length\":255,\"allow_empty_query\":\"0\",\"show_url\":\"1\",\"show_autosuggest\":\"1\",\"show_suggested_query\":\"1\",\"show_explained_query\":\"1\",\"show_advanced\":\"1\",\"show_advanced_tips\":\"1\",\"expand_advanced\":\"0\",\"show_date_filters\":\"0\",\"sort_order\":\"relevance\",\"sort_direction\":\"desc\",\"highlight_terms\":\"1\",\"opensearch_name\":\"\",\"opensearch_description\":\"\",\"batch_size\":\"50\",\"memory_table_limit\":30000,\"title_multiplier\":\"1.7\",\"text_multiplier\":\"0.7\",\"meta_multiplier\":\"1.2\",\"path_multiplier\":\"2.0\",\"misc_multiplier\":\"0.3\",\"stem\":\"1\",\"stemmer\":\"snowball\",\"enable_logging\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 0, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{\"name\":\"com_joomlaupdate\",\"type\":\"component\",\"creationDate\":\"February 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.2\",\"description\":\"COM_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\"}', '{\"updatesource\":\"default\",\"customurl\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 0, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{\"name\":\"com_tags\",\"type\":\"component\",\"creationDate\":\"December 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"COM_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"tag_layout\":\"_:default\",\"save_history\":\"1\",\"history_limit\":5,\"show_tag_title\":\"0\",\"tag_list_show_tag_image\":\"0\",\"tag_list_show_tag_description\":\"0\",\"tag_list_image\":\"\",\"tag_list_orderby\":\"title\",\"tag_list_orderby_direction\":\"ASC\",\"show_headings\":\"0\",\"tag_list_show_date\":\"0\",\"tag_list_show_item_image\":\"0\",\"tag_list_show_item_description\":\"0\",\"tag_list_item_maximum_characters\":0,\"return_any_or_all\":\"1\",\"include_children\":\"0\",\"maximum\":200,\"tag_list_language_filter\":\"all\",\"tags_layout\":\"_:default\",\"all_tags_orderby\":\"title\",\"all_tags_orderby_direction\":\"ASC\",\"all_tags_show_tag_image\":\"0\",\"all_tags_show_tag_descripion\":\"0\",\"all_tags_tag_maximum_characters\":20,\"all_tags_show_tag_hits\":\"0\",\"filter_field\":\"1\",\"show_pagination_limit\":\"1\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"tag_field_ajax_mode\":\"1\",\"show_feed_link\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 0, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{\"name\":\"com_contenthistory\",\"type\":\"component\",\"creationDate\":\"May 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_CONTENTHISTORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contenthistory\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 0, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 1, '{\"name\":\"com_ajax\",\"type\":\"component\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_AJAX_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ajax\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 0, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{\"name\":\"com_postinstall\",\"type\":\"component\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"COM_POSTINSTALL_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(33, 0, 'com_fields', 'component', 'com_fields', '', 1, 1, 1, 0, '{\"name\":\"com_fields\",\"type\":\"component\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(34, 0, 'com_associations', 'component', 'com_associations', '', 1, 1, 1, 0, '{\"name\":\"com_associations\",\"type\":\"component\",\"creationDate\":\"Januar 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"COM_ASSOCIATIONS_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 0, 'LIB_PHPUTF8', 'library', 'phputf8', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPUTF8\",\"type\":\"library\",\"creationDate\":\"2006\",\"author\":\"Harry Fuecks\",\"copyright\":\"Copyright various authors\",\"authorEmail\":\"hfuecks@gmail.com\",\"authorUrl\":\"http:\\/\\/sourceforge.net\\/projects\\/phputf8\",\"version\":\"0.5\",\"description\":\"LIB_PHPUTF8_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phputf8\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 0, 'LIB_JOOMLA', 'library', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"LIB_JOOMLA\",\"type\":\"library\",\"creationDate\":\"2008\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"https:\\/\\/www.joomla.org\",\"version\":\"13.1\",\"description\":\"LIB_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"mediaversion\":\"da368d2548bb80ba360ce2d0afb8ad82\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 0, 'LIB_IDNA', 'library', 'idna_convert', '', 0, 1, 1, 1, '{\"name\":\"LIB_IDNA\",\"type\":\"library\",\"creationDate\":\"2004\",\"author\":\"phlyLabs\",\"copyright\":\"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de\",\"authorEmail\":\"phlymail@phlylabs.de\",\"authorUrl\":\"http:\\/\\/phlylabs.de\",\"version\":\"0.8.0\",\"description\":\"LIB_IDNA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"idna_convert\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 0, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{\"name\":\"FOF\",\"type\":\"library\",\"creationDate\":\"2015-04-22 13:15:32\",\"author\":\"Nicholas K. Dionysopoulos \\/ Akeeba Ltd\",\"copyright\":\"(C)2011-2015 Nicholas K. Dionysopoulos\",\"authorEmail\":\"nicholas@akeebabackup.com\",\"authorUrl\":\"https:\\/\\/www.akeebabackup.com\",\"version\":\"2.4.3\",\"description\":\"LIB_FOF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fof\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(106, 0, 'LIB_PHPASS', 'library', 'phpass', '', 0, 1, 1, 1, '{\"name\":\"LIB_PHPASS\",\"type\":\"library\",\"creationDate\":\"2004-2006\",\"author\":\"Solar Designer\",\"copyright\":\"\",\"authorEmail\":\"solar@openwall.com\",\"authorUrl\":\"http:\\/\\/www.openwall.com\\/phpass\\/\",\"version\":\"0.3\",\"description\":\"LIB_PHPASS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpass\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 0, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_archive\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_archive\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 0, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 0, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_popular\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 0, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{\"name\":\"mod_banners\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BANNERS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_banners\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 0, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{\"name\":\"mod_breadcrumbs\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_BREADCRUMBS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_breadcrumbs\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 0, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 0, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 0, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{\"name\":\"mod_footer\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FOOTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_footer\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 0, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 0, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 0, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_news\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_NEWS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_news\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 0, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{\"name\":\"mod_random_image\",\"type\":\"module\",\"creationDate\":\"July 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RANDOM_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_random_image\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 0, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{\"name\":\"mod_related_items\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_RELATED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_related_items\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 0, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{\"name\":\"mod_search\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SEARCH_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_search\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 0, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{\"name\":\"mod_stats\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 0, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{\"name\":\"mod_syndicate\",\"type\":\"module\",\"creationDate\":\"May 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SYNDICATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_syndicate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 0, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{\"name\":\"mod_users_latest\",\"type\":\"module\",\"creationDate\":\"December 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_USERS_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_users_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 0, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{\"name\":\"mod_whosonline\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WHOSONLINE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_whosonline\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 0, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{\"name\":\"mod_wrapper\",\"type\":\"module\",\"creationDate\":\"October 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_WRAPPER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_wrapper\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 0, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_category\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_category\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 0, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{\"name\":\"mod_articles_categories\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_articles_categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 0, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{\"name\":\"mod_languages\",\"type\":\"module\",\"creationDate\":\"February 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"MOD_LANGUAGES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_languages\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 0, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{\"name\":\"mod_finder\",\"type\":\"module\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 0, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{\"name\":\"mod_custom\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_CUSTOM_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_custom\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 0, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{\"name\":\"mod_feed\",\"type\":\"module\",\"creationDate\":\"July 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_FEED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_feed\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 0, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{\"name\":\"mod_latest\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LATEST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_latest\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 0, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{\"name\":\"mod_logged\",\"type\":\"module\",\"creationDate\":\"January 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGGED_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_logged\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 0, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{\"name\":\"mod_login\",\"type\":\"module\",\"creationDate\":\"March 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_LOGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_login\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 0, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{\"name\":\"mod_menu\",\"type\":\"module\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 0, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{\"name\":\"mod_popular\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_popular\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 0, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{\"name\":\"mod_quickicon\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_QUICKICON_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_quickicon\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 0, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{\"name\":\"mod_status\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_status\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 0, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{\"name\":\"mod_submenu\",\"type\":\"module\",\"creationDate\":\"Feb 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_SUBMENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_submenu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 0, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{\"name\":\"mod_title\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TITLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_title\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 0, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{\"name\":\"mod_toolbar\",\"type\":\"module\",\"creationDate\":\"Nov 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_TOOLBAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_toolbar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 0, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{\"name\":\"mod_multilangstatus\",\"type\":\"module\",\"creationDate\":\"September 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_MULTILANGSTATUS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_multilangstatus\"}', '{\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 0, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{\"name\":\"mod_version\",\"type\":\"module\",\"creationDate\":\"January 2012\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_VERSION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_version\"}', '{\"format\":\"short\",\"product\":\"1\",\"cache\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 0, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{\"name\":\"mod_stats_admin\",\"type\":\"module\",\"creationDate\":\"July 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"MOD_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_stats_admin\"}', '{\"serverinfo\":\"0\",\"siteinfo\":\"0\",\"counter\":\"0\",\"increase\":\"0\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 0, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_popular\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_POPULAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_popular\"}', '{\"maximum\":\"5\",\"timeframe\":\"alltime\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 0, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{\"name\":\"mod_tags_similar\",\"type\":\"module\",\"creationDate\":\"January 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.1.0\",\"description\":\"MOD_TAGS_SIMILAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_tags_similar\"}', '{\"maximum\":\"5\",\"matchtype\":\"any\",\"owncache\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 0, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_gmail\",\"type\":\"plugin\",\"creationDate\":\"February 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_GMAIL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"gmail\"}', '{\"applysuffix\":\"0\",\"suffix\":\"\",\"verifypeer\":\"1\",\"user_blacklist\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 0, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{\"name\":\"plg_authentication_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 0, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{\"name\":\"plg_authentication_ldap\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LDAP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"ldap\"}', '{\"host\":\"\",\"port\":\"389\",\"use_ldapV3\":\"0\",\"negotiate_tls\":\"0\",\"no_referrals\":\"0\",\"auth_method\":\"bind\",\"base_dn\":\"\",\"search_string\":\"\",\"users_dn\":\"\",\"username\":\"admin\",\"password\":\"bobby7\",\"ldap_fullname\":\"fullName\",\"ldap_email\":\"mail\",\"ldap_uid\":\"uid\"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(403, 0, 'plg_content_contact', 'plugin', 'contact', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_contact\",\"type\":\"plugin\",\"creationDate\":\"January 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.2\",\"description\":\"PLG_CONTENT_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0);
INSERT INTO `kutah_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(404, 0, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_emailcloak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"emailcloak\"}', '{\"mode\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(406, 0, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_loadmodule\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOADMODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"loadmodule\"}', '{\"style\":\"xhtml\"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 0, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"multipage_toc\":\"1\",\"showall\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 0, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_pagenavigation\",\"type\":\"plugin\",\"creationDate\":\"January 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_PAGENAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagenavigation\"}', '{\"position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 0, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_vote\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 0, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_codemirror\",\"type\":\"plugin\",\"creationDate\":\"28 March 2011\",\"author\":\"Marijn Haverbeke\",\"copyright\":\"Copyright (C) 2014 - 2017 by Marijn Haverbeke <marijnh@gmail.com> and others\",\"authorEmail\":\"marijnh@gmail.com\",\"authorUrl\":\"http:\\/\\/codemirror.net\\/\",\"version\":\"5.25.2\",\"description\":\"PLG_CODEMIRROR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"codemirror\"}', '{\"lineNumbers\":\"1\",\"lineWrapping\":\"1\",\"matchTags\":\"1\",\"matchBrackets\":\"1\",\"marker-gutter\":\"1\",\"autoCloseTags\":\"1\",\"autoCloseBrackets\":\"1\",\"autoFocus\":\"1\",\"theme\":\"default\",\"tabmode\":\"indent\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 0, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{\"name\":\"plg_editors_none\",\"type\":\"plugin\",\"creationDate\":\"September 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_NONE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"none\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 0, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{\"name\":\"plg_editors_tinymce\",\"type\":\"plugin\",\"creationDate\":\"2005-2017\",\"author\":\"Ephox Corporation\",\"copyright\":\"Ephox Corporation\",\"authorEmail\":\"N\\/A\",\"authorUrl\":\"http:\\/\\/www.tinymce.com\",\"version\":\"4.5.7\",\"description\":\"PLG_TINY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tinymce\"}', '{\"configuration\":{\"toolbars\":{\"2\":{\"toolbar1\":[\"bold\",\"underline\",\"strikethrough\",\"|\",\"undo\",\"redo\",\"|\",\"bullist\",\"numlist\",\"|\",\"pastetext\"]},\"1\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"formatselect\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"code\",\"|\",\"hr\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"pastetext\",\"preview\"]},\"0\":{\"menu\":[\"edit\",\"insert\",\"view\",\"format\",\"table\",\"tools\"],\"toolbar1\":[\"bold\",\"italic\",\"underline\",\"strikethrough\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"alignjustify\",\"|\",\"styleselect\",\"|\",\"formatselect\",\"fontselect\",\"fontsizeselect\",\"|\",\"searchreplace\",\"|\",\"bullist\",\"numlist\",\"|\",\"outdent\",\"indent\",\"|\",\"undo\",\"redo\",\"|\",\"link\",\"unlink\",\"anchor\",\"image\",\"|\",\"code\",\"|\",\"forecolor\",\"backcolor\",\"|\",\"fullscreen\",\"|\",\"table\",\"|\",\"subscript\",\"superscript\",\"|\",\"charmap\",\"emoticons\",\"media\",\"hr\",\"ltr\",\"rtl\",\"|\",\"cut\",\"copy\",\"paste\",\"pastetext\",\"|\",\"visualchars\",\"visualblocks\",\"nonbreaking\",\"blockquote\",\"template\",\"|\",\"print\",\"preview\",\"codesample\",\"insertdatetime\",\"removeformat\"]}},\"setoptions\":{\"2\":{\"access\":[\"1\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"1\":{\"access\":[\"6\",\"2\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"0\",\"advlist\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"},\"0\":{\"access\":[\"7\",\"4\",\"8\"],\"skin\":\"0\",\"skin_admin\":\"0\",\"mobile\":\"0\",\"drag_drop\":\"1\",\"path\":\"\",\"entity_encoding\":\"raw\",\"lang_mode\":\"1\",\"text_direction\":\"ltr\",\"content_css\":\"1\",\"content_css_custom\":\"\",\"relative_urls\":\"1\",\"newlines\":\"0\",\"use_config_textfilters\":\"0\",\"invalid_elements\":\"script,applet,iframe\",\"valid_elements\":\"\",\"extended_elements\":\"\",\"resizing\":\"1\",\"resize_horizontal\":\"1\",\"element_path\":\"1\",\"wordcount\":\"1\",\"image_advtab\":\"1\",\"advlist\":\"1\",\"contextmenu\":\"1\",\"custom_plugin\":\"\",\"custom_button\":\"\"}}},\"sets_amount\":3,\"html_height\":\"550\",\"html_width\":\"750\"}', '', '', 655, '2017-07-16 18:56:21', 3, 0),
(413, 0, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_article\",\"type\":\"plugin\",\"creationDate\":\"October 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_ARTICLE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"article\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 0, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_image\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_IMAGE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"image\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 0, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"August 2004\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 0, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_readmore\",\"type\":\"plugin\",\"creationDate\":\"March 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_READMORE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"readmore\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 0, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_categories\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 0, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_contacts\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 0, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_content\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 0, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"November 2005\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '{\"search_limit\":\"50\",\"search_content\":\"1\",\"search_archived\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 0, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_languagefilter\",\"type\":\"plugin\",\"creationDate\":\"July 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagefilter\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 0, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_p3p\",\"type\":\"plugin\",\"creationDate\":\"September 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_P3P_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"p3p\"}', '{\"headers\":\"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 0, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_cache\",\"type\":\"plugin\",\"creationDate\":\"February 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CACHE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cache\"}', '{\"browsercache\":\"0\",\"cachetime\":\"15\"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 0, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_debug\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_DEBUG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"debug\"}', '{\"profile\":\"1\",\"queries\":\"1\",\"memory\":\"1\",\"language_files\":\"1\",\"language_strings\":\"1\",\"strip-first\":\"1\",\"strip-prefix\":\"\",\"strip-suffix\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 0, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_log\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_LOG_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"log\"}', '', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 0, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 0, 1, 1, '{\"name\":\"plg_system_redirect\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_REDIRECT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"redirect\"}', '', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 0, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_remember\",\"type\":\"plugin\",\"creationDate\":\"April 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_REMEMBER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"remember\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 0, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_sef\",\"type\":\"plugin\",\"creationDate\":\"December 2007\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEF_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sef\"}', '', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 0, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{\"name\":\"plg_system_logout\",\"type\":\"plugin\",\"creationDate\":\"April 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"logout\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 0, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_contactcreator\",\"type\":\"plugin\",\"creationDate\":\"August 2009\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTACTCREATOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contactcreator\"}', '{\"autowebpage\":\"\",\"category\":\"34\",\"autopublish\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 0, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{\"name\":\"plg_user_joomla\",\"type\":\"plugin\",\"creationDate\":\"December 2006\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '{\"autoregister\":\"1\",\"mail_to_user\":\"1\",\"forceLogout\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 0, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{\"name\":\"plg_user_profile\",\"type\":\"plugin\",\"creationDate\":\"January 2008\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_USER_PROFILE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"profile\"}', '{\"register-require_address1\":\"1\",\"register-require_address2\":\"1\",\"register-require_city\":\"1\",\"register-require_region\":\"1\",\"register-require_country\":\"1\",\"register-require_postal_code\":\"1\",\"register-require_phone\":\"1\",\"register-require_website\":\"1\",\"register-require_favoritebook\":\"1\",\"register-require_aboutme\":\"1\",\"register-require_tos\":\"1\",\"register-require_dob\":\"1\",\"profile-require_address1\":\"1\",\"profile-require_address2\":\"1\",\"profile-require_city\":\"1\",\"profile-require_region\":\"1\",\"profile-require_country\":\"1\",\"profile-require_postal_code\":\"1\",\"profile-require_phone\":\"1\",\"profile-require_website\":\"1\",\"profile-require_favoritebook\":\"1\",\"profile-require_aboutme\":\"1\",\"profile-require_tos\":\"1\",\"profile-require_dob\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 0, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{\"name\":\"plg_extension_joomla\",\"type\":\"plugin\",\"creationDate\":\"May 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 0, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_joomla\",\"type\":\"plugin\",\"creationDate\":\"November 2010\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_JOOMLA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomla\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 0, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{\"name\":\"plg_system_languagecode\",\"type\":\"plugin\",\"creationDate\":\"November 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"languagecode\"}', '', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 0, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_joomlaupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"joomlaupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 0, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_extensionupdate\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"extensionupdate\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 0, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 0, 1, 0, '{\"name\":\"plg_captcha_recaptcha\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.4.0\",\"description\":\"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"recaptcha\"}', '{\"public_key\":\"\",\"private_key\":\"\",\"theme\":\"clean\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 0, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_highlight\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"highlight\"}', '', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 0, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{\"name\":\"plg_content_finder\",\"type\":\"plugin\",\"creationDate\":\"December 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_CONTENT_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 0, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_categories\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CATEGORIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"categories\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 0, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_contacts\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTACTS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contacts\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 0, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_content\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_CONTENT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"content\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 0, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_newsfeeds\",\"type\":\"plugin\",\"creationDate\":\"August 2011\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"newsfeeds\"}', '', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(447, 0, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_tags\",\"type\":\"plugin\",\"creationDate\":\"February 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_FINDER_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 0, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_totp\",\"type\":\"plugin\",\"creationDate\":\"August 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"totp\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 0, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{\"name\":\"plg_authentication_cookie\",\"type\":\"plugin\",\"creationDate\":\"July 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_AUTH_COOKIE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"cookie\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 0, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{\"name\":\"plg_twofactorauth_yubikey\",\"type\":\"plugin\",\"creationDate\":\"September 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.2.0\",\"description\":\"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"yubikey\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(451, 0, 'plg_search_tags', 'plugin', 'tags', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_tags\",\"type\":\"plugin\",\"creationDate\":\"March 2014\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.0.0\",\"description\":\"PLG_SEARCH_TAGS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"tags\"}', '{\"search_limit\":\"50\",\"show_tagged_items\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(452, 0, 'plg_system_updatenotification', 'plugin', 'updatenotification', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_updatenotification\",\"type\":\"plugin\",\"creationDate\":\"May 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_UPDATENOTIFICATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"updatenotification\"}', '{\"lastrun\":1510076067}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(453, 0, 'plg_editors-xtd_module', 'plugin', 'module', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_module\",\"type\":\"plugin\",\"creationDate\":\"October 2015\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_MODULE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"module\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(454, 0, 'plg_system_stats', 'plugin', 'stats', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_stats\",\"type\":\"plugin\",\"creationDate\":\"November 2013\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.5.0\",\"description\":\"PLG_SYSTEM_STATS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"stats\"}', '{\"mode\":3,\"lastrun\":\"\",\"unique_id\":\"d4577386c2d5f2b189ef6d6522b06bb3a295ea36\",\"interval\":12}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(455, 0, 'plg_installer_packageinstaller', 'plugin', 'packageinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"plg_installer_packageinstaller\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_PACKAGEINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"packageinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(456, 0, 'PLG_INSTALLER_FOLDERINSTALLER', 'plugin', 'folderinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_FOLDERINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_FOLDERINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"folderinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(457, 0, 'PLG_INSTALLER_URLINSTALLER', 'plugin', 'urlinstaller', 'installer', 0, 1, 1, 1, '{\"name\":\"PLG_INSTALLER_URLINSTALLER\",\"type\":\"plugin\",\"creationDate\":\"May 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.6.0\",\"description\":\"PLG_INSTALLER_URLINSTALLER_PLUGIN_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"urlinstaller\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(458, 0, 'plg_quickicon_phpversioncheck', 'plugin', 'phpversioncheck', 'quickicon', 0, 1, 1, 1, '{\"name\":\"plg_quickicon_phpversioncheck\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_QUICKICON_PHPVERSIONCHECK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"phpversioncheck\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(459, 0, 'plg_editors-xtd_menu', 'plugin', 'menu', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_menu\",\"type\":\"plugin\",\"creationDate\":\"August 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_MENU_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"menu\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(460, 0, 'plg_editors-xtd_contact', 'plugin', 'contact', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_contact\",\"type\":\"plugin\",\"creationDate\":\"October 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_CONTACT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"contact\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(461, 0, 'plg_system_fields', 'plugin', 'fields', 'system', 0, 1, 1, 0, '{\"name\":\"plg_system_fields\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_SYSTEM_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(462, 0, 'plg_fields_calendar', 'plugin', 'calendar', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_calendar\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CALENDAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"calendar\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(463, 0, 'plg_fields_checkboxes', 'plugin', 'checkboxes', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_checkboxes\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_CHECKBOXES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"checkboxes\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(464, 0, 'plg_fields_color', 'plugin', 'color', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_color\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_COLOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"color\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(465, 0, 'plg_fields_editor', 'plugin', 'editor', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_editor\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_EDITOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"editor\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(466, 0, 'plg_fields_imagelist', 'plugin', 'imagelist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_imagelist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_IMAGELIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"imagelist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(467, 0, 'plg_fields_integer', 'plugin', 'integer', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_integer\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_INTEGER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"integer\"}', '{\"multiple\":\"0\",\"first\":\"1\",\"last\":\"100\",\"step\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(468, 0, 'plg_fields_list', 'plugin', 'list', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_list\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_LIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"list\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(469, 0, 'plg_fields_media', 'plugin', 'media', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_media\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_MEDIA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"media\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(470, 0, 'plg_fields_radio', 'plugin', 'radio', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_radio\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_RADIO_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"radio\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(471, 0, 'plg_fields_sql', 'plugin', 'sql', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_sql\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_SQL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"sql\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(472, 0, 'plg_fields_text', 'plugin', 'text', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_text\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXT_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"text\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(473, 0, 'plg_fields_textarea', 'plugin', 'textarea', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_textarea\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_TEXTAREA_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"textarea\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(474, 0, 'plg_fields_url', 'plugin', 'url', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_url\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_URL_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"url\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(475, 0, 'plg_fields_user', 'plugin', 'user', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_user\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"user\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(476, 0, 'plg_fields_usergrouplist', 'plugin', 'usergrouplist', 'fields', 0, 1, 1, 0, '{\"name\":\"plg_fields_usergrouplist\",\"type\":\"plugin\",\"creationDate\":\"March 2016\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_FIELDS_USERGROUPLIST_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"usergrouplist\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(477, 0, 'plg_content_fields', 'plugin', 'fields', 'content', 0, 1, 1, 0, '{\"name\":\"plg_content_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_CONTENT_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(478, 0, 'plg_editors-xtd_fields', 'plugin', 'fields', 'editors-xtd', 0, 1, 1, 0, '{\"name\":\"plg_editors-xtd_fields\",\"type\":\"plugin\",\"creationDate\":\"February 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.0\",\"description\":\"PLG_EDITORS-XTD_FIELDS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"fields\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 0, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{\"name\":\"beez3\",\"type\":\"template\",\"creationDate\":\"25 November 2009\",\"author\":\"Angie Radtke\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"a.radtke@derauftritt.de\",\"authorUrl\":\"http:\\/\\/www.der-auftritt.de\",\"version\":\"3.1.0\",\"description\":\"TPL_BEEZ3_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"navposition\":\"center\",\"templatecolor\":\"nature\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 0, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{\"name\":\"hathor\",\"type\":\"template\",\"creationDate\":\"May 2010\",\"author\":\"Andrea Tarr\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"3.0.0\",\"description\":\"TPL_HATHOR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"showSiteName\":\"0\",\"colourChoice\":\"0\",\"boldText\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(506, 0, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{\"name\":\"protostar\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(507, 0, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{\"name\":\"isis\",\"type\":\"template\",\"creationDate\":\"3\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_ISIS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"\",\"logoFile\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 802, 'English (en-GB)', 'language', 'en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.4\",\"description\":\"en-GB site language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 802, 'English (en-GB)', 'language', 'en-GB', '', 1, 1, 1, 1, '{\"name\":\"English (en-GB)\",\"type\":\"language\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.4\",\"description\":\"en-GB administrator language\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 0, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{\"name\":\"files_joomla\",\"type\":\"file\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"(C) 2005 - 2017 Open Source Matters. All rights reserved\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.4\",\"description\":\"FILES_JOOMLA_XML_DESCRIPTION\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(802, 0, 'English (en-GB) Language Pack', 'package', 'pkg_en-GB', '', 0, 1, 1, 1, '{\"name\":\"English (en-GB) Language Pack\",\"type\":\"package\",\"creationDate\":\"July 2017\",\"author\":\"Joomla! Project\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"www.joomla.org\",\"version\":\"3.7.4.1\",\"description\":\"en-GB language pack\",\"group\":\"\",\"filename\":\"pkg_en-GB\"}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `kutah_extensions` (`extension_id`, `package_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(10000, 0, 'com_work', 'component', 'com_work', '', 1, 1, 0, 0, '{\"name\":\"com_work\",\"type\":\"component\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"\\n    \",\"group\":\"\",\"filename\":\"work\"}', '{\"history_limit\":\"10\",\"save_history\":\"1\",\"service_save_history\":\"1\",\"video_save_history\":\"1\",\"gallery_save_history\":\"1\",\"item_save_history\":\"1\",\"service_layout\":\"_:default\",\"show_service_icons\":\"1\",\"show_service_print_icon\":\"1\",\"show_service_email_icon\":\"1\",\"keep_service_itemid\":\"1\",\"show_service_navigation\":\"1\",\"show_service_readmore\":\"1\",\"service_readmore_limit\":\"100\",\"show_service_readmore_name\":\"1\",\"show_service_noauth\":\"1\",\"show_service_name\":\"1\",\"link_service_names\":\"1\",\"show_service_intro\":\"1\",\"show_service_image\":\"1\",\"show_service_image_width\":\"100\",\"show_service_image_height\":\"0\",\"show_service_urls\":\"1\",\"show_service_urls_position\":\"1\",\"show_service_tags\":\"1\",\"show_service_created_by\":\"1\",\"link_service_created_by\":\"1\",\"show_service_created_by_alias\":\"1\",\"show_service_created\":\"1\",\"show_service_modified\":\"1\",\"show_service_publish_up\":\"1\",\"show_service_publish_down\":\"1\",\"show_service_hits\":\"1\",\"show_service_admin\":\"1\",\"show_service_vote\":\"1\",\"service_num_leading\":\"\",\"service_num_intro\":\"\",\"service_num_columns\":\"\",\"service_num_links\":\"\",\"service_multi_column_order\":\"0\",\"service_initial_direction\":\"ASC\",\"show_service_filter_field\":\"name\",\"show_service_headings\":\"1\",\"show_service_pagination\":\"2\",\"show_service_pagination_results\":\"1\",\"show_service_pagination_limit\":\"1\",\"service_num_per_page\":\"5\",\"show_no_services\":\"1\",\"show_service_add_link\":\"1\",\"list_show_service_date\":\"0\",\"list_show_service_hits\":\"1\",\"list_show_service_created_by\":\"1\",\"list_show_service_ordering\":\"0\",\"video_layout\":\"_:default\",\"show_video_icons\":\"1\",\"show_video_print_icon\":\"1\",\"show_video_email_icon\":\"1\",\"keep_video_itemid\":\"1\",\"show_video_navigation\":\"1\",\"show_video_readmore\":\"1\",\"video_readmore_limit\":\"100\",\"show_video_readmore_name\":\"1\",\"show_video_noauth\":\"1\",\"show_video_name\":\"1\",\"link_video_names\":\"1\",\"show_video_intro\":\"1\",\"show_video_image\":\"1\",\"show_video_image_width\":\"100\",\"show_video_image_height\":\"0\",\"show_video_urls\":\"1\",\"show_video_urls_position\":\"1\",\"show_video_tags\":\"1\",\"show_video_created_by\":\"1\",\"link_video_created_by\":\"1\",\"show_video_created_by_alias\":\"1\",\"show_video_created\":\"1\",\"show_video_modified\":\"1\",\"show_video_publish_up\":\"1\",\"show_video_publish_down\":\"1\",\"show_video_hits\":\"1\",\"show_video_admin\":\"1\",\"show_video_vote\":\"1\",\"video_num_leading\":\"\",\"video_num_intro\":\"\",\"video_num_columns\":\"\",\"video_num_links\":\"\",\"video_multi_column_order\":\"0\",\"video_initial_direction\":\"ASC\",\"show_video_filter_field\":\"name\",\"show_video_headings\":\"1\",\"show_video_pagination\":\"2\",\"show_video_pagination_results\":\"1\",\"show_video_pagination_limit\":\"1\",\"video_num_per_page\":\"5\",\"show_no_videos\":\"1\",\"show_video_add_link\":\"1\",\"list_show_video_date\":\"0\",\"list_show_video_hits\":\"1\",\"list_show_video_created_by\":\"1\",\"list_show_video_ordering\":\"0\",\"gallery_layout\":\"_:default\",\"show_gallery_icons\":\"1\",\"show_gallery_print_icon\":\"1\",\"show_gallery_email_icon\":\"1\",\"keep_gallery_itemid\":\"1\",\"show_gallery_navigation\":\"1\",\"show_gallery_readmore\":\"1\",\"gallery_readmore_limit\":\"100\",\"show_gallery_readmore_name\":\"1\",\"show_gallery_noauth\":\"1\",\"show_gallery_name\":\"1\",\"link_gallery_names\":\"1\",\"show_gallery_intro\":\"1\",\"show_gallery_image\":\"1\",\"show_gallery_image_width\":\"100\",\"show_gallery_image_height\":\"0\",\"show_gallery_urls\":\"1\",\"show_gallery_urls_position\":\"1\",\"show_gallery_tags\":\"1\",\"show_gallery_created_by\":\"1\",\"link_gallery_created_by\":\"1\",\"show_gallery_created_by_alias\":\"1\",\"show_gallery_created\":\"1\",\"show_gallery_modified\":\"1\",\"show_gallery_publish_up\":\"1\",\"show_gallery_publish_down\":\"1\",\"show_gallery_hits\":\"1\",\"show_gallery_admin\":\"1\",\"show_gallery_vote\":\"1\",\"gallery_num_leading\":\"\",\"gallery_num_intro\":\"\",\"gallery_num_columns\":\"\",\"gallery_num_links\":\"\",\"gallery_multi_column_order\":\"0\",\"gallery_initial_direction\":\"ASC\",\"show_gallery_filter_field\":\"name\",\"show_gallery_headings\":\"1\",\"show_gallery_pagination\":\"2\",\"show_gallery_pagination_results\":\"1\",\"show_gallery_pagination_limit\":\"1\",\"gallery_num_per_page\":\"5\",\"show_no_galleries\":\"1\",\"show_gallery_add_link\":\"1\",\"list_show_gallery_date\":\"0\",\"list_show_gallery_hits\":\"1\",\"list_show_gallery_created_by\":\"1\",\"list_show_gallery_ordering\":\"0\",\"item_layout\":\"_:default\",\"show_item_icons\":\"1\",\"show_item_print_icon\":\"1\",\"show_item_email_icon\":\"1\",\"keep_item_itemid\":\"1\",\"show_item_navigation\":\"1\",\"show_item_category_breadcrumb\":\"1\",\"limit_category_item_navigation\":\"1\",\"show_item_readmore\":\"1\",\"item_readmore_limit\":\"100\",\"show_item_readmore_name\":\"1\",\"show_item_noauth\":\"1\",\"show_item_name\":\"1\",\"link_item_names\":\"1\",\"show_item_intro\":\"1\",\"show_item_image\":\"1\",\"show_item_image_width\":\"100\",\"show_item_image_height\":\"0\",\"show_item_urls\":\"1\",\"show_item_urls_position\":\"1\",\"show_item_category\":\"1\",\"link_item_category\":\"1\",\"show_item_parent_category\":\"1\",\"link_item_parent_category\":\"1\",\"show_item_tags\":\"1\",\"show_item_created_by\":\"1\",\"link_item_created_by\":\"1\",\"show_item_created_by_alias\":\"1\",\"show_item_created\":\"1\",\"show_item_modified\":\"1\",\"show_item_publish_up\":\"1\",\"show_item_publish_down\":\"1\",\"show_item_hits\":\"1\",\"show_item_admin\":\"1\",\"show_item_vote\":\"1\",\"item_num_leading\":\"\",\"item_num_intro\":\"\",\"item_num_columns\":\"\",\"item_num_links\":\"\",\"item_multi_column_order\":\"0\",\"item_orderby_pri\":\"none\",\"item_orderby_sec\":\"none\",\"item_initial_direction\":\"ASC\",\"show_item_filter_field\":\"name\",\"show_item_headings\":\"1\",\"show_item_pagination\":\"2\",\"show_item_pagination_results\":\"1\",\"show_item_pagination_limit\":\"1\",\"item_num_per_page\":\"5\",\"show_no_items\":\"1\",\"show_item_add_link\":\"1\",\"list_show_item_date\":\"0\",\"list_show_item_hits\":\"1\",\"list_show_item_created_by\":\"1\",\"list_show_item_ordering\":\"0\",\"show_categories_base_description\":\"1\",\"show_empty_categories\":\"1\",\"show_subcategories_desc\":\"1\",\"show_categories_max_level\":\"-1\",\"show_categories_noauth\":\"1\",\"items_to_display\":\"\",\"show_categories_num_items\":\"1\",\"show_cat_title\":\"1\",\"show_cat_description\":\"1\",\"show_cat_description_image\":\"1\",\"show_cat_subcat_heading\":\"1\",\"show_empty_cat\":\"1\",\"show_subcat_desc\":\"1\",\"show_category_max_level\":\"-1\",\"show_cat_noauth\":\"1\",\"show_cat_num_items\":\"1\",\"show_cat_tags\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 0, 'plg_finder_services', 'plugin', 'services', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_services\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_FINDER_SERVICES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"services\"}', '{\"sub_layout\":\"_:default\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10002, 0, 'plg_finder_videos', 'plugin', 'videos', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_videos\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_FINDER_VIDEOS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"videos\"}', '{\"sub_layout\":\"_:default\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10003, 0, 'plg_finder_galleries', 'plugin', 'galleries', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_galleries\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_FINDER_GALLERIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"galleries\"}', '{\"sub_layout\":\"_:default\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10004, 0, 'plg_finder_items', 'plugin', 'items', 'finder', 0, 1, 1, 0, '{\"name\":\"plg_finder_items\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_FINDER_ITEMS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"items\"}', '{\"sub_layout\":\"_:default\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10005, 0, 'plg_search_services', 'plugin', 'services', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_services\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_SEARCH_SERVICES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"services\"}', '{\"layout\":\"_:default\",\"search_limit\":\"50\",\"search_services\":\"1\",\"search_archived_services\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10006, 0, 'plg_search_videos', 'plugin', 'videos', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_videos\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_SEARCH_VIDEOS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"videos\"}', '{\"layout\":\"_:default\",\"search_limit\":\"50\",\"search_videos\":\"1\",\"search_archived_videos\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10007, 0, 'plg_search_galleries', 'plugin', 'galleries', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_galleries\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_SEARCH_GALLERIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"galleries\"}', '{\"layout\":\"_:default\",\"search_limit\":\"50\",\"search_galleries\":\"1\",\"search_archived_galleries\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10008, 0, 'plg_search_items', 'plugin', 'items', 'search', 0, 1, 1, 0, '{\"name\":\"plg_search_items\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_SEARCH_ITEMS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"items\"}', '{\"layout\":\"_:default\",\"search_limit\":\"50\",\"search_items\":\"1\",\"search_archived_items\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10009, 0, 'plg_work_finder', 'plugin', 'finder', 'work', 0, 1, 1, 0, '{\"name\":\"plg_work_finder\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_WORK_FINDER_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"finder\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10010, 0, 'plg_work_itemnavigation', 'plugin', 'itemnavigation', 'work', 0, 1, 1, 0, '{\"name\":\"plg_work_itemnavigation\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_WORK_ITEMNAVIGATION_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"itemnavigation\"}', '{\"service_position\":\"1\",\"service_relative\":\"1\",\"keep_service_itemid\":\"1\",\"video_position\":\"1\",\"video_relative\":\"1\",\"keep_video_itemid\":\"1\",\"gallery_position\":\"1\",\"gallery_relative\":\"1\",\"keep_gallery_itemid\":\"1\",\"item_position\":\"1\",\"item_relative\":\"1\",\"keep_item_itemid\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10011, 0, 'plg_work_pagebreak', 'plugin', 'pagebreak', 'work', 0, 1, 1, 0, '{\"name\":\"plg_work_pagebreak\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_WORK_PAGEBREAK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"pagebreak\"}', '{\"title\":\"1\",\"service_index\":\"1\",\"service_index_text\":\"\",\"multipage_toc\":\"1\",\"showall\":\"1\",\"style\":\"pages\",\"video_index\":\"1\",\"video_index_text\":\"\",\"gallery_index\":\"1\",\"gallery_index_text\":\"\",\"item_index\":\"1\",\"item_index_text\":\"\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10012, 0, 'plg_work_vote', 'plugin', 'vote', 'work', 0, 1, 1, 0, '{\"name\":\"plg_work_vote\",\"type\":\"plugin\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"PLG_WORK_VOTE_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"vote\"}', '{\"service_position\":\"1\",\"video_position\":\"1\",\"gallery_position\":\"1\",\"item_position\":\"1\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10013, 0, 'mod_work', 'module', 'mod_work', '', 0, 1, 0, 0, '{\"name\":\"mod_work\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"MOD_WORK_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_work\"}', '{\"user_id\":\"0\",\"count\":\"5\",\"list-style\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10014, 0, 'mod_work_services', 'module', 'mod_work_services', '', 0, 1, 0, 0, '{\"name\":\"mod_work_services\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"MOD_WORK_SERVICES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_work_services\"}', '{\"service_layout\":\"_:default\",\"count\":\"5\",\"show_featured\":\"\",\"user_id\":\"0\",\"list-style\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10015, 0, 'mod_work_videos', 'module', 'mod_work_videos', '', 0, 1, 0, 0, '{\"name\":\"mod_work_videos\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"MOD_WORK_VIDEOS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_work_videos\"}', '{\"video_layout\":\"_:default\",\"count\":\"5\",\"show_featured\":\"\",\"user_id\":\"0\",\"list-style\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 0, 'mod_work_galleries', 'module', 'mod_work_galleries', '', 0, 1, 0, 0, '{\"name\":\"mod_work_galleries\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"MOD_WORK_GALLERIES_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_work_galleries\"}', '{\"gallery_layout\":\"_:default\",\"count\":\"5\",\"show_featured\":\"\",\"user_id\":\"0\",\"list-style\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 0, 'mod_work_items', 'module', 'mod_work_items', '', 0, 1, 0, 0, '{\"name\":\"mod_work_items\",\"type\":\"module\",\"creationDate\":\"July 2017\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"MOD_WORK_ITEMS_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"mod_work_items\"}', '{\"item_layout\":\"_:default\",\"count\":\"5\",\"catid\":\"\",\"show_featured\":\"\",\"user_id\":\"0\",\"list-style\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 10020, 'Georgianka-GE', 'language', 'ka-GE', '', 1, 1, 0, 0, '{\"name\":\"Georgian (ka-GE)\",\"type\":\"language\",\"creationDate\":\"2017-07-05\",\"author\":\"Georgian Translation Team\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"z.cherkezishvili@gmail.com\",\"authorUrl\":\"https:\\/\\/github.com\\/Zurabch\",\"version\":\"3.7.3.1\",\"description\":\"ka-GE administrator language\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 10020, 'Georgianka-GE', 'language', 'ka-GE', '', 0, 1, 0, 0, '{\"name\":\"Georgian (ka-GE)\",\"type\":\"language\",\"creationDate\":\"2017-07-05\",\"author\":\"Georgian Translation Team\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"z.cherkezishvili@gmail.com\",\"authorUrl\":\"https:\\/\\/github.com\\/Zurabch\",\"version\":\"3.7.3.1\",\"description\":\"ka-GE site language\",\"group\":\"\",\"filename\":\"install\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10020, 0, 'Georgian (ka-GE) Language Pack', 'package', 'pkg_ka-GE', '', 0, 1, 1, 0, '{\"name\":\"Georgian (ka-GE) Language Pack\",\"type\":\"package\",\"creationDate\":\"2017-07-05\",\"author\":\"Georgian Translation Team\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters. All rights reserved.\",\"authorEmail\":\"z.cherkezishvili@gmail.com\",\"authorUrl\":\"https:\\/\\/github.com\\/Zurabch\",\"version\":\"3.7.3.1\",\"description\":\"Joomla 3.7.3.1 Georgian Language Package\",\"group\":\"\",\"filename\":\"pkg_ka-GE\"}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10021, 0, 'work', 'template', 'work', '', 0, 1, 1, 0, '{\"name\":\"work\",\"type\":\"template\",\"creationDate\":\"4\\/30\\/2012\",\"author\":\"Kyle Ledbetter\",\"copyright\":\"Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.\",\"authorEmail\":\"admin@joomla.org\",\"authorUrl\":\"\",\"version\":\"1.0\",\"description\":\"TPL_PROTOSTAR_XML_DESCRIPTION\",\"group\":\"\",\"filename\":\"templateDetails\"}', '{\"templateColor\":\"#08C\",\"templateBackgroundColor\":\"#F4F6F7\",\"logoFile\":\"\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10022, 0, 'com_selectfile', 'component', 'com_selectfile', '', 1, 1, 1, 0, '{\"name\":\"com_selectfile\",\"type\":\"component\",\"creationDate\":\"August 2015\",\"author\":\"Unknown\",\"copyright\":\"\\n    \",\"authorEmail\":\"\",\"authorUrl\":\"\",\"version\":\"1.0.0\",\"description\":\"\\n    \",\"group\":\"\",\"filename\":\"selectfile\"}', '{\"size_layout\":\"_:default\",\"show_size_icons\":\"1\",\"show_size_print_icon\":\"1\",\"show_size_email_icon\":\"1\",\"keep_size_itemid\":\"1\",\"show_size_navigation\":\"1\",\"show_size_readmore\":\"1\",\"size_readmore_limit\":\"100\",\"show_size_readmore_name\":\"1\",\"show_size_noauth\":\"1\",\"show_size_name\":\"1\",\"link_size_names\":\"1\",\"show_size_created_by\":\"1\",\"link_size_created_by\":\"1\",\"show_size_created_by_alias\":\"1\",\"show_size_created\":\"1\",\"show_size_modified\":\"1\",\"show_size_admin\":\"1\",\"size_initial_direction\":\"ASC\",\"show_size_filter_field\":\"name\",\"show_size_headings\":\"1\",\"show_size_pagination\":\"2\",\"show_size_pagination_results\":\"1\",\"show_size_pagination_limit\":\"1\",\"size_num_per_page\":\"5\",\"show_no_sizes\":\"1\",\"show_size_add_link\":\"1\",\"list_show_size_date\":\"0\",\"list_show_size_created_by\":\"1\",\"list_show_size_ordering\":\"0\"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_fields`
--

CREATE TABLE `kutah_fields` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `label` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `default_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'text',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldparams` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_fields_categories`
--

CREATE TABLE `kutah_fields_categories` (
  `field_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_fields_groups`
--

CREATE TABLE `kutah_fields_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `context` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_fields_values`
--

CREATE TABLE `kutah_fields_values` (
  `field_id` int(10) UNSIGNED NOT NULL,
  `item_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Allow references to items which have strings as ids, eg. none db systems.',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_filters`
--

CREATE TABLE `kutah_finder_filters` (
  `filter_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links`
--

CREATE TABLE `kutah_finder_links` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(400) DEFAULT NULL,
  `description` text,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double UNSIGNED NOT NULL DEFAULT '0',
  `sale_price` double UNSIGNED NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms0`
--

CREATE TABLE `kutah_finder_links_terms0` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms1`
--

CREATE TABLE `kutah_finder_links_terms1` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms2`
--

CREATE TABLE `kutah_finder_links_terms2` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms3`
--

CREATE TABLE `kutah_finder_links_terms3` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms4`
--

CREATE TABLE `kutah_finder_links_terms4` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms5`
--

CREATE TABLE `kutah_finder_links_terms5` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms6`
--

CREATE TABLE `kutah_finder_links_terms6` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms7`
--

CREATE TABLE `kutah_finder_links_terms7` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms8`
--

CREATE TABLE `kutah_finder_links_terms8` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_terms9`
--

CREATE TABLE `kutah_finder_links_terms9` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_termsa`
--

CREATE TABLE `kutah_finder_links_termsa` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_termsb`
--

CREATE TABLE `kutah_finder_links_termsb` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_termsc`
--

CREATE TABLE `kutah_finder_links_termsc` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_termsd`
--

CREATE TABLE `kutah_finder_links_termsd` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_termse`
--

CREATE TABLE `kutah_finder_links_termse` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_links_termsf`
--

CREATE TABLE `kutah_finder_links_termsf` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `term_id` int(10) UNSIGNED NOT NULL,
  `weight` float UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_taxonomy`
--

CREATE TABLE `kutah_finder_taxonomy` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `access` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kutah_finder_taxonomy`
--

INSERT INTO `kutah_finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_taxonomy_map`
--

CREATE TABLE `kutah_finder_taxonomy_map` (
  `link_id` int(10) UNSIGNED NOT NULL,
  `node_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_terms`
--

CREATE TABLE `kutah_finder_terms` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_terms_common`
--

CREATE TABLE `kutah_finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kutah_finder_terms_common`
--

INSERT INTO `kutah_finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('any', 'en'),
('are', 'en'),
('aren\'t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn\'t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_tokens`
--

CREATE TABLE `kutah_finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `weight` float UNSIGNED NOT NULL DEFAULT '1',
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_tokens_aggregate`
--

CREATE TABLE `kutah_finder_tokens_aggregate` (
  `term_id` int(10) UNSIGNED NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `phrase` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `term_weight` float UNSIGNED NOT NULL,
  `context` tinyint(1) UNSIGNED NOT NULL DEFAULT '2',
  `context_weight` float UNSIGNED NOT NULL,
  `total_weight` float UNSIGNED NOT NULL,
  `language` char(3) NOT NULL DEFAULT ''
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_finder_types`
--

CREATE TABLE `kutah_finder_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_followers`
--

CREATE TABLE `kutah_followers` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `follow` int(11) NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_followers`
--

INSERT INTO `kutah_followers` (`id`, `user`, `follow`, `date`, `publish`) VALUES
(1, 657, 655, '2017-08-01 16:29:01', 1),
(2, 658, 655, '2017-08-01 16:29:01', 1),
(3, 655, 657, '2017-08-01 17:07:09', 0),
(4, 655, 658, '2017-08-01 17:07:09', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_languages`
--

CREATE TABLE `kutah_languages` (
  `lang_id` int(11) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lang_code` char(7) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title_native` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sef` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sitename` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_languages`
--

INSERT INTO `kutah_languages` (`lang_id`, `asset_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 0, 'en-GB', 'English (en-GB)', 'English (United Kingdom)', 'en', 'en_gb', '', '', '', '', 1, 1, 2),
(2, 61, 'ka-GE', 'Georgian (Georgia)', 'ქართული (საქართველო)', 'ka', 'ka_ge', '', '', '', '', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_menu`
--

CREATE TABLE `kutah_menu` (
  `id` int(11) NOT NULL,
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `path` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `pre_icon` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_menu`
--

INSERT INTO `kutah_menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`, `pre_icon`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 85, 0, '*', 0, ''),
(2, 'main', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 1, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1, ''),
(3, 'main', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1, ''),
(4, 'main', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 1, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1, ''),
(5, 'main', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1, ''),
(6, 'main', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 1, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1, ''),
(7, 'main', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 1, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 21, 26, 0, '*', 1, ''),
(8, 'main', 'com_contact_contacts', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 1, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 22, 23, 0, '*', 1, ''),
(9, 'main', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 1, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 24, 25, 0, '*', 1, ''),
(10, 'main', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 1, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 27, 30, 0, '*', 1, ''),
(11, 'main', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 1, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 28, 29, 0, '*', 1, ''),
(13, 'main', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 31, 36, 0, '*', 1, ''),
(14, 'main', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 1, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 32, 33, 0, '*', 1, ''),
(15, 'main', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 1, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 34, 35, 0, '*', 1, ''),
(16, 'main', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 1, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 37, 38, 0, '*', 1, ''),
(17, 'main', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 1, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 39, 40, 0, '*', 1, ''),
(18, 'main', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 1, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 41, 42, 0, '*', 1, ''),
(19, 'main', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 1, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 43, 44, 0, '*', 1, ''),
(20, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 1, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 45, 46, 0, '', 1, ''),
(21, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 1, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 47, 48, 0, '*', 1, ''),
(22, 'main', 'com_associations', 'Multilingual Associations', '', 'Multilingual Associations', 'index.php?option=com_associations', 'component', 1, 1, 1, 34, 0, '0000-00-00 00:00:00', 0, 0, 'class:associations', 0, '', 49, 50, 0, '*', 1, ''),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{\"featured_categories\":[\"\"],\"layout_type\":\"blog\",\"num_leading_articles\":\"1\",\"num_intro_articles\":\"3\",\"num_columns\":\"3\",\"num_links\":\"0\",\"multi_column_order\":\"1\",\"orderby_pri\":\"\",\"orderby_sec\":\"front\",\"order_date\":\"\",\"show_pagination\":\"2\",\"show_pagination_results\":\"1\",\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_readmore\":\"\",\"show_readmore_title\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_noauth\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"page_title\":\"\",\"show_page_heading\":1,\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 51, 52, 1, '*', 0, ''),
(102, 'main', 'COM_WORK', 'com-work', '', 'com-work', 'index.php?option=com_work', 'component', 1, 1, 1, 10000, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_work/images/work.png', 0, '{}', 53, 64, 0, '', 1, ''),
(103, 'main', 'COM_WORK_SERVICES', 'com-work-services', '', 'com-work/com-work-services', 'index.php?option=com_work&view=services', 'component', 1, 102, 2, 10000, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_work/images/work-services.png', 0, '{}', 54, 55, 0, '', 1, ''),
(104, 'main', 'COM_WORK_VIDEOS', 'com-work-videos', '', 'com-work/com-work-videos', 'index.php?option=com_work&view=videos', 'component', 1, 102, 2, 10000, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_work/images/work-videos.png', 0, '{}', 56, 57, 0, '', 1, ''),
(105, 'main', 'COM_WORK_GALLERIES', 'com-work-galleries', '', 'com-work/com-work-galleries', 'index.php?option=com_work&view=galleries', 'component', 1, 102, 2, 10000, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_work/images/work-galleries.png', 0, '{}', 58, 59, 0, '', 1, ''),
(106, 'main', 'COM_WORK_ITEMS', 'com-work-items', '', 'com-work/com-work-items', 'index.php?option=com_work&view=items', 'component', 1, 102, 2, 10000, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_work/images/work-items.png', 0, '{}', 60, 61, 0, '', 1, ''),
(107, 'main', 'COM_WORK_CATEGORIES', 'com-work-categories', '', 'com-work/com-work-categories', 'index.php?option=com_categories&extension=com_work', 'component', 1, 102, 2, 10000, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_work/images/work-categories.png', 0, '{}', 62, 63, 0, '', 1, ''),
(108, 'usermenu', 'ჩემი ნამუშევრები', 'chemi-namushevrebi', '', 'chemi-namushevrebi', 'index.php?option=com_work&view=items&layout=myitems', 'component', 1, 1, 1, 10000, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"filter_item_categories\":[\"\"],\"filter_item_featured\":\"\",\"filter_item_archived\":\"0\",\"item_initial_sort\":\"\",\"item_initial_direction\":\"ASC\",\"show_item_filter_field\":\"\",\"show_item_headings\":\"\",\"show_item_pagination\":\"\",\"show_item_pagination_results\":\"\",\"show_item_pagination_limit\":\"\",\"item_num_per_page\":\"\",\"show_no_items\":\"\",\"show_item_add_link\":\"\",\"list_show_item_date\":\"\",\"item_date_format\":\"\",\"list_show_item_hits\":\"\",\"list_show_item_created_by\":\"\",\"list_show_item_ordering\":\"\",\"item_layout\":\"_:default\",\"show_item_icons\":\"\",\"show_item_print_icon\":\"\",\"show_item_email_icon\":\"\",\"item_info_block_position\":\"\",\"keep_item_itemid\":\"\",\"show_item_navigation\":\"\",\"show_item_category_breadcrumb\":\"\",\"limit_category_item_navigation\":\"\",\"show_item_readmore\":\"\",\"item_readmore_limit\":\"100\",\"show_item_readmore_name\":\"\",\"show_item_noauth\":\"\",\"show_item_name\":\"\",\"link_item_names\":\"\",\"show_item_intro\":\"\",\"show_item_image\":\"\",\"show_item_image_float\":\"\",\"show_item_image_width\":\"\",\"show_item_image_height\":\"\",\"show_item_intro_image\":\"\",\"show_item_intro_image_float\":\"\",\"show_item_intro_image_width\":\"\",\"show_item_intro_image_height\":\"\",\"show_item_urls\":\"\",\"show_item_urls_position\":\"\",\"show_item_category\":\"\",\"link_item_category\":\"\",\"show_item_parent_category\":\"\",\"link_item_parent_category\":\"\",\"show_item_tags\":\"\",\"show_item_created_by\":\"\",\"link_item_created_by\":\"\",\"show_item_created_by_alias\":\"\",\"show_item_created\":\"\",\"show_item_modified\":\"\",\"show_item_publish_up\":\"\",\"show_item_publish_down\":\"\",\"show_item_hits\":\"\",\"show_item_admin\":\"\",\"show_item_vote\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 11, 12, 0, '*', 0, 'fa  fa-home'),
(109, 'usermenu', 'ჩემი პროფილი', 'chemi-propili', '', 'chemi-propili', 'index.php?option=com_users&view=profile', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 15, 18, 0, '*', 0, ''),
(110, 'usermenu', 'პროფილის რედაქტირება', 'propilis-redaktireba', '', 'propilis-redaktireba', 'index.php?option=com_users&view=profile&layout=edit', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 19, 20, 0, '*', 0, ''),
(111, 'usermenu', 'ნამუშევრის დამატება', 'namushevris-damateba', '', 'chemi-propili/namushevris-damateba', 'index.php?option=com_work&view=itemform&layout=edit', 'component', 1, 109, 2, 10000, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"enable_category\":\"0\",\"catid\":\"8\",\"show_item_name\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 16, 17, 0, '*', 0, ''),
(112, 'privacy-terms', 'Privacy Policy', 'privacy-policy', '', 'privacy-policy', 'index.php?option=com_content&view=article&id=1', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 65, 66, 0, '*', 0, ''),
(113, 'privacy-terms', 'Terms & Conditions', 'terms-conditions', '', 'terms-conditions', 'index.php?option=com_content&view=article&id=2', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"show_title\":\"\",\"link_titles\":\"\",\"show_intro\":\"\",\"info_block_position\":\"\",\"info_block_show_title\":\"\",\"show_category\":\"\",\"link_category\":\"\",\"show_parent_category\":\"\",\"link_parent_category\":\"\",\"show_associations\":\"\",\"show_author\":\"\",\"link_author\":\"\",\"show_create_date\":\"\",\"show_modify_date\":\"\",\"show_publish_date\":\"\",\"show_item_navigation\":\"\",\"show_vote\":\"\",\"show_icons\":\"\",\"show_print_icon\":\"\",\"show_email_icon\":\"\",\"show_hits\":\"\",\"show_tags\":\"\",\"show_noauth\":\"\",\"urls_position\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 67, 68, 0, '*', 0, ''),
(114, 'top-login', 'შესვლა', 'login', '', 'login', 'index.php?option=com_users&view=login', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"loginredirectchoice\":\"1\",\"login_redirect_url\":\"\",\"login_redirect_menuitem\":\"109\",\"logindescription_show\":\"1\",\"login_description\":\"\",\"login_image\":\"\",\"logoutredirectchoice\":\"1\",\"logout_redirect_url\":\"\",\"logout_redirect_menuitem\":\"101\",\"logoutdescription_show\":\"1\",\"logout_description\":\"\",\"logout_image\":\"\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 69, 70, 0, '*', 0, ''),
(115, 'top-login', 'რეგისტრაცია', 'register', '', 'register', 'index.php?option=com_users&view=registration', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 71, 72, 0, '*', 0, ''),
(116, 'top-socials', ' ', 'facebook', '', 'facebook', '#', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 1, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"fa fa-facebook\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 73, 74, 0, '*', 0, ''),
(117, 'top-socials', ' ', 'twitter', '', 'twitter', '', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 1, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"fa fa-twitter\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 75, 76, 0, '*', 0, ''),
(118, 'top-socials', ' ', 'google', '', 'google', '', 'url', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 1, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"fa fa-google-plus\",\"menu-anchor_rel\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1}', 77, 78, 0, '*', 0, ''),
(119, 'usermenu', 'პროფესიონალები', 'propesionalebi', '', 'propesionalebi', 'index.php?option=com_work&view=professionals', 'component', 1, 1, 1, 10000, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"filter_item_categories\":[\"\"],\"filter_item_featured\":\"\",\"filter_item_archived\":\"0\",\"item_initial_sort\":\"\",\"item_initial_direction\":\"ASC\",\"show_item_filter_field\":\"\",\"show_item_headings\":\"\",\"show_item_pagination\":\"\",\"show_item_pagination_results\":\"\",\"show_item_pagination_limit\":\"\",\"item_num_per_page\":\"\",\"show_no_items\":\"\",\"show_item_add_link\":\"\",\"list_show_item_date\":\"\",\"item_date_format\":\"\",\"list_show_item_hits\":\"\",\"list_show_item_created_by\":\"\",\"list_show_item_ordering\":\"\",\"item_layout\":\"_:default\",\"show_item_icons\":\"\",\"show_item_print_icon\":\"\",\"show_item_email_icon\":\"\",\"item_info_block_position\":\"\",\"keep_item_itemid\":\"\",\"show_item_navigation\":\"\",\"show_item_category_breadcrumb\":\"\",\"limit_category_item_navigation\":\"\",\"show_item_readmore\":\"\",\"item_readmore_limit\":\"100\",\"show_item_readmore_name\":\"\",\"show_item_noauth\":\"\",\"show_item_name\":\"\",\"link_item_names\":\"\",\"show_item_intro\":\"\",\"show_item_image\":\"\",\"show_item_image_float\":\"\",\"show_item_image_width\":\"\",\"show_item_image_height\":\"\",\"show_item_intro_image\":\"\",\"show_item_intro_image_float\":\"\",\"show_item_intro_image_width\":\"\",\"show_item_intro_image_height\":\"\",\"show_item_urls\":\"\",\"show_item_urls_position\":\"\",\"show_item_category\":\"\",\"link_item_category\":\"\",\"show_item_parent_category\":\"\",\"link_item_parent_category\":\"\",\"show_item_tags\":\"\",\"show_item_created_by\":\"\",\"link_item_created_by\":\"\",\"show_item_created_by_alias\":\"\",\"show_item_created\":\"\",\"show_item_modified\":\"\",\"show_item_publish_up\":\"\",\"show_item_publish_down\":\"\",\"show_item_hits\":\"\",\"show_item_admin\":\"\",\"show_item_vote\":\"\",\"show_feed_link\":\"1\",\"feed_summary\":\"0\",\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 13, 14, 0, '*', 0, ''),
(120, 'main', 'COM_SELECTFILE', 'com-selectfile', '', 'com-selectfile', 'index.php?option=com_selectfile', 'component', 1, 1, 1, 10022, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_selectfile/images/selectfile.png', 0, '{}', 79, 82, 0, '', 1, ''),
(121, 'main', 'COM_SELECTFILE_SIZES', 'com-selectfile-sizes', '', 'com-selectfile/com-selectfile-sizes', 'index.php?option=com_selectfile&view=sizes', 'component', 1, 120, 2, 10022, 0, '0000-00-00 00:00:00', 0, 1, '../media/com_selectfile/images/selectfile-sizes.png', 0, '{}', 80, 81, 0, '', 1, ''),
(122, 'additional', 'Public profile', 'public-profile', '', 'public-profile', 'index.php?option=com_users&view=pprofile', 'component', 1, 1, 1, 25, 0, '0000-00-00 00:00:00', 0, 1, ' ', 0, '{\"menu-anchor_title\":\"\",\"menu-anchor_css\":\"\",\"menu_image\":\"\",\"menu_text\":1,\"menu_show\":1,\"page_title\":\"\",\"show_page_heading\":\"\",\"page_heading\":\"\",\"pageclass_sfx\":\"\",\"menu-meta_description\":\"\",\"menu-meta_keywords\":\"\",\"robots\":\"\",\"secure\":0}', 83, 84, 0, '*', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_menu_types`
--

CREATE TABLE `kutah_menu_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `menutype` varchar(24) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(48) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_menu_types`
--

INSERT INTO `kutah_menu_types` (`id`, `asset_id`, `menutype`, `title`, `description`, `client_id`) VALUES
(1, 0, 'mainmenu', 'Main Menu', 'The main menu for the site', 0),
(2, 69, 'usermenu', 'UserMenu', 'UserMenu', 0),
(3, 83, 'privacy-terms', 'Privacy & terms', '', 0),
(4, 88, 'top-login', 'Top login', '', 0),
(5, 90, 'top-socials', 'top socials', '', 0),
(6, 95, 'additional', 'Additional', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_messages`
--

CREATE TABLE `kutah_messages` (
  `message_id` int(10) UNSIGNED NOT NULL,
  `user_id_from` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `user_id_to` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_messages_cfg`
--

CREATE TABLE `kutah_messages_cfg` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `cfg_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_modules`
--

CREATE TABLE `kutah_modules` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) UNSIGNED NOT NULL DEFAULT '1',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_modules`
--

INSERT INTO `kutah_modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(1, 39, 'Main Menu', '', '', 1, 'main-menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"usermenu\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"showPreIcon\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"sf-menu sf-arrows\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"_menu\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(2, 40, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 41, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{\"count\":\"5\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(4, 42, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{\"count\":\"5\",\"ordering\":\"c_dsc\",\"catid\":\"\",\"user_id\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(8, 43, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 44, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 45, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{\"count\":\"5\",\"name\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(12, 46, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{\"layout\":\"\",\"moduleclass_sfx\":\"\",\"shownew\":\"1\",\"showhelp\":\"1\",\"cache\":\"0\"}', 1, '*'),
(13, 47, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 48, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 49, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(16, 50, 'Login Form', '', '', 1, 'main-page-login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{\"pretext\":\"\",\"posttext\":\"\",\"login\":\"109\",\"logout\":\"101\",\"greeting\":\"1\",\"profilelink\":\"0\",\"name\":\"0\",\"usesecure\":\"0\",\"usetext\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(17, 51, 'Breadcrumbs', '', '', 1, 'position-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 1, '{\"moduleclass_sfx\":\"\",\"showHome\":\"1\",\"homeText\":\"\",\"showComponent\":\"1\",\"separator\":\"\",\"cache\":\"0\",\"cache_time\":\"0\",\"cachemode\":\"itemid\"}', 0, '*'),
(79, 52, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(86, 53, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{\"format\":\"short\",\"product\":\"1\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"0\"}', 1, '*'),
(87, 55, 'Display Work Items', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_work', 1, 1, '', 0, '*'),
(88, 56, 'Display Services', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_work_services', 1, 1, '', 0, '*'),
(89, 57, 'Display Videos', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_work_videos', 1, 1, '', 0, '*'),
(90, 58, 'Display Galleries', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_work_galleries', 1, 1, '', 0, '*'),
(91, 59, 'Display Items', '', '', 0, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_work_items', 1, 1, '', 0, '*'),
(92, 70, 'მომხმარებლის მენიუ', '', '', 1, 'position-7', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 2, 1, '{\"menutype\":\"usermenu\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"tag_id\":\"\",\"class_sfx\":\"\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(93, 71, 'Promo text main page', '', '<div class=\"position-center-center col-md-10\">\r\n<h1>აი , ისიც! სოციალური ქსელი, რომელსაც დიდხანს ელოდი! ახალი სიტყვა დასაქმების ბაზარზე!</h1>\r\n<h6>დარეგისტრირდი, შექმენი შენი პროფილი და გააცანი შენი საქმე მათ, ვინც შენნაირ პროფესიონალს ეძებს.</h6>\r\n</div>', 1, 'promo-text', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(94, 72, 'Main page serices', '', '<ul class=\"row\"><!-- SECTION -->\r\n<li class=\"col-md-4\">\r\n<div class=\"ser-inn\">\r\n<h4>Stay in touch with your colleagues</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur.</p>\r\n</div>\r\n</li>\r\n<!-- SECTION -->\r\n<li class=\"col-md-4\">\r\n<div class=\"ser-inn\">\r\n<h4>Get the latest news in your industry</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur.</p>\r\n</div>\r\n</li>\r\n<!-- SECTION  -->\r\n<li class=\"col-md-4\">\r\n<div class=\"ser-inn\">\r\n<h4>Share what’s up with you</h4>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue conseqaut nibbhi ellit ipsum consectetur.</p>\r\n</div>\r\n</li>\r\n</ul>', 1, 'main-page-services', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(95, 73, 'what our users say ', '', '<div class=\"testi\">\r\n<div class=\"texti-slide\"><!-- SLide -->\r\n<div class=\"clints-text\">\r\n<div class=\"text-in\">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>\r\n</div>\r\n<div class=\"avatar\">\r\n<div class=\"media-left\"><a href=\"#.\"> <img src=\"images/theme/clients-avatar-1.jpg\" alt=\"\" /> </a></div>\r\n<div class=\"media-body\">\r\n<h6>John Kevin Mara</h6>\r\nsmashingmagazine.com</div>\r\n</div>\r\n</div>\r\n<!-- SLide -->\r\n<div class=\"clints-text\">\r\n<div class=\"text-in\">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>\r\n</div>\r\n<div class=\"avatar\">\r\n<div class=\"media-left\"><a href=\"#.\"> <img src=\"images/theme/clients-avatar-1.jpg\" alt=\"\" /> </a></div>\r\n<div class=\"media-body\">\r\n<h6>John Kevin Mara</h6>\r\nsmashingmagazine.com</div>\r\n</div>\r\n</div>\r\n<!-- SLide -->\r\n<div class=\"clints-text\">\r\n<div class=\"text-in\">\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Proin nibh augue.</p>\r\n</div>\r\n<div class=\"avatar\">\r\n<div class=\"media-left\"><a href=\"#.\"> <img src=\"images/theme/clients-avatar-1.jpg\" alt=\"\" /> </a></div>\r\n<div class=\"media-body\">\r\n<h6>John Kevin Mara</h6>\r\nsmashingmagazine.com</div>\r\n</div>\r\n</div>\r\n<!-- SLide --></div>\r\n</div>', 1, 'clients-says', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(96, 74, 'Our Sponsors', '', '<div class=\"sponsors-slider \" style=\"opacity: 1; display: block;\">\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo1.png\" alt=\"\" /></div>\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo1.png\" alt=\"\" /></div>\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo3.png\" alt=\"\" /></div>\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo4.png\" alt=\"\" /></div>\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo5.png\" alt=\"\" /></div>\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo6.png\" alt=\"\" /></div>\r\n<div class=\"item\"><img class=\"img-responsive\" src=\"images/theme/sponsor_logo4.png\" alt=\"\" /></div>\r\n</div>', 1, 'sponsors', 655, '2017-07-14 19:33:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(97, 75, 'App Image', '', '<div class=\"app-images\">\r\n<div class=\"container\">\r\n<div class=\"row\"><!-- TEXT -->\r\n<div class=\"col-md-6 text-center text-area\">\r\n<h1>SocialMe for your Smartphone</h1>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n<a href=\"#.\"> App Store</a></div>\r\n<!-- APP IMAGE -->\r\n<div class=\"col-md-6 text-right\"><img src=\"images/theme/app-img.png\" alt=\"\" /></div>\r\n</div>\r\n</div>\r\n</div>', 1, 'app-image', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(98, 76, 'pro content 1', '', '<div class=\"row\"><!-- PRO IMAGE -->\r\n<div class=\"col-md-6 pro-inside\" style=\"background: url(\'images/pro-img-1.jpg\') center center no-repeat;\"> </div>\r\n<!-- PRO CONTENT -->\r\n<div class=\"col-md-6 pro-inside\">\r\n<div class=\"position-center-center col-md-6\">\r\n<h1>Interact with other professionals</h1>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n</div>\r\n</div>\r\n</div>', 1, 'pro-content-1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(99, 77, 'pro content 2', '', '<div class=\"row\"><!-- PRO TEXT -->\r\n<div class=\"col-md-6 pro-inside\">\r\n<div class=\"position-center-center col-md-6\">\r\n<h1>Collaborate on a project</h1>\r\n<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n</div>\r\n</div>\r\n<!-- PRO BACKGROUND -->\r\n<div class=\"col-md-6 pro-inside\" style=\"background: url(\'images/pro-img-2.jpg\') center center no-repeat;\"> </div>\r\n</div>', 1, 'pro-content-2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(100, 78, 'Photostream', '', '<ul class=\"photos-list\">\r\n<li><img src=\"images/theme/photostream4.jpg\" alt=\"\" /></li>\r\n<li><img src=\"images/theme/photostream6.jpg\" alt=\"\" /></li>\r\n<li><img src=\"images/theme/photostream3.jpg\" alt=\"\" /></li>\r\n<li><img src=\"images/theme/photostream2.jpg\" alt=\"\" /></li>\r\n<li><img src=\"images/theme/photostream1.jpg\" alt=\"\" /></li>\r\n<li><img src=\"images/theme/photostream.jpg\" alt=\"\" /></li>\r\n</ul>', 1, 'photo-stream', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(101, 79, 'Newsletter', '', '<p>Subscribe to our newsletter to receive our latest news and updates. We do not spam.</p>\r\n<form class=\"newsletter-form\" action=\"#\"><input type=\"email\" placeholder=\"Enter your email address\" /> <input class=\"btn btn-primary\" type=\"submit\" value=\"Subscribe\" /></form>', 1, 'newsletter', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(102, 80, 'Twitter Feed', '', '<ul class=\"twitter-feed\">\r\n<li>RT <a href=\"#\">@no1son</a>: Now this <a href=\"#\">http://t.co/TSfMW1qMAW</a> is one hell of a stunning site!!! Awesome work guys <a href=\"#\">@AIRNAUTS</a> <a class=\"time\" href=\"#\">May 25</a></li>\r\n<li>Check out the wordpress version of Tucson - <a href=\"#\">http://t.co/sBlU3GbapT</a> <a class=\"time\" href=\"#\">May 22</a></li>\r\n</ul>', 1, 'twitter-feed', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(103, 81, 'contact info', '', '<p><a class=\"logo\" href=\"#\"><img src=\"images/theme/logo.png\" alt=\"\" /></a></p>\r\n<ul class=\"contact-info has-bg-image contain\" style=\"background-image: url(\'images/footer-map-bg.png\');\" data-bg-image=\"/images/theme/footer-map-bg.png\">\r\n<li><address>795 Folsom Ave, Suite 600, San Francisco, CA 94107</address></li>\r\n<li><a href=\"tel:\">(123) 456-7890</a></li>\r\n<li><a href=\"mailto:\">first.last@example.com</a></li>\r\n</ul>', 1, 'contact-info', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(104, 85, 'Privacy & terms', '', '', 1, 'privacy-terms', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"privacy-terms\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"showPreIcon\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"links\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(105, 86, 'Copyright', '', '<p>Copyright © 2015 <a href=\"#\">UOUAPPS</a>. All Rights reserved.</p>', 1, 'copyright', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(106, 87, 'search', '', '', 1, 'top-search', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 1, '{\"label\":\"\",\"width\":\"\",\"text\":\"\",\"button\":\"1\",\"button_pos\":\"right\",\"imagebutton\":\"0\",\"button_text\":\"\",\"opensearch\":\"1\",\"opensearch_title\":\"\",\"set_itemid\":\"0\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(107, 89, 'Top login', '', '', 1, 'top-login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"top-login\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"showPreIcon\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"authentication\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(108, 91, 'Top socials', '', '', 1, 'top-socials', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{\"menutype\":\"top-socials\",\"base\":\"\",\"startLevel\":\"1\",\"endLevel\":\"0\",\"showAllChildren\":\"1\",\"showPreIcon\":\"0\",\"tag_id\":\"\",\"class_sfx\":\"social\",\"window_open\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"itemid\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(109, 92, 'Search Professionals', '', '<div class=\"profile-bnr sub-bnr user-profile-bnr\">\r\n<div class=\"position-center-center\">\r\n<div class=\"container\">\r\n<h2>Search Professionals</h2>\r\n</div>\r\n</div>\r\n</div>', 1, 'before-component', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*'),
(110, 93, 'search-pro', '', '<div class=\"search-pro\">\r\n<div class=\"map-search\">\r\n<div class=\"container\">\r\n<div class=\"row\">\r\n<div class=\"col-md-12\">\r\n<div class=\"map-toggleButton\"><i class=\"fa fa-bars\">-</i></div>\r\n<div class=\"map-search-fields\">\r\n<div class=\"field\"><input type=\"text\" placeholder=\"Keyword\" /></div>\r\n<div class=\"field\"><input type=\"text\" placeholder=\"Location\" /></div>\r\n<div class=\"field custom-select-box\"><input type=\"text\" placeholder=\"Profession\" /></div>\r\n</div>\r\n<div class=\"search-button\"><button>Search Professionals</button></div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 1, 'before-component', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{\"prepare_content\":\"0\",\"backgroundimage\":\"\",\"layout\":\"_:default\",\"moduleclass_sfx\":\"\",\"cache\":\"1\",\"cache_time\":\"900\",\"cachemode\":\"static\",\"module_tag\":\"div\",\"bootstrap_size\":\"0\",\"header_tag\":\"h3\",\"header_class\":\"\",\"style\":\"0\"}', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_modules_menu`
--

CREATE TABLE `kutah_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_modules_menu`
--

INSERT INTO `kutah_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(16, 0),
(17, 0),
(79, 0),
(86, 0),
(92, 0),
(93, 0),
(94, 0),
(95, 0),
(96, 0),
(97, 0),
(98, 0),
(99, 0),
(100, 0),
(101, 0),
(102, 0),
(103, 0),
(104, 0),
(105, 0),
(106, 0),
(107, 0),
(108, 0),
(109, 119),
(110, 119);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_newsfeeds`
--

CREATE TABLE `kutah_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `link` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `cache_time` int(10) UNSIGNED NOT NULL DEFAULT '3600',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadata` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_overrider`
--

CREATE TABLE `kutah_overrider` (
  `id` int(10) NOT NULL COMMENT 'Primary Key',
  `constant` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `string` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_postinstall_messages`
--

CREATE TABLE `kutah_postinstall_messages` (
  `postinstall_message_id` bigint(20) UNSIGNED NOT NULL,
  `extension_id` bigint(20) NOT NULL DEFAULT '700' COMMENT 'FK to #__extensions',
  `title_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for the title',
  `description_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Lang key for description',
  `action_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `language_extension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'com_postinstall' COMMENT 'Extension holding lang keys',
  `language_client_id` tinyint(3) NOT NULL DEFAULT '1',
  `type` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'link' COMMENT 'Message type - message, link, action',
  `action_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'RAD URI to the PHP file containing action method',
  `action` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '' COMMENT 'Action method name or URL',
  `condition_file` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'RAD URI to file holding display condition method',
  `condition_method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Display condition method, must return boolean',
  `version_introduced` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '3.2.0' COMMENT 'Version when this message was introduced',
  `enabled` tinyint(3) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_postinstall_messages`
--

INSERT INTO `kutah_postinstall_messages` (`postinstall_message_id`, `extension_id`, `title_key`, `description_key`, `action_key`, `language_extension`, `language_client_id`, `type`, `action_file`, `action`, `condition_file`, `condition_method`, `version_introduced`, `enabled`) VALUES
(1, 700, 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_TITLE', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_BODY', 'PLG_TWOFACTORAUTH_TOTP_POSTINSTALL_ACTION', 'plg_twofactorauth_totp', 1, 'action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_action', 'site://plugins/twofactorauth/totp/postinstall/actions.php', 'twofactorauth_postinstall_condition', '3.2.0', 1),
(2, 700, 'COM_CPANEL_WELCOME_BEGINNERS_TITLE', 'COM_CPANEL_WELCOME_BEGINNERS_MESSAGE', '', 'com_cpanel', 1, 'message', '', '', '', '', '3.2.0', 1),
(3, 700, 'COM_CPANEL_MSG_STATS_COLLECTION_TITLE', 'COM_CPANEL_MSG_STATS_COLLECTION_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/statscollection.php', 'admin_postinstall_statscollection_condition', '3.5.0', 1),
(4, 700, 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_BODY', 'PLG_SYSTEM_UPDATENOTIFICATION_POSTINSTALL_UPDATECACHETIME_ACTION', 'plg_system_updatenotification', 1, 'action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_action', 'site://plugins/system/updatenotification/postinstall/updatecachetime.php', 'updatecachetime_postinstall_condition', '3.6.3', 1),
(5, 700, 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_TITLE', 'COM_CPANEL_MSG_JOOMLA40_PRE_CHECKS_BODY', '', 'com_cpanel', 1, 'message', '', '', 'admin://components/com_admin/postinstall/joomla40checks.php', 'admin_postinstall_joomla40checks_condition', '3.7.0', 1),
(6, 700, 'TPL_HATHOR_MESSAGE_POSTINSTALL_TITLE', 'TPL_HATHOR_MESSAGE_POSTINSTALL_BODY', 'TPL_HATHOR_MESSAGE_POSTINSTALL_ACTION', 'tpl_hathor', 1, 'action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_action', 'admin://templates/hathor/postinstall/hathormessage.php', 'hathormessage_postinstall_condition', '3.7.0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_redirect_links`
--

CREATE TABLE `kutah_redirect_links` (
  `id` int(10) UNSIGNED NOT NULL,
  `old_url` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `new_url` varchar(2048) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `referer` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `header` smallint(3) NOT NULL DEFAULT '301'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_schemas`
--

CREATE TABLE `kutah_schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_schemas`
--

INSERT INTO `kutah_schemas` (`extension_id`, `version_id`) VALUES
(700, '3.7.4-2017-07-05'),
(10000, '1.0.0'),
(10022, '1.0.0');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_selectfile_sizes`
--

CREATE TABLE `kutah_selectfile_sizes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `codename` varchar(255) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `quality` int(11) NOT NULL,
  `imgedit` int(11) NOT NULL,
  `watermark` int(11) NOT NULL,
  `ineditor` int(11) NOT NULL,
  `publish` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_selectfile_sizes`
--

INSERT INTO `kutah_selectfile_sizes` (`id`, `name`, `state`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `ordering`, `access`, `codename`, `width`, `height`, `quality`, `imgedit`, `watermark`, `ineditor`, `publish`) VALUES
(1, 'Original', 1, '2015-08-07 12:02:36', 906, '', '2015-08-31 12:06:15', 906, 906, '2015-09-07 13:16:36', 1, 1, '0', 1000, 1, 80, 1, -1, 0, 0),
(2, 'მთავარი გვერდის პროდუქტების მოდული', 1, '2015-08-25 14:15:02', 906, '', '2016-07-19 07:53:51', 906, 0, '0000-00-00 00:00:00', 2, 1, 'image1', 246, 176, 80, 3, 0, 0, 0),
(3, 'პროდუქტის სლაიდერის დიდი სურათი', 1, '2015-08-25 14:16:10', 906, '', '2016-07-20 08:41:33', 906, 0, '0000-00-00 00:00:00', 3, 1, 'image2', 550, 352, 80, 3, 0, 0, 0),
(4, 'სლაიდერის სურათები', 1, '2015-09-04 08:59:08', 906, '', '2016-07-13 08:14:40', 906, 0, '0000-00-00 00:00:00', 4, 1, 'image9', 1280, 416, 80, 3, -1, 0, 0),
(5, 'პრომო ვიდეო გალერეის დიდი სურათი', 1, '2015-09-07 08:54:45', 906, '', '2015-09-07 08:57:06', 906, 0, '0000-00-00 00:00:00', 5, 1, 'image5', 292, 196, 80, 3, -1, 0, 0),
(6, 'ჟურნალის მოდულის სურათი', 1, '2015-09-08 11:48:52', 906, '', '2015-09-08 11:54:06', 906, 0, '0000-00-00 00:00:00', 6, 1, 'image6', 226, 1, 80, 1, -1, 0, 0),
(7, 'ჟურნალის არქივის სურათი', 1, '2015-09-08 12:42:45', 906, '', '2015-09-08 12:44:09', 906, 0, '0000-00-00 00:00:00', 7, 1, 'image7', 320, 1, 80, 1, -1, 0, 0),
(8, 'გამოკითხვის მოდულის სურათი', 1, '2015-09-09 06:34:48', 906, '', '2015-09-09 06:35:47', 906, 906, '2015-09-14 12:36:30', 8, 1, 'image8', 292, 1, 80, 1, -1, 0, 0),
(9, 'facebook', 1, '2015-09-10 07:54:37', 906, '', '2015-09-10 07:57:46', 906, 906, '2015-09-18 09:33:49', 9, 1, 'image15', 470, 246, 80, 3, -1, 0, 0),
(10, 'ფოტო გალერეის კარუსელი', 1, '2015-09-10 08:12:38', 906, '', '2015-09-10 08:13:13', 906, 906, '2015-09-22 13:36:49', 10, 1, 'image10', 128, 100, 80, 3, -1, 0, 0),
(11, 'ფოტო გალერეის კატეგორიის სურათები', 1, '2015-09-10 09:38:58', 906, '', '2015-09-10 09:39:37', 906, 0, '0000-00-00 00:00:00', 11, 1, 'image11', 214, 176, 80, 3, -1, 0, 0),
(12, 'არტიკლების კატეგორიის სურათი', 1, '2015-09-11 07:24:44', 906, '', '2016-07-12 10:59:48', 906, 0, '0000-00-00 00:00:00', 12, 1, 'image12', 220, 238, 80, 3, -1, 0, 0),
(13, 'მთავარი გვერდის მარჯვენა სვეტის მოდულები', 1, '2015-09-11 11:11:46', 906, '', '2015-09-11 11:12:31', 906, 0, '0000-00-00 00:00:00', 13, 1, 'image13', 292, 354, 80, 3, -1, 0, 0),
(14, 'მსგავსი სიახლეები', 1, '2015-09-11 11:57:37', 906, '', '2015-09-11 11:58:53', 906, 0, '0000-00-00 00:00:00', 14, 1, 'image14', 214, 166, 80, 3, -1, 0, 0),
(15, 'Public portfolio image', 1, '2015-09-11 12:03:51', 906, '', '2017-08-17 18:28:44', 655, 0, '0000-00-00 00:00:00', 15, 1, 'image22', 585, 330, 80, 3, -1, 0, 0),
(16, 'Blog post image', 1, '2015-09-11 13:29:01', 906, '', '2017-08-16 19:04:23', 655, 0, '0000-00-00 00:00:00', 16, 1, 'image16', 870, 491, 80, 4, -1, 0, 0),
(17, 'Portfolio in image', 1, '2015-09-11 13:29:44', 906, '', '2017-08-16 18:19:47', 655, 0, '0000-00-00 00:00:00', 17, 1, 'image17', 640, 359, 80, 4, -1, 0, 0),
(18, 'small image', 1, '2015-09-14 12:07:05', 906, '', '2017-08-01 12:31:57', 655, 0, '0000-00-00 00:00:00', 18, 1, 'image18', 38, 38, 80, 3, -1, 0, 0),
(20, 'Public portfolio avatar', 1, '2015-09-16 12:47:18', 906, '', '2017-07-23 19:18:57', 655, 0, '0000-00-00 00:00:00', 19, 1, 'image19', 245, 220, 80, 4, -1, 0, 0),
(21, 'Professionals image', 1, '2015-09-18 09:35:43', 906, '', '2017-07-22 15:42:55', 655, 0, '0000-00-00 00:00:00', 20, 1, 'image3', 265, 215, 80, 3, -1, 0, 0),
(22, 'Avatar image', 1, '2015-09-22 12:22:50', 906, '', '2017-07-22 09:18:40', 655, 0, '0000-00-00 00:00:00', 21, 1, 'image20', 309, 233, 80, 3, -1, 0, 0),
(23, 'მთავარი გვერდის მარჯვენა სვეტის მოდულები', -2, '2015-09-30 09:11:02', 906, '', '2015-09-30 09:11:48', 906, 0, '0000-00-00 00:00:00', 0, 1, 'image21', 292, 350, 80, 3, -1, 0, 0),
(24, 'პროდუქტს კატეგორიის სურათი', 1, '2016-07-20 14:00:21', 906, '', '2016-07-20 14:00:48', 906, 0, '0000-00-00 00:00:00', 22, 1, 'image4', 316, 226, 80, 3, -1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_session`
--

CREATE TABLE `kutah_session` (
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(3) UNSIGNED DEFAULT NULL,
  `guest` tinyint(4) UNSIGNED DEFAULT '1',
  `time` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` mediumtext COLLATE utf8mb4_unicode_ci,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_session`
--

INSERT INTO `kutah_session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('gl9k0vua08rdnkil6f018he1a1', 0, 0, '1510076070', NULL, 655, 'admin'),
('s4pmnk25v88us06o7cnq6hltf4', 0, 1, '1510076076', NULL, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_tags`
--

CREATE TABLE `kutah_tags` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `path` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metadesc` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_tags`
--

INSERT INTO `kutah_tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '', '', '', '', 655, '2017-07-13 17:21:28', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_template_styles`
--

CREATE TABLE `kutah_template_styles` (
  `id` int(10) UNSIGNED NOT NULL,
  `template` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `client_id` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `home` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_template_styles`
--

INSERT INTO `kutah_template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(4, 'beez3', 0, '0', 'Beez3 - Default', '{\"wrapperSmall\":\"53\",\"wrapperLarge\":\"72\",\"logo\":\"images\\/joomla_black.png\",\"sitetitle\":\"Joomla!\",\"sitedescription\":\"Open Source Content Management\",\"navposition\":\"left\",\"templatecolor\":\"personal\",\"html5\":\"0\"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{\"showSiteName\":\"0\",\"colourChoice\":\"\",\"boldText\":\"0\"}'),
(7, 'protostar', 0, '0', 'protostar - Default', '{\"templateColor\":\"\",\"logoFile\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}'),
(8, 'isis', 1, '1', 'isis - Default', '{\"templateColor\":\"\",\"logoFile\":\"\"}'),
(10, 'work', 0, '1', 'work - Default', '{\"templateColor\":\"#0088cc\",\"templateBackgroundColor\":\"#f4f6f7\",\"logoFile\":\"images\\/theme\\/logo.png\",\"sitetitle\":\"\",\"sitedescription\":\"\",\"googleFont\":\"1\",\"googleFontName\":\"Open+Sans\",\"fluidContainer\":\"0\"}');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_ucm_base`
--

CREATE TABLE `kutah_ucm_base` (
  `ucm_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_ucm_content`
--

CREATE TABLE `kutah_ucm_content` (
  `core_content_id` int(10) UNSIGNED NOT NULL,
  `core_type_alias` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_alias` varchar(400) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '',
  `core_body` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_checked_out_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_featured` tinyint(4) UNSIGNED NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `core_publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_content_item_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'ID from the individual type table',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `core_images` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_urls` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_metadesc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `core_catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Contains core content data in name spaced fields';

-- --------------------------------------------------------

--
-- Table structure for table `kutah_ucm_history`
--

CREATE TABLE `kutah_ucm_history` (
  `version_id` int(10) UNSIGNED NOT NULL,
  `ucm_item_id` int(10) UNSIGNED NOT NULL,
  `ucm_type_id` int(10) UNSIGNED NOT NULL,
  `version_note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Optional version name',
  `save_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `character_count` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Number of characters in this version.',
  `sha1_hash` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'SHA1 hash of the version_data column.',
  `version_data` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'json-encoded string of version data',
  `keep_forever` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=auto delete; 1=keep'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_ucm_history`
--

INSERT INTO `kutah_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(1, 8, 10000, '', '2017-07-13 17:47:29', 655, 613, '184e0df910525355e329e0c175d916119c98a7b5', '{\"id\":8,\"asset_id\":62,\"parent_id\":\"1\",\"lft\":\"11\",\"rgt\":12,\"level\":1,\"path\":null,\"extension\":\"com_work\",\"title\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"2017-07-13-17-47-29\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 17:47:29\",\"modified_user_id\":null,\"modified_time\":\"2017-07-13 17:47:29\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(2, 8, 10000, '', '2017-07-13 18:04:48', 655, 654, '0e4fe1c8af810c38ec57d1ba61964d98c2502a38', '{\"id\":8,\"asset_id\":\"62\",\"parent_id\":\"1\",\"lft\":\"11\",\"rgt\":\"12\",\"level\":\"1\",\"path\":\"2017-07-13-17-47-29\",\"extension\":\"com_work\",\"title\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"2017-07-13-18-04-48\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 17:47:32\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 17:47:29\",\"modified_user_id\":\"655\",\"modified_time\":\"2017-07-13 18:04:48\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(3, 8, 10000, '', '2017-07-13 18:05:17', 655, 654, 'af5db00b4ee07107884507b66f69c464a4ba6348', '{\"id\":8,\"asset_id\":\"62\",\"parent_id\":\"1\",\"lft\":\"11\",\"rgt\":\"12\",\"level\":\"1\",\"path\":\"2017-07-13-18-04-48\",\"extension\":\"com_work\",\"title\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"2017-07-13-18-05-17\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 18:05:13\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 17:47:29\",\"modified_user_id\":\"655\",\"modified_time\":\"2017-07-13 18:05:17\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(4, 1, 10004, '', '2017-07-13 18:06:52', 655, 1945, 'd28e993e23d90843cd78d18fc9107d0d99e900af', '{\"id\":1,\"name\":\"sdsd\",\"alias\":\"sdsd\",\"description\":\"\",\"intro\":\"\",\"images\":\"{\\\"image_url\\\":\\\"\\\",\\\"image_alt_text\\\":\\\"\\\",\\\"image_caption\\\":\\\"\\\",\\\"intro_image_url\\\":\\\"\\\",\\\"intro_image_alt_text\\\":\\\"\\\",\\\"intro_image_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urla_text\\\":\\\"\\\",\\\"urla_target\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlb_text\\\":\\\"\\\",\\\"urlb_target\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlc_text\\\":\\\"\\\",\\\"urlc_target\\\":\\\"\\\"}\",\"catid\":\"8\",\"state\":\"1\",\"publish_up\":\"2017-07-13 18:06:52\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-07-13 18:06:48\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-13 18:06:52\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":1,\"ordering\":1,\"params\":\"{\\\"show_item_icons\\\":\\\"\\\",\\\"show_item_print_icon\\\":\\\"\\\",\\\"show_item_email_icon\\\":\\\"\\\",\\\"item_info_block_position\\\":\\\"\\\",\\\"show_item_readmore\\\":\\\"\\\",\\\"show_item_readmore_name\\\":\\\"\\\",\\\"item_readmore_limit\\\":\\\"100\\\",\\\"item_alternative_readmore\\\":\\\"\\\",\\\"show_item_noauth\\\":\\\"\\\",\\\"show_item_name\\\":\\\"\\\",\\\"link_item_names\\\":\\\"\\\",\\\"show_item_intro\\\":\\\"\\\",\\\"show_item_image\\\":\\\"\\\",\\\"show_item_image_float\\\":\\\"\\\",\\\"show_item_image_width\\\":\\\"\\\",\\\"show_item_image_height\\\":\\\"\\\",\\\"show_item_intro_image\\\":\\\"\\\",\\\"show_item_intro_image_float\\\":\\\"\\\",\\\"show_item_intro_image_width\\\":\\\"\\\",\\\"show_item_intro_image_height\\\":\\\"\\\",\\\"show_item_urls\\\":\\\"\\\",\\\"show_item_urls_position\\\":\\\"\\\",\\\"show_item_category\\\":\\\"\\\",\\\"link_item_category\\\":\\\"\\\",\\\"show_item_parent_category\\\":\\\"\\\",\\\"link_item_parent_category\\\":\\\"\\\",\\\"show_item_tags\\\":\\\"\\\",\\\"show_item_created_by\\\":\\\"\\\",\\\"link_item_created_by\\\":\\\"\\\",\\\"show_item_created_by_alias\\\":\\\"\\\",\\\"show_item_created\\\":\\\"\\\",\\\"show_item_modified\\\":\\\"\\\",\\\"show_item_publish_up\\\":\\\"\\\",\\\"show_item_publish_down\\\":\\\"\\\",\\\"show_item_hits\\\":\\\"\\\",\\\"show_item_admin\\\":\\\"\\\",\\\"show_item_vote\\\":\\\"\\\"}\",\"hits\":null,\"featured\":\"0\",\"language\":\"*\",\"asset_id\":63,\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(5, 1, 10004, '', '2017-07-13 18:07:08', 655, 2022, '0de9354d9caa4fa002675a8ce0791a7ae1c724d7', '{\"id\":1,\"name\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"sdsd\",\"description\":\"\",\"intro\":\"\",\"images\":\"{\\\"image_url\\\":\\\"\\\",\\\"image_alt_text\\\":\\\"\\\",\\\"image_caption\\\":\\\"\\\",\\\"intro_image_url\\\":\\\"\\\",\\\"intro_image_alt_text\\\":\\\"\\\",\\\"intro_image_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urla_text\\\":\\\"\\\",\\\"urla_target\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlb_text\\\":\\\"\\\",\\\"urlb_target\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlc_text\\\":\\\"\\\",\\\"urlc_target\\\":\\\"\\\"}\",\"catid\":\"8\",\"state\":\"1\",\"publish_up\":\"2017-07-13 18:06:52\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-07-13 18:06:48\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-13 18:07:08\",\"modified_by\":\"655\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 18:06:52\",\"version\":2,\"ordering\":\"1\",\"params\":\"{\\\"show_item_icons\\\":\\\"\\\",\\\"show_item_print_icon\\\":\\\"\\\",\\\"show_item_email_icon\\\":\\\"\\\",\\\"item_info_block_position\\\":\\\"\\\",\\\"show_item_readmore\\\":\\\"\\\",\\\"show_item_readmore_name\\\":\\\"\\\",\\\"item_readmore_limit\\\":\\\"100\\\",\\\"item_alternative_readmore\\\":\\\"\\\",\\\"show_item_noauth\\\":\\\"\\\",\\\"show_item_name\\\":\\\"\\\",\\\"link_item_names\\\":\\\"\\\",\\\"show_item_intro\\\":\\\"\\\",\\\"show_item_image\\\":\\\"\\\",\\\"show_item_image_float\\\":\\\"\\\",\\\"show_item_image_width\\\":\\\"\\\",\\\"show_item_image_height\\\":\\\"\\\",\\\"show_item_intro_image\\\":\\\"\\\",\\\"show_item_intro_image_float\\\":\\\"\\\",\\\"show_item_intro_image_width\\\":\\\"\\\",\\\"show_item_intro_image_height\\\":\\\"\\\",\\\"show_item_urls\\\":\\\"\\\",\\\"show_item_urls_position\\\":\\\"\\\",\\\"show_item_category\\\":\\\"\\\",\\\"link_item_category\\\":\\\"\\\",\\\"show_item_parent_category\\\":\\\"\\\",\\\"link_item_parent_category\\\":\\\"\\\",\\\"show_item_tags\\\":\\\"\\\",\\\"show_item_created_by\\\":\\\"\\\",\\\"link_item_created_by\\\":\\\"\\\",\\\"show_item_created_by_alias\\\":\\\"\\\",\\\"show_item_created\\\":\\\"\\\",\\\"show_item_modified\\\":\\\"\\\",\\\"show_item_publish_up\\\":\\\"\\\",\\\"show_item_publish_down\\\":\\\"\\\",\\\"show_item_hits\\\":\\\"\\\",\\\"show_item_admin\\\":\\\"\\\",\\\"show_item_vote\\\":\\\"\\\"}\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"63\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(6, 1, 10004, '', '2017-07-13 18:07:12', 655, 2018, '127b942b9bf3f607b7e19a2c30ce852473166832', '{\"id\":1,\"name\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"\",\"description\":\"\",\"intro\":\"\",\"images\":\"{\\\"image_url\\\":\\\"\\\",\\\"image_alt_text\\\":\\\"\\\",\\\"image_caption\\\":\\\"\\\",\\\"intro_image_url\\\":\\\"\\\",\\\"intro_image_alt_text\\\":\\\"\\\",\\\"intro_image_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urla_text\\\":\\\"\\\",\\\"urla_target\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlb_text\\\":\\\"\\\",\\\"urlb_target\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlc_text\\\":\\\"\\\",\\\"urlc_target\\\":\\\"\\\"}\",\"catid\":\"8\",\"state\":\"1\",\"publish_up\":\"2017-07-13 18:06:52\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-07-13 18:06:48\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-13 18:07:12\",\"modified_by\":\"655\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 18:07:08\",\"version\":3,\"ordering\":\"1\",\"params\":\"{\\\"show_item_icons\\\":\\\"\\\",\\\"show_item_print_icon\\\":\\\"\\\",\\\"show_item_email_icon\\\":\\\"\\\",\\\"item_info_block_position\\\":\\\"\\\",\\\"show_item_readmore\\\":\\\"\\\",\\\"show_item_readmore_name\\\":\\\"\\\",\\\"item_readmore_limit\\\":\\\"100\\\",\\\"item_alternative_readmore\\\":\\\"\\\",\\\"show_item_noauth\\\":\\\"\\\",\\\"show_item_name\\\":\\\"\\\",\\\"link_item_names\\\":\\\"\\\",\\\"show_item_intro\\\":\\\"\\\",\\\"show_item_image\\\":\\\"\\\",\\\"show_item_image_float\\\":\\\"\\\",\\\"show_item_image_width\\\":\\\"\\\",\\\"show_item_image_height\\\":\\\"\\\",\\\"show_item_intro_image\\\":\\\"\\\",\\\"show_item_intro_image_float\\\":\\\"\\\",\\\"show_item_intro_image_width\\\":\\\"\\\",\\\"show_item_intro_image_height\\\":\\\"\\\",\\\"show_item_urls\\\":\\\"\\\",\\\"show_item_urls_position\\\":\\\"\\\",\\\"show_item_category\\\":\\\"\\\",\\\"link_item_category\\\":\\\"\\\",\\\"show_item_parent_category\\\":\\\"\\\",\\\"link_item_parent_category\\\":\\\"\\\",\\\"show_item_tags\\\":\\\"\\\",\\\"show_item_created_by\\\":\\\"\\\",\\\"link_item_created_by\\\":\\\"\\\",\\\"show_item_created_by_alias\\\":\\\"\\\",\\\"show_item_created\\\":\\\"\\\",\\\"show_item_modified\\\":\\\"\\\",\\\"show_item_publish_up\\\":\\\"\\\",\\\"show_item_publish_down\\\":\\\"\\\",\\\"show_item_hits\\\":\\\"\\\",\\\"show_item_admin\\\":\\\"\\\",\\\"show_item_vote\\\":\\\"\\\"}\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"63\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(7, 1, 10004, '', '2017-07-13 18:07:16', 655, 2019, '85f7d482e72402adeae12c63e175d9388a686450', '{\"id\":1,\"name\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"2\",\"description\":\"\",\"intro\":\"\",\"images\":\"{\\\"image_url\\\":\\\"\\\",\\\"image_alt_text\\\":\\\"\\\",\\\"image_caption\\\":\\\"\\\",\\\"intro_image_url\\\":\\\"\\\",\\\"intro_image_alt_text\\\":\\\"\\\",\\\"intro_image_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urla_text\\\":\\\"\\\",\\\"urla_target\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlb_text\\\":\\\"\\\",\\\"urlb_target\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlc_text\\\":\\\"\\\",\\\"urlc_target\\\":\\\"\\\"}\",\"catid\":\"8\",\"state\":\"1\",\"publish_up\":\"2017-07-13 18:06:52\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-07-13 18:06:48\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-13 18:07:16\",\"modified_by\":\"655\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 18:07:12\",\"version\":4,\"ordering\":\"1\",\"params\":\"{\\\"show_item_icons\\\":\\\"\\\",\\\"show_item_print_icon\\\":\\\"\\\",\\\"show_item_email_icon\\\":\\\"\\\",\\\"item_info_block_position\\\":\\\"\\\",\\\"show_item_readmore\\\":\\\"\\\",\\\"show_item_readmore_name\\\":\\\"\\\",\\\"item_readmore_limit\\\":\\\"100\\\",\\\"item_alternative_readmore\\\":\\\"\\\",\\\"show_item_noauth\\\":\\\"\\\",\\\"show_item_name\\\":\\\"\\\",\\\"link_item_names\\\":\\\"\\\",\\\"show_item_intro\\\":\\\"\\\",\\\"show_item_image\\\":\\\"\\\",\\\"show_item_image_float\\\":\\\"\\\",\\\"show_item_image_width\\\":\\\"\\\",\\\"show_item_image_height\\\":\\\"\\\",\\\"show_item_intro_image\\\":\\\"\\\",\\\"show_item_intro_image_float\\\":\\\"\\\",\\\"show_item_intro_image_width\\\":\\\"\\\",\\\"show_item_intro_image_height\\\":\\\"\\\",\\\"show_item_urls\\\":\\\"\\\",\\\"show_item_urls_position\\\":\\\"\\\",\\\"show_item_category\\\":\\\"\\\",\\\"link_item_category\\\":\\\"\\\",\\\"show_item_parent_category\\\":\\\"\\\",\\\"link_item_parent_category\\\":\\\"\\\",\\\"show_item_tags\\\":\\\"\\\",\\\"show_item_created_by\\\":\\\"\\\",\\\"link_item_created_by\\\":\\\"\\\",\\\"show_item_created_by_alias\\\":\\\"\\\",\\\"show_item_created\\\":\\\"\\\",\\\"show_item_modified\\\":\\\"\\\",\\\"show_item_publish_up\\\":\\\"\\\",\\\"show_item_publish_down\\\":\\\"\\\",\\\"show_item_hits\\\":\\\"\\\",\\\"show_item_admin\\\":\\\"\\\",\\\"show_item_vote\\\":\\\"\\\"}\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"63\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(8, 1, 10004, '', '2017-07-13 18:11:05', 655, 2029, '902ae637dd8700d9b26139d83d0b3a84ee21654a', '{\"id\":1,\"name\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"mshenebloba\",\"description\":\"\",\"intro\":\"\",\"images\":\"{\\\"image_url\\\":\\\"\\\",\\\"image_alt_text\\\":\\\"\\\",\\\"image_caption\\\":\\\"\\\",\\\"intro_image_url\\\":\\\"\\\",\\\"intro_image_alt_text\\\":\\\"\\\",\\\"intro_image_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urla_text\\\":\\\"\\\",\\\"urla_target\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlb_text\\\":\\\"\\\",\\\"urlb_target\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlc_text\\\":\\\"\\\",\\\"urlc_target\\\":\\\"\\\"}\",\"catid\":\"8\",\"state\":\"1\",\"publish_up\":\"2017-07-13 18:06:52\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-07-13 18:06:48\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-13 18:11:05\",\"modified_by\":\"655\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 18:11:01\",\"version\":8,\"ordering\":\"1\",\"params\":\"{\\\"show_item_icons\\\":\\\"\\\",\\\"show_item_print_icon\\\":\\\"\\\",\\\"show_item_email_icon\\\":\\\"\\\",\\\"item_info_block_position\\\":\\\"\\\",\\\"show_item_readmore\\\":\\\"\\\",\\\"show_item_readmore_name\\\":\\\"\\\",\\\"item_readmore_limit\\\":\\\"100\\\",\\\"item_alternative_readmore\\\":\\\"\\\",\\\"show_item_noauth\\\":\\\"\\\",\\\"show_item_name\\\":\\\"\\\",\\\"link_item_names\\\":\\\"\\\",\\\"show_item_intro\\\":\\\"\\\",\\\"show_item_image\\\":\\\"\\\",\\\"show_item_image_float\\\":\\\"\\\",\\\"show_item_image_width\\\":\\\"\\\",\\\"show_item_image_height\\\":\\\"\\\",\\\"show_item_intro_image\\\":\\\"\\\",\\\"show_item_intro_image_float\\\":\\\"\\\",\\\"show_item_intro_image_width\\\":\\\"\\\",\\\"show_item_intro_image_height\\\":\\\"\\\",\\\"show_item_urls\\\":\\\"\\\",\\\"show_item_urls_position\\\":\\\"\\\",\\\"show_item_category\\\":\\\"\\\",\\\"link_item_category\\\":\\\"\\\",\\\"show_item_parent_category\\\":\\\"\\\",\\\"link_item_parent_category\\\":\\\"\\\",\\\"show_item_tags\\\":\\\"\\\",\\\"show_item_created_by\\\":\\\"\\\",\\\"link_item_created_by\\\":\\\"\\\",\\\"show_item_created_by_alias\\\":\\\"\\\",\\\"show_item_created\\\":\\\"\\\",\\\"show_item_modified\\\":\\\"\\\",\\\"show_item_publish_up\\\":\\\"\\\",\\\"show_item_publish_down\\\":\\\"\\\",\\\"show_item_hits\\\":\\\"\\\",\\\"show_item_admin\\\":\\\"\\\",\\\"show_item_vote\\\":\\\"\\\"}\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"63\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(9, 8, 10000, '', '2017-07-13 18:11:34', 655, 646, 'a923131d8ae7818690475964fc8343983dd06268', '{\"id\":8,\"asset_id\":\"62\",\"parent_id\":\"1\",\"lft\":\"11\",\"rgt\":\"12\",\"level\":\"1\",\"path\":\"2017-07-13-18-05-17\",\"extension\":\"com_work\",\"title\":\"\\u10db\\u10e8\\u10d4\\u10dc\\u10d4\\u10d1\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"mshenebloba\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":\"655\",\"checked_out_time\":\"2017-07-13 18:11:30\",\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 17:47:29\",\"modified_user_id\":\"655\",\"modified_time\":\"2017-07-13 18:11:34\",\"hits\":\"0\",\"language\":\"*\",\"version\":\"1\"}', 0),
(10, 9, 10000, '', '2017-07-13 18:11:47', 655, 583, 'a9b448780d7637f5afc75bc8ae7a64ed31be6f64', '{\"id\":9,\"asset_id\":64,\"parent_id\":\"1\",\"lft\":\"13\",\"rgt\":14,\"level\":1,\"path\":null,\"extension\":\"com_work\",\"title\":\"\\u10e0\\u10d4\\u10db\\u10dd\\u10dc\\u10e2\\u10d8\",\"alias\":\"remonti\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 18:11:47\",\"modified_user_id\":null,\"modified_time\":\"2017-07-13 18:11:47\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(11, 10, 10000, '', '2017-07-13 18:13:04', 655, 641, '6a4c10e1b7a65c2da47b514cb7818d37e8e04c8b', '{\"id\":10,\"asset_id\":65,\"parent_id\":\"1\",\"lft\":\"15\",\"rgt\":16,\"level\":1,\"path\":null,\"extension\":\"com_work\",\"title\":\"\\u10db\\u10dd\\u10db\\u10e1\\u10d0\\u10ee\\u10e3\\u10e0\\u10d4\\u10d1\\u10d0                            \",\"alias\":\"momsakhureba\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 18:13:04\",\"modified_user_id\":null,\"modified_time\":\"2017-07-13 18:13:04\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(12, 11, 10000, '', '2017-07-13 18:13:21', 655, 612, '7b028100e456ec59d2813f9c6acf9b47fe187e10', '{\"id\":11,\"asset_id\":66,\"parent_id\":\"1\",\"lft\":\"17\",\"rgt\":18,\"level\":1,\"path\":null,\"extension\":\"com_work\",\"title\":\"\\u10ef\\u10d0\\u10dc\\u10db\\u10e0\\u10d7\\u10d4\\u10da\\u10dd\\u10d1\\u10d0\",\"alias\":\"janmrteloba\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 18:13:21\",\"modified_user_id\":null,\"modified_time\":\"2017-07-13 18:13:21\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(13, 12, 10000, '', '2017-07-13 18:13:32', 655, 591, '92d2eb08720a2367a746c16271cd706fd7fe8df7', '{\"id\":12,\"asset_id\":67,\"parent_id\":\"1\",\"lft\":\"19\",\"rgt\":20,\"level\":1,\"path\":null,\"extension\":\"com_work\",\"title\":\"\\u10e1\\u10d8\\u10da\\u10d0\\u10db\\u10d0\\u10d6\\u10d4\",\"alias\":\"silamaze\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 18:13:32\",\"modified_user_id\":null,\"modified_time\":\"2017-07-13 18:13:32\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(14, 13, 10000, '', '2017-07-13 18:13:57', 655, 656, '8f4c5deb2520d94fa215bad76ae942efd3ab6448', '{\"id\":13,\"asset_id\":68,\"parent_id\":\"1\",\"lft\":\"21\",\"rgt\":22,\"level\":1,\"path\":null,\"extension\":\"com_work\",\"title\":\"\\u10d0\\u10e3\\u10e5\\u10ea\\u10d8\\u10dd\\u10dc\\u10d8, \\u10e2\\u10d4\\u10dc\\u10d3\\u10d4\\u10e0\\u10d8            \",\"alias\":\"auktsioni-tenderi\",\"note\":\"\",\"description\":\"\",\"published\":\"1\",\"checked_out\":null,\"checked_out_time\":null,\"access\":\"1\",\"params\":\"{\\\"category_layout\\\":\\\"\\\",\\\"image\\\":\\\"\\\",\\\"image_alt\\\":\\\"\\\"}\",\"metadesc\":\"\",\"metakey\":\"\",\"metadata\":\"{\\\"author\\\":\\\"\\\",\\\"robots\\\":\\\"\\\"}\",\"created_user_id\":\"655\",\"created_time\":\"2017-07-13 18:13:57\",\"modified_user_id\":null,\"modified_time\":\"2017-07-13 18:13:57\",\"hits\":\"0\",\"language\":\"*\",\"version\":null}', 0),
(15, 1, 1, '', '2017-07-16 16:17:32', 655, 1776, '44773c2050546a60cf27848fd0c10be2df41976a', '{\"id\":1,\"asset_id\":82,\"title\":\"Privacy Policy\",\"alias\":\"privacy-policy\",\"introtext\":\"<p>Privacy Policy<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-07-16 16:17:32\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-16 16:17:32\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-07-16 16:17:32\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(16, 2, 1, '', '2017-07-16 16:19:29', 655, 1794, '4c4d3ba4a25f930a4c63b833f70539c9cc21a8bc', '{\"id\":2,\"asset_id\":84,\"title\":\"Terms & Conditions\",\"alias\":\"terms-conditions\",\"introtext\":\"<p>Terms &amp;amp; Conditions<\\/p>\",\"fulltext\":\"\",\"state\":1,\"catid\":\"2\",\"created\":\"2017-07-16 16:19:29\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-07-16 16:19:29\",\"modified_by\":null,\"checked_out\":null,\"checked_out_time\":null,\"publish_up\":\"2017-07-16 16:19:29\",\"publish_down\":\"0000-00-00 00:00:00\",\"images\":\"{\\\"image_intro\\\":\\\"\\\",\\\"float_intro\\\":\\\"\\\",\\\"image_intro_alt\\\":\\\"\\\",\\\"image_intro_caption\\\":\\\"\\\",\\\"image_fulltext\\\":\\\"\\\",\\\"float_fulltext\\\":\\\"\\\",\\\"image_fulltext_alt\\\":\\\"\\\",\\\"image_fulltext_caption\\\":\\\"\\\"}\",\"urls\":\"{\\\"urla\\\":false,\\\"urlatext\\\":\\\"\\\",\\\"targeta\\\":\\\"\\\",\\\"urlb\\\":false,\\\"urlbtext\\\":\\\"\\\",\\\"targetb\\\":\\\"\\\",\\\"urlc\\\":false,\\\"urlctext\\\":\\\"\\\",\\\"targetc\\\":\\\"\\\"}\",\"attribs\":\"{\\\"article_layout\\\":\\\"\\\",\\\"show_title\\\":\\\"\\\",\\\"link_titles\\\":\\\"\\\",\\\"show_tags\\\":\\\"\\\",\\\"show_intro\\\":\\\"\\\",\\\"info_block_position\\\":\\\"\\\",\\\"info_block_show_title\\\":\\\"\\\",\\\"show_category\\\":\\\"\\\",\\\"link_category\\\":\\\"\\\",\\\"show_parent_category\\\":\\\"\\\",\\\"link_parent_category\\\":\\\"\\\",\\\"show_associations\\\":\\\"\\\",\\\"show_author\\\":\\\"\\\",\\\"link_author\\\":\\\"\\\",\\\"show_create_date\\\":\\\"\\\",\\\"show_modify_date\\\":\\\"\\\",\\\"show_publish_date\\\":\\\"\\\",\\\"show_item_navigation\\\":\\\"\\\",\\\"show_icons\\\":\\\"\\\",\\\"show_print_icon\\\":\\\"\\\",\\\"show_email_icon\\\":\\\"\\\",\\\"show_vote\\\":\\\"\\\",\\\"show_hits\\\":\\\"\\\",\\\"show_noauth\\\":\\\"\\\",\\\"urls_position\\\":\\\"\\\",\\\"alternative_readmore\\\":\\\"\\\",\\\"article_page_title\\\":\\\"\\\",\\\"show_publishing_options\\\":\\\"\\\",\\\"show_article_options\\\":\\\"\\\",\\\"show_urls_images_backend\\\":\\\"\\\",\\\"show_urls_images_frontend\\\":\\\"\\\"}\",\"version\":1,\"ordering\":null,\"metakey\":\"\",\"metadesc\":\"\",\"access\":\"1\",\"hits\":null,\"metadata\":\"{\\\"robots\\\":\\\"\\\",\\\"author\\\":\\\"\\\",\\\"rights\\\":\\\"\\\",\\\"xreference\\\":\\\"\\\"}\",\"featured\":\"0\",\"language\":\"*\",\"xreference\":\"\"}', 0),
(17, 2, 10004, '', '2017-08-14 19:17:31', 655, 548, '4a961a1108dc519ad546eb277ed5b8c4fc29cf00', '{\"id\":2,\"name\":\"zxczxc\",\"alias\":\"zxczxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:17:31\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:17:31\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":2,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":98,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(18, 3, 10004, '', '2017-08-14 19:24:11', 655, 548, '5439967f1b8fc92eaf8711087adcb93cf644404c', '{\"id\":3,\"name\":\"zxczxc\",\"alias\":\"zxczxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:24:11\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:24:11\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":3,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":99,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(19, 4, 10004, '', '2017-08-14 19:25:27', 655, 549, 'f8ccc21715efaf24b557fd67776fc9c15a75776e', '{\"id\":4,\"name\":\"zxczxc\",\"alias\":\"zxczxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:25:27\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:25:27\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":4,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":100,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(20, 5, 10004, '', '2017-08-14 19:27:08', 655, 549, '2b120459ea835ee6a1ec5fe20b352a58e64ba1ff', '{\"id\":5,\"name\":\"zxczxc\",\"alias\":\"zxczxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:27:08\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:27:08\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":5,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":101,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(21, 6, 10004, '', '2017-08-14 19:27:37', 655, 549, '4d88023cc729ea3af8568151f4b562baf8dfd44e', '{\"id\":6,\"name\":\"zxczxc\",\"alias\":\"zxczxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:27:37\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:27:37\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":6,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":102,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(22, 7, 10004, '', '2017-08-14 19:35:28', 655, 549, '3735c89b355c7d830e48696306d01a7aaf893e6a', '{\"id\":7,\"name\":\"zxczxc\",\"alias\":\"zxczxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:35:28\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:35:28\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":7,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":103,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(23, 8, 10004, '', '2017-08-14 19:37:19', 655, 545, 'ec691fc5726f02a4e8e2cd7debd4566e69d89a8f', '{\"id\":8,\"name\":\"dfdf\",\"alias\":\"dfdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:37:19\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:37:19\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":8,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":104,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(24, 9, 10004, '', '2017-08-14 19:45:13', 655, 545, 'b2800568b1dd347b1dd48d8b1dcea0f0bd8b14d5', '{\"id\":9,\"name\":\"dfdf\",\"alias\":\"dfdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:45:13\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:45:13\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":9,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":105,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(25, 10, 10004, '', '2017-08-14 19:45:54', 655, 547, '1ab8e3ad875751122569567a95b2cae1daa231f3', '{\"id\":10,\"name\":\"dfdf\",\"alias\":\"dfdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:45:54\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:45:54\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":10,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":106,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(26, 11, 10004, '', '2017-08-14 19:46:11', 655, 547, '3274c5a96e5c4bdd8cbd342068369df5dd72002e', '{\"id\":11,\"name\":\"dfdf\",\"alias\":\"dfdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:46:11\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:46:11\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":11,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":107,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(27, 12, 10004, '', '2017-08-14 19:47:15', 655, 547, '2c738bc8718a1748bc4630fe30aa406ef1fa6a4d', '{\"id\":12,\"name\":\"dfdf\",\"alias\":\"dfdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:47:15\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:47:15\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":12,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":108,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(28, 13, 10004, '', '2017-08-14 19:50:23', 655, 547, 'e3ab73ef2271a64fc4c3dd0f70fcdde0db9be86a', '{\"id\":13,\"name\":\"fdgd\",\"alias\":\"fdgd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:50:23\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:50:23\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":13,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":109,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(29, 14, 10004, '', '2017-08-14 19:50:32', 655, 547, 'e52c09a7ca6fc3143b0be762bcf5964da64e2d94', '{\"id\":14,\"name\":\"fdgd\",\"alias\":\"fdgd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:50:32\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-14 19:50:32\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":14,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":110,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(30, 14, 10004, '', '2017-08-14 19:53:33', 655, 547, '4b0761f29bcd3b8d5ac803c3685fecac23385b51', '{\"id\":14,\"name\":\"fdgd\",\"alias\":\"fdgd\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-14 19:50:32\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-14 19:53:33\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"14\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"110\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(31, 15, 10004, '', '2017-08-15 17:14:52', 655, 545, '672c37beede66b833de96a0414b112c9a6118713', '{\"id\":15,\"name\":\"sdf\",\"alias\":\"sdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 17:14:52\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 17:14:52\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":15,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":111,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(32, 15, 10004, '', '2017-08-15 17:15:00', 655, 545, 'a1bb769493716d95be016817dcb881e8e3d27fac', '{\"id\":15,\"name\":\"sdf\",\"alias\":\"sdf\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 17:14:52\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-15 17:15:00\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"15\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"111\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(33, 16, 10004, '', '2017-08-15 17:16:35', 655, 551, '81077ae7add21959c8502a2bd93e93ec770b08e5', '{\"id\":16,\"name\":\"sdfsdf\",\"alias\":\"sdfsdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 17:16:35\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 17:16:35\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":16,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":112,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(34, 17, 10004, '', '2017-08-15 17:25:45', 655, 547, '035c1cf196b4fa0aa3749205ae8902a10ab8431d', '{\"id\":17,\"name\":\"aasd\",\"alias\":\"aasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 17:25:45\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 17:25:45\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":17,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":113,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(35, 18, 10004, '', '2017-08-15 17:27:40', 655, 545, '87e8c9b7e88bc23791a9cb0387a5b2f171f88ed0', '{\"id\":18,\"name\":\"asd\",\"alias\":\"asd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":9,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 17:27:40\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 17:27:40\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":18,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":114,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(36, 18, 10004, '', '2017-08-15 17:29:08', 655, 545, '484886cc15c0e437e2669be0869437344ef5e82c', '{\"id\":18,\"name\":\"asd\",\"alias\":\"asd\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":9,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 17:27:40\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-15 17:29:08\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"18\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"114\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(37, 1, 10003, '', '2017-08-15 18:13:12', 655, 620, 'b5238bd0cc2a9bf3d4f0c534cd6e354651a6f1e2', '{\"id\":1,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 18:13:12\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 18:13:12\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":115,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\18\\\\5ee0c0943b38e37a8aa0f78ab751c5bb.jpg\",\"item_id\":18}', 0),
(38, 19, 10004, '', '2017-08-15 19:07:24', 655, 545, '43ce51fe4e19a4a8c1437d39b1d74b73d2961985', '{\"id\":19,\"name\":\"asd\",\"alias\":\"asd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:07:24\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:07:24\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":19,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":116,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(39, 20, 10004, '', '2017-08-15 19:07:56', 655, 551, '6af497db6bb9a78e5d21e86073d0efe2d8733521', '{\"id\":20,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:07:56\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:07:56\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":20,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":117,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(40, 2, 10003, '', '2017-08-15 19:07:57', 655, 618, 'e41aab1d55c4e42e1928715a9ef357b45305e8c1', '{\"id\":2,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:07:57\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:07:57\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":118,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\28b5f15b790d766e471ffb7c581dc4ec.jpg\",\"item_id\":0}', 0),
(41, 20, 10004, '', '2017-08-15 19:08:28', 655, 551, 'fa42b4a5348c7d2c010c17610c361d234f95ee8e', '{\"id\":20,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:07:56\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-15 19:08:28\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"20\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"117\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(42, 3, 10003, '', '2017-08-15 19:08:28', 655, 620, '46365f6a726e84479e763e8a6f7774d0d408c1ad', '{\"id\":3,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:08:28\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:08:28\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":119,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\20\\\\5e202a4f93976570af8bb7a94ee73e2b.jpg\",\"item_id\":20}', 0),
(43, 21, 10004, '', '2017-08-15 19:10:12', 655, 551, 'e9e536273d8ea8cc344e705078a6c64737ce35f8', '{\"id\":21,\"name\":\"sdfsdf\",\"alias\":\"sdfsdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":9,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:10:12\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:10:12\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":21,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":120,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(44, 4, 10003, '', '2017-08-15 19:10:12', 655, 618, '8fa320972e6f9e938342be05235496cc454d40bc', '{\"id\":4,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:10:12\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:10:12\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":121,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\207ddda734bd5245b95fda6f262cbaa4.jpg\",\"item_id\":0}', 0),
(45, 22, 10004, '', '2017-08-15 19:12:09', 655, 551, 'efddf0f0061e7059b9929624542b90cb5f0bd322', '{\"id\":22,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:12:09\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:12:09\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":22,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":122,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(46, 5, 10003, '', '2017-08-15 19:12:10', 655, 618, '273bac45be0313382425d4a1ca62e912007f3c72', '{\"id\":5,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:12:10\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:12:10\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":123,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\d3cf11b463a7b96861e7cb568e3ead8f.jpg\",\"item_id\":0}', 0),
(47, 6, 10003, '', '2017-08-15 19:12:10', 655, 618, '261b0888b01daca8b0d4f96e290e8a4f8c86a584', '{\"id\":6,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:12:10\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:12:10\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":124,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\57df4188491ff1ca6da597ac774f545d.jpg\",\"item_id\":0}', 0);
INSERT INTO `kutah_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(48, 23, 10004, '', '2017-08-15 19:16:28', 655, 545, 'a78d2ffeab2c9414c8e0fa00f908212e9a6ddd31', '{\"id\":23,\"name\":\"sdf\",\"alias\":\"sdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":9,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:16:28\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:16:28\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":23,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":125,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(49, 7, 10003, '', '2017-08-15 19:16:29', 655, 618, '1d2c3958810d9ee00e7fd9a4ef7ea1b61017f094', '{\"id\":7,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:16:29\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:16:29\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":126,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\351066b6e9a87a42a46692302217f438.jpg\",\"item_id\":0}', 0),
(50, 23, 10004, '', '2017-08-15 19:16:51', 655, 545, '6dfc2704f5cd8795e250ac699e712bd12e7b6820', '{\"id\":23,\"name\":\"sdf\",\"alias\":\"sdf\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":9,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:16:28\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-15 19:16:51\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"23\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"125\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\"}', 0),
(51, 8, 10003, '', '2017-08-15 19:17:04', 655, 620, 'e786dfb094ad7b56e8316a5581f0d50e38d0a023', '{\"id\":8,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:17:04\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:17:04\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":127,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\23\\\\5eda2145c15a9b4523c615168383b7e4.jpg\",\"item_id\":23}', 0),
(52, 9, 10003, '', '2017-08-15 19:17:04', 655, 620, '4dbca2571f08906dc5489164d1f85fbd472e7347', '{\"id\":9,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:17:04\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:17:04\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":128,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\23\\\\1b7f5739a7e938b1132bc82409928111.jpg\",\"item_id\":23}', 0),
(53, 24, 10004, '', '2017-08-15 19:20:06', 655, 551, '1c0df7437aef68ae6d25f358a642f9953572e3ab', '{\"id\":24,\"name\":\"sdfsdf\",\"alias\":\"sdfsdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:20:06\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:20:06\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":24,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":129,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(54, 10, 10003, '', '2017-08-15 19:20:06', 655, 619, '43e3f6d58b155c20b86beb62ee2ab82ea268d3fe', '{\"id\":10,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:20:06\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:20:06\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":130,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\5cc651d3df1ccb76380ccc7e35a1d84f.jpg\",\"item_id\":0}', 0),
(55, 25, 10004, '', '2017-08-15 19:21:38', 655, 551, '9927b16e4c9dfadb02e8ec351bc9325798ff890e', '{\"id\":25,\"name\":\"dfgdfg\",\"alias\":\"dfgdfg\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:21:38\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:21:38\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":25,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":131,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(56, 11, 10003, '', '2017-08-15 19:21:38', 655, 619, '686c32b81027de1298572a55e5d0325686110db7', '{\"id\":11,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:21:38\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:21:38\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":132,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\f40a2bf33c7312acf528271c1fe203ea.jpg\",\"item_id\":0}', 0),
(57, 26, 10004, '', '2017-08-15 19:24:11', 655, 553, '393ef023e44d6884556cab5ac603a7c3f1bd8a5b', '{\"id\":26,\"name\":\"dfgdfgd\",\"alias\":\"dfgdfgd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:24:11\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:24:11\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":26,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":133,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(58, 12, 10003, '', '2017-08-15 19:24:11', 655, 619, 'd5916387ffbc0965a8dfeffba9659ca39a89d611', '{\"id\":12,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:24:11\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:24:11\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":134,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\435992ee8706a234c73d2626d19921aa.jpg\",\"item_id\":0}', 0),
(59, 13, 10003, '', '2017-08-15 19:24:12', 655, 619, '29fbcaa881c69d4034d16cad6794f25daf354531', '{\"id\":13,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:24:12\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:24:12\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":135,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\8ccdab19c5ab009833e44cafa5ec49cd.jpg\",\"item_id\":0}', 0),
(60, 27, 10004, '', '2017-08-15 19:29:34', 655, 581, 'ff2d1dd3bbf7381ca32971d11069ad73b3e1e9ea', '{\"id\":27,\"name\":\"sdfsdf\",\"alias\":\"sdfsdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:29:34\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:29:34\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":27,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":136,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":null,\"youtubeLink\":null}', 0),
(61, 14, 10003, '', '2017-08-15 19:29:34', 655, 619, 'b40bbeb17ddff1e19339a77a959859960f26db6d', '{\"id\":14,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:29:34\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:29:34\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":137,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\1638ef28d12f1262dd5fd37b003200ba.jpg\",\"item_id\":0}', 0),
(62, 15, 10003, '', '2017-08-15 19:29:35', 655, 619, 'baa14a32b1df56243e8fa82974189f8e51154375', '{\"id\":15,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:29:35\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:29:35\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":138,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\20324c96fdbb72d28114e4211d5ef5c7.jpg\",\"item_id\":0}', 0),
(63, 28, 10004, '', '2017-08-15 19:32:08', 655, 640, '94b2953d7a85ee7ca58e87ceca51384a1765bf1d', '{\"id\":28,\"name\":\"sdfsdf\",\"alias\":\"sdfsdf\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:32:08\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:32:08\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":28,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":139,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"https:\\/\\/mail.ru\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(64, 16, 10003, '', '2017-08-15 19:32:08', 655, 619, 'dd8989b345cfda717e75470bc54bb1d5e131b5ed', '{\"id\":16,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:32:08\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:32:08\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":140,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\17356475c6865cb6d1d817726b24cb63.jpg\",\"item_id\":0}', 0),
(65, 17, 10003, '', '2017-08-15 19:32:09', 655, 619, '6cdfc8bd5b18cdb76edc9137c32372539ded63f7', '{\"id\":17,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:32:09\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:32:09\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":141,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\7c31f4618bc50e76bde6eb9032903aba.jpg\",\"item_id\":0}', 0),
(66, 29, 10004, '', '2017-08-15 19:45:13', 655, 639, '83bd2d9adfb93fddbd4361ae8782d073065ddc4c', '{\"id\":29,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:45:13\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:45:13\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":29,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":142,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/mail.ru\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=HE6w2vWWTrs\"}', 0),
(67, 18, 10003, '', '2017-08-15 19:45:13', 655, 619, '0b4c5dbe38e0f02b28efb48c09ae27766a67a8d5', '{\"id\":18,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:45:13\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:45:13\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":143,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\71d8ff61a8cd1bdfbd30898f68794d19.jpg\",\"item_id\":0}', 0),
(68, 19, 10003, '', '2017-08-15 19:45:14', 655, 619, '42b9677dde88089c798f2c6b6850c77972fa0a0f', '{\"id\":19,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:45:14\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:45:14\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":144,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\37f394b81818afb944bbaf2d08fba7c3.jpg\",\"item_id\":0}', 0),
(69, 20, 10003, '', '2017-08-15 19:45:14', 655, 619, 'f9c28acced76d907e6e6669dfc5ee12d94faaacc', '{\"id\":20,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:45:14\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:45:14\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":145,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\9f386ee486727760d2aabf44c4421283.jpg\",\"item_id\":0}', 0),
(70, 21, 10003, '', '2017-08-15 19:45:15', 655, 619, 'bc492f127d30e310f4fb441f4a0877eb8f2030e3', '{\"id\":21,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:45:15\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:45:15\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":146,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\ef9a23f22b68a3091b5cf59df9f8fb88.jpg\",\"item_id\":0}', 0),
(71, 30, 10004, '', '2017-08-15 19:50:56', 655, 645, 'd087bb3ca5184b5fcbcab3e7fd2d6563a9122212', '{\"id\":30,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:50:56\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:50:56\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":30,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":147,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(72, 22, 10003, '', '2017-08-15 19:50:56', 655, 619, 'd4b901d89623990ab1a0e78bd7e1625fe7a95ce2', '{\"id\":22,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:50:56\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:50:56\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":148,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\a0c244141de17313f720a10afa76ef95.jpg\",\"item_id\":0}', 0),
(73, 30, 10004, '', '2017-08-15 19:52:36', 655, 645, 'a3a0096c10465504b905e7f3ee9032ef9d3b1eb4', '{\"id\":30,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:50:56\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-15 19:52:36\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"30\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"147\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(74, 23, 10003, '', '2017-08-15 19:52:37', 655, 621, 'b6b1d9fce24b1b4f1edafce9d868549361296d97', '{\"id\":23,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:52:37\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:52:37\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":149,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\30\\\\235d8a4121f620f6dbc6e38c2c2b3076.jpg\",\"item_id\":30}', 0),
(75, 31, 10004, '', '2017-08-15 19:53:29', 655, 645, 'd1777053b0177784bf437cb6970ad0a63e6845f3', '{\"id\":31,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:53:29\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:53:29\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":31,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":150,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(76, 24, 10003, '', '2017-08-15 19:53:29', 655, 619, '8a1a14e2ccf37cc6b47fc3b924025eeb7c6444c6', '{\"id\":24,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:53:29\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:53:29\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":151,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\2996c7c244a6cf3fa30dec1e047d1d13.jpg\",\"item_id\":0}', 0),
(77, 31, 10004, '', '2017-08-15 19:56:26', 655, 645, '56714cbb154592ffa62757cc9e50d820123d399b', '{\"id\":31,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:53:29\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-08-15 19:56:26\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"31\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"150\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(78, 32, 10004, '', '2017-08-15 19:58:15', 655, 647, '32ced43b7b60de4e58f7c4b4b756edcb07437022', '{\"id\":32,\"name\":\"fghdfgh\",\"alias\":\"fghdfgh\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:58:15\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:58:15\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":32,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":152,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(79, 25, 10003, '', '2017-08-15 19:58:15', 655, 619, '637a32c3e3ea4ab49cd0c70cefd8a129732fd163', '{\"id\":25,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:58:15\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:58:15\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":153,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\8c71765b1dc636edb70a198bf8190709.jpg\",\"item_id\":0}', 0),
(80, 26, 10003, '', '2017-08-15 19:58:15', 655, 619, '999773803d43287f83241d31a0840254f0206e11', '{\"id\":26,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 19:58:15\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 19:58:15\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":154,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\84b2fdfb38a90d2d409fc91a77484073.jpg\",\"item_id\":0}', 0),
(81, 33, 10004, '', '2017-08-15 20:03:11', 655, 581, '1a6b60da5912dcceb0ef48ebfc24815fcc33e70d', '{\"id\":33,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":9,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:03:11\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:03:11\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":33,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":155,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":null,\"youtubeLink\":null}', 0),
(82, 27, 10003, '', '2017-08-15 20:03:11', 655, 619, '70c6e5406e88cfb37ca0743f4e889b7062f4fb68', '{\"id\":27,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:03:11\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:03:11\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":156,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\3f2060bb3317897faf7ccafbaa9d8374.jpg\",\"item_id\":0}', 0),
(83, 34, 10004, '', '2017-08-15 20:05:39', 655, 579, 'f27f2ff25f0851d58de52ab5f1aa899d7568d5ea', '{\"id\":34,\"name\":\"sasas\",\"alias\":\"sasas\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:05:39\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:05:39\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":34,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":157,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":null,\"youtubeLink\":null}', 0),
(84, 28, 10003, '', '2017-08-15 20:05:39', 655, 619, 'ab77e1067e720a1bbcd54df2446b1fb6c8da5219', '{\"id\":28,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:05:39\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:05:39\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":158,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\e2308ef68c7e4710e8ae897c5ab4e52f.jpg\",\"item_id\":0}', 0),
(85, 29, 10003, '', '2017-08-15 20:05:40', 655, 619, 'f50fad14ada875d15f0a89ff96559e40921c1631', '{\"id\":29,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:05:40\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:05:40\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":159,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\8db7e098fc58dbda7c530b6cdc0a3558.jpg\",\"item_id\":0}', 0),
(86, 35, 10004, '', '2017-08-15 20:07:30', 655, 581, '01302d60f74a2e23308c4a57fba337d3593145ba', '{\"id\":35,\"name\":\"asdasd\",\"alias\":\"asdasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:07:30\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:07:30\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":35,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":160,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":null,\"youtubeLink\":null}', 0),
(87, 30, 10003, '', '2017-08-15 20:07:30', 655, 619, 'b46bc4f5b1d9eb668293d04623642975d96106d6', '{\"id\":30,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:07:30\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:07:30\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":161,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\5936ec53cd0772d6b064a068effb5df2.jpg\",\"item_id\":0}', 0),
(88, 31, 10003, '', '2017-08-15 20:07:31', 655, 619, 'd0ab7e2eebd3afba9cb27df9fd47152ab72e11a3', '{\"id\":31,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:07:31\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:07:31\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":162,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\0\\\\753068f46f88dd46a982a605ec6b677a.jpg\",\"item_id\":0}', 0),
(89, 36, 10004, '', '2017-08-15 20:13:49', 655, 645, '66d319036293fae88b430da2b8a636292a74b9f2', '{\"id\":36,\"name\":\"ertert\",\"alias\":\"ertert\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:13:49\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:13:49\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":36,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":163,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(90, 37, 10004, '', '2017-08-15 20:14:25', 655, 649, '6c2ea87a744697fd443cd4dd9a86e5cec3d1764f', '{\"id\":37,\"name\":\"ZXCZXCZX\",\"alias\":\"zxczxczx\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:14:25\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:14:25\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":37,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":164,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=p67_XgWRKwk\"}', 0),
(91, 38, 10004, '', '2017-08-15 20:16:08', 655, 577, '0ca5795e5db6f6878ea60fc65d4864f82068eb1b', '{\"id\":38,\"name\":\"azXc\",\"alias\":\"azxc\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":9,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:16:08\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:16:08\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":38,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":165,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":null,\"youtubeLink\":null}', 0),
(92, 32, 10003, '', '2017-08-15 20:16:08', 655, 621, 'ed29b64128108f9bac5e651086ea26dcc9a8b262', '{\"id\":32,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:16:08\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:16:08\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":166,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\38\\\\976af3f910100241f71f33df3fccfeca.jpg\",\"item_id\":38}', 0),
(93, 33, 10003, '', '2017-08-15 20:16:09', 655, 621, 'b04635ee06ce032cff5f6ece796ef6d08ca22831', '{\"id\":33,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:16:09\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-15 20:16:09\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":167,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\38\\\\99320678f4c5fba5cbde4e31bd096ac6.jpg\",\"item_id\":38}', 0),
(94, 39, 10004, '', '2017-08-16 17:28:03', 655, 645, 'd2048bee87b66b1cb8c26ec5949f1684c887d7e7', '{\"id\":39,\"name\":\"sadasd\",\"alias\":\"sadasd\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 17:28:03\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 17:28:03\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":39,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":168,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=AdByXzUnBoY\"}', 0),
(95, 34, 10003, '', '2017-08-16 17:28:04', 655, 621, 'b4a5ddbd950dd8a2134800629222204c78c8fbc8', '{\"id\":34,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 17:28:04\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 17:28:04\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":169,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\39\\\\bdef0da0544d55c4832ea32d5cb8b069.jpg\",\"item_id\":39}', 0),
(96, 35, 10003, '', '2017-08-16 17:28:04', 655, 621, '93670a127f4a012135fcf676f33fcfbcc134d2d5', '{\"id\":35,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 17:28:04\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 17:28:04\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":170,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\39\\\\f0c855b1d91e693f7efaafeacbd2ff76.jpg\",\"item_id\":39}', 0),
(97, 40, 10004, '', '2017-08-16 20:19:59', 655, 867, '6b655acf8f741c8c3ce23ea8f648f960e5a6ee8c', '{\"id\":40,\"name\":\"\\u10ed\\u10dd\\u10dc\\u10e5\\u10d0\\u10eb\\u10d8\\u10e1 \\u10e5\\u10e3\\u10e9\\u10d0\\u10d6\\u10d4 \\u10db\\u10dd\\u10db\\u10ee\\u10d3\\u10d0\\u10e0\\u10d8 \\u10db\\u10d9\\u10d5\\u10da\\u10d4\\u10da\\u10dd\\u10d1\\u10d8\\u10e1\",\"alias\":\"chonkadzis-kuchaze-momkhdari-mkvlelobis\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":8,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:19:59\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:19:59\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":40,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":171,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=bHzXWkAkyms\"}', 0),
(98, 36, 10003, '', '2017-08-16 20:20:00', 655, 621, 'f4e74391e108c17033d156c79187133bddff1bab', '{\"id\":36,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:20:00\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:20:00\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":172,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\40\\\\0ff78ddcdad020448180796ab74343dd.jpg\",\"item_id\":40}', 0),
(99, 37, 10003, '', '2017-08-16 20:20:00', 655, 621, '70c516855d1d9f31bec3bf31242efc46d49ff93c', '{\"id\":37,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:20:00\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:20:00\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":173,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\40\\\\ec409e390f93b430ffda744586566fd8.jpg\",\"item_id\":40}', 0),
(100, 38, 10003, '', '2017-08-16 20:20:01', 655, 621, '2a5faf1c776472b4be9e8d0ad3ee38605055e8e3', '{\"id\":38,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:20:01\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:20:01\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":174,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\40\\\\ceb6d588cf37a8239708feba9f5a62fc.jpg\",\"item_id\":40}', 0),
(101, 39, 10003, '', '2017-08-16 20:20:01', 655, 621, 'da2454bbd268b105d611dc3e237c1c5eafd92e6f', '{\"id\":39,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:20:01\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:20:01\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":175,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\40\\\\cd3721570a613cb2f8958c69f4d1d366.jpg\",\"item_id\":40}', 0),
(102, 40, 10003, '', '2017-08-16 20:20:02', 655, 621, '5a3ae843a8c0cb5f8618a25e01c1873cfee7af8f', '{\"id\":40,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:20:02\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:20:02\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":176,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\40\\\\8de81d1f120708e29871e312ae32c548.jpg\",\"item_id\":40}', 0),
(103, 41, 10003, '', '2017-08-16 20:20:02', 655, 621, '555854932cd0a6e9223616a944d292eaf17c12c2', '{\"id\":41,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:20:02\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-08-16 20:20:02\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":177,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\40\\\\6e67d71768198893aea8bab084d1eda1.jpg\",\"item_id\":40}', 0),
(104, 41, 10004, '', '2017-09-02 11:53:54', 655, 572, 'ccabd4576fd65597746aa20d26a6e846f48c071f', '{\"id\":41,\"name\":\"\",\"alias\":\"\",\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"catid\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 11:53:54\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 11:53:54\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":41,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":178,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"url\":null,\"youtubeLink\":null}', 0),
(105, 1, 10004, '', '2017-09-02 12:37:23', 655, 608, '0dc71e6dc51fe392bdf5bb2001d4ad7fd0bdc485', '{\"id\":1,\"name\":\"\",\"school_id\":\"2\",\"degree_id\":\"1\",\"start\":1940,\"end\":1943,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"zxczx\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 12:37:23\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 12:37:23\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":1,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":63,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(106, 2, 10004, '', '2017-09-02 12:39:03', 655, 608, 'ba44fbfb210b9a5f852235504cedfd73a4d4974c', '{\"id\":2,\"name\":\"\",\"school_id\":\"2\",\"degree_id\":\"1\",\"start\":1940,\"end\":1943,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"zxczx\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 12:39:03\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 12:39:03\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":2,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":98,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(107, 3, 10004, '', '2017-09-02 12:40:19', 655, 608, 'd225761f595e3308aa47453ee4be01f4bc55a2f3', '{\"id\":3,\"name\":\"\",\"school_id\":\"2\",\"degree_id\":\"1\",\"start\":1940,\"end\":1943,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"zxczx\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 12:40:19\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 12:40:19\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":3,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":99,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(108, 4, 10004, '', '2017-09-02 12:42:05', 655, 609, '2a1b9461cad06b0a960a0b7ba3231abe0eb1a4ea', '{\"id\":4,\"name\":\"\",\"school_id\":\"2\",\"degree_id\":\"1\",\"start\":1940,\"end\":1943,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"zxczx\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 12:42:05\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 12:42:05\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":4,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":100,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0);
INSERT INTO `kutah_ucm_history` (`version_id`, `ucm_item_id`, `ucm_type_id`, `version_note`, `save_date`, `editor_user_id`, `character_count`, `sha1_hash`, `version_data`, `keep_forever`) VALUES
(109, 5, 10004, '', '2017-09-02 12:44:30', 655, 609, '82ae8bbb2cbeaa2c15f7877b423e430ba62d2563', '{\"id\":5,\"name\":\"\",\"school_id\":\"2\",\"degree_id\":\"1\",\"start\":1940,\"end\":1943,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"zxczx\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 12:44:30\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 12:44:30\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":5,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":101,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(110, 6, 10004, '', '2017-09-02 12:51:52', 655, 607, '6020783a9fb6cad22e7c69fdf2ac2ae96da3dc3c', '{\"id\":6,\"name\":\"\",\"school_id\":3,\"degree_id\":\"1\",\"start\":1947,\"end\":1955,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"assas\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 12:51:52\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 12:51:52\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":6,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":102,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(111, 6, 10004, '', '2017-09-02 14:42:16', 655, 1246, '176b12ed8022c2ea54eb3db72014ef4d5c2b6355', '{\"id\":6,\"name\":\"\",\"school_id\":\"3\",\"degree_id\":\"1\",\"start\":1953,\"end\":1955,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"\\u10e1\\u10d0\\u10e5\\u10d0\\u10e0\\u10d7\\u10d5\\u10d4\\u10da\\u10dd\\u10e1 \\u10de\\u10e0\\u10d4\\u10db\\u10d8\\u10d4\\u10e0-\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10db\\u10d0 \\u10d2\\u10d8\\u10dd\\u10e0\\u10d2\\u10d8 \\u10d9\\u10d5\\u10d8\\u10e0\\u10d8\\u10d9\\u10d0\\u10e8\\u10d5\\u10d8\\u10da\\u10db\\u10d0 \\u10d8\\u10db\\u10d4\\u10e0\\u10d4\\u10d7\\u10d8\\u10e1 \\u10e0\\u10d4\\u10d2\\u10d8\\u10dd\\u10dc\\u10e8\\u10d8 \\u201d\\u10e5\\u10d0\\u10e0\\u10d7\\u10e3\\u10da\\u10d8 \\u10dd\\u10ea\\u10dc\\u10d4\\u10d1\\u10d8\\u10e1\\u201d \\u10db\\u10d4\\u10e0\\u10dd\\u10d1\\u10d8\\u10e1 \\u10d9\\u10d0\\u10dc\\u10d3\\u10d8\\u10d3\\u10d0\\u10e2\\u10d4\\u10d1\\u10d8 \\u10d3\\u10d0\\u10d0\\u10e1\\u10d0\\u10ee\\u10d4\\u10da\\u10d0.\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 14:42:16\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 14:42:16\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":7,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":102,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(112, 6, 10004, '', '2017-09-02 14:43:31', 655, 1246, '34bca0c4fa9d164e28b35caf02a5774cdcb7609f', '{\"id\":6,\"name\":\"\",\"school_id\":\"3\",\"degree_id\":\"1\",\"start\":1940,\"end\":1955,\"area_of_study_id\":\"1\",\"alias\":\"\",\"description\":\"\\u10e1\\u10d0\\u10e5\\u10d0\\u10e0\\u10d7\\u10d5\\u10d4\\u10da\\u10dd\\u10e1 \\u10de\\u10e0\\u10d4\\u10db\\u10d8\\u10d4\\u10e0-\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10db\\u10d0 \\u10d2\\u10d8\\u10dd\\u10e0\\u10d2\\u10d8 \\u10d9\\u10d5\\u10d8\\u10e0\\u10d8\\u10d9\\u10d0\\u10e8\\u10d5\\u10d8\\u10da\\u10db\\u10d0 \\u10d8\\u10db\\u10d4\\u10e0\\u10d4\\u10d7\\u10d8\\u10e1 \\u10e0\\u10d4\\u10d2\\u10d8\\u10dd\\u10dc\\u10e8\\u10d8 \\u201d\\u10e5\\u10d0\\u10e0\\u10d7\\u10e3\\u10da\\u10d8 \\u10dd\\u10ea\\u10dc\\u10d4\\u10d1\\u10d8\\u10e1\\u201d \\u10db\\u10d4\\u10e0\\u10dd\\u10d1\\u10d8\\u10e1 \\u10d9\\u10d0\\u10dc\\u10d3\\u10d8\\u10d3\\u10d0\\u10e2\\u10d4\\u10d1\\u10d8 \\u10d3\\u10d0\\u10d0\\u10e1\\u10d0\\u10ee\\u10d4\\u10da\\u10d0.\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 14:43:31\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 14:43:31\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":8,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":102,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(113, 7, 10004, '', '2017-09-02 15:49:20', 655, 673, 'a49f1702a2c7d1d9ad5a5ba8c21338d5cbd4d9b3', '{\"id\":7,\"name\":\"\",\"school_id\":4,\"degree_id\":6,\"start\":1946,\"end\":2016,\"area_of_study_id\":6,\"alias\":\"\",\"description\":\"self.userEducation self.userEducation self.userEducation self.userEducation\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 15:49:20\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-02 15:49:20\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":9,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":103,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(114, 8, 10004, '', '2017-09-03 19:01:58', 655, 620, '85fc94336020ee51c34748532b1061c99329309c', '{\"id\":8,\"name\":\"sdf\",\"school_id\":null,\"degree_id\":null,\"start\":null,\"end\":null,\"area_of_study_id\":null,\"alias\":\"sdf\",\"description\":\"sdfsdf\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-03 19:01:58\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-03 19:01:58\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":10,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":104,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(115, 1, 10004, '', '2017-09-03 19:04:21', 655, 962, 'cb7ee8efa9831431678ea9f976d42e218bd4eb8a', '{\"id\":1,\"name\":\"web \\u10de\\u10e0\\u10dd\\u10d2\\u10e0\\u10d0\\u10db\\u10d8\\u10e1\\u10e2\\u10d8\",\"company_id\":3,\"country_id\":78,\"start_year\":2008,\"end_year\":2010,\"start_month\":4,\"end_month\":12,\"currently\":0,\"city\":\"\\u10d7\\u10d1\\u10d8\\u10da\\u10d8\\u10e1\\u10d8\",\"alias\":\"web-programisti\",\"description\":\"\\u10d5\\u10d4\\u10d1-\\u10d3\\u10d4\\u10d5\\u10d4\\u10da\\u10dd\\u10de\\u10d4\\u10e0\\u10d8 , \\u10db\\u10d7\\u10d0\\u10d5\\u10d0\\u10e0\\u10d8 \\u10de\\u10e0\\u10dd\\u10d2\\u10e0\\u10d0\\u10db\\u10d8\\u10e1\\u10e2\\u10d8\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-03 19:04:21\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-03 19:04:21\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":1,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":63,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(116, 1, 10004, '', '2017-09-05 17:48:53', 655, 964, 'e8ace3ac1d8472973ae94b1ad268fb85dd7034fb', '{\"id\":1,\"name\":\"web \\u10de\\u10e0\\u10dd\\u10d2\\u10e0\\u10d0\\u10db\\u10d8\\u10e1\\u10e2\\u10d8\",\"company_id\":\"3\",\"country_id\":78,\"start_year\":2008,\"end_year\":2010,\"start_month\":4,\"end_month\":12,\"currently\":1,\"city\":\"\\u10d7\\u10d1\\u10d8\\u10da\\u10d8\\u10e1\\u10d8\",\"alias\":\"web-programisti\",\"description\":\"\\u10d5\\u10d4\\u10d1-\\u10d3\\u10d4\\u10d5\\u10d4\\u10da\\u10dd\\u10de\\u10d4\\u10e0\\u10d8 , \\u10db\\u10d7\\u10d0\\u10d5\\u10d0\\u10e0\\u10d8 \\u10de\\u10e0\\u10dd\\u10d2\\u10e0\\u10d0\\u10db\\u10d8\\u10e1\\u10e2\\u10d8\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-05 17:48:53\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-05 17:48:53\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":2,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":63,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(117, 1, 10004, '', '2017-09-05 17:49:08', 655, 966, '423b26a839f7cf1af3f823b1090d19141696a691', '{\"id\":1,\"name\":\"web \\u10de\\u10e0\\u10dd\\u10d2\\u10e0\\u10d0\\u10db\\u10d8\\u10e1\\u10e2\\u10d81\",\"company_id\":\"3\",\"country_id\":78,\"start_year\":2008,\"end_year\":2010,\"start_month\":4,\"end_month\":12,\"currently\":1,\"city\":\"\\u10d7\\u10d1\\u10d8\\u10da\\u10d8\\u10e1\\u10d8\",\"alias\":\"web-programisti1\",\"description\":\"\\u10d5\\u10d4\\u10d1-\\u10d3\\u10d4\\u10d5\\u10d4\\u10da\\u10dd\\u10de\\u10d4\\u10e0\\u10d8 , \\u10db\\u10d7\\u10d0\\u10d5\\u10d0\\u10e0\\u10d8 \\u10de\\u10e0\\u10dd\\u10d2\\u10e0\\u10d0\\u10db\\u10d8\\u10e1\\u10e2\\u10d8\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-05 17:49:08\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-05 17:49:08\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":3,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":63,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(118, 2, 10004, '', '2017-09-05 18:04:19', 655, 687, 'd8ebde2923d127dc6cafff9c98daea803bd5780f', '{\"id\":2,\"name\":\"Veb programmer\",\"company_id\":\"3\",\"country_id\":78,\"start_year\":1957,\"end_year\":1958,\"start_month\":4,\"end_month\":12,\"currently\":0,\"city\":\"tbilisi\",\"alias\":\"veb-programmer\",\"description\":\"sdfsdf\",\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-05 18:04:19\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-05 18:04:19\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":1,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":98,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\"}', 0),
(119, 40, 10004, '', '2017-09-09 17:29:41', 655, 869, 'ca3fdb85c790d9779e3802f96b3bf7a20aa4498b', '{\"id\":\"40\",\"name\":\"\\u10ed\\u10dd\\u10dc\\u10e5\\u10d0\\u10eb\\u10d8\\u10e1 \\u10e5\\u10e3\\u10e9\\u10d0\\u10d6\\u10d4 \\u10db\\u10dd\\u10db\\u10ee\\u10d3\\u10d0\\u10e0\\u10d8 \\u10db\\u10d9\\u10d5\\u10da\\u10d4\\u10da\\u10dd\\u10d1\\u10d8\\u10e1\",\"alias\":\"chonkadzis-kuchaze-momkhdari-mkvlelobis\",\"description\":\"\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:19:59\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-09-09 17:29:41\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"40\",\"params\":\"\",\"hits\":\"2\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"171\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=bHzXWkAkyms\"}', 0),
(120, 40, 10004, '', '2017-09-09 17:35:34', 655, 1454, '11cd9af7072abd2c7457a14c37f2097eec0fdf64', '{\"id\":\"40\",\"name\":\"\\u10ed\\u10dd\\u10dc\\u10e5\\u10d0\\u10eb\\u10d8\\u10e1 \\u10e5\\u10e3\\u10e9\\u10d0\\u10d6\\u10d4 \\u10db\\u10dd\\u10db\\u10ee\\u10d3\\u10d0\\u10e0\\u10d8 \\u10db\\u10d9\\u10d5\\u10da\\u10d4\\u10da\\u10dd\\u10d1\\u10d8\\u10e1\",\"alias\":\"chonkadzis-kuchaze-momkhdari-mkvlelobis\",\"description\":\"\\u10ed\\u10dd\\u10dc\\u10e5\\u10d0\\u10eb\\u10d8\\u10e1 \\u10e5\\u10e3\\u10e9\\u10d0\\u10d6\\u10d4 \\u10db\\u10dd\\u10db\\u10ee\\u10d3\\u10d0\\u10e0\\u10d8 \\u10db\\u10d9\\u10d5\\u10da\\u10d4\\u10da\\u10dd\\u10d1\\u10d8\\u10e1\\u10ed\\u10dd\\u10dc\\u10e5\\u10d0\\u10eb\\u10d8\\u10e1 \\u10e5\\u10e3\\u10e9\\u10d0\\u10d6\\u10d4 \\u10db\\u10dd\\u10db\\u10ee\\u10d3\\u10d0\\u10e0\\u10d8 \\u10db\\u10d9\\u10d5\\u10da\\u10d4\\u10da\\u10dd\\u10d1\\u10d8\\u10e1\\u10ed\\u10dd\\u10dc\\u10e5\\u10d0\\u10eb\\u10d8\\u10e1 \\u10e5\\u10e3\\u10e9\\u10d0\\u10d6\\u10d4 \\u10db\\u10dd\\u10db\\u10ee\\u10d3\\u10d0\\u10e0\\u10d8 \\u10db\\u10d9\\u10d5\\u10da\\u10d4\\u10da\\u10dd\\u10d1\\u10d8\\u10e1\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-16 20:19:59\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-09-09 17:35:34\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"40\",\"params\":\"\",\"hits\":\"5\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"171\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"https:\\/\\/www.youtube.com\\/watch?v=bHzXWkAkyms\"}', 0),
(121, 41, 10004, '', '2017-09-09 17:36:42', 655, 1226, '5e685ca2ceec0b329ef31d10a665d89092c50116', '{\"id\":\"41\",\"name\":\"\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\",\"alias\":\"administratsiuli-shenoba\",\"description\":\"\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":8,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-02 11:53:54\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-09-09 17:36:42\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"41\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"178\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\",\"url\":\"\",\"youtubeLink\":\"\"}', 0),
(122, 42, 10003, '', '2017-09-09 17:36:43', 655, 621, '1ab515b6b26bd81254ceb103ee7101e8d46cdb18', '{\"id\":42,\"name\":\"image\",\"alias\":null,\"description\":null,\"intro\":null,\"images\":null,\"urls\":null,\"state\":1,\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-09-09 17:36:43\",\"created_by\":\"655\",\"created_by_alias\":null,\"modified\":\"2017-09-09 17:36:43\",\"modified_by\":\"655\",\"checked_out\":null,\"checked_out_time\":null,\"version\":null,\"ordering\":null,\"params\":null,\"hits\":null,\"featured\":null,\"language\":null,\"asset_id\":179,\"access\":1,\"metakey\":null,\"metadesc\":null,\"robots\":null,\"author\":null,\"xreference\":\"\",\"src\":\"images\\\\work\\\\items\\\\41\\\\2050fc259d6b89b26400110a6240575c.jpg\",\"item_id\":41}', 0),
(123, 38, 10004, '', '2017-09-09 17:54:41', 655, 1101, '3621b01ad8079c5b33b5bea9ff3a49e29c93e85a', '{\"id\":\"38\",\"name\":\"\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\",\"alias\":\"azxc\",\"description\":\"\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\\u10d0\\u10d3\\u10db\\u10d8\\u10dc\\u10d8\\u10e1\\u10e2\\u10e0\\u10d0\\u10ea\\u10d8\\u10e3\\u10da\\u10d8 \\u10e8\\u10d4\\u10dc\\u10dd\\u10d1\\u10d0\",\"intro\":\"\",\"images\":\"\",\"urls\":\"\",\"catid\":9,\"state\":\"1\",\"publish_up\":\"0000-00-00 00:00:00\",\"publish_down\":\"0000-00-00 00:00:00\",\"created\":\"2017-08-15 20:16:08\",\"created_by\":\"655\",\"created_by_alias\":\"\",\"modified\":\"2017-09-09 17:54:41\",\"modified_by\":\"655\",\"checked_out\":\"0\",\"checked_out_time\":\"0000-00-00 00:00:00\",\"version\":\"1\",\"ordering\":\"38\",\"params\":\"\",\"hits\":\"0\",\"featured\":\"0\",\"language\":\"*\",\"asset_id\":\"165\",\"access\":\"1\",\"metakey\":\"\",\"metadesc\":\"\",\"robots\":\"\",\"author\":\"\",\"xreference\":\"\",\"url\":\"http:\\/\\/rlestate.ru\\/\",\"youtubeLink\":\"\"}', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_updates`
--

CREATE TABLE `kutah_updates` (
  `update_id` int(11) NOT NULL,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `element` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `folder` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `detailsurl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `infourl` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Available Updates';

-- --------------------------------------------------------

--
-- Table structure for table `kutah_update_sites`
--

CREATE TABLE `kutah_update_sites` (
  `update_site_id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `location` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  `extra_query` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Update Sites';

--
-- Dumping data for table `kutah_update_sites`
--

INSERT INTO `kutah_update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`, `extra_query`) VALUES
(1, 'Joomla! Core', 'collection', 'https://update.joomla.org/core/list.xml', 0, 0, ''),
(2, 'Accredited Joomla! Translations', 'collection', 'https://update.joomla.org/language/translationlist_3.xml', 1, 0, ''),
(3, 'Joomla! Update Component Update Site', 'extension', 'https://update.joomla.org/core/extensions/com_joomlaupdate.xml', 1, 0, ''),
(4, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_update_sites_extensions`
--

CREATE TABLE `kutah_update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Links extensions to update sites';

--
-- Dumping data for table `kutah_update_sites_extensions`
--

INSERT INTO `kutah_update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 802),
(3, 28),
(4, 10020);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_usergroups`
--

CREATE TABLE `kutah_usergroups` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_usergroups`
--

INSERT INTO `kutah_usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 18, 'Public'),
(2, 1, 8, 15, 'Registered'),
(3, 2, 9, 14, 'Author'),
(4, 3, 10, 13, 'Editor'),
(5, 4, 11, 12, 'Publisher'),
(6, 1, 4, 7, 'Manager'),
(7, 6, 5, 6, 'Administrator'),
(8, 1, 16, 17, 'Super Users'),
(9, 1, 2, 3, 'Guest');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_users`
--

CREATE TABLE `kutah_users` (
  `id` int(11) NOT NULL,
  `name` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `surname` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `params` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  `requireReset` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Require user to reset password on next login',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_users`
--

INSERT INTO `kutah_users` (`id`, `name`, `surname`, `username`, `email`, `password`, `block`, `sendEmail`, `registerDate`, `lastvisitDate`, `activation`, `params`, `lastResetTime`, `resetCount`, `otpKey`, `otep`, `requireReset`, `description`, `phone`) VALUES
(655, 'Super User', 'frr', 'admin', 'mixas.8383@gmail.com', '$2y$10$nAkGBKjCLNYekr4NEayjhOEKB/XDhqbl6taBdH.jNTMJ1rYLcEsdy', 0, 1, '2017-07-13 17:21:29', '2017-11-07 17:34:32', '0', '{}', '0000-00-00 00:00:00', 0, '', '', 0, 'some descriptionდ', '+995558188806'),
(656, 'asdasdasd', '', 'sdf@sdf.sdf', 'sdf@sdf.sdf', '$2y$10$poqAP5gum6rb39mrJEbqTuqP2Q6.pewiP22HgFXPEYEYRCeZLVGaK', 0, 0, '2017-07-17 18:13:19', '0000-00-00 00:00:00', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0, '', ''),
(657, 'ზხცზხც', 'ზხცზხც', 'asdasfsd@dfsd.sdf', 'asdasfsd@dfsd.sdf', '$2y$10$HVntfW2vW0tZDrzca39nX.AbCdCo1so4kFYUnbj8yNvKP/aeuBX2q', 0, 0, '2017-07-17 18:39:14', '2017-09-18 18:57:14', '', '{}', '0000-00-00 00:00:00', 0, '', '', 0, '', ''),
(658, 'სდფსდფსდფ', 'დფგდფგ', 'dfgdf@dfgdf.dfg', 'dfgdf@dfgdf.dfg', '$2y$10$CSt6Mf3K7zaJZi1rxqw5bu9IWNBlAAGkTwDUfztPREF.4llX8EAkG', 0, 0, '2017-07-17 18:42:50', '0000-00-00 00:00:00', '', '{}', '0000-00-00 00:00:00', 0, '', '', 0, '', ''),
(659, 'asdfasdfsadf', '', 'bill', 'bill@ss.jk', '$2y$10$BRmdChnT08JySwUEtLPYLugWy3M66ddzGNmXIMJvgmYP6seej6q4m', 0, 0, '2017-09-19 17:26:31', '0000-00-00 00:00:00', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0, '', ''),
(660, 'Games cook', '', 'games', 'games@ss.jk', '$2y$10$a8qR52QLHcNGovyTqCn2kurBt5Pr3181Nuenkwhb7QXhFgaW5xM.u', 0, 0, '2017-09-19 17:27:29', '0000-00-00 00:00:00', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0, '', ''),
(661, 'Greg Sabrinashvili', '', 'greg', 'greg@ss.jk', '$2y$10$T49WTewt3e2P8F6Bm983YuFhvwyvTPDtyhjDabxwkFJfbMPUnr3je', 0, 0, '2017-09-19 17:28:42', '0000-00-00 00:00:00', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0, '', ''),
(662, 'Zura', '', 'Zura', 'Zura@ss.jk', '$2y$10$66ynjkIRQn7Sq.kLFN2eaOGydTM3dH2JGFFBEsI/spF9XwZX5BBhu', 0, 0, '2017-09-19 17:29:44', '0000-00-00 00:00:00', '', '{\"admin_style\":\"\",\"admin_language\":\"\",\"language\":\"\",\"editor\":\"\",\"helpsite\":\"\",\"timezone\":\"\"}', '0000-00-00 00:00:00', 0, '', '', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_user_keys`
--

CREATE TABLE `kutah_user_keys` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `series` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `invalid` tinyint(4) NOT NULL,
  `time` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `uastring` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_user_notes`
--

CREATE TABLE `kutah_user_notes` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) UNSIGNED NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_user_profiles`
--

CREATE TABLE `kutah_user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `kutah_user_usergroup_map`
--

CREATE TABLE `kutah_user_usergroup_map` (
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_user_usergroup_map`
--

INSERT INTO `kutah_user_usergroup_map` (`user_id`, `group_id`) VALUES
(655, 8),
(656, 2),
(657, 2),
(658, 2),
(659, 2),
(660, 2),
(661, 2),
(662, 2);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_utf8_conversion`
--

CREATE TABLE `kutah_utf8_conversion` (
  `converted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_utf8_conversion`
--

INSERT INTO `kutah_utf8_conversion` (`converted`) VALUES
(2);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_viewlevels`
--

CREATE TABLE `kutah_viewlevels` (
  `id` int(10) UNSIGNED NOT NULL COMMENT 'Primary Key',
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'JSON encoded access control.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kutah_viewlevels`
--

INSERT INTO `kutah_viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 2, '[6,2,8]'),
(3, 'Special', 3, '[6,3,8]'),
(5, 'Guest', 1, '[9]'),
(6, 'Super Users', 4, '[8]');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_category_map`
--

CREATE TABLE `kutah_work_category_map` (
  `id` int(11) NOT NULL,
  `catid` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=skil->item; 2=skill->category; 3=skill->service; 4=skill->user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_category_map`
--

INSERT INTO `kutah_work_category_map` (`id`, `catid`, `item_id`, `type`) VALUES
(1, 8, 9, 0),
(2, 8, 10, 1),
(3, 8, 11, 1),
(4, 10, 11, 1),
(5, 11, 11, 1),
(6, 8, 12, 1),
(7, 10, 12, 1),
(8, 11, 12, 1),
(9, 8, 13, 1),
(10, 9, 13, 1),
(25, 8, 14, 1),
(29, 8, 15, 1),
(30, 8, 16, 1),
(31, 9, 16, 1),
(32, 8, 17, 1),
(46, 9, 18, 1),
(47, 8, 19, 1),
(50, 9, 21, 1),
(51, 8, 22, 1),
(58, 8, 24, 1),
(59, 8, 25, 1),
(60, 8, 26, 1),
(61, 10, 26, 1),
(62, 8, 27, 1),
(63, 8, 28, 1),
(64, 8, 29, 1),
(68, 8, 31, 1),
(69, 8, 32, 1),
(70, 9, 33, 1),
(71, 8, 34, 1),
(72, 8, 35, 1),
(73, 8, 36, 1),
(74, 8, 37, 1),
(76, 8, 39, 1),
(77, 9, 39, 1),
(86, 8, 40, 1),
(87, 9, 40, 1),
(88, 8, 41, 1),
(89, 9, 38, 1);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_companies`
--

CREATE TABLE `kutah_work_companies` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_companies`
--

INSERT INTO `kutah_work_companies` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(1, '', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-03 18:42:12', 655, '', '2017-09-03 18:42:12', 655, 0, '0000-00-00 00:00:00', 1, 1, '', 0, 0, '*', 63, 1, '', '', '', '', ''),
(2, 'first school', 'first-school', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-03 18:42:32', 655, '', '2017-09-03 18:42:32', 655, 0, '0000-00-00 00:00:00', 1, 2, '', 0, 0, '*', 98, 1, '', '', '', '', ''),
(3, 'websolutions', 'websolutions', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-03 19:04:21', 655, '', '2017-09-03 19:04:21', 655, 0, '0000-00-00 00:00:00', 1, 3, '', 0, 0, '*', 99, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_degrees`
--

CREATE TABLE `kutah_work_degrees` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_degrees`
--

INSERT INTO `kutah_work_degrees` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(1, 'ბკალავრი', 'first-degree', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:33:49', 655, '', '2017-09-02 12:33:49', 655, 0, '0000-00-00 00:00:00', 1, 1, '', 0, 0, '*', 63, 1, '', '', '', '', ''),
(2, 'ინჟინერი', 'first-area', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:34:01', 655, '', '2017-09-02 12:34:01', 655, 0, '0000-00-00 00:00:00', 1, 2, '', 0, 0, '*', 98, 1, '', '', '', '', ''),
(3, 'დოქტორი', 'first-area', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:37:23', 655, '', '2017-09-02 12:37:23', 655, 0, '0000-00-00 00:00:00', 1, 3, '', 0, 0, '*', 99, 1, '', '', '', '', ''),
(4, 'მოსწავლე', 'first-area', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:39:03', 655, '', '2017-09-02 12:39:03', 655, 0, '0000-00-00 00:00:00', 1, 4, '', 0, 0, '*', 100, 1, '', '', '', '', ''),
(5, 'ასისტენტი', 'first-area', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:40:19', 655, '', '2017-09-02 12:40:19', 655, 0, '0000-00-00 00:00:00', 1, 5, '', 0, 0, '*', 101, 1, '', '', '', '', ''),
(6, 'first degree', 'first-degree', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 15:49:20', 655, '', '2017-09-02 15:49:20', 655, 0, '0000-00-00 00:00:00', 1, 6, '', 0, 0, '*', 102, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_education`
--

CREATE TABLE `kutah_work_education` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `school_id` int(11) NOT NULL,
  `degree_id` int(11) NOT NULL,
  `start` int(11) NOT NULL,
  `end` int(11) NOT NULL,
  `area_of_study_id` int(11) NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_education`
--

INSERT INTO `kutah_work_education` (`id`, `name`, `school_id`, `degree_id`, `start`, `end`, `area_of_study_id`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(4, '', 2, 1, 1940, 1943, 1, '', 'საქართველოს პრემიერ-მინისტრმა გიორგი კვირიკაშვილმა იმერეთის რეგიონში ”ქართული ოცნების” მერობის კანდიდატები დაასახელა.', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:42:05', 655, '', '2017-09-02 12:42:05', 655, 0, '0000-00-00 00:00:00', 1, 4, '', 0, 0, '*', 100, 1, '', '', '', '', ''),
(6, '', 3, 1, 1940, 1955, 1, '', 'საქართველოს პრემიერ-მინისტრმა გიორგი კვირიკაშვილმა იმერეთის რეგიონში ”ქართული ოცნების” მერობის კანდიდატები დაასახელა.', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 14:43:31', 655, '', '2017-09-02 14:43:31', 655, 0, '0000-00-00 00:00:00', 1, 8, '', 0, 0, '*', 102, 1, '', '', '', '', ''),
(7, '', 4, 6, 1946, 2016, 6, '', 'self.userEducation self.userEducation self.userEducation self.userEducation', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 15:49:20', 655, '', '2017-09-02 15:49:20', 655, 0, '0000-00-00 00:00:00', 1, 9, '', 0, 0, '*', 103, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_employment`
--

CREATE TABLE `kutah_work_employment` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `company_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `start_year` int(11) NOT NULL,
  `end_year` int(11) NOT NULL,
  `start_month` int(11) NOT NULL,
  `end_month` int(11) NOT NULL,
  `currently` tinyint(4) NOT NULL,
  `city` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_employment`
--

INSERT INTO `kutah_work_employment` (`id`, `name`, `company_id`, `country_id`, `start_year`, `end_year`, `start_month`, `end_month`, `currently`, `city`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(2, 'Veb programmer', 3, 78, 1957, 1958, 4, 12, 0, 'tbilisi', 'veb-programmer', 'sdfsdf', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-05 18:04:19', 655, '', '2017-09-05 18:04:19', 655, 0, '0000-00-00 00:00:00', 1, 1, '', 0, 0, '*', 98, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_galleries`
--

CREATE TABLE `kutah_work_galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `src` varchar(255) NOT NULL,
  `item_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_galleries`
--

INSERT INTO `kutah_work_galleries` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`, `src`, `item_id`) VALUES
(1, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 18:13:12', 655, '', '2017-08-15 18:13:12', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 115, 1, '', '', '', '', '', 'images\\work\\items\\18\\5ee0c0943b38e37a8aa0f78ab751c5bb.jpg', 18),
(2, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:07:57', 655, '', '2017-08-15 19:07:57', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 118, 1, '', '', '', '', '', 'images\\work\\items\\0\\28b5f15b790d766e471ffb7c581dc4ec.jpg', 0),
(4, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:10:12', 655, '', '2017-08-15 19:10:12', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 121, 1, '', '', '', '', '', 'images\\work\\items\\0\\207ddda734bd5245b95fda6f262cbaa4.jpg', 0),
(5, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:12:10', 655, '', '2017-08-15 19:12:10', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 123, 1, '', '', '', '', '', 'images\\work\\items\\0\\d3cf11b463a7b96861e7cb568e3ead8f.jpg', 0),
(6, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:12:10', 655, '', '2017-08-15 19:12:10', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 124, 1, '', '', '', '', '', 'images\\work\\items\\0\\57df4188491ff1ca6da597ac774f545d.jpg', 0),
(7, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:16:29', 655, '', '2017-08-15 19:16:29', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 126, 1, '', '', '', '', '', 'images\\work\\items\\0\\351066b6e9a87a42a46692302217f438.jpg', 0),
(10, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:20:06', 655, '', '2017-08-15 19:20:06', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 130, 1, '', '', '', '', '', 'images\\work\\items\\0\\5cc651d3df1ccb76380ccc7e35a1d84f.jpg', 0),
(11, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:21:38', 655, '', '2017-08-15 19:21:38', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 132, 1, '', '', '', '', '', 'images\\work\\items\\0\\f40a2bf33c7312acf528271c1fe203ea.jpg', 0),
(12, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:24:11', 655, '', '2017-08-15 19:24:11', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 134, 1, '', '', '', '', '', 'images\\work\\items\\0\\435992ee8706a234c73d2626d19921aa.jpg', 0),
(13, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:24:12', 655, '', '2017-08-15 19:24:12', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 135, 1, '', '', '', '', '', 'images\\work\\items\\0\\8ccdab19c5ab009833e44cafa5ec49cd.jpg', 0),
(14, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:29:34', 655, '', '2017-08-15 19:29:34', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 137, 1, '', '', '', '', '', 'images\\work\\items\\0\\1638ef28d12f1262dd5fd37b003200ba.jpg', 0),
(15, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:29:35', 655, '', '2017-08-15 19:29:35', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 138, 1, '', '', '', '', '', 'images\\work\\items\\0\\20324c96fdbb72d28114e4211d5ef5c7.jpg', 0),
(16, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:32:08', 655, '', '2017-08-15 19:32:08', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 140, 1, '', '', '', '', '', 'images\\work\\items\\0\\17356475c6865cb6d1d817726b24cb63.jpg', 0),
(17, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:32:09', 655, '', '2017-08-15 19:32:09', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 141, 1, '', '', '', '', '', 'images\\work\\items\\0\\7c31f4618bc50e76bde6eb9032903aba.jpg', 0),
(18, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:45:13', 655, '', '2017-08-15 19:45:13', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 143, 1, '', '', '', '', '', 'images\\work\\items\\0\\71d8ff61a8cd1bdfbd30898f68794d19.jpg', 0),
(19, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:45:14', 655, '', '2017-08-15 19:45:14', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 144, 1, '', '', '', '', '', 'images\\work\\items\\0\\37f394b81818afb944bbaf2d08fba7c3.jpg', 0),
(20, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:45:14', 655, '', '2017-08-15 19:45:14', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 145, 1, '', '', '', '', '', 'images\\work\\items\\0\\9f386ee486727760d2aabf44c4421283.jpg', 0),
(21, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:45:15', 655, '', '2017-08-15 19:45:15', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 146, 1, '', '', '', '', '', 'images\\work\\items\\0\\ef9a23f22b68a3091b5cf59df9f8fb88.jpg', 0),
(22, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:50:56', 655, '', '2017-08-15 19:50:56', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 148, 1, '', '', '', '', '', 'images\\work\\items\\0\\a0c244141de17313f720a10afa76ef95.jpg', 0),
(24, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:53:29', 655, '', '2017-08-15 19:53:29', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 151, 1, '', '', '', '', '', 'images\\work\\items\\0\\2996c7c244a6cf3fa30dec1e047d1d13.jpg', 0),
(25, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:58:15', 655, '', '2017-08-15 19:58:15', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 153, 1, '', '', '', '', '', 'images\\work\\items\\0\\8c71765b1dc636edb70a198bf8190709.jpg', 0),
(26, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 19:58:15', 655, '', '2017-08-15 19:58:15', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 154, 1, '', '', '', '', '', 'images\\work\\items\\0\\84b2fdfb38a90d2d409fc91a77484073.jpg', 0),
(27, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:03:11', 655, '', '2017-08-15 20:03:11', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 156, 1, '', '', '', '', '', 'images\\work\\items\\0\\3f2060bb3317897faf7ccafbaa9d8374.jpg', 0),
(28, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:05:39', 655, '', '2017-08-15 20:05:39', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 158, 1, '', '', '', '', '', 'images\\work\\items\\0\\e2308ef68c7e4710e8ae897c5ab4e52f.jpg', 0),
(29, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:05:40', 655, '', '2017-08-15 20:05:40', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 159, 1, '', '', '', '', '', 'images\\work\\items\\0\\8db7e098fc58dbda7c530b6cdc0a3558.jpg', 0),
(30, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:07:30', 655, '', '2017-08-15 20:07:30', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 161, 1, '', '', '', '', '', 'images\\work\\items\\0\\5936ec53cd0772d6b064a068effb5df2.jpg', 0),
(31, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:07:31', 655, '', '2017-08-15 20:07:31', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 162, 1, '', '', '', '', '', 'images\\work\\items\\0\\753068f46f88dd46a982a605ec6b677a.jpg', 0),
(32, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:16:08', 655, '', '2017-08-15 20:16:08', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 166, 1, '', '', '', '', '', 'images\\work\\items\\38\\976af3f910100241f71f33df3fccfeca.jpg', 38),
(34, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-16 17:28:04', 655, '', '2017-08-16 17:28:04', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 169, 1, '', '', '', '', '', 'images\\work\\items\\39\\bdef0da0544d55c4832ea32d5cb8b069.jpg', 39),
(36, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-16 20:20:00', 655, '', '2017-08-16 20:20:00', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 172, 1, '', '', '', '', '', 'images\\work\\items\\40\\0ff78ddcdad020448180796ab74343dd.jpg', 40),
(37, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-16 20:20:00', 655, '', '2017-08-16 20:20:00', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 173, 1, '', '', '', '', '', 'images\\work\\items\\40\\ec409e390f93b430ffda744586566fd8.jpg', 40),
(42, 'image', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-09 17:36:43', 655, '', '2017-09-09 17:36:43', 655, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 179, 1, '', '', '', '', '', 'images\\work\\items\\41\\2050fc259d6b89b26400110a6240575c.jpg', 41);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_items`
--

CREATE TABLE `kutah_work_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `catid` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `url` varchar(255) NOT NULL,
  `youtubeLink` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_items`
--

INSERT INTO `kutah_work_items` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `catid`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`, `url`, `youtubeLink`) VALUES
(38, 'ადმინისტრაციული შენობა', 'azxc', 'ადმინისტრაციული შენობაადმინისტრაციული შენობაადმინისტრაციული შენობა', '', '', '', 9, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-15 20:16:08', 655, '', '2017-09-09 17:54:41', 655, 0, '0000-00-00 00:00:00', 1, 38, '', 1, 0, '*', 165, 1, '', '', '', '', '', 'http://rlestate.ru/', ''),
(39, 'კარფანჯრების გალერეა', 'sadasd', 'თამარ მეარაყიშვილის დაკავებას სახალხო დამცველი აფასებს როგორც უკანონოდ თავისუფლების აღკვეთას, სამოქალაქო აქტივისტზე ზეწოლად და მისი გამოხატვის თავისუფლების შეზღუდვას. \r\n\r\n„ეს დაკავება ასევე უნდა შეფასდეს, როგორც მისი იძულება სამუდამოდ დატოვოს ახალგორი. შემაშფოთებელია, რომ სამოქალაქო აქტივისტებსა და არასამთავრობო ორგანიზაციებზე ზეწოლამ რეგულარული ხასიათი მიიღო ე.წ. სამხრეთ ოსეთში“, - ნათქვამია სახალხო დამცველის მიერ გავრცელებულ განცხადებაში. \r\n\r\nსახალხო დამცველი მოუწოდებს დე ფაქტო ხელისუფლებას დაუყოვნებლივ გაათავისუფლოს თამარ მეარაყიშვილი. ასევე, მოუწოდებს საქართველოს მთავრობას მიიღოს ყველა ზომა მისი უსაფრთხოების დასაცავად და გასათავისუფლებლად.\r\n', '', '', '', 8, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-16 17:28:03', 655, '', '2017-08-16 17:28:03', 655, 0, '0000-00-00 00:00:00', 1, 34, '', 90, 0, '*', 168, 1, '', '', '', '', '', 'http://rlestate.ru/', 'https://www.youtube.com/watch?v=p67_XgWRKwk'),
(40, 'ჭონქაძის ქუჩაზე მომხდარი მკვლელობის', 'chonkadzis-kuchaze-momkhdari-mkvlelobis', 'ჭონქაძის ქუჩაზე მომხდარი მკვლელობისჭონქაძის ქუჩაზე მომხდარი მკვლელობისჭონქაძის ქუჩაზე მომხდარი მკვლელობის', '', '', '', 8, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-08-16 20:19:59', 655, '', '2017-09-09 17:35:34', 655, 0, '0000-00-00 00:00:00', 1, 40, '', 5, 0, '*', 171, 1, '', '', '', '', '', 'http://rlestate.ru/', 'https://www.youtube.com/watch?v=bHzXWkAkyms'),
(41, 'ადმინისტრაციული შენობა', 'administratsiuli-shenoba', 'ადმინისტრაციული შენობაადმინისტრაციული შენობაადმინისტრაციული შენობაადმინისტრაციული შენობა', '', '', '', 8, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 11:53:54', 655, '', '2017-09-09 17:36:42', 655, 0, '0000-00-00 00:00:00', 1, 41, '', 1, 0, '*', 178, 1, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_proffesions`
--

CREATE TABLE `kutah_work_proffesions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_proffesions`
--

INSERT INTO `kutah_work_proffesions` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(1, 'Proffesion', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(2, 'programmer', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(3, 'developer', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(4, 'killer', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(5, 'skanner', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(6, 'table', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(7, 'window', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(8, 'big data worker', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_proffesions_map`
--

CREATE TABLE `kutah_work_proffesions_map` (
  `id` int(11) NOT NULL,
  `proffesion_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=skil->item; 2=skill->category; 3=skill->service; 4=skill->user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_proffesions_map`
--

INSERT INTO `kutah_work_proffesions_map` (`id`, `proffesion_id`, `item_id`, `type`) VALUES
(44, 3, 655, 4),
(45, 1, 655, 4),
(46, 4, 655, 4),
(47, 5, 655, 4),
(48, 2, 655, 4);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_rating`
--

CREATE TABLE `kutah_work_rating` (
  `id` int(10) UNSIGNED NOT NULL,
  `content_type` varchar(50) NOT NULL DEFAULT '',
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `rating_count` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_schools`
--

CREATE TABLE `kutah_work_schools` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_schools`
--

INSERT INTO `kutah_work_schools` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(1, 'საჯარო სკოლა 121', 'zx', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:00:30', 655, '', '2017-09-02 12:00:30', 655, 0, '0000-00-00 00:00:00', 1, 1, '', 0, 0, '*', 63, 1, '', '', '', '', ''),
(2, 'სახელმჭიფო უნივერსიტეტი', 'zxss', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:02:02', 655, '', '2017-09-02 12:02:02', 655, 0, '0000-00-00 00:00:00', 1, 2, '', 0, 0, '*', 98, 1, '', '', '', '', ''),
(3, 'საქართველოს ტექნიკური უნივერსიტეტი', 'sasas', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:51:52', 655, '', '2017-09-02 12:51:52', 655, 0, '0000-00-00 00:00:00', 1, 3, '', 0, 0, '*', 99, 1, '', '', '', '', ''),
(4, 'first school', 'first-school', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 15:49:20', 655, '', '2017-09-02 15:49:20', 655, 0, '0000-00-00 00:00:00', 1, 4, '', 0, 0, '*', 100, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_services`
--

CREATE TABLE `kutah_work_services` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_skils`
--

CREATE TABLE `kutah_work_skils` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_skils`
--

INSERT INTO `kutah_work_skils` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(1, 'html', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(2, 'JavaScript', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(3, 'item skill1', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(4, 'item skill2', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(5, 'category skill1', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(6, 'category skill2', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(7, 'user skill1', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', ''),
(8, 'user skill2', '', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', 1, 0, '', 0, 0, '*', 0, 0, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_skils_map`
--

CREATE TABLE `kutah_work_skils_map` (
  `id` int(11) NOT NULL,
  `skill_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL COMMENT '1=skil->item; 2=skill->category; 3=skill->service; 4=skill->user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_skils_map`
--

INSERT INTO `kutah_work_skils_map` (`id`, `skill_id`, `item_id`, `type`) VALUES
(1, 3, 1, 1),
(2, 4, 1, 1),
(3, 5, 8, 2),
(4, 6, 9, 2),
(7, 8, 39, 1),
(8, 7, 39, 1),
(9, 5, 39, 1),
(10, 6, 39, 1),
(75, 5, 40, 1),
(76, 6, 40, 1),
(77, 8, 40, 1),
(78, 7, 40, 1),
(79, 8, 41, 1),
(80, 8, 38, 1),
(81, 7, 655, 4),
(82, 8, 655, 4),
(83, 2, 655, 4),
(84, 5, 655, 4),
(85, 4, 655, 4);

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_study_areas`
--

CREATE TABLE `kutah_work_study_areas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kutah_work_study_areas`
--

INSERT INTO `kutah_work_study_areas` (`id`, `name`, `alias`, `description`, `intro`, `images`, `urls`, `state`, `publish_up`, `publish_down`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `version`, `ordering`, `params`, `hits`, `featured`, `language`, `asset_id`, `access`, `metakey`, `metadesc`, `robots`, `author`, `xreference`) VALUES
(1, 'ინფორმატიკა', 'first-area', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 12:42:05', 655, '', '2017-09-02 12:42:05', 655, 0, '0000-00-00 00:00:00', 1, 1, '', 0, 0, '*', 63, 1, '', '', '', '', ''),
(2, 'first area', 'first-area', '', '', '', '', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2017-09-02 15:49:20', 655, '', '2017-09-02 15:49:20', 655, 0, '0000-00-00 00:00:00', 1, 2, '', 0, 0, '*', 98, 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `kutah_work_videos`
--

CREATE TABLE `kutah_work_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `intro` mediumtext NOT NULL,
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` varchar(5120) NOT NULL,
  `hits` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `featured` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '*',
  `asset_id` int(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `access` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `robots` varchar(50) NOT NULL DEFAULT '',
  `author` varchar(20) NOT NULL DEFAULT '',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kutah_assets`
--
ALTER TABLE `kutah_assets`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_asset_name` (`name`),
  ADD KEY `idx_lft_rgt` (`lft`,`rgt`),
  ADD KEY `idx_parent_id` (`parent_id`);

--
-- Indexes for table `kutah_associations`
--
ALTER TABLE `kutah_associations`
  ADD PRIMARY KEY (`context`,`id`),
  ADD KEY `idx_key` (`key`);

--
-- Indexes for table `kutah_banners`
--
ALTER TABLE `kutah_banners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100)),
  ADD KEY `idx_banner_catid` (`catid`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_banner_clients`
--
ALTER TABLE `kutah_banner_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_own_prefix` (`own_prefix`),
  ADD KEY `idx_metakey_prefix` (`metakey_prefix`(100));

--
-- Indexes for table `kutah_banner_tracks`
--
ALTER TABLE `kutah_banner_tracks`
  ADD PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  ADD KEY `idx_track_date` (`track_date`),
  ADD KEY `idx_track_type` (`track_type`),
  ADD KEY `idx_banner_id` (`banner_id`);

--
-- Indexes for table `kutah_categories`
--
ALTER TABLE `kutah_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_idx` (`extension`,`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_chats`
--
ALTER TABLE `kutah_chats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_chats_messages`
--
ALTER TABLE `kutah_chats_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_contact_details`
--
ALTER TABLE `kutah_contact_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `kutah_content`
--
ALTER TABLE `kutah_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `kutah_contentitem_tag_map`
--
ALTER TABLE `kutah_contentitem_tag_map`
  ADD UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  ADD KEY `idx_tag_type` (`tag_id`,`type_id`),
  ADD KEY `idx_date_id` (`tag_date`,`tag_id`),
  ADD KEY `idx_core_content_id` (`core_content_id`);

--
-- Indexes for table `kutah_content_frontpage`
--
ALTER TABLE `kutah_content_frontpage`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `kutah_content_rating`
--
ALTER TABLE `kutah_content_rating`
  ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `kutah_content_types`
--
ALTER TABLE `kutah_content_types`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `idx_alias` (`type_alias`(100));

--
-- Indexes for table `kutah_country`
--
ALTER TABLE `kutah_country`
  ADD PRIMARY KEY (`id`,`Code`);

--
-- Indexes for table `kutah_extensions`
--
ALTER TABLE `kutah_extensions`
  ADD PRIMARY KEY (`extension_id`),
  ADD KEY `element_clientid` (`element`,`client_id`),
  ADD KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  ADD KEY `extension` (`type`,`element`,`folder`,`client_id`);

--
-- Indexes for table `kutah_fields`
--
ALTER TABLE `kutah_fields`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_user_id` (`created_user_id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_fields_categories`
--
ALTER TABLE `kutah_fields_categories`
  ADD PRIMARY KEY (`field_id`,`category_id`);

--
-- Indexes for table `kutah_fields_groups`
--
ALTER TABLE `kutah_fields_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_context` (`context`(191)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_fields_values`
--
ALTER TABLE `kutah_fields_values`
  ADD KEY `idx_field_id` (`field_id`),
  ADD KEY `idx_item_id` (`item_id`(191));

--
-- Indexes for table `kutah_finder_filters`
--
ALTER TABLE `kutah_finder_filters`
  ADD PRIMARY KEY (`filter_id`);

--
-- Indexes for table `kutah_finder_links`
--
ALTER TABLE `kutah_finder_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `idx_type` (`type_id`),
  ADD KEY `idx_title` (`title`(100)),
  ADD KEY `idx_md5` (`md5sum`),
  ADD KEY `idx_url` (`url`(75)),
  ADD KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  ADD KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`);

--
-- Indexes for table `kutah_finder_links_terms0`
--
ALTER TABLE `kutah_finder_links_terms0`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms1`
--
ALTER TABLE `kutah_finder_links_terms1`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms2`
--
ALTER TABLE `kutah_finder_links_terms2`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms3`
--
ALTER TABLE `kutah_finder_links_terms3`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms4`
--
ALTER TABLE `kutah_finder_links_terms4`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms5`
--
ALTER TABLE `kutah_finder_links_terms5`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms6`
--
ALTER TABLE `kutah_finder_links_terms6`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms7`
--
ALTER TABLE `kutah_finder_links_terms7`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms8`
--
ALTER TABLE `kutah_finder_links_terms8`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_terms9`
--
ALTER TABLE `kutah_finder_links_terms9`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_termsa`
--
ALTER TABLE `kutah_finder_links_termsa`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_termsb`
--
ALTER TABLE `kutah_finder_links_termsb`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_termsc`
--
ALTER TABLE `kutah_finder_links_termsc`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_termsd`
--
ALTER TABLE `kutah_finder_links_termsd`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_termse`
--
ALTER TABLE `kutah_finder_links_termse`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_links_termsf`
--
ALTER TABLE `kutah_finder_links_termsf`
  ADD PRIMARY KEY (`link_id`,`term_id`),
  ADD KEY `idx_term_weight` (`term_id`,`weight`),
  ADD KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`);

--
-- Indexes for table `kutah_finder_taxonomy`
--
ALTER TABLE `kutah_finder_taxonomy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `state` (`state`),
  ADD KEY `ordering` (`ordering`),
  ADD KEY `access` (`access`),
  ADD KEY `idx_parent_published` (`parent_id`,`state`,`access`);

--
-- Indexes for table `kutah_finder_taxonomy_map`
--
ALTER TABLE `kutah_finder_taxonomy_map`
  ADD PRIMARY KEY (`link_id`,`node_id`),
  ADD KEY `link_id` (`link_id`),
  ADD KEY `node_id` (`node_id`);

--
-- Indexes for table `kutah_finder_terms`
--
ALTER TABLE `kutah_finder_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD UNIQUE KEY `idx_term` (`term`),
  ADD KEY `idx_term_phrase` (`term`,`phrase`),
  ADD KEY `idx_stem_phrase` (`stem`,`phrase`),
  ADD KEY `idx_soundex_phrase` (`soundex`,`phrase`);

--
-- Indexes for table `kutah_finder_terms_common`
--
ALTER TABLE `kutah_finder_terms_common`
  ADD KEY `idx_word_lang` (`term`,`language`),
  ADD KEY `idx_lang` (`language`);

--
-- Indexes for table `kutah_finder_tokens`
--
ALTER TABLE `kutah_finder_tokens`
  ADD KEY `idx_word` (`term`),
  ADD KEY `idx_context` (`context`);

--
-- Indexes for table `kutah_finder_tokens_aggregate`
--
ALTER TABLE `kutah_finder_tokens_aggregate`
  ADD KEY `token` (`term`),
  ADD KEY `keyword_id` (`term_id`);

--
-- Indexes for table `kutah_finder_types`
--
ALTER TABLE `kutah_finder_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`);

--
-- Indexes for table `kutah_followers`
--
ALTER TABLE `kutah_followers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_languages`
--
ALTER TABLE `kutah_languages`
  ADD PRIMARY KEY (`lang_id`),
  ADD UNIQUE KEY `idx_sef` (`sef`),
  ADD UNIQUE KEY `idx_langcode` (`lang_code`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_menu`
--
ALTER TABLE `kutah_menu`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`(100),`language`),
  ADD KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  ADD KEY `idx_menutype` (`menutype`),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_menu_types`
--
ALTER TABLE `kutah_menu_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_menutype` (`menutype`);

--
-- Indexes for table `kutah_messages`
--
ALTER TABLE `kutah_messages`
  ADD PRIMARY KEY (`message_id`),
  ADD KEY `useridto_state` (`user_id_to`,`state`);

--
-- Indexes for table `kutah_messages_cfg`
--
ALTER TABLE `kutah_messages_cfg`
  ADD UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`);

--
-- Indexes for table `kutah_modules`
--
ALTER TABLE `kutah_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `published` (`published`,`access`),
  ADD KEY `newsfeeds` (`module`,`published`),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_modules_menu`
--
ALTER TABLE `kutah_modules_menu`
  ADD PRIMARY KEY (`moduleid`,`menuid`);

--
-- Indexes for table `kutah_newsfeeds`
--
ALTER TABLE `kutah_newsfeeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`published`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_language` (`language`),
  ADD KEY `idx_xreference` (`xreference`);

--
-- Indexes for table `kutah_overrider`
--
ALTER TABLE `kutah_overrider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_postinstall_messages`
--
ALTER TABLE `kutah_postinstall_messages`
  ADD PRIMARY KEY (`postinstall_message_id`);

--
-- Indexes for table `kutah_redirect_links`
--
ALTER TABLE `kutah_redirect_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_old_url` (`old_url`(100)),
  ADD KEY `idx_link_modifed` (`modified_date`);

--
-- Indexes for table `kutah_schemas`
--
ALTER TABLE `kutah_schemas`
  ADD PRIMARY KEY (`extension_id`,`version_id`);

--
-- Indexes for table `kutah_selectfile_sizes`
--
ALTER TABLE `kutah_selectfile_sizes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_session`
--
ALTER TABLE `kutah_session`
  ADD PRIMARY KEY (`session_id`),
  ADD KEY `userid` (`userid`),
  ADD KEY `time` (`time`);

--
-- Indexes for table `kutah_tags`
--
ALTER TABLE `kutah_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_idx` (`published`,`access`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_path` (`path`(100)),
  ADD KEY `idx_left_right` (`lft`,`rgt`),
  ADD KEY `idx_alias` (`alias`(100)),
  ADD KEY `idx_language` (`language`);

--
-- Indexes for table `kutah_template_styles`
--
ALTER TABLE `kutah_template_styles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_template` (`template`),
  ADD KEY `idx_home` (`home`);

--
-- Indexes for table `kutah_ucm_base`
--
ALTER TABLE `kutah_ucm_base`
  ADD PRIMARY KEY (`ucm_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_item_id`),
  ADD KEY `idx_ucm_type_id` (`ucm_type_id`),
  ADD KEY `idx_ucm_language_id` (`ucm_language_id`);

--
-- Indexes for table `kutah_ucm_content`
--
ALTER TABLE `kutah_ucm_content`
  ADD PRIMARY KEY (`core_content_id`),
  ADD KEY `tag_idx` (`core_state`,`core_access`),
  ADD KEY `idx_access` (`core_access`),
  ADD KEY `idx_alias` (`core_alias`(100)),
  ADD KEY `idx_language` (`core_language`),
  ADD KEY `idx_title` (`core_title`(100)),
  ADD KEY `idx_modified_time` (`core_modified_time`),
  ADD KEY `idx_created_time` (`core_created_time`),
  ADD KEY `idx_content_type` (`core_type_alias`(100)),
  ADD KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  ADD KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  ADD KEY `idx_core_created_user_id` (`core_created_user_id`),
  ADD KEY `idx_core_type_id` (`core_type_id`);

--
-- Indexes for table `kutah_ucm_history`
--
ALTER TABLE `kutah_ucm_history`
  ADD PRIMARY KEY (`version_id`),
  ADD KEY `idx_ucm_item_id` (`ucm_type_id`,`ucm_item_id`),
  ADD KEY `idx_save_date` (`save_date`);

--
-- Indexes for table `kutah_updates`
--
ALTER TABLE `kutah_updates`
  ADD PRIMARY KEY (`update_id`);

--
-- Indexes for table `kutah_update_sites`
--
ALTER TABLE `kutah_update_sites`
  ADD PRIMARY KEY (`update_site_id`);

--
-- Indexes for table `kutah_update_sites_extensions`
--
ALTER TABLE `kutah_update_sites_extensions`
  ADD PRIMARY KEY (`update_site_id`,`extension_id`);

--
-- Indexes for table `kutah_usergroups`
--
ALTER TABLE `kutah_usergroups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  ADD KEY `idx_usergroup_title_lookup` (`title`),
  ADD KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  ADD KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE;

--
-- Indexes for table `kutah_users`
--
ALTER TABLE `kutah_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_name` (`name`(100)),
  ADD KEY `idx_block` (`block`),
  ADD KEY `username` (`username`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `kutah_user_keys`
--
ALTER TABLE `kutah_user_keys`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `series` (`series`),
  ADD UNIQUE KEY `series_2` (`series`),
  ADD UNIQUE KEY `series_3` (`series`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `kutah_user_notes`
--
ALTER TABLE `kutah_user_notes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_user_id` (`user_id`),
  ADD KEY `idx_category_id` (`catid`);

--
-- Indexes for table `kutah_user_profiles`
--
ALTER TABLE `kutah_user_profiles`
  ADD UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`);

--
-- Indexes for table `kutah_user_usergroup_map`
--
ALTER TABLE `kutah_user_usergroup_map`
  ADD PRIMARY KEY (`user_id`,`group_id`);

--
-- Indexes for table `kutah_viewlevels`
--
ALTER TABLE `kutah_viewlevels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `idx_assetgroup_title_lookup` (`title`);

--
-- Indexes for table `kutah_work_category_map`
--
ALTER TABLE `kutah_work_category_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_work_companies`
--
ALTER TABLE `kutah_work_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_degrees`
--
ALTER TABLE `kutah_work_degrees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_education`
--
ALTER TABLE `kutah_work_education`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_employment`
--
ALTER TABLE `kutah_work_employment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_galleries`
--
ALTER TABLE `kutah_work_galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_items`
--
ALTER TABLE `kutah_work_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_catid` (`catid`),
  ADD KEY `idx_featured_catid` (`featured`,`catid`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_proffesions`
--
ALTER TABLE `kutah_work_proffesions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_proffesions_map`
--
ALTER TABLE `kutah_work_proffesions_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_work_rating`
--
ALTER TABLE `kutah_work_rating`
  ADD PRIMARY KEY (`id`),
  ADD KEY `content_type_id` (`content_type`,`content_id`);

--
-- Indexes for table `kutah_work_schools`
--
ALTER TABLE `kutah_work_schools`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_services`
--
ALTER TABLE `kutah_work_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_skils`
--
ALTER TABLE `kutah_work_skils`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_skils_map`
--
ALTER TABLE `kutah_work_skils_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kutah_work_study_areas`
--
ALTER TABLE `kutah_work_study_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- Indexes for table `kutah_work_videos`
--
ALTER TABLE `kutah_work_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_access` (`access`),
  ADD KEY `idx_checkout` (`checked_out`),
  ADD KEY `idx_state` (`state`),
  ADD KEY `idx_createdby` (`created_by`),
  ADD KEY `idx_ordering` (`ordering`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kutah_assets`
--
ALTER TABLE `kutah_assets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=180;
--
-- AUTO_INCREMENT for table `kutah_banners`
--
ALTER TABLE `kutah_banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_banner_clients`
--
ALTER TABLE `kutah_banner_clients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_categories`
--
ALTER TABLE `kutah_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `kutah_chats`
--
ALTER TABLE `kutah_chats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kutah_chats_messages`
--
ALTER TABLE `kutah_chats_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `kutah_contact_details`
--
ALTER TABLE `kutah_contact_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_content`
--
ALTER TABLE `kutah_content`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kutah_content_types`
--
ALTER TABLE `kutah_content_types`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10006;
--
-- AUTO_INCREMENT for table `kutah_country`
--
ALTER TABLE `kutah_country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT for table `kutah_extensions`
--
ALTER TABLE `kutah_extensions`
  MODIFY `extension_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10023;
--
-- AUTO_INCREMENT for table `kutah_fields`
--
ALTER TABLE `kutah_fields`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_fields_groups`
--
ALTER TABLE `kutah_fields_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_finder_filters`
--
ALTER TABLE `kutah_finder_filters`
  MODIFY `filter_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_finder_links`
--
ALTER TABLE `kutah_finder_links`
  MODIFY `link_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_finder_taxonomy`
--
ALTER TABLE `kutah_finder_taxonomy`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kutah_finder_terms`
--
ALTER TABLE `kutah_finder_terms`
  MODIFY `term_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_finder_types`
--
ALTER TABLE `kutah_finder_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_followers`
--
ALTER TABLE `kutah_followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kutah_languages`
--
ALTER TABLE `kutah_languages`
  MODIFY `lang_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kutah_menu`
--
ALTER TABLE `kutah_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;
--
-- AUTO_INCREMENT for table `kutah_menu_types`
--
ALTER TABLE `kutah_menu_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kutah_messages`
--
ALTER TABLE `kutah_messages`
  MODIFY `message_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_modules`
--
ALTER TABLE `kutah_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `kutah_newsfeeds`
--
ALTER TABLE `kutah_newsfeeds`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_overrider`
--
ALTER TABLE `kutah_overrider`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key';
--
-- AUTO_INCREMENT for table `kutah_postinstall_messages`
--
ALTER TABLE `kutah_postinstall_messages`
  MODIFY `postinstall_message_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kutah_redirect_links`
--
ALTER TABLE `kutah_redirect_links`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_selectfile_sizes`
--
ALTER TABLE `kutah_selectfile_sizes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `kutah_tags`
--
ALTER TABLE `kutah_tags`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `kutah_template_styles`
--
ALTER TABLE `kutah_template_styles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kutah_ucm_content`
--
ALTER TABLE `kutah_ucm_content`
  MODIFY `core_content_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_ucm_history`
--
ALTER TABLE `kutah_ucm_history`
  MODIFY `version_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `kutah_updates`
--
ALTER TABLE `kutah_updates`
  MODIFY `update_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_update_sites`
--
ALTER TABLE `kutah_update_sites`
  MODIFY `update_site_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kutah_usergroups`
--
ALTER TABLE `kutah_usergroups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kutah_users`
--
ALTER TABLE `kutah_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=663;
--
-- AUTO_INCREMENT for table `kutah_user_keys`
--
ALTER TABLE `kutah_user_keys`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_user_notes`
--
ALTER TABLE `kutah_user_notes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_viewlevels`
--
ALTER TABLE `kutah_viewlevels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary Key', AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kutah_work_category_map`
--
ALTER TABLE `kutah_work_category_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;
--
-- AUTO_INCREMENT for table `kutah_work_companies`
--
ALTER TABLE `kutah_work_companies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `kutah_work_degrees`
--
ALTER TABLE `kutah_work_degrees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `kutah_work_education`
--
ALTER TABLE `kutah_work_education`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kutah_work_employment`
--
ALTER TABLE `kutah_work_employment`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kutah_work_galleries`
--
ALTER TABLE `kutah_work_galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `kutah_work_items`
--
ALTER TABLE `kutah_work_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `kutah_work_proffesions`
--
ALTER TABLE `kutah_work_proffesions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kutah_work_proffesions_map`
--
ALTER TABLE `kutah_work_proffesions_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `kutah_work_rating`
--
ALTER TABLE `kutah_work_rating`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_work_schools`
--
ALTER TABLE `kutah_work_schools`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kutah_work_services`
--
ALTER TABLE `kutah_work_services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `kutah_work_skils`
--
ALTER TABLE `kutah_work_skils`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kutah_work_skils_map`
--
ALTER TABLE `kutah_work_skils_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `kutah_work_study_areas`
--
ALTER TABLE `kutah_work_study_areas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `kutah_work_videos`
--
ALTER TABLE `kutah_work_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
